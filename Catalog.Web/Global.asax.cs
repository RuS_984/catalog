﻿using Catalog.Web.Infrastructure;
using Catalog.Web.Models;
using DataModel.AppLogger;
using DataModel.DBWrapper.Factories;
using DataModel.SupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Catalog.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            

            Logger.FileLogName = "FileLogWeb" + (new Random().Next());
            Logger.DBLogName = "DBLogWeb" + (new Random().Next());

            AppData.InitApp();

        }


        /*
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError";
                        break;
                    default:
                        action = "HttpError";
                        break;
                }
                Session["error"] = exception.Message;
                // clear error on server
                Server.ClearError();

                //Response.Redirect(String.Format("~/Home/HttpError/?message={0}", exception.Message));
            }
        }
        */
    }
}
