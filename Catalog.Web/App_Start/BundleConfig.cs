﻿using System.Web;
using System.Web.Optimization;

namespace Catalog.Web
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                      "~/Scripts/knockout-3.4.2.js",
                      "~/Scripts/knockout.mapping-latest.debug.js",
                      "~/Scripts/knockout.validation.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/NavBar.css",
                      "~/Content/table.css",
                      "~/Content/SideBar.css",
                      "~/Content/ErrorPanel.css",
                      "~/Content/RepositoryEditor.css",
                      "~/Content/DocTree.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/myscripts").Include(
                     "~/Scripts/Myscripts/*.js",
                     "~/Scripts/KnockoutVM/*.js"
                     ));
        }
    }
}
