﻿function CRUDViewModel() {

    const self = this;

    self.asmTree;
    self.oldNum;

    ko.validation.rules['mustEqual'] = {
        validator: function (val, otherVal) {
            return val === otherVal;
        },
        message: 'The field must equal {0}'
    };

    ko.validation.rules['validDate'] = {
        validator: function (val, validate) {
            if (val === "") { return false; };
            return Date.parse(val);
        },
        message: "Date is not valid"
    };

    ko.validation.rules['minDate'] = {
        validator: function (val, minDate) {


            if (Date.parse(val) < minDate) { return false; };
            return true;
        },
        message: function (val) { return "Date > " + new Date(val).toLocaleDateString(); }
    };

    ko.validation.rules['maxDate'] = {
        validator: function (val, maxDate) {

            if (Date.parse(val) > maxDate) { return false; }
            return true;

        },
        message: function (val) { return "Date < " + new Date(val).toLocaleDateString(); }
    };

    ko.validation.rules['validText'] = {
        validator: function (val, validate) {

            if (val === "") { return false; };

            return val.match(/^[A-Za-zА-Яа-я0-9. ]+$/igm);



        },
        message: "Разрешены только букы, цифры и точка"
    };


    ko.validation.rules['checkRep'] = {
        validator: function (val, rep) {
            return !(rep.indexOf(val) > -1);
        },
        message: "Значение используется"
    };





    ko.validation.registerExtenders();


    self.CurrInventarnNum = ko.observable("");

    self.Operation = ko.observable("");
    self.RepInventarnNum = ko.observableArray("");
    self.RepProjectName = ko.observableArray("");
    self.RepOboznachenie = ko.observableArray("");
    self.RepLiteral = ko.observableArray("");
    self.RepIspolniteli = ko.observableArray("");

    self.DocData = new function () {
        this.Id = ko.observable("");
        this.InventarnNum = ko.observable("").extend({
            required: { params: true, message: 'Инвентарный номер должен быть указан' },
            min: 1,
            digit: true,
            checkRep: self.RepInventarnNum
        });

        this.ProjectName = ko.observable("");

        this.Oboznachenie = ko.observable("").extend({
            required: { params: true, message: 'Обозначение должено быть указано' },
            validText: true,
            minLength: 3,
            checkRep: self.RepOboznachenie
        });

        this.Naimenovanie = ko.observable("").extend({
            required: { params: true, message: 'Наименование должено быть указано' },
            minLength: 3,
            //mustEqual: "TEST"
        });

        this.Pervichoepreminenie = ko.observable("").extend({
            required: { params: true, message: 'Первичное применение должено быть указано' },
            minLength: 3
        });

        this.Literal = ko.observable("");

        this.Date = ko.observable("").extend({
            required: { params: true, message: 'Дата должена быть указана' },
            validDate: true,
            minDate: Date.parse("02-01-1970"),
            maxDate: Date.now()

        });



        //this.Date = ko.observable("").extend({ date: false });





        this.IsAsm = ko.observable("");
        this.Razrabotal = ko.observable("");
        this.Proveril = ko.observable("");
        this.Tehcontrol = ko.observable("");
        this.Normokontrol = ko.observable("");
        this.Utverdil = ko.observable("");
        self.HeaderText = ko.observable("");
    };



    self.Init = function (data) {

        var row = data.tableRow;

        // #region Инициализация репозиториев
        self.RepInventarnNum(data.inventarnNum);
        self.RepProjectName(data.projectName);
        self.RepOboznachenie(data.oboznachenie);
        self.RepIspolniteli(data.ispolniteli);
        self.RepLiteral(data.literal);


        // #endregion

        self.Operation(data.operation);


        self.DocData.Id(row.Id);

        self.DocData.InventarnNum(row.InventarnNum);
        self.DocData.ProjectName(row.ProjectName);
        self.DocData.Oboznachenie(row.Oboznachenie);
        self.DocData.Naimenovanie(row.Naimenovanie);
        self.DocData.Pervichoepreminenie(row.Pervichoepreminenie);
        self.DocData.Literal(row.Literal);
        self.DocData.Date(row.Date);
        self.DocData.IsAsm(row.IsAsm);
        self.DocData.Razrabotal(row.Razrabotal);
        self.DocData.Proveril(row.Proveril);
        self.DocData.Tehcontrol(row.Tehcontrol);
        self.DocData.Normokontrol(row.Normokontrol);
        self.DocData.Utverdil(row.Utverdil);

        self.oldNum = row.InventarnNum;

        self.CurrInventarnNum(self.DocData.InventarnNum());
        self.HeaderText(data.headerText);
    };


    var clear = function () {
        self.RepInventarnNum([]);
        self.RepProjectName([]);
        self.RepOboznachenie([]);
        self.RepIspolniteli([]);
        self.RepLiteral([]);

        self.Operation("");

        self.DocData.Id("");
        self.DocData.InventarnNum("");
        self.DocData.ProjectName("");
        self.DocData.Oboznachenie("");
        self.DocData.Naimenovanie("");
        self.DocData.Pervichoepreminenie("");
        self.DocData.Literal("");
        self.DocData.Date("");
        self.DocData.IsAsm("");
        self.DocData.Razrabotal("");
        self.DocData.Proveril("");
        self.DocData.Tehcontrol("");
        self.DocData.Normokontrol("");
        self.DocData.Utverdil("");
    };

    self.close = function () {
        console.log(asmTree.GetNewDepend());
        rightsidebar.Close();
        asmTree.CloseTree();

        clear();
    };

    self.open = function () {

        asmTree.SetDependency(this.DocData.Oboznachenie());
        asmTree.Reset();
        asmTree.GetElem();
        rightsidebar.Open();
    };

    self.errors = ko.validation.group(self.DocData);

    self.submitData = function () {


        if (self.errors().length > 0) {
            self.errors.showAllMessages(true);
            this.errors().forEach(function (data) {
                //alert(data);
            });
            return;
        }


        // #region name
        var action = $('#AddUpdateDocJSON').val();
        var res = ko.mapping.toJSON(self.DocData);
        var res1 = ko.mapping.toJS(self.DocData);
        var dep = asmTree.GetNewDepend();
        dep.NewNum = res1.InventarnNum;
        dep.OldNum = self.oldNum;
        var depjson = JSON.stringify(asmTree.GetNewDepend());
        var dat = { 'tr': res1, 'dc': dep };
        $.ajax({
            url: action,
            //data:  res,

            //data: {
            //    "res": res,
            //    "ForAdd": dep.ForAdd,
            //    "ForDelete": dep.ForDelete,
            //    "ForAddRoot": dep.ForAddRoot,
            //    "forDeleteRoot": dep.forDeleteRoot,
            //},

            //data: {
            //    "tr": res
            //    ,"dc": res
            //
            //},

            data: JSON.stringify(dat),

            datatype: 'json',
            contentType: "application/json; charset=utf-8",
            success: function () {
                self.close();
                GetTable();
            },

            complete: function () {
            },

            type: "POST",

            error: function (xhr, status, err) {
                var errdat = JSON.parse(xhr.responseText);
                ErrorWindow.ShowError(errdat);
            }
        });
        // #endregion
    };

    setAsmTree = function (tree) {

        asmTree = tree;
    };

    this.SetAsmTree = setAsmTree;
}







