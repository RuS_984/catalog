﻿function ErrorViewModel() {
 
    const self = this;
 
    self.visible = ko.observable(false);
    self.errorHeader = ko.observable("");
    self.errorText = ko.observable("");
    self.errorFooter = ko.observable("");

    self.ShowError = function(data) {
        self.errorHeader(data.ErrorHeader);
        self.errorText(data.ErrorText);
        self.errorFooter(data.ErrorType);;
        self.Show();
    };
 
   self.Show = function () {
       errormodal.Show();
   }
   
   self.Hide = function () { 
       errormodal.Hide();
   }
}