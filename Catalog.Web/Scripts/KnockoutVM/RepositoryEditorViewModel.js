﻿function RepositoryEditorViewModel() {

    const self = this;
    self.itemToAdd = ko.observable("");
    self.RepositoryName = ko.observable("");
    self.RepositoryList = ko.observableArray("");
    self.RepositoryTable = ko.observable("");
    self.selectedItem = ko.observable("");

    self.newName = ko.observable("");


    self.Init = function (data) {


        self.RepositoryName(data.RepositoryName);
        self.RepositoryList(data.RepositoryList);
        self.RepositoryTable(data.RepositoryTable);
        self.renameClose();
    };


    self.close = function () {
        topsidebar.Close();
        GetTable();
    }

    self.open = function () {
        topsidebar.Open();
    }


    self.addItem = function () {
        if ((self.itemToAdd() !== "") && (self.RepositoryList.indexOf(self.itemToAdd()) < 0)) // Prevent blanks and duplicates
        {
            // #region name


            var action = $('#AddRep').val();
            var newValue = self.itemToAdd()

            $.ajax({
                url: action,
                data: {
                    "RepositoryTable": self.RepositoryTable(),
                    "NewValue": newValue
                },

                success: function () {
                    self.RepositoryList.push(newValue);
                    self.itemToAdd("");
                    $(".Rerository-List__Entity").hover(
                        function () {
                            $(this).find('.Rerository-List__Entity-Buttons').css("display", "block");
                        }, function () {
                            $(this).find('.Rerository-List__Entity-Buttons').css("display", "none");
                        }
                    );

                },

                complete: function () {
                },

                type: "POST",

                error: function (xhr, status, err) {
                    var errdat = JSON.parse(xhr.responseText);
                    ErrorWindow.ShowError(errdat);
                }
            })

        }




        // #endregion


    };
    //----


    self.deleteItem = function () {
        if ((self.selectedItem() !== "")) // Prevent blanks and duplicates
        {





            // #region name


            var action = $('#DelRep').val();
            var delValue = self.selectedItem()

            $.ajax({
                url: action,
                data: {
                    "RepositoryTable": self.RepositoryTable(),
                    "DelValue": delValue
                },

                success: function () {
                    self.RepositoryList.remove(delValue);
                },

                complete: function () {
                },

                type: "POST",

                error: function (xhr, status, err) {
                    var errdat = JSON.parse(xhr.responseText);
                    ErrorWindow.ShowError(errdat);
                }
            })

        }

        // #endregion

    };






    self.deleteItemByName = function (name) {
        if ((name !== "")) // Prevent blanks and duplicates
        {
            if (!confirm("Удалить запись?")) {
                return;
            }

            // #region name
            var action = $('#DelRep').val();

            $.ajax({
                url: action,
                data: {
                    "RepositoryTable": self.RepositoryTable(),
                    "DelValue": name
                },

                success: function () {
                    self.RepositoryList.remove(name);
                },

                complete: function () {
                },

                type: "POST",

                error: function (xhr, status, err) {
                    var errdat = JSON.parse(xhr.responseText);
                    ErrorWindow.ShowError(errdat);
                }
            })

        }

        // #endregion

    };


    self.renameItem1 = function () {
        if ((self.selectedItem() !== "")) // Prevent blanks and duplicates
        {
            // #region name


            var action = $('#RenRep').val();
            var oldValue = self.selectedItem()
            var newValue = self.selectedItem()

            $.ajax({
                url: action,
                data: {
                    "RepositoryTable": self.RepositoryTable(),
                    "DelValue": delValue
                },

                //datatype: 'json',
                //contentType: "application/json; charset=utf-8",
                success: function () {
                    self.RepositoryList.remove(delValue);
                    self.selectedItem(); // Clear selection
                    //self.close()
                    //GetTable();

                },

                complete: function () {
                },

                type: "POST",

                error: function (xhr, status, err) {
                    var errdat = JSON.parse(xhr.responseText);
                    ErrorWindow.ShowError(errdat);
                }
            })

        }




        // #endregion




        //this.itemToAdd(""); // Clear the text box
    };

    self.renameClose = function () {
        var elem = $('div.renameElem');
        elem.closest('li').find('div.List__Entity__Data').css("display", "block");
        $('div.renameElem').remove();


    }

    self.renameItem = function (val, elm) {
        // #region name



        self.selectedItem(val);


        var elem = elm;

        var elm1 = $('div.renameElem');
        if (elm1.length) {
            var elm2 = elm1.closest('li')
            elm1.remove();
            elm2.find('div.List__Entity__Data').css("display", "block");
        }


        elem.find('div.List__Entity__Data').css("display", "none");

        var li1 = elem.closest('li.Rerository-List__Entity')
        var ta = '<textarea class="form-control add" rows="2" data-bind="value:newName"  style="max-width: 360px;">' + val + '</textarea>'
        var btn = '<p><button onclick= "renameEnt()">Переименовать</button> <button onclick= "renameCancel()" style = "float:right;" > Отмена</button></p>'
        elem.append('<div class= "renameElem">' + ta + btn + '</div>');


        renameCancel = function () {
            $('div.renameElem').remove();
            elem.find('div.List__Entity__Data').css("display", "block");

        }

        renameEnt = function () {
            if (!confirm("Переименовать запись?")) {
                return;
            }

            var newVal = jQuery(".renameElem textarea").val();

            var action = $('#RenRep').val();
            $.ajax({
                url: action,
                data: {
                    "RepositoryTable": self.RepositoryTable(),
                    "oldValue": self.selectedItem(),
                    "newValue": newVal
                },

                success: function (res) {
                    self.RepositoryList(res);
                                        $(".Rerository-List__Entity").hover(
                        function () {
                            $(this).find('.Rerository-List__Entity-Buttons').css("display", "block");
                        }, function () {
                            $(this).find('.Rerository-List__Entity-Buttons').css("display", "none");
                        })

                },

                complete: function () {
                },

                type: "POST",

                error: function (xhr, status, err) {
                    var errdat = JSON.parse(xhr.responseText);
                    ErrorWindow.ShowError(errdat);
                }
            })
        }
        // #endregion

    };



    self.removeSelected = function () {
        self.RepositoryList.removeAll(self.selectedItems());
        self.selectedItems([]);
    };

    // #region name

}