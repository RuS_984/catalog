﻿
// #region forDelete
function openRightMenu_() {
    document.getElementById("rightMenu").style.display = "block";
}
function closeRightMenu_() {
    document.getElementById("rightMenu").style.display = "none";
}


function showErrorModal_() {
    document.getElementById("errormodal").style.display = "block";
}

function hideErrorModal_() {
    document.getElementById("errormodal").style.display = "none";
}
function rightsidebarclose_() {
    closeRightMenu();
    $("#rightMenu div.sidebar-content").html('');
    $("#overlay").removeClass("overlay");
    $("body").removeClass("noscroll");
}

function myRowUpdate11_(RowIndex) {
    var xx = $("#myUrl").val();
    $.ajax({
        url: xx,

        data: {
            "rowIndex": RowIndex,
        },

        success: function (result) {
            $("#rightMenu div.sidebar-content").html(result);
            $("#overlay").addClass("overlay");
            $("body").addClass("noscroll");
            openRightMenu();
        },

        HttpMethod: "GET",
    })
}


function myRowUpdate22_(RowIndex) {
    var xx = $("#myUrl1").val();
    $.ajax({
        url: xx,

        data: {
            "rowIndex": RowIndex,
        },

        success: function (result) {
            CrudViewModel.Init(result);
            $("#overlay").addClass("overlay");
            $("body").addClass("noscroll");
            rightsidebar.Open();
        },

        HttpMethod: "GET",
    })
}


function rightSideBar_() {
    const self = this;
    self.Open = function () {
        document.getElementById("rightMenu").style.display = "block";
    };

    self.Close = function () {
        document.getElementById("rightMenu").style.display = "none";
    };

}

function leftSideBar_() {
    const self = this;
    self.Open = function () {
        document.getElementById("leftMenu").style.display = "block";
    };

    self.Close = function () {
        document.getElementById("leftMenu").style.display = "none";
    };

}

var SideBar_ = new function () {
    this.Right = new function () {
        this.Open = function () {
            document.getElementById("rightMenu").style.display = "block";
        };

        this.Close = function () {
            document.getElementById("rightMenu").style.display = "none";
        };
    };
    this.Left = new function () {
        this.Open = function () {
            document.getElementById("rightMenu").style.display = "block";
        };

        this.Close = function () {
            document.getElementById("rightMenu").style.display = "none";
        };

    };

}


function errortest() {
    var xx = $("#myError").val();

    $.ajax({
        url: xx,


        success: function () {

        },

        type: "POST",

        error: function (xhr, status, err) {

            var errdat = JSON.parse(xhr.responseText);
            ErrorWindow.ShowError(errdat);

            document.getElementById('errormodal').style.display = 'block';
            $('#overlay').addClass('overlay');

        }
    })
}

// #endregion


var rightsidebar = {

    Open: function () {
        $("#overlay").addClass("overlay");
        $("body").addClass("noscroll");
        $("#rightsidebar").css({ right: "0px" })
    },
    Close: function () {
        $("#rightsidebar").css({ right: "-300px" })
        setTimeout(function () {
            $("#overlay").removeClass("overlay");
            $("body").removeClass("noscroll");
        }, 1000);

    }
}

var leftsidebar = {

    Open: function () {
        $("#overlay").addClass("overlay");
        $("body").addClass("noscroll");
        $("#leftsidebar").css({ left: "0px" })
    },
    Close: function () {
        $("#leftsidebar").css({ left: "-300px" })
        setTimeout(function () {
            $("#overlay").removeClass("overlay");
            $("body").removeClass("noscroll");
        }, 1000);
    }
}

var errormodal = {
    Show: function () {
        $("#overlay").addClass("overlay");
        $("body").addClass("noscroll");
        document.getElementById('errormodal').style.display = 'block';
    },
    Hide: function () {
        document.getElementById('errormodal').style.display = 'none';
            $("#overlay").removeClass("overlay");
            $("body").removeClass("noscroll");
    }
}

var topsidebar = {
    Open: function () {
        $("#overlay").addClass("overlay");
        $("body").addClass("noscroll");
        $("#topsidebar").css({ top: "0px" })
    },
    Close: function () {
        $("#topsidebar").css({ top: "-350px" })
        setTimeout(function () {
            $("#overlay").removeClass("overlay");
            $("body").removeClass("noscroll");
        }, 1000);
    }
}
