﻿
function MyTree(param) {
    "use strict";

    var elem = param.Element;
    var sidebar = param.SideBar;
    var isAsm = param.IsAsm;
    var dependency = param.Dependency;

    var addedNodes = {};
    var deletedNodes = {};

    var openedlst = [];
    var checkedlst = [];



    var forAdd = [];
    var forDelete = [];
    var forAddRoot = [];
    var forDeleteRoot = [];





    var pause = 0;

    function getElem( isupdated) {

        checkedlst = [];

        if (!isAsm) {

            $(".asmdoc-tree-content").html("");

            if ($(elem).has('ul').length === 0) {
                render();
            }
            else {
                sidebar.Open();

            }
            $('.sidebar-left .close').removeClass('invisible');
            $('.fulldoc-tree-content').removeClass('invisible');
        }
        else {

            $(".asmdoc-tree-content").html("");

            $('.fulldoc-tree-content').addClass('invisible');
            $('.sidebar-left .close').addClass('invisible');
            renderasm();
            sidebar.Open();

        }


    }


    function setDependency(dep) {
        dependency = dep;
        
    }


    function reset() {
        forAdd = [];
        forDelete = [];
        forAddRoot = [];
        forDeleteRoot = [];

    }



    function render() {

        GetRootTree();

        elem.onmousedown = function () {
            return false;
        };

        elem.onclick = function (event) {
            if (event.target.classList.contains('.hasChild')) {
                toggle();
            }


        }
    }

    function GetRootTree() {
        var xx = $("#RootTree").val();
        $.ajax({
            url: xx,

            success: function (result) {

                var ul = document.createElement('ul');

                ul.style['list-style-type'] = 'none';
                ul.classList.add('tree-rootnode');

                for (var i in result) {
                    ul.appendChild(treeNode(result[i]));
                }

                ul.onmousedown = function () {
                    return false;
                };

                ul.onclick = function (event) {
                    if (event.target.parentElement.classList.contains('hasChild')) {
                        toggle(event.target.parentElement);

                    }

                }

                elem.append(ul);

                sidebar.Open();
                alert(checkedlst);
            },
            HttpMethod: "POST"
        })
    }


    function GetSubTree(node) {
        var xx = $("#SubTree").val();
        $.ajax({
            url: xx,

            beforeSend: function () {
                node.classList.add('loading');
            },

            data: {
                "node": node.dataset.name,
            },

            success: function (result) {

                node.classList.remove('loading');

                var ul = document.createElement('ul');

                ul.classList.add('tree-subnode');

                ul.style['list-style-type'] = 'none';
                for ( var i in result.SubNodes) {
                    ul.appendChild(treeNode(result.SubNodes[i]));
                }

                node.append(ul);



            },

            HttpMethod: "POST"
        })
    }



    function renderasm() {

        GetAsmRootTree(dependency);
    }


    function GetAsmRootTree() {
        var xx = $("#AsmRootTree").val();
        $.ajax({
            url: xx,

            data: {
                "Dependency": dependency
            },


            success: function (result) {
                openedlst = result.opendList;
                //openedlst.push('');

                var ul = document.createElement('ul');

                ul.style['list-style-type'] = 'none';
                ul.classList.add('tree-rootnode');

                //for (i in result.rootNodes) {
                for (var i = 0; i < result.rootNodes.length; i++){
                    var tn1 = treeNode(result.rootNodes[i]);
                    tn1.dataset.id = result.rootNodes[i].documentId;
                    tn1.classList.add('rootNode');
                    ul.appendChild(tn1);
                    if (openedlst.indexOf(result.rootNodes[i].text) > -1) {
                        open(tn1);
                    }

                }

                ul.onmousedown = function () {
                    return false;
                };

                ul.onclick = function (event) {
                    if (event.target.parentElement.classList.contains('hasChild')) {
                        toggle(event.target.parentElement);
                    }
                    if (event.target.classList.contains('chbox')) {
                        var nodeText = event.target.parentElement.innerText;
                        var nodeid = event.target.parentElement.parentElement.dataset.id
                        var _foradd;
                        var _fordelete;
                        if (event.target.parentElement.parentElement.classList.contains('rootNode')) {
                            _foradd = forAddRoot;
                            _fordelete = forDeleteRoot;
                        }
                        else {
                            _foradd = forAdd;
                            _fordelete = forDelete;

                        }
                        //alert(event.target.parentElement.innerText + ' ||  checkedlst:' + checkedlst);


                        if (event.target.hasAttribute('checked')) {
                            event.target.removeAttribute('checked');



                            if (checkedlst.indexOf(nodeText) > -1) {
                                //_fordelete.push(nodeText);
                                _fordelete.push(nodeid);
                                //alert('_fordelete_1:' + _fordelete);
                            }

                            else {
                                //remove1(_foradd, nodeText);
                                remove1(_foradd, nodeid);
                                //alert('_foradd_1:' + _foradd);
                            }


                        }
                        else {
                            event.target.setAttribute('checked', '')
                            if (checkedlst.indexOf(nodeText) > -1) {
                                //remove1(_fordelete, nodeText);
                                remove1(_fordelete, nodeid);
                                //alert('_fordelete_2:' + _fordelete);
                            }
                            else {
                                //_foradd.push(nodeText);
                                _foradd.push(nodeid);
                                //alert('_foradd_2:' + _foradd);
                            }

                        }




                    }

                }



                elem.append(ul);

                sidebar.Open();
                //alert('GetAsmRootTree:  ' + checkedlst);
            },
            HttpMethod: "POST"
        })
    }


    function GetAsmSubTree(node) {
        var xx = $("#AsmSubTree").val();
        var dep = "";

        if ("text" in node) {
            dep = node.text;
        }
        else {
            dep = node.dataset.name
        }


        $.ajax({
            url: xx,

            beforeSend: function () {
                node.classList.add('loading');
            },

            data: {
                "node": dep,
                "Dependency": dependency
            },

            success: function (result) {

                node.classList.remove('loading');

                var ul = document.createElement('ul');

                ul.classList.add('tree-subnode');

                ul.style['list-style-type'] = 'none';
                //for (i in result.SubNodes) {
                for (var i = 0; i < result.SubNodes.length; i++){
                    var tn1 = treeNode(result.SubNodes[i]);
                    tn1.dataset.id = result.SubNodes[i].documentId;

                    ul.appendChild(tn1);


                    if (openedlst.indexOf(result.SubNodes[i].text) > -1) {
                        open(tn1);
                    }

                }

                node.append(ul);



                //alert('GetAsmSubTree:  ' + checkedlst);
            },

            HttpMethod: "POST"
        })
    }


    function GetChanges() {

    }





    function treeNode(nodeObj) {

        var li = document.createElement('LI');
        var iHtml = '';
        var nodeText = nodeObj.text;

        if (nodeObj.hasChild) {

            //li.className = 'hasChild';
            li.classList.add('hasChild');
        }

        li.classList.add('leaf');

        iHtml += nodeText;


        var span = document.createElement('span');

        if (isAsm) {

            

            var inp = document.createElement('input');
            inp.classList.add('chbox');
            inp.setAttribute("type", "checkbox");

            if (nodeObj.isChecked) {

                inp.setAttribute("checked", "checked");
                checkedlst.push(nodeObj.text);
                //alert('treeNode:  ' + checkedlst);
                //li.classList.add('open');

                if (nodeObj.hasChild) {
                    //GetAsmSubTree(nodeObj)
                    //open(nodeObj);
                }

            }

            if (nodeText != dependency) {
                span.appendChild(inp);
            }
        }



        span.innerHTML += iHtml;
        //span.classList.add('hasChild');

        li.appendChild(span);

        li.dataset.name = nodeText;
        li.dataset.id = nodeObj.documentId;

        return li;
    }


    function remove1(array, element) {
        const index = array.indexOf(element);

        if (index !== -1) {
            array.splice(index, 1);
        }
    }


    function open(elem_) {
        if ($(elem_).has('ul').length === 0) {
            if (!isAsm) {
                GetSubTree(elem_);
            }
            else {
                GetAsmSubTree(elem_);
            }
        }
        elem_.classList.add('open');
    };

    function close(elem_) {
        elem_.classList.remove('open');
    };

    function toggle(elem_) {
        if (elem_.classList.contains('open')) close(elem_);
        else open(elem_);
        //alert('toggle')
    };

    function closeTree() {
        sidebar.Close();
    };

    function getChecked() {
        return checkedlst;
    }

    function getNewDepend() {
        var dep = {
            ForAdd: forAdd,
            ForDelete: forDelete,
            ForAddRoot: forAddRoot,
            ForDeleteRoot: forDeleteRoot,
            OldNum: -1,
            NewNum: -1
        };

        return dep;

    }




    this.GetElem = getElem;
    this.SetDependency = setDependency;
    this.CloseTree = closeTree;
    this.GetChecked = getChecked;
    this.GetNewDepend = getNewDepend;
    this.Reset = reset;

    
}
