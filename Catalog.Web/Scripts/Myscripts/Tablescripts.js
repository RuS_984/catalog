﻿function mytable3(CurPage, ItemsPerPage) {
    $.ajax({
        url: '@Url.Action(actionName:"Table", controllerName:"Table")',
        data: {
            "curPage": CurPage,
            "itemsPerPage": ItemsPerPage
        },
        success: function (result) {
            $("#MainTable").html(result);
        },
        HttpMethod: "GET",
    })
}

//_-----------------------------------------------------------------------------------------------------------


function GetTable(CurPage, ItemsPerPage) {
    var xx = $("#GetTable").val();
    $.ajax({
        url: xx,

        beforeSend: function () {
            $("#MainTable").html('');
            $("#loading-bar").show();
        },

        data: {
            "curPage": CurPage,
            "itemsPerPage": ItemsPerPage
        },

        complete: function () {
            $("#loading-bar").hide();
        },

        success: function (result) {
            $("#MainTable").html(result);
        },

        HttpMethod: "GET",
    })
}

function AddUpdateRow(docId) {
    var xx = $("#AddUpdateRowJSON").val();
    $.ajax({
        url: xx,

        data: {
            "docId": docId,
        },

        success: function (result) {
            CrudViewModel.Init(result);
            CrudViewModel.open();
            //result.tableRow.Oboznachenie

            //window.myTree1.SetDependency(result.tableRow.Oboznachenie);


            //myTree1.GetElem();
        },

        HttpMethod: "POST",
    })
}

function DeleteDoc(docId) {
    var xx = $("#DeleteRowJSON").val();
    $.ajax({
        url: xx,

        data: {
            "docId": docId,
        },

        success: function (result) {
            GetTable();;
        },

        HttpMethod: "POST"
    })
}

function GetRepository(repName) {
    var xx = $("#GetRep").val();
    $.ajax({
        url: xx,

        data: {
            "RepositoryName": repName,
        },

        success: function (result) {
            RepositoryEditorViewModel.Init(result);

            $('#RepositoryEditor').ready(function () {

                $(".Rerository-List__Entity").hover(
                    function () {
                        $(this).find('.Rerository-List__Entity-Buttons').css("display", "block");
                    }, function () {
                        $(this).find('.Rerository-List__Entity-Buttons').css("display", "none");
                    }
                );
            });

            RepositoryEditorViewModel.open();

        },

        HttpMethod: "POST",
    })
}


//_-----------------------------------------------------------------------------------------------------------


function myTablexxx(elem) {
    "use strict";
    var element = elem;

    function getTable(CurPage, ItemsPerPage) {
        var xx = $("#GetTable").val();
        $.ajax({
            url: xx,

            beforeSend: function () {
                element.html('');
                $("#loading-bar").show();
            },

            data: {
                "curPage": CurPage,
                "itemsPerPage": ItemsPerPage
            },

            complete: function () {
                $("#loading-bar").hide();
            },

            success: function (result) {
                element.html(result);
            },

            HttpMethod: "GET",
        })
    }


    function addUpdateRow(docId) {
        var xx = $("#AddUpdateRowJSON").val();
        $.ajax({
            url: xx,

            data: {
                "docId": docId,
            },

            success: function (result) {
                CrudViewModel.Init(result);
                CrudViewModel.open();
            },

            HttpMethod: "POST",
        })
    }

    function deleteDoc(docId) {
        var xx = $("#DeleteRowJSON").val();
        $.ajax({
            url: xx,

            data: {
                "docId": docId,
            },

            success: function (result) {
                getTable();
            },

            HttpMethod: "POST"
        })
    }


    return {
        GetTable: function (curPage, itemsPerPage) {
            getTable(curPage, itemsPerPage)
        },
        AddUpdateRow: function (docId) {
            addUpdateRow(docId)
        },
        DeleteDoc: function (docId) {
            deleteDoc(docId)
        }

    }
}



