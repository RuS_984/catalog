﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Catalog.Web.Models
{
    public class ErrorData
    {
        [JsonProperty]
        public string ErrorType { get; set;}
        //string ErrorCode { get; set; }
        [JsonProperty]
        public string ErrorHeader { get; set; }

        [JsonProperty]
        public string ErrorText { get; set; }

        [JsonProperty]
        public string ErrorStack { get; set; }


        public ErrorData(string errorType="", string errorHeader = "", string errorText = "", string errorStack = "")
        {
            ErrorType   = errorType;
            ErrorHeader = errorHeader;
            ErrorText = errorText;
            ErrorStack = errorStack;
        }

        public string GetJsonString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);

        }
    }
}