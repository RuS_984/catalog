﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using DataModel.DBWrapper;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Catalog.Web.Models
{
    public class DBModel
    {
        DataTable dt;

        List<TableRow> rowList = new List<TableRow>();

        public DBModel(DataTable DT)
        {
            dt = DT;
        }

        public int Count
        {
            get { return dt.Rows.Count; }
        }

        public List<TableRow> GetRows(int startIndex, int rowquantity, int endIndex = -1)
        {
            if (startIndex < 0)
            {
                throw new ArgumentException("startIndex");
            }

            if (rowquantity < 0)
            {
                throw new ArgumentException("rowquantity");
            }

            for (int i = startIndex; i < (startIndex + rowquantity); i++)
            {
                rowList.Add(GetRowStruct(dt.Rows[i].ItemArray));
            }

            return rowList;
        }


        public List<TableRow> GetRows1(int currentPage, int itemsPerPage)
        {
            rowList.Clear();
            if (currentPage < 0)
            {
                throw new ArgumentException("currentPage");
            }

            if (itemsPerPage < 0)
            {
                throw new ArgumentException("itemsPerPage");
            }

            int startIndex = (currentPage - 1) * itemsPerPage;

            int endIndex = startIndex + itemsPerPage;

            if (endIndex > dt.Rows.Count)
            {
                endIndex = dt.Rows.Count;
            }

            for (int i = startIndex; i < (endIndex); i++)
            {
                rowList.Add(GetRowStruct(dt.Rows[i].ItemArray));
            }

            return rowList;
        }


        public TableRow GetRow(int index)
        {
            if (index < 0 || index > dt.Rows.Count - 1)
            {
                throw new ArgumentException("index");
            }

            return GetRowStruct(dt.Rows[index].ItemArray);
        }


        public TableRow GetRowById(int id)
        {

            foreach (DataRow item in dt.Rows)
            {
                if ((int)item[0] == id)
                {
                    return GetRowStruct(item.ItemArray);
                }
            }

            return null;

        }

        public TableRow GetRowByInvNum(int invNum)
        {

            foreach (DataRow item in dt.Rows)
            {
                if ((int)item[1] == invNum)
                {
                    return GetRowStruct(item.ItemArray);
                }
            }

            return null;
        }

        public TableRow GetRowStruct(object[] cells)
        {
            TableRow tr = new TableRow();

            tr.Id = (int)cells[0];
            tr.InventarnNum = (int)cells[1];


            tr.ProjectName = cells[2].ToString();
            tr.Oboznachenie = cells[3].ToString();
            tr.Naimenovanie = cells[4].ToString();

            tr.Pervichoepreminenie = cells[5].ToString();
            tr.Date = DateTime.Parse(cells[6].ToString()).ToString("yyyy-MM-dd");

            tr.Literal = cells[7].ToString();

            tr.IsAsm = (bool)cells[8];

            tr.Razrabotal = cells[9].ToString();
            tr.Proveril = cells[10].ToString();
            tr.Tehcontrol = cells[11].ToString();
            tr.Normokontrol = cells[12].ToString();
            tr.Utverdil = cells[13].ToString();

            return tr;
        }
    }

    public class TableRow
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Инвентарный номер")]
        //[StringLength(10, MinimumLength = 3)]
        public int InventarnNum { get; set; }

        [Required]
        [Display(Name = "Название проекта")]
        public string ProjectName { get; set; }

        //[Required]
        [Display(Name = "Обозначение")]
        public string Oboznachenie { get; set; }

        //[Required]
        [Display(Name = "Наименование")]
        public string Naimenovanie { get; set; }

        //[Required]
        [Display(Name = "Первичное применение")]
        public string Pervichoepreminenie { get; set; }

        //[Required]
        [Display(Name = "Литера")]
        public string Literal { get; set; }

        //[Required]
        [Display(Name = "Дата подписания")]
        public string Date { get; set; }

        //[Required]
        [Display(Name = "Сборка")]
        public bool IsAsm { get; set; }

        //[Required]
        [Display(Name = "Разработал")]
        public string Razrabotal { get; set; }

        //[Required]
        [Display(Name = "Проверил")]
        public string Proveril { get; set; }

        //[Required]
        [Display(Name = "Тех контроль")]
        public string Tehcontrol { get; set; }

        //[Required]
        [Display(Name = "Нормоконтроль")]
        public string Normokontrol { get; set; }

        //[Required]
        [Display(Name = "Утвердил")]
        public string Utverdil { get; set; }

        public TableRow()
        {
            Id = -1;
            InventarnNum = -1;
            ProjectName = string.Empty;
            Oboznachenie = string.Empty;
            Naimenovanie = string.Empty;
            Pervichoepreminenie = string.Empty;
            Literal = string.Empty;
            Date = string.Empty;
            IsAsm = false;
            Razrabotal = string.Empty;
            Proveril = string.Empty;
            Tehcontrol = string.Empty;
            Normokontrol = string.Empty;
            Utverdil = string.Empty;

        }

        public void Clear()
        {
            Id = -1;
            InventarnNum = -1;
            ProjectName = string.Empty;
            Oboznachenie = string.Empty;
            Naimenovanie = string.Empty;
            Pervichoepreminenie = string.Empty;
            Literal = string.Empty;
            Date = string.Empty;
            IsAsm = false;
            Razrabotal = string.Empty;
            Proveril = string.Empty;
            Tehcontrol = string.Empty;
            Normokontrol = string.Empty;
            Utverdil = string.Empty;

        }

        public void SetDefaultData()
        {
            Id = -1;
            InventarnNum = -1;
            ProjectName = string.Empty;
            Oboznachenie = string.Empty;
            Naimenovanie = string.Empty;
            Pervichoepreminenie = string.Empty;
            Literal = string.Empty;
            Date = "2016.10.25";
            IsAsm = false;
            Razrabotal = string.Empty;
            Proveril = string.Empty;
            Tehcontrol = string.Empty;
            Normokontrol = string.Empty;
            Utverdil = string.Empty;

        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is MainTableStruct)
            {
                string thisDate = DateTime.Parse(((TableRow)obj).Date).ToString("yyyy.MM.dd");
                string objDate = DateTime.Parse(((TableRow)obj).Date).ToString("yyyy.MM.dd");

                if (((TableRow)obj).Id == this.Id
                    && ((TableRow)obj).InventarnNum == this.InventarnNum
                    && ((TableRow)obj).ProjectName == this.ProjectName
                    && ((TableRow)obj).Oboznachenie == this.Oboznachenie
                    && ((TableRow)obj).Naimenovanie == this.Naimenovanie
                    && ((TableRow)obj).Pervichoepreminenie == this.Pervichoepreminenie
                    && ((TableRow)obj).Literal == this.Literal
                    && objDate == thisDate
                    && ((TableRow)obj).Razrabotal == this.Razrabotal
                    && ((TableRow)obj).Proveril == this.Proveril
                    && ((TableRow)obj).Tehcontrol == this.Tehcontrol
                    && ((TableRow)obj).Normokontrol == this.Normokontrol
                    && ((TableRow)obj).Utverdil == this.Utverdil
                    )
                {
                    return true;
                }
            }
            return false;
        }


        public static bool operator ==(TableRow p1, TableRow p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(TableRow p1, TableRow p2)
        {
            return !p1.Equals(p2);
        }
    }

    public class DocDependency
    {
        private List<int> forAdd = new List<int>();
        private List<int> forDelete = new List<int>();
        private List<int> forAddRoot = new List<int>();
        private List<int> forDeleteRoot = new List<int>();




        public List<int> ForAdd
        {
            get { return forAdd; }
            set { forAdd = value; }
        }
        public List<int> ForDelete
        {
            get { return forDelete; }
            set { forDelete = value; }
        }
        public List<int> ForAddRoot
        {
            get { return forAddRoot; }
            set { forAddRoot = value; }
        }
        public List<int> ForDeleteRoot
        {
            get { return forDeleteRoot; }
            set { forDeleteRoot = value; }
        }

        public int OldNum { get; set; }
        public int NewNum { get; set; }




    }


}