﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catalog.Web.Models
{
    public class NavBarModel
    {
        public string Brand { get; set; }
        public List<NavBarMenuItem> NavBarMenuItemsLeft;
        public List<NavBarMenuItem> NavBarMenuItemsRight;

        public NavBarModel()
        {
            NavBarMenuItemsLeft  = new List<NavBarMenuItem>();
            NavBarMenuItemsRight = new List<NavBarMenuItem>();
        }
    };

    public class NavBarMenuItem
    {
        public string Name { get; set; }

        public string href { get; set; }

        public string Target { get; set; }

        public string UniqeClass { get; set; }

        public string HTMLId { get; set; }

        bool isDropDown;

        [JsonIgnore]
        public bool IsDropDown
        {
            get
            {
                return SubItems.Count > 0;
            }
        }

        [JsonProperty]
        List<NavBarMenuItem> subItems = new List<NavBarMenuItem>();

        [JsonIgnore]
        public List<NavBarMenuItem> SubItems
        {
            get
            {
                return subItems;
            }
            set
            {
                subItems = value;
            }
        }

        [JsonProperty]
        string glyphiconClass;

        [JsonIgnore]
        public string GlyphiconClass
        {
            get
            {
                return "glyphicon " + glyphiconClass;
            }
            set
            {
                glyphiconClass = value;
            }
        }

        bool hasGlyphicon;

        [JsonIgnore]
        public bool HasGlyphicon
        {
            get
            {
                return glyphiconClass != string.Empty;
            }
        }

        public NavBarMenuItem(string name)
        {
            Name = name;
            Target = string.Empty;
            UniqeClass = string.Empty;
            HTMLId = string.Empty;
        }
    
    }
}