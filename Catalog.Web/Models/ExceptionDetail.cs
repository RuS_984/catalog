﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catalog.Web.Models
{
    public class ExceptionDetail
    {
        public int Id { get; set; }
        public string ExceptionMessage { get; set; }    // сообщение об исключении
        public string ControllerName { get; set; }  // контроллер, где возникло исключение
        public string ActionName { get; set; }  // действие, где возникло исключение
        public string StackTrace { get; set; }  // стек исключения
        public DateTime Date { get; set; }  // дата и время исключения
        public ErrorData errorData { get; set; }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            string nl = Environment.NewLine;

            sb.Append(nl + "Id: " + Id + nl);
            sb.Append("ControllerName: " + ControllerName + nl);
            sb.Append("ActionName: " + ActionName + nl);
            sb.Append("StackTrace: " + StackTrace + nl);
            sb.Append("Date: " + Date.ToString() + nl);
            sb.Append("ErrorData " +  nl);
            sb.Append("ErrorHeader: " + errorData.ErrorHeader + nl);
            sb.Append("ErrorType: " + errorData.ErrorType + nl);
            sb.Append("ErrorText: " + errorData.ErrorText + nl);
            sb.Append("ErrorStack: " + errorData.ErrorStack + nl);




            return sb.ToString();
        }
    }
}