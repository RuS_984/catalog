﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catalog.Web.Models
{
    public class RepositoryModel
    {
        public List<string> RepositoryList;
        public string  RepositoryName;
        public string  RepositoryTable;

    }

    public class RepositoryAddModel
    {
        public string  NewValue;
        public string  RepositoryTable;

    }
}