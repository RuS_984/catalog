﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catalog.Web.Models
{
    public class TableInfo
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public List<TableRow> Rows { get; set; }
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }

        public string[] ColNames { get; set; }
    }

    public static class PageParams
    {
        public static int[] ItemCount = {10, 20, 30, 50, 100};
    }
}