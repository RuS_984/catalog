﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catalog.Web.Models
{
    public class CRUDModel
    {
        public TableRow tableRow { get; set; }
        public List<string> inventarnNum;

        public List<string>  projectName;
        public List<string>  oboznachenie;

        public List<string> literal;
        public List<string> ispolniteli;

        public string operation { get; set; }
        public string headerText { get; set; }

    } 
}