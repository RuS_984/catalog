﻿using Catalog.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using static DataModel.SupportClasses.ExceptionHandler;

namespace Catalog.Web.Infrastructure
{
    public static class ExceptionParser
    {
        public static ErrorData Parse(Exception ex)
        {
            //ErrorData ErData = new ErrorData();

            string errType = string.Empty;
            string errHeader = string.Empty;
            string errText = string.Empty;
            string errStack = string.Empty;




            if (ex is SqlCeException)
            {
                errType = "SQL";
                errHeader = "Ошибка работы с БД";
                switch (((SqlCeException)ex).NativeError)
                {
                    case 25011:
                        {
                            errText = "Несоответсвующий файл БД";
                            break;
                        }
                    case 25017:
                        {
                            errText = "Файл БД поврежден";
                            break;
                        }
                    case 25035:
                        {
                            errText ="Файл занят другим процессом";
                            break;
                        }
                    default:
                        {
                            //MessageBox.Show("MainExceptionHandle SqlCeException\n"
                            //                                + "Message: " + ex.Message
                            //                                + "NativeError: \n" + ((SqlCeException)ex).NativeError);
                            errText = "Произошла неизвестная ошибка";
                            break;
                        }
                }
            }
            else if (ex is SQLEntityException)
            {
                errType = "SQL";
                errText = SQLEntityExceptionParser((SQLEntityException)ex);
            }
            else if (ex is Exception)
            {
                errType = "Общая";
                errHeader = "Общая Ошибка";
                errText = "Произошла неизвестная ошибка";
            }




            return new ErrorData(
                errorType: errType,
                errorHeader: errHeader,
                errorText: errText,
                errorStack: errStack
                );

        }

        static string SQLEntityExceptionParser(SQLEntityException ex)
        {
            string mes = string.Empty;// "Невозможно удалить выбраный проект { 0}";

            switch (ex.SQLNativeError)
            {
                //Изменения в БД противоречащие FOREIGN KEY Constraint
                //Невозможно удалить первичный ключ, так как еще существуют ссылки на этот ключ
                //Foreign key constraint
                case 25025:
                    {
                        if (ex.SQLTableName == DBTableName.Project)
                        { mes = string.Format("Невозможно удалить выбраный проект {0}", ex.SQLEntityName); }
                        else if (ex.SQLTableName == DBTableName.Ispolniteli)
                        { { mes = string.Format("Невозможно удалить выбраного исполнителя  {0}", ex.SQLEntityName); } }

                        //MessageBox.Show(//"MainExceptionHandle SQLEntityException\n" + ex.Message + "\n"
                        //         "mes:" + mes + "\n"
                        //        + "SQLCENativeError:" + ex.SQLCENativeError + "\n"
                        //        + "SQLTableName:" + ex.SQLTableName + "\n"
                        //        + "SQLEntityID:" + ex.SQLEntityID + "\n"
                        //        + "SQLEntityName:" + ex.SQLEntityName
                        //        + "OperationType:" + ex.OperationType.ToString()
                        //        );
                        break;
                    }

                //Повторяющееся значение невозможно вставить в уникальный индекс.
                case 25016:
                    {
                        if (ex.SQLTableName == DBTableName.Project)
                        { mes = string.Format("Невозможно создать или переименовать проект, т.к. проект с именем {0} уже существует", ex.SQLEntityName); }
                        else if (ex.SQLTableName == DBTableName.Ispolniteli)
                        { { mes = string.Format("Невозможно создать или переименовать исполнителя, т.к. исполнитель с именем {0} уже существует", ex.SQLEntityName); } }

                        //MessageBox.Show("MainExceptionHandle SQLEntityException\n" + ex.Message + "\n"
                        //        + "mes:" + mes + "\n"
                        //        + "SQLCENativeError:" + ex.SQLCENativeError + "\n"
                        //        + "SQLTableName:" + ex.SQLTableName + "\n"
                        //        + "SQLEntityID:" + ex.SQLEntityID + "\n"
                        //        + "SQLEntityName:" + ex.SQLEntityName + "\n"
                        //        + "OperationType:" + ex.OperationType.ToString()
                        //        );
                        break;
                    }

                    //Файл не является файлом формата базы данных SQL Server Compact.

                    
            }
            return mes;
        }

    }
}