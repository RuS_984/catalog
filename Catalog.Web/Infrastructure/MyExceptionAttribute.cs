﻿using Catalog.Web.Models;
using DataModel.AppLogger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Catalog.Web.Infrastructure
{
    #region ForDelete

    public class MyExceptionAttribute1 : HandleErrorAttribute// FilterAttribute, IExceptionFilter
    {//ExceptionHandler.SQLEntityException
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                string message = filterContext.Exception.GetType().ToString();

                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.RequestContext.HttpContext.Response.Headers.Add("X-ErrorMessage", message);
                filterContext.ExceptionHandled = true;

                Dictionary<string, string> err = new Dictionary<string, string>
                {
                    { "James", "9001" },
                    { "Jo", "3474" },
                    { "Jess", "11926" }
                };

                string ddd = Newtonsoft.Json.JsonConvert.SerializeObject(err);

                filterContext.Result = new JsonResult
                {
                    Data = ddd,
                    ContentType = null,
                    ContentEncoding = null,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };
            }
        }

        private JsonResult JsonHelper(Exception ex)
        {
            return new JsonResult
            {
                Data = "message",
                ContentType = null,
                ContentEncoding = null,
                JsonRequestBehavior = JsonRequestBehavior.DenyGet
            };
        }
    }

    public class RangeExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled &&
              filterContext.Exception is DivideByZeroException)
            {
                if (!filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    string val = (((DivideByZeroException)filterContext.Exception).Message);
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "RangeError",
                        ViewData = new ViewDataDictionary<string>(val)
                    };
                    filterContext.ExceptionHandled = true;

                    //----
                }

                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    string message = (((DivideByZeroException)filterContext.Exception).Message);

                    filterContext.HttpContext.Response.StatusCode = 500;
                    filterContext.RequestContext.HttpContext.Response.Headers.Add("X-ErrorMessage", message);
                    filterContext.ExceptionHandled = true;

                    ErrorData errData = new ErrorData(
                        errorType: "SQL", errorHeader: "Ошибка работы с БД",
                        errorText: "Ошибка при добавлении", errorStack: ""
                        );
                    string msg = errData.ToString();

                    filterContext.Result = new JsonResult
                    {
                        Data = msg,
                        ContentType = null,
                        ContentEncoding = null,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                //----
            }
        }
    }

    #endregion ForDelete

    public class MyExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                if (!filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    string val = filterContext.Exception.Message;
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "RangeError",
                        ViewData = new ViewDataDictionary<string>(val)
                    };
                    filterContext.ExceptionHandled = true;
                }

                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 500;
                    filterContext.ExceptionHandled = true;

                    ErrorData errData = ExceptionParser.Parse(filterContext.Exception);
                    /*
                      if (filterContext.Exception is SQLEntityException)
                      {
                          SQLEntityException ed = (SQLEntityException)filterContext.Exception;
                          errData = new ErrorData(
                          errorType: "SQL",
                          errorHeader: "Ошибка работы с БД",
                          errorText: ed.Message,
                          errorStack: ed.StackTrace
                          );
                          filterContext.RequestContext.HttpContext.Response.Headers.Add("X-ErrorMessage", ed.Message);
                          filterContext.Result = JsonHelper(errData);
                          return;
                      }
                      else if (filterContext.Exception is Exception)
                      {
                          Exception ed = (Exception)filterContext.Exception;
                          errData = new ErrorData(
                          errorType: "Общая",
                          errorHeader: "Ошибка приложения",
                          errorText: ed.Message,
                          errorStack: ed.StackTrace
                          );
                          filterContext.RequestContext.HttpContext.Response.Headers.Add("X-ErrorMessage", ed.Message);
                          filterContext.Result = JsonHelper(errData);
                          return;
                      }
                    */

                    ExceptionDetail exceptionDetail = new ExceptionDetail()
                    {
                        ExceptionMessage = filterContext.Exception.Message,
                        StackTrace = filterContext.Exception.StackTrace,
                        ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                        ActionName = filterContext.RouteData.Values["action"].ToString(),
                        Date = DateTime.Now ,
                        errorData = errData
                    };

                    Logger.Log(logTarget: LogTarget.FileLogger, logLevel: LogLevel.Debug,
                        Message: exceptionDetail.ToString());

                    filterContext.RequestContext.HttpContext.Response.Headers.Add("X-ErrorMessage",
                        filterContext.Exception.Message);
                    filterContext.Result = JsonHelper(errData);
                }
            }
        }

        private ContentResult JsonHelper(ErrorData errData)
        {
            return new ContentResult
            {
                Content = JsonConvert.SerializeObject(errData),
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }
    }
}