﻿using Catalog.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Catalog.Web.Infrastructure
{
    public static class PagingHelpers
    {

        public static MvcHtmlString PageLinks(this HtmlHelper html,
                                      TableInfo pagingInfo,
                                      Func<int, string> pageUrl
                                      , string jsScript)
        {

            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.AddCssClass("page" + i.ToString());
                tag.MergeAttribute("href", string.Format("javascript:" + jsScript + "({0}, null)", i.ToString())); //pageUrl(i)

                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString PageRowCount(this HtmlHelper html
                              , int itemsPerPage
                              , string jsScript)
        {
            StringBuilder result = new StringBuilder();

            TagBuilder tagSelect = new TagBuilder("select");
            tagSelect.AddCssClass(" input-sm");
            tagSelect.MergeAttribute("name", "datatable_length");
            tagSelect.MergeAttribute("aria-controls", "datatable");
            tagSelect.MergeAttribute("onchange", "javascript:" + jsScript + "(1, this.value);");

            foreach (int item in PageParams.ItemCount)
            {
                string val = item.ToString();
                TagBuilder tagOpt = new TagBuilder("option");
                tagOpt.MergeAttribute("value", val);

                if (item == itemsPerPage)
                {
                    tagOpt.MergeAttribute("selected", "");
                }

                tagOpt.InnerHtml += val;
                tagSelect.InnerHtml += tagOpt.ToString();
            }

            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("dataTables_length");
            tag.MergeAttribute("id", "datatable_length");
            tag.InnerHtml += "Show ";
            tag.InnerHtml += tagSelect;
            tag.InnerHtml += " entries";

            return MvcHtmlString.Create(tag.ToString());

        }
    }
}