﻿using Catalog.Web.Models;
using DataModel.DBWrapper.Factories;
using DataModel.SupportClasses;
using DBData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Catalog.Web.Infrastructure
{
    public static class AppData
    {


        public static TableInfo TIM { get; set; }
        public static DBModel dbModel { get; set; }

        public static bool isUpdated;


        public static ProjectRepository ProjectRep;
        public static IspolniteliRepository IspolniteliRep;
        public static OboznachenieRepository OboznachenieRep;
        public static FirstUsageRepository PervichnoePrimenenieRep;
        public static InventarNumberRepository InventarNumberRep;
        public static LiteralRepository LiteralRep;

        public static DocumentsTree BFDT;
        public static DocumentsTree BADT;

        static AppData()
        {
            TIM = new TableInfo()
            {
                CurrentPage = 1,
                ItemsPerPage = PageParams.ItemCount[0],
                TotalItems = -1,
                ColNames = null
            };
            isUpdated = true;
        }

        public static void UpdeateRepository()
        {
            //ViewBag.ProjectRep = CommonData.ProjectTable.GetEntities();
            //ViewBag.OboznachenieRep = CommonData.OboznachenieTable.GetEntities();
            //ViewBag.FirstUsageRep = CommonData.FirstUsageTable.GetEntities();
            //ViewBag.LiteralRep = CommonData.LiteralTable.GetEntities();
            //ViewBag.IspolniteliRep = CommonData.IspolniteliTable.GetEntities();
            //ViewBag.InventarNumberRep = CommonData.InventarNumberTable.GetEntities();

            //ProjectRep = new ProjectRepository(CommonData.ProjectTable);
            //IspolniteliRep = new IspolniteliRepository(CommonData.IspolniteliTable);
            //OboznachenieRep = new OboznachenieRepository(CommonData.OboznachenieTable);
            //PervichnoePrimenenieRep = new FirstUsageRepository(CommonData.FirstUsageTable);
            //InventarNumberRep = new InventarNumberRepository(CommonData.InventarNumberTable);
            //LiteralRep = new LiteralRepository(CommonData.LiteralTable);
        }


        public static void InitApp()
        {
            string fn = System.Web.Hosting.HostingEnvironment.MapPath(
                WebConfigurationManager.AppSettings["DBFile"]
                );
            string db =
                "Data Source = " + fn;

            CommonData.LastFilterParam = "documents.inv_num";

            CommonData.dbSQL = DbProviderFactory.GetProvider(
            DataModel.DBWrapper.DBType.MSSQLCE,
            db
            //WebConfigurationManager.ConnectionStrings["DBContext"].ConnectionString
            );

            CommonData.MainTable = CommonData.dbSQL.GetMainTable();
            CommonData.MainTable.DBRequest = DBprocessing.SQLRequest();
            CommonData.MainTable.OrderedBy = CommonData.LastFilterParam;
            CommonData.MainTable.GetMainTable(IsOrdered: true);

            dbModel = new DBModel(CommonData.MainTable.MainDBdt);

            TIM.TotalItems = dbModel.Count;

            TIM.ColNames = CommonData.ColumnNames;

            TIM.Rows = dbModel.GetRows1(TIM.CurrentPage, TIM.ItemsPerPage);

            CommonData.ProjectTable = CommonData.dbSQL.GetProjectTable();
            CommonData.OboznachenieTable = CommonData.dbSQL.GetOboznachenieTable();
            CommonData.FirstUsageTable = CommonData.dbSQL.GetFirstUsageTable();
            CommonData.LiteralTable = CommonData.dbSQL.GetLiteralTable();
            CommonData.IspolniteliTable = CommonData.dbSQL.GetIspolniteliTable();
            CommonData.InventarNumberTable = CommonData.dbSQL.GetInventarNumberTable();
            CommonData.DependencyTable = CommonData.dbSQL.GetDependencyTable();

            ProjectRep = new ProjectRepository(CommonData.ProjectTable);
            IspolniteliRep = new IspolniteliRepository(CommonData.IspolniteliTable);
            OboznachenieRep = new OboznachenieRepository(CommonData.OboznachenieTable);
            PervichnoePrimenenieRep = new FirstUsageRepository(CommonData.FirstUsageTable);
            InventarNumberRep = new InventarNumberRepository(CommonData.InventarNumberTable);
            LiteralRep = new LiteralRepository(CommonData.LiteralTable);



            BFDT = new DocumentsTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetFullDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );

            BADT = new DocumentsTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetAsmDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );


        }
    }
}