﻿using Catalog.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace Catalog.Web.Infrastructure
{
    public static class NavBarHelper
    {

        public static MvcHtmlString SetBrand(this HtmlHelper html, string brand, string href)
        {
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("navbar-header");
            TagBuilder tagButton = new TagBuilder("button");
            tagButton.AddCssClass("navbar-toggle collapsed");
            tagButton.MergeAttribute("type", "button");
            tagButton.MergeAttribute("data-toggle", "collapse");
            tagButton.MergeAttribute("data-target", "#bs-example-navbar-collapse-1");
            tagButton.MergeAttribute("aria-expanded", "false");

            TagBuilder tagSpan1 = new TagBuilder("span");
            tagSpan1.AddCssClass("sr-only");
            tagSpan1.InnerHtml += "Toggle navigation";

            TagBuilder tagSpan2 = new TagBuilder("span");
            tagSpan2.AddCssClass("icon-bar");

            //Икона меню
            tagButton.InnerHtml += (tagSpan1.ToString() +
                tagSpan2.ToString() + tagSpan2.ToString() + tagSpan2);


            TagBuilder tagAHref = new TagBuilder("a");
            tagAHref.AddCssClass("navbar-brand");
            tagAHref.MergeAttribute("href", href);
            tagAHref.InnerHtml += brand;

            tag.InnerHtml += tagButton.ToString();
            tag.InnerHtml += tagAHref.ToString();

            return new MvcHtmlString(tag.ToString());
        }

        public static MvcHtmlString SetBrand1(this HtmlHelper html, string brand, string href)
        {
            TagBuilder tag = new TagBuilder("p");
            tag.AddCssClass("navbar-header");
            tag.InnerHtml += "dfjghdghg";

            return new MvcHtmlString(tag.ToString());
        }

        public static MvcHtmlString GetMenu(this HtmlHelper html, IEnumerable<NavBarMenuItem> items)
        {
            string res = string.Empty;

            foreach (var item in items)
            {
                if (item.IsDropDown)
                {
                    res += html.DropDownItem(item);
                }
                else
                {
                    res += html.SingleLevelItem(item);
                }
            }
            return new MvcHtmlString(res);
        }

        public static MvcHtmlString DropDownItem(this HtmlHelper html, NavBarMenuItem item)
        {
            TagBuilder tag = new TagBuilder("li");
            tag.AddCssClass("dropdown");
            if (!string.IsNullOrEmpty(item.UniqeClass))
            {
                tag.AddCssClass(item.UniqeClass);
            }

            TagBuilder tagAHref = new TagBuilder("a");
            tagAHref.AddCssClass("dropdown-toggle");
            tagAHref.MergeAttribute("href", item.href);
            tagAHref.MergeAttribute("id", item.HTMLId);
            tagAHref.MergeAttribute("data-toggle", "dropdown");
            tagAHref.MergeAttribute("role", "button");
            tagAHref.MergeAttribute("aria-haspopup", "true");
            tagAHref.MergeAttribute("aria-expanded", "false");
            if (item.HasGlyphicon)
            {
                TagBuilder tagSpanGlyph = new TagBuilder("span");
                tagSpanGlyph.MergeAttribute("aria-hidden", "true");
                tagSpanGlyph.AddCssClass(item.GlyphiconClass);
                
                tagAHref.InnerHtml += tagSpanGlyph.ToString();
            }
            tagAHref.InnerHtml += item.Name;
            TagBuilder tagSpan = new TagBuilder("span");
            tagSpan.AddCssClass("caret");
            tagAHref.InnerHtml += tagSpan.ToString();

            TagBuilder tagUL = new TagBuilder("ul");
            tagUL.AddCssClass("dropdown-menu");

            foreach (NavBarMenuItem subitem in item.SubItems)
            {
                {
                    if (subitem.Name == "separator")
                    { tagUL.InnerHtml += Separator(); }
                    else
                    { tagUL.InnerHtml += html.SingleLevelItem(subitem).ToString(); }
                }
            }

            tag.InnerHtml += tagAHref.ToString();
            tag.InnerHtml += tagUL.ToString();

            return new MvcHtmlString(tag.ToString());
        }

        public static MvcHtmlString SingleLevelItem(this HtmlHelper html, NavBarMenuItem item)
        {
            TagBuilder tag = new TagBuilder("li");

            TagBuilder tagAHref = new TagBuilder("a");

            tagAHref.AddCssClass(item.UniqeClass);
            tagAHref.MergeAttribute("href", item.href);
            tagAHref.MergeAttribute("id", item.HTMLId);
            if (item.HasGlyphicon)
            {
                TagBuilder tagSpan = new TagBuilder("span");
                tagSpan.MergeAttribute("aria-hidden", "true");
                
                tagSpan.AddCssClass(item.GlyphiconClass);
                tagAHref.InnerHtml += tagSpan.ToString();
            }

            if (string.IsNullOrEmpty(item.Target))
            {
                tagAHref.MergeAttribute("target", item.Target);
            }

            tagAHref.InnerHtml += item.Name;
            tag.InnerHtml += tagAHref.ToString();

            return new MvcHtmlString(tag.ToString());
        }

        public static MvcHtmlString Separator()
        {
            TagBuilder tag = new TagBuilder("li");
            tag.AddCssClass("divider");
            tag.MergeAttribute("role", "button");

            return new MvcHtmlString(tag.ToString());
        }
    }
}