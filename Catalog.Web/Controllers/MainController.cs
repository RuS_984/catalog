﻿using DataModel.DBWrapper.Factories;
using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using DataModel.SupportClasses;
using System.Data;
using DataModel.DBWrapper;
using Catalog.Web.Models;

using Catalog.Web.Infrastructure;
using Newtonsoft.Json;
using System.IO;
using DBData.Repository;

namespace Catalog.Web.Controllers
{
    
    public class MainController : Controller
    {
        LayoutModel lm;
        public MainController()
        {
            //-------------------------------------
            //CommonData.LastFilterParam = "documents.inv_num";

            //CommonData.dbSQL = DbProviderFactory.GetProvider(
            //DataModel.DBWrapper.DBType.MSSQLCE,
            //WebConfigurationManager.ConnectionStrings["DBContext"].ConnectionString);

            //CommonData.MainTable = CommonData.dbSQL.GetMainTable();
            //CommonData.MainTable.DBRequest = DBprocessing.SQLRequest();
            //CommonData.MainTable.OrderedBy = CommonData.LastFilterParam;
            //CommonData.MainTable.GetMainTable(IsOrdered: true);

            //dt = CommonData.MainTable.MainDBdt;
            //dbm = new DBModel(dt);

            //-------------------------------------------------------

            //tim = new TableInfo()
            //{
            //    CurrentPage = 1,
            //    ItemsPerPage = PageParams.ItemCount[2],
            //    TotalItems = dbm.Count,
            //    ColNames = CommonData.ColumnNames

            //};
            //tim.Rows  = dbm.GetRows1(tim.CurrentPage, tim.ItemsPerPage);
            //tim1 = new TableInfo();
        }

        // GET: Main
        //[Profile]
        public ActionResult Index()
        {
            int a = 10, b = 0;
            //int result = a / b;





            ViewBag.ColumnNames = CommonData.ColumnNames;

            lm = new LayoutModel();


            lm.nbm = GetNavBar();

            //lm.ti =tim;

            //lm.rows = dbm.GetRows1(lm.ti.CurrentPage, lm.ti.ItemsPerPage);

            //ViewBag.ColumnNames = CommonData.ColumnNames;
            ////return View(dbm.GetRows(0, 5));

            //if (AppData.TIM.TotalItems == -1)
            //{
            //    AppData.TIM.TotalItems = dbm.Count;
            //}

            //if (AppData.TIM.ColNames == null)
            //{
            //    AppData.TIM.ColNames = CommonData.ColumnNames;
            //}


            //if (AppData.TIM.Rows == null)
            //{
            //    AppData.TIM.Rows = dbm.GetRows1(AppData.TIM.CurrentPage, AppData.TIM.ItemsPerPage);
            //}

            //if (Request.IsAjaxRequest())
            //{
            //   // return View(lm);
            //}
            Guid g;

            g = Guid.NewGuid();

            HttpContext.Response.Cookies["id"].Value = g.GetHashCode().ToString();

            AppData.UpdeateRepository();
            return View(lm);
        }


        [MyException]
        public ActionResult MyError()
        {
            try
            {
                int a = 10, b = 0;
                int result = a / b;
            }
            catch (Exception ex)
            {
                throw;
            }
            //return Content(result.ToString());
            return null;

            
        }

        public PartialViewResult Table1(int? curPage, int? itemsPerPage)
        {
            if (curPage != null)
            {
                AppData.TIM.CurrentPage = (int)curPage;
            }

            if (itemsPerPage != null)
            {
                AppData.TIM.ItemsPerPage = (int)itemsPerPage;
            }

            AppData.TIM.Rows = AppData.dbModel.GetRows1(AppData.TIM.CurrentPage, AppData.TIM.ItemsPerPage);
            return PartialView("_Table1", AppData.TIM);
        }

        public NavBarModel GetNavBar()
        {

            //NavBarModel nbm = new NavBarModel();
            //nbm.Brand = WebConfigurationManager.AppSettings["Brand"];


            //nbm.NavBarMenuItemsLeft.AddRange(GetLeftNavBar());
            //nbm.NavBarMenuItemsRight.AddRange(GetRightNavBar());

            //string jsonString = JsonConvert.SerializeObject(nbm, Formatting.Indented);

            //NavBarModel nbm1 = new NavBarModel();
            //nbm1 = JsonConvert.DeserializeObject<NavBarModel>(jsonString);
            //nbm1.Brand = "JSON Copy";

            // @"e:\ПРОГРАММИРОВАНИЕ\CSharp\Catalog_NEW\Catalog\Catalog.Web\App_Data\menuJSON.txt";
            string fn = Server.MapPath( WebConfigurationManager.AppSettings["MenuPath"]);

            //using (StreamWriter file =
            //    System.IO.File.CreateText(fn))
            //{
            //    JsonSerializer serializer = new JsonSerializer();
            //    serializer.Formatting = Formatting.Indented;
            //    serializer.Serialize(file, nbm1);

            //}

            NavBarModel nbm2 = new NavBarModel();
            nbm2 = JsonConvert.DeserializeObject<NavBarModel>(System.IO.File.ReadAllText(fn));
            //nbm2.Brand = "Deserialized";

            return nbm2;// PartialViewResult(dbm.GetRows(0, 5));
            //return View();
        }

        public List<NavBarMenuItem> GetLeftNavBar()
        {

            List<NavBarMenuItem> nbm = new List<NavBarMenuItem>();

            #region single item Test
            NavBarMenuItem  nbi1 = new NavBarMenuItem("Создать");
            nbi1.href = "#";
            nbi1.UniqeClass = "RowAdd";
            nbi1.GlyphiconClass = "glyphicon-plus";
            nbi1.HTMLId = "RowAdd";
            #endregion

            #region New DropDown Test
            NavBarMenuItem  nbiDropDownMain = new NavBarMenuItem("Редактор справочника")
            {
                GlyphiconClass = "glyphicon-book",
                href = "#",
                UniqeClass = "Spravochniki",
                HTMLId = "Spravochniki",
                SubItems = new List<NavBarMenuItem>
                {
                    new NavBarMenuItem("Справочник 1")
                    {
                      GlyphiconClass = "glyphicon-th-list",
                      href = "#",
                      UniqeClass = "Spravochnik1",
                      HTMLId = "Spravochnik1"
                    },

                    //new NavBarMenuItem("separator"),

                    new NavBarMenuItem("Справочник 2")
                    {
                      GlyphiconClass = "glyphicon-th-list",
                      href = "#",
                      UniqeClass = "Spravochnik2",
                      HTMLId = "Spravochnik2"
                    },

                    new NavBarMenuItem("Справочник 3")
                    {
                      GlyphiconClass = "glyphicon-th-list",
                      href = "#",
                      UniqeClass = "Spravochnik3",
                      HTMLId = "Spravochnik3"
                    }
                    //,new NavBarMenuItem("separator")

                }
            };
            #endregion

            #region single item 2 Test
            NavBarMenuItem  nbi2 = new NavBarMenuItem("Дерево проектов")
            {
                GlyphiconClass = "glyphicon-list-alt",
                href = "#",
                UniqeClass = "ProjectTree",
                HTMLId = "ProjectTree"

            };
            #endregion

            nbm.Add(nbi1);
            nbm.Add(nbiDropDownMain);
            nbm.Add(nbi2);

            return nbm;// PartialViewResult(dbm.GetRows(0, 5));
            //return View();
        }

        public List<NavBarMenuItem> GetRightNavBar()
        {
            List<NavBarMenuItem> nbm = new List<NavBarMenuItem>();

            #region single item Test
            NavBarMenuItem  nbi1 = new NavBarMenuItem("single item Test");
            nbi1.href = "#";
            nbi1.GlyphiconClass = "glyphicon-remove";
            nbi1.UniqeClass = "single-item-Test";
            nbi1.HTMLId = "single-item-Test";
            #endregion

            #region New DropDown Test
            NavBarMenuItem  nbiDropDownMain = new NavBarMenuItem("Curent User Name")
            {
                GlyphiconClass = "glyphicon-user",
                href = "#",
                SubItems = new List<NavBarMenuItem>
                {
                    new NavBarMenuItem("Preferance")
                    {
                      GlyphiconClass = "glyphicon-cog",
                      href = "#"
                    },

                    new NavBarMenuItem("Logout")
                    {
                      GlyphiconClass = "glyphicon-log-out",
                      href = "#"
                    }



                }
            };
            #endregion

            nbm.Add(nbi1);
            nbm.Add(nbiDropDownMain);

            return nbm;// PartialViewResult(dbm.GetRows(0, 5));
            //return View();
        }
    }
}