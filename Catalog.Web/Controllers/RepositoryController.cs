﻿using Catalog.Web.Infrastructure;
using Catalog.Web.Models;
using DataModel.SupportClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Catalog.Web.Controllers
{
    public class RepositoryController : Controller
    {
        // GET: Repository
        [MyException]
        public ActionResult GetRepository(string RepositoryName)
        {
            RepositoryModel rm = new RepositoryModel();

            AppData.UpdeateRepository();

            switch (RepositoryName)
            {
                case "ProjectName":
                    rm.RepositoryName = "Названия проектов";
                    rm.RepositoryList = AppData.ProjectRep.GetList();
                    break;
                case "Literal":
                    rm.RepositoryName = "Литеры";
                    rm.RepositoryList = AppData.LiteralRep.GetList(); ;
                    break;
                case "Ispolniteli":
                    rm.RepositoryName = "Исполнители";
                    rm.RepositoryList = AppData.IspolniteliRep.GetList();
                    break;
                default:
                    break;
            }

            rm.RepositoryTable = RepositoryName;



            string msg = JsonConvert.SerializeObject(rm);

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }


        [MyException]
        public ActionResult AddToRepository(string RepositoryTable, string NewValue)
        {


            switch (RepositoryTable)
            {
                case "ProjectName":
                    AppData.ProjectRep.AddToDB(NewValue);
                    break;
                case "Literal":
                    AppData.LiteralRep.AddToDB(NewValue); ;
                    break;
                case "Ispolniteli":
                    AppData.IspolniteliRep.AddToDB(NewValue);
                    break;
                default:
                    break;
            }

            AppData.UpdeateRepository();

            string msg = JsonConvert.SerializeObject("done");

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }


        [MyException]
        public ActionResult DeleteFromRepository(string RepositoryTable, string DelValue)
        {

            
            switch (RepositoryTable)
            {
                case "ProjectName":
                    AppData.ProjectRep.DeleteFromDB(DelValue);
                    break;
                case "Literal":
                    AppData.LiteralRep.DeleteFromDB(DelValue);
                    break;
                case "Ispolniteli":
                    AppData.IspolniteliRep.DeleteFromDB(DelValue);
                    break;
                default:
                    break;
            }


            string msg = JsonConvert.SerializeObject("done");

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }

        [MyException]
        public ActionResult RenameInRepository(string RepositoryTable, string oldValue, string newValue)
        {

            List<string> res = new List<string>();

            switch (RepositoryTable)
            {
                case "ProjectName":
                    AppData.ProjectRep.RenameInDB(oldValue, newValue);
                    AppData.UpdeateRepository();
                    res = AppData.ProjectRep.GetList();
                    break;
                case "Literal":
                    AppData.LiteralRep.RenameInDB(oldValue, newValue);
                    AppData.UpdeateRepository();
                    res = AppData.LiteralRep.GetList();
                    break;
                case "Ispolniteli":
                    AppData.IspolniteliRep.RenameInDB(oldValue, newValue);
                    AppData.UpdeateRepository();
                    res = AppData.IspolniteliRep.GetList();
                    break;
                default:
                    break;
            }

            string msg = JsonConvert.SerializeObject(res);

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }



    }
}