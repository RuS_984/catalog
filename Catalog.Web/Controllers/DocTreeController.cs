﻿using Catalog.Web.Infrastructure;
using Catalog.Web.Models;
using DataModel.SupportClasses;
using DBData.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

using Catalog.Web.Infrastructure;

namespace Catalog.Web.Controllers
{
    public class DocTreeController : Controller
    {
        //TreeNode TN;

        // GET: Repository
        [MyException]
        public void Init()
        {
            //BuildNodeTree BFNT = new BuildNodeTree(
            //    DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
            //    DependencyTable: CommonData.DependencyTable.GetFullDependencyTable(),
            //    ProjectTable: CommonData.ProjectTable.GetProjectTable()
            //    );

            //BuildNodeTree BANT = new BuildNodeTree(
            //    DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
            //    DependencyTable: CommonData.DependencyTable.GetAsmDependencyTable(),
            //    ProjectTable: CommonData.ProjectTable.GetProjectTable()
            //);




            //TreeNode TN = (BFNT.BuildTree() as System.Web.UI.WebControls.TreeNode);
        }


        public DocTreeController()
        {
            Init();

        }


        public ActionResult GetTreeRoot()
        {
            AppData.BFDT.BuildTree();
            //Thread.Sleep(1000);
            string str = AppData.BFDT.GetRootTreeJSON();

            return new ContentResult
            {
                Content = str,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }

        public ActionResult GetChildNodes(string node)
        {
            AppData.BFDT.BuildTree();
            string str = AppData.BFDT.GetChildTreeJSON(NodeText: node);

            //Thread.Sleep(new Random().Next(0,2000));

            return new ContentResult
            {
                Content = str,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }


        public ActionResult GetAsmTreeRoot(string Dependency)
        {
            //Thread.Sleep(1000);
            AppData.BADT.BuildTree();
            AppData.BADT.CheckDependency(Dependency);

            string str = AppData.BADT.GetRootTreeJSON();
            string str1 = AppData.BADT.GetAsmDependentTreeJSON();
            //string str = "";

            return new ContentResult
            {
                Content = str1,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }

        public ActionResult GetAsmChildNodes(string node, string Dependency)
        {
            //AppData.BADT.BuildTree();
            string str = AppData.BADT.GetChildTreeJSON(NodeText: node);
            //string str = "";

            //Thread.Sleep(new Random().Next(0,2000));
                        
            return new ContentResult
            {
                Content = str,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }






    }
}