﻿using Catalog.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Catalog.Web.Models;
using DataModel.SupportClasses;
using DataModel.DBWrapper;

using Catalog.Web.Infrastructure;
using Newtonsoft.Json;

namespace Catalog.Web.Controllers
{
    [MyException]
    public class TableController : Controller
    {
        // TODO Решить проблему с исключениями

        // GET: Table
        //[Profile]
        [HttpGet]
        public PartialViewResult Table(int? curPage, int? itemsPerPage)
        {
            //Thread.Sleep(1000);
            AppData.InitApp();
            if (curPage != null)
            {
                AppData.TIM.CurrentPage = (int)curPage;
            }

            if (itemsPerPage != null)
            {
                AppData.TIM.ItemsPerPage = (int)itemsPerPage;
            }

            AppData.TIM.Rows = AppData.dbModel.GetRows1(AppData.TIM.CurrentPage, AppData.TIM.ItemsPerPage);
            return PartialView(AppData.TIM);
        }

        //-_----------------------        
        #region For Delete

        public void __UpdateRow(TableRow tr)
        {
            CommonData.MainTable.UpdateRecord(GetDBStruct(tr));
            AppData.InitApp();
            AppData.UpdeateRepository();
            //Table(1, null);
        }

        public PartialViewResult __RowUpdate(int rowIndex)
        {
            if (rowIndex > 0)
            {
                return PartialView("_CreateUpdate", AppData.dbModel.GetRow(rowIndex - 1));
            }
            else
            {
                TableRow tr = new TableRow();


                return PartialView("_CreateUpdate", tr);
            }
        }

        //[HandleError()]
        //[HandleError(ExceptionType = typeof( DivideByZeroException), View = "Error")]
        //[MyException()]
        public ActionResult __AddRow1(TableRow tr)
        {
            try
            {
                //int i = 5, j = 0;
                //int k = i/j;
                MainTableStruct mts = GetDBStruct(tr);
                CommonData.MainTable.CreateRecord(mts);
                AppData.InitApp();
                AppData.UpdeateRepository();

            }
            catch (Exception ex)
            {
                throw;
                //return  View("Error");
                //return RedirectToAction(actionName: "CheckError", controllerName: "Errorhandling", routeValues:ex);
                //return View("Error");

            }

            return null;
            //Table(1, null);
        }

        public PartialViewResult __RowUpdate2(int rowIndex)
        {


            if (rowIndex > 0)
            {
                return PartialView("_CreateUpdate2", AppData.dbModel.GetRowById(rowIndex));
            }
            else
            {
                TableRow tr = new TableRow();

                tr.Id = 0;
                return PartialView("_CreateUpdate2", tr);
            }
        }

        public PartialViewResult __RowUpdate3(int rowIndex)
        {

            if (rowIndex > 0)
            {
                return PartialView("_CreateUpdate3", AppData.dbModel.GetRowById(rowIndex));
            }
            else
            {
                TableRow tr = new TableRow();

                tr.Id = 0;
                return PartialView("_CreateUpdate3", tr);
            }
        }

        //[HttpPost]
        public void __UpdateRowJSON1(TableRow tr)
        {


            CommonData.MainTable.UpdateRecord(GetDBStruct(tr));
            AppData.InitApp();
            AppData.UpdeateRepository();
            Table(1, null);
        }

        #endregion
        //-_----------------------

        //{ForAdd: Array(1), ForDelete: Array(0), ForAddRoot: Array(1), forDeleteRoot: Array(0)}
        [MyException]                       
        public void AddUpdateDocJSON(TableRow tr, DocDependency dc)

        {
            if (tr.InventarnNum < 0)
            {
                return;
            }


            try
            {
                if (tr.Id <= 0)
                {
                    CommonData.MainTable.CreateRecord(GetDBStruct(tr));
                }
                else
                {
                    CommonData.MainTable.UpdateRecord(GetDBStruct(tr));
                }


                CommonData.DependencyTable.UpdateDB(
                    ForAdd: dc.ForAdd, ForAddRoot: dc.ForAddRoot,
                    ForDelete: dc.ForDelete, ForDeleteRoot: dc.ForDeleteRoot,
                    DocId:(tr.InventarnNum).ToString(), IsAsm:tr.IsAsm);


                if (dc.NewNum != dc.OldNum)
                {
                    CommonData.DependencyTable.ChangeDocumentID(
                        oldDocuments_id: dc.OldNum,
                        newDcuments_id: dc.NewNum,
                        isAsm: tr.IsAsm
                        );
                }





            }
            catch (Exception ex)
            {
                throw;
            }

            AppData.InitApp();
            AppData.UpdeateRepository();
            Table(null, null);
        }

        [MyException]
        public ActionResult AddUpdateRowJSON(int docId)
        {
            CRUDModel cm = new CRUDModel();

            cm.inventarnNum = AppData.InventarNumberRep.GetList();
            cm.projectName = AppData.ProjectRep.GetList();
            cm.oboznachenie = AppData.OboznachenieRep.GetList();
            cm.literal = AppData.LiteralRep.GetList();
            cm.ispolniteli = AppData.IspolniteliRep.GetList();

            if (docId > 0)
            {
                cm.tableRow = AppData.dbModel.GetRowById(docId);
                cm.operation = "Обновить";
                cm.headerText = "Редактирование записи №" + cm.tableRow.InventarnNum;
            }
            else
            {
                cm.tableRow = new TableRow();
                cm.tableRow.Id = -1;
                cm.tableRow.InventarnNum = CommonData.MainTable.MaxInvNum+1;
                cm.operation = "Создать";
                cm.headerText = "Создание новой записи";
            }

            string msg = JsonConvert.SerializeObject(cm);

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };

        }

        [MyException]
        public ActionResult DeleteRowJSON(int docId)
        {
            TableRow tr;

            tr = AppData.dbModel.GetRowById(docId);

            int res = CommonData.MainTable.DeleteRecordByRow(
                    RowId: tr.Id,
                    DocumentId: tr.InventarnNum
                    );

            string msg = JsonConvert.SerializeObject(res);

            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                ContentEncoding = System.Text.Encoding.UTF8
            };
        }

        private MainTableStruct GetDBStruct(TableRow tr)
        {
            MainTableStruct mdbs = new MainTableStruct();
            mdbs.Clear();

            mdbs.InventarnNum = (tr.InventarnNum > 0) ? tr.InventarnNum : 0;

            mdbs.ProjectID = AppData.ProjectRep[tr.ProjectName].Id;

            mdbs.Oboznachenie = tr.Oboznachenie;

            mdbs.Naimenovanie = tr.Naimenovanie;

            mdbs.Pervichoepreminenie = (tr.Pervichoepreminenie != null) ? tr.Pervichoepreminenie : string.Empty;

            mdbs.Literal = AppData.LiteralRep[tr.Literal].Id;

            mdbs.Date = tr.Date;

            mdbs.IsAsm = Convert.ToBoolean(tr.IsAsm);

            mdbs.Razrabotal = AppData.IspolniteliRep[tr.Razrabotal].Id;

            mdbs.Proveril = AppData.IspolniteliRep[tr.Proveril].Id;

            mdbs.Tehcontrol = AppData.IspolniteliRep[tr.Tehcontrol].Id;

            mdbs.Normokontrol = AppData.IspolniteliRep[tr.Normokontrol].Id;

            mdbs.Utverdil = AppData.IspolniteliRep[tr.Utverdil].Id;

            mdbs.Id = Convert.ToInt32(tr.Id);

            return mdbs;
        }
    }
}