﻿using System.ComponentModel;
using NUnit.Framework;

using DataModel.SupportClasses;


namespace DataModel.UnitTests.SupportClasses
{
    [TestFixture]
    class ParseFileNameTests
    {

        [Test]
        public void ParseFileName01_ShortUnknownFileName_ReturnDefaultFileNameStructWithExtension()
        {
            string Name = "АБВГ.dvdgshj.abc";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.dvdgshj.abc",
                Naimenovanie = "none",
                DocType = DocumentType.None
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn, fn_test);


        }

        [Test]
        public void ParseFileName02_CorrectPartFileName_ReturnCerrectFileNameStruct()
        {
            string Name = "АБВГ.111111.002 Заглушка.m3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Part
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);

            
            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName03_CorrectPartFileNameWithIspoln_ReturnCerrectFileNameStruct()
        {
            string Name = "АБВГ.111111.002-01 Заглушка.m3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002-01",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Part
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName04_ShortPartFileName_ReturnDefaultFileNameStructWithoutExtension()
        {
            string Name = "АБВГ.dvdgshj.m3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.dvdgshj",
                Naimenovanie = "none",
                DocType = DocumentType.Part
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn); 
        }


        [Test]
        public void ParseFileName05_CorrectAsmWithOutSBFileName_ReturnCerrectAsmFileNameStruct()
        {
            string Name = "АБВГ.111111.002 Заглушка.a3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002 СБ",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Assemble
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName06_CorrectAsmWithOutSBFileNameWithIspoln_ReturnCerrectAsmFileNameStruct()
        {
            string Name = "АБВГ.111111.002-01 Заглушка.a3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002-01 СБ",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Assemble
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }


        [Test]
        public void ParseFileName07_CorrectAsmWith_SB_FileName_ReturnCerrectAsmFileNameStruct()
        {
            string Name = "АБВГ.111111.002 СБ Заглушка.a3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002 СБ",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Assemble
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName08_CorrectAsmWith_SB_FileNameWithIspoln_ReturnCerrectAsmFileNameStruct()
        {
            string Name = "АБВГ.111111.002-01 СБ Заглушка.a3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002-01 СБ",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Assemble
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName09_CorrectAsmWith_SBFileName_ReturnCerrectAsmFileNameStruct()
        {
            string Name = "АБВГ.111111.002 СБ Заглушка.a3d";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002 СБ",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Assemble
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName10_CorrectSpwWithOutSPFileName_ReturnCerrectSpwFileNameStruct()
        {
            string Name = "АБВГ.123456.002 Заглушка.spw";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.123456.002 СП",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Specification
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }

        [Test]
        public void ParseFileName11_CorrectSpwWithOutSPFileNameWithIspoln_ReturnCerrectSpwFileNameStruct()
        {
            string Name = "АБВГ.111111.002-01 Заглушка.spw";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002-01 СП",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Specification
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }


        [Test]
        public void ParseFileName12_CorrectSpwWith_SP_FileName_ReturnCerrectSpwFileNameStruct()
        {
            string Name = "АБВГ.111111.002 СП Заглушка.spw";
            FileName fn_test = new FileName()
            {
                Oboznachenie = "АБВГ.111111.002 СП",
                Naimenovanie = "Заглушка",
                DocType = DocumentType.Specification
            };
            FileName fn;

            fn = ParseFileName.ParseName(Name);


            Assert.AreEqual(fn_test, fn);
        }










    }


}
