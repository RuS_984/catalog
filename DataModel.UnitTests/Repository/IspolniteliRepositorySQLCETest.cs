﻿using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Collections.Generic;

using System.Linq;

using NUnit.Framework;

using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;
using DataModel.DBWrapper.Factories;
using DataModel.DBWrapper;
using DataModel.DBWrapper.Providers.SqlCeDb;

using DBData.Entities;
using System;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using DataModel.UnitTests.DBWrapper.SqlCeDbTests;
using DBData.Repository;
using static DataModel.SupportClasses.ExceptionHandler;

namespace DataModel.UnitTests.Repository
{
    [TestFixture]
    public class IspolniteliRepositorySQLCETest
    {
        string testData = "IspTestName";
        string testDataRename = "IspNewTestName";

        string testDataForDuplicate = "IspDataForDupl";

        DatabaseProvider dbSQL;
        IspolniteliTableAbstract ITA;
        IspolniteliRepository ispRep;
        int _id1;


        [OneTimeSetUp]
        public void BeforeTestSuit()
        {
            //Получение провайдера из фабрики и проверка типа провайдера
            dbSQL = DbProviderFactory.GetProvider(
                DataModel.DBWrapper.DBType.MSSQLCE,
                SQLCEParams.SQLCEConStirng
                );
            Assert.AreEqual(
                expected: typeof(SqlCeDbProvider), 
                actual: dbSQL.GetType(),
                message: "Not Correct Provider expected:" + typeof(SqlCeDbProvider)
                + ", actual:" + dbSQL.GetType());


            //Получение таблицы из фабрики и проверка типа таблицы
            ITA = dbSQL.GetIspolniteliTable();
            Assert.AreEqual(
                expected: typeof(IspolniteliTableSqlCe), 
                actual: ITA.GetType(),
                message: "Not Correct Table expected:" + typeof(IspolniteliTableSqlCe).Name
                + " actual:" + ITA.GetType().Name);

            
            //Подгатовка таблиц: удаление данных таблиц и сброс счетчиков
            //LTA.ExecuteBatchCommand(SQLCEParams.DeleteAllFromTable("Literal"));

            ispRep = new IspolniteliRepository(ITA);


            ispRep.DeleteFromDB(testData);
            ispRep.DeleteFromDB(testDataRename);
            ispRep.DeleteFromDB(testDataForDuplicate);

            //Проверка что таблица пуста
            //int i =  litRep.Count;
            //Assert.AreEqual(expected: 0, actual: i, message: "Literal Repository is not empty");

        }




        [Test, Order(2)]
        public void IspolniteliRepository01_AddNewData_Successful()
        {

            //Получение количества записей перед добавлением строки
            ispRep.GetEntities();




            int numBeforeAdd = ispRep.Count;

            //Добавления строки
            ispRep.AddToDB(testData);

            //Получение данных из таблицы
            ispRep.GetEntities();
            int numAfterAdd =  ispRep.Count;

            //Проверка факта добавления строки изходя из увеличения количества записей
            Assert.AreEqual(expected: 1, actual: numAfterAdd - numBeforeAdd, message: "Row was not added");


            //Проверка, что данные занесены корректно
            bool RowContains = ispRep.ContainsName(testData); //res.Count(p => p.Name.ToUpper() == testData.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row data Error");

            _id1 = 0;// res[0].Id;


        }




        [Test, Order(3), Obsolete]
        public void IspolniteliRepository02_AddDuplicateData_CatachSqlCeException()
        {
            //!!ОШИБКА! Результат Сообщение:	
            //DataModel.SupportClasses.ExceptionHandler+SQLEntityException : Ошибка в приложении.
            //Assert.That(LTA.AddToDB(testData), Throws.TypeOf<SQLEntityException>()
            //.With.Property("OperationType").EqualTo(DBOperationType.AddToDB)
            //);

            //litRep.GetEntities();
            //bool RowContains = litRep.ContainsName(testData); //res.Count(p => p.Name.ToUpper() == testData.ToUpper()) > 0;
            //Assert.AreEqual(expected: true, actual: RowContains, message: "Row data Error");


            var exception = Assert.Throws<SQLEntityException>(() => ispRep.AddToDB(testData));
            
            Assert.AreEqual(exception.OperationType, DBOperationType.AddToDB);

        }



        [Test, Order(4)]
        public void IspolniteliRepository03_RenameInDB_Successful()
        {
            //Переименование testData в testDataRename
            ispRep.RenameInDB(oldValue: testData, newValue: testDataRename);

            

            //Проверка изменения текста записи
            bool RowContains = ispRep.ContainsName(testDataRename);
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row was not renamed");

        }


        [Test, Order(5)]
        public void IspolniteliRepository04_RenameInDBDuplicate_CatachSqlCeException()
        {
            //Добавления строки с текстом testDataForDuplicate
            ispRep.AddToDB(testDataForDuplicate);

            //Проверка добавления строки с текстом testDataForDuplicate
            //List<Literal> res = LTA.GetEntities();
            bool RowContains = ispRep.ContainsName(testDataForDuplicate);
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row was not added");

            //Получения ID новой строки
            //int dupid = (from n in res where (n.Name.ToUpper() == testDataForDuplicate.ToUpper()) select n.Id).First();

            //Плпытка переименования строки и проверко получения исключения
            //Assert.Throws<SQLEntityException>(() =>
            //        LTA.RenameInDB(oldValue: testDataForDuplicate, newValue: testDataRename, oldValueId: dupid)
            //  );
            var exception = Assert.Throws<SQLEntityException>(() => ispRep.RenameInDB(oldValue: testDataForDuplicate,
                newValue: testDataRename));

            Assert.AreEqual(exception.OperationType, DBOperationType.RenameInDB);

        }



        [Test, Order(6)]
        public void IspolniteliRepository05_DeleteFromDB_Successful()
        {

            //Получение количества строк в таблице
            ispRep.GetEntities();
            int i = ispRep.Count;

            //Проверка наличия удаляемой строки
            bool RowContains = ispRep.ContainsName(testDataRename);//res.Count(p => p.Name.ToUpper() == testDataRename.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row not found");

            //Удаление строки
            ispRep.DeleteFromDB(testDataRename);

            //Проверка удаления строки (количество записей уменьшилось на 1)
            //res = LTA.GetEntities();
            Assert.AreEqual(expected: i - 1, actual: ispRep.Count, message: "Row was not deleted");

            //Проверка, что удалена строка с нужным тестом
            RowContains = ispRep.ContainsName(testDataRename);//res.Count(p => p.Name.ToUpper() == testDataRename.ToUpper()) > 0;
            Assert.False(RowContains);



        }



        [OneTimeTearDown]
        public void AfterTestSuit()
        {
            //List<Literal> res;

            //ispRep.DeleteFromDB(testData);
            //ispRep.DeleteFromDB(testDataRename);
            //ispRep.DeleteFromDB(testDataForDuplicate);





            //Удаление данных таблиц и сброс счетчиков
            //LTA.ExecuteBatchCommand(SQLCEParams.DeleteAllFromTable("Literal"));

            //Проверка что таблица пуста
            //litRep.GetEntities();

            //Assert.AreEqual(expected: 0, actual: litRep.Count, message: "Table is not empty");
        }
    }




}
