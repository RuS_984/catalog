﻿using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Collections.Generic;

using System.Linq;

using NUnit.Framework;

using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;
using DataModel.DBWrapper.Factories;
using DataModel.DBWrapper;
using DataModel.DBWrapper.Providers.SqlCeDb;

using DBData.Entities;
using System;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using static DataModel.SupportClasses.ExceptionHandler;

namespace DataModel.UnitTests.DBWrapper.SqlCeDbTests
{

    [TestFixture]
    class LiteralTableSqlCeTest
    {
        string testData = "TestName";
        string testDataRename = "NewTestName";

        string testDataForDuplicate = "testDataForDuplicate";

        DatabaseProvider dbSQL;
        LiteralTableAbstract LTA;
        List<Literal> LitLst;
        //SqlCeDbProvider sqlCeProvider;
        int _id;


        [OneTimeSetUp]
        public void BeforeTestSuit()
        {
            //Получение провайдера из фабрики и проверка типа провайдера
            dbSQL = DbProviderFactory.GetProvider(
                DataModel.DBWrapper.DBType.MSSQLCE,
                SQLCEParams.SQLCEConStirng
                );
            Assert.AreEqual(expected: typeof(SqlCeDbProvider), actual: dbSQL.GetType(), 
                message: "Not Correct Provider expected:" + typeof(SqlCeDbProvider)
                + ", actual:" + dbSQL.GetType());


            //Получение таблицы из фабрики и проверка типа таблицы
            LTA = dbSQL.GetLiteralTable();
            Assert.AreEqual(expected: typeof(LiteralTableSqlCe), actual: LTA.GetType(),
                message: "Not Correct Table expected:"+ typeof(LiteralTableSqlCe).Name 
                + " actual:" + LTA.GetType().Name);

            
            //Подгатовка таблиц: удаление данных таблиц и сброс счетчиков
            //LTA.ExecuteBatchCommand(SQLCEParams.DeleteAllFromTable("Literal"));

            //Проверка что таблица пуста
            //int i =  LTA.GetEntities().Count;
            //Assert.AreEqual(expected: 0, actual: i, message: "Literal table is not empty");

        }


        //[Test, Order(1)]
        //public void LiteralTableSqlCe_TableConnection_Successful()
        //{

        //    List<Literal> res =  LTA.GetEntities();




        //}

        [Test, Order(2)]
        public void LiteralTableSqlCe_AddNewData_Successful()
        {

            LitLst = new List<Literal>();
            LitLst = LTA.GetEntities();

            //if (LitLst.Contains(testData))

            //Получение количества записей перед добавлением строки
            int numBeforeAdd =  LTA.GetEntities().Count;

            //Добавления строки
            LTA.AddToDB(testData);

            //Получение данных из таблицы
            List<Literal> res = LTA.GetEntities();

            //Проверка факта добавления строки изходя из увеличения количества записей
            Assert.AreEqual(expected: 1, actual: res.Count - numBeforeAdd, message: "Row was not added");


            //Проверка, что данные занесены корректно
            bool RowContains = res.Count(p => p.Name.ToUpper() == testData.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row data Error");

            _id = res[0].Id;


        }


        [Test, Order(3)]
        
        //[ExpectedException(typeof(InvalidOperationException)]
        public void LiteralTableSqlCe_AddDuplicateData_CatachSqlCeException()
        {
            //!!ОШИБКА! Результат Сообщение:	
            //DataModel.SupportClasses.ExceptionHandler+SQLEntityException : Ошибка в приложении.
            //Assert.That(LTA.AddToDB(testData), Throws.TypeOf<SQLEntityException>()
            //.With.Property("OperationType").EqualTo(DBOperationType.AddToDB)
            //);

            var exception = Assert.Throws<SQLEntityException>(() => LTA.AddToDB(testData));
            Assert.AreEqual(exception.OperationType, DBOperationType.AddToDB);

        }




        [Test, Order(4)]
        public void LiteralTableSqlCe_RenameInDB_Successful()
        {
            //Переименование testData в testDataRename
            LTA.RenameInDB(oldValue: testData, newValue: testDataRename, oldValueId: _id);

            //Получение данных из таблицы
            List<Literal> res = LTA.GetEntities();

            //Проверка изменения текста записи
            bool RowContains = res.Count(p => p.Name.ToUpper() == testDataRename.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row was not renamed");

        }


        [Test, Order(5)]
        public void LiteralTableSqlCe_RenameInDBDuplicate_CatachSqlCeException()
        {
            //Добавления строки с текстом testDataForDuplicate
            LTA.AddToDB(testDataForDuplicate);

            //Проверка добавления строки с текстом testDataForDuplicate
            List<Literal> res = LTA.GetEntities();
            bool RowContains = res.Count(p => p.Name.ToUpper() == testDataForDuplicate.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row was not added");

            //Получения ID новой строки
            int dupid = (from n in res where (n.Name.ToUpper() == testDataForDuplicate.ToUpper()) select n.Id).First();

            //Плпытка переименования строки и проверко получения исключения
            Assert.Throws<SQLEntityException>(() =>
                    LTA.RenameInDB(oldValue: testDataForDuplicate, newValue: testDataRename, oldValueId: dupid)
              );


        }



        [Test, Order(6)]
        public void LiteralTableSqlCe_DeleteFromDB_Successful()
        {

            //Получение количества строк в таблице
            List<Literal> res = LTA.GetEntities();
            int i = res.Count;

            //Проверка наличия удаляемой строки
            bool RowContains = res.Count(p => p.Name.ToUpper() == testDataRename.ToUpper()) > 0;
            Assert.AreEqual(expected: true, actual: RowContains, message: "Row not found");

            //Удаление строки
            LTA.DeleteFromDB(testDataRename,_id);

            //Проверка удаления строки (количество записей уменьшилось на 1)
            res = LTA.GetEntities();
            Assert.AreEqual(expected: i-1, actual: res.Count, message: "Row was not deleted");

            //Проверка, что удалена строка с нужным тестом
            RowContains = res.Count(p => p.Name.ToUpper() == testDataRename.ToUpper()) > 0;
            Assert.False(RowContains);



        }




        [OneTimeTearDown]
        public void AfterTestSuit()
        {
            List<Literal> res;

            //Удаление данных таблиц и сброс счетчиков
            LTA.ExecuteBatchCommand(SQLCEParams.DeleteAllFromTable(SQLCEParams.TablesName.Literal));

            //Проверка что таблица пуста
            res = LTA.GetEntities();
            Assert.AreEqual(expected: 0, actual: res.Count, message: "Table is not empty");
        }

    }



}
