﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.UnitTests.DBWrapper.SqlCeDbTests
{
    static class SQLCEParams
    {
        static public string SQLCEConStirng =
            @"Data Source = e:\__PROGRAMMING\CSharp\Catalog_NEW\Catalog\DataModel.UnitTests\TestDB\Catalog DB Test.sdf";

        static public string DeleteAllFromTable2(string TableName)
        {
            return @"delete literals
                     go
                     ALTER TABLE literals ALTER COLUMN literal_id IDENTITY (1,1)
                     go";
        }

        static public string[] DeleteAllFromTable(TablesName TableName)
        {
            

            string[] Dependency = {
                        "delete dependency",
                        "ALTER TABLE dependency ALTER COLUMN ID IDENTITY (1,1)"
            };


            string[] Docs = {
                        "delete docs",
                        "ALTER TABLE docs ALTER COLUMN ID IDENTITY (1,1)"
            };


            string[] Documents = {
                        "delete documents",
                        "ALTER TABLE documents ALTER COLUMN id IDENTITY (1,1)"
            };


            string[] Ispolniteli = {
                        "delete ispolniteli",
                        "ALTER TABLE ispolniteli ALTER COLUMN ispoln_id IDENTITY (1,1)"
            };


            string[] Literal = {
                        "delete literals",
                        "ALTER TABLE literals ALTER COLUMN literal_id IDENTITY (1,1)"
            };



            string[] Project = {
                        "delete project",
                        "ALTER TABLE project ALTER COLUMN project_id IDENTITY (1,1)"
            };




            switch (TableName)
            {
                case TablesName.Dependency:
                    return Dependency;
                    break;

                case TablesName.Docs:
                    return Docs;
                    break;

                case TablesName.Documents:
                    return Documents;
                    break;


                case TablesName.Ispolniteli:
                    return Ispolniteli;
                    break;

                case TablesName.Literal:
                    return Literal;
                    break;


                case TablesName.Project:
                    return Project;
                    break;


                case TablesName.All:
                    IEnumerable<string> lst = 
                    Dependency.Concat(Documents).Concat(Docs)
                       .Concat(Ispolniteli).Concat(Literal).Concat(Project);
                    return lst.ToArray();
                    break;
                default:
                    return new string[] { };
                    break;
            }


        }

        public enum TablesName
        {
            Dependency,
            Docs,
            Documents,
            Ispolniteli,
            Literal,
            Project,
            All


        }
    }
}
