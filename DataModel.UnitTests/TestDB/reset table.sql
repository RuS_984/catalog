﻿delete dependency
go
ALTER TABLE dependency ALTER COLUMN ID IDENTITY (1,1)
go

delete docs
go
ALTER TABLE docs ALTER COLUMN ID IDENTITY (1,1)
go
						
delete documents
go
ALTER TABLE documents ALTER COLUMN id IDENTITY (1,1)
go

delete ispolniteli
go
ALTER TABLE ispolniteli ALTER COLUMN ispoln_id IDENTITY (1,1)
go
            
delete literals
go
ALTER TABLE literals ALTER COLUMN literal_id IDENTITY (1,1)
go
            
delete project
go
ALTER TABLE project ALTER COLUMN project_id IDENTITY (1,1)
go


