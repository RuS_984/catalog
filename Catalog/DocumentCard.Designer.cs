﻿namespace Catalog
{
    partial class DocumentCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Edit_tSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.utverdilVal_lbl = new System.Windows.Forms.Label();
            this.normoKontrVal_lbl = new System.Windows.Forms.Label();
            this.utverdil_lbl = new System.Windows.Forms.Label();
            this.tehKontrVal_lbl = new System.Windows.Forms.Label();
            this.normoKontr_lbl = new System.Windows.Forms.Label();
            this.proverilVal_lbl = new System.Windows.Forms.Label();
            this.tehKontr_lbl = new System.Windows.Forms.Label();
            this.razrabVal_lbl = new System.Windows.Forms.Label();
            this.proveril_lbl = new System.Windows.Forms.Label();
            this.dateTimeVal_lbl = new System.Windows.Forms.Label();
            this.razrab_lbl = new System.Windows.Forms.Label();
            this.naimenVal_lbl = new System.Windows.Forms.Label();
            this.dateTime_lbl = new System.Windows.Forms.Label();
            this.invNum_lbl = new System.Windows.Forms.Label();
            this.pervPrimenVal_lbl = new System.Windows.Forms.Label();
            this.naimen_lbl = new System.Windows.Forms.Label();
            this.pervPrimen_lbl = new System.Windows.Forms.Label();
            this.oboznVal_lbl = new System.Windows.Forms.Label();
            this.prjNameVal_lbl = new System.Windows.Forms.Label();
            this.obozn_lbl = new System.Windows.Forms.Label();
            this.invNumVal_lbl = new System.Windows.Forms.Label();
            this.prjName_lbl = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(207, 440);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Edit_tSMI});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(155, 26);
            // 
            // Edit_tSMI
            // 
            this.Edit_tSMI.Name = "Edit_tSMI";
            this.Edit_tSMI.Size = new System.Drawing.Size(154, 22);
            this.Edit_tSMI.Text = "Редактировать";
            this.Edit_tSMI.Click += new System.EventHandler(this.Edit_tSMI_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listBox1);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(665, 442);
            this.splitContainer1.SplitterDistance = 209;
            this.splitContainer1.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(3, 276);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(207, 84);
            this.listBox1.TabIndex = 32;
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 44);
            this.label1.TabIndex = 31;
            this.label1.Text = "Прикрепленные файлы";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.utverdilVal_lbl, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.normoKontrVal_lbl, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.utverdil_lbl, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.tehKontrVal_lbl, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.normoKontr_lbl, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.proverilVal_lbl, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tehKontr_lbl, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.razrabVal_lbl, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.proveril_lbl, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.dateTimeVal_lbl, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.razrab_lbl, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.naimenVal_lbl, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateTime_lbl, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.invNum_lbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pervPrimenVal_lbl, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.naimen_lbl, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pervPrimen_lbl, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.oboznVal_lbl, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.prjNameVal_lbl, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.obozn_lbl, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.invNumVal_lbl, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.prjName_lbl, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(397, 227);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // utverdilVal_lbl
            // 
            this.utverdilVal_lbl.AutoSize = true;
            this.utverdilVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.utverdilVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.utverdilVal_lbl.Location = new System.Drawing.Point(223, 200);
            this.utverdilVal_lbl.Name = "utverdilVal_lbl";
            this.utverdilVal_lbl.Size = new System.Drawing.Size(0, 27);
            this.utverdilVal_lbl.TabIndex = 11;
            // 
            // normoKontrVal_lbl
            // 
            this.normoKontrVal_lbl.AutoSize = true;
            this.normoKontrVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.normoKontrVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.normoKontrVal_lbl.Location = new System.Drawing.Point(223, 180);
            this.normoKontrVal_lbl.Name = "normoKontrVal_lbl";
            this.normoKontrVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.normoKontrVal_lbl.TabIndex = 13;
            // 
            // utverdil_lbl
            // 
            this.utverdil_lbl.AutoSize = true;
            this.utverdil_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.utverdil_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.utverdil_lbl.Location = new System.Drawing.Point(120, 200);
            this.utverdil_lbl.Name = "utverdil_lbl";
            this.utverdil_lbl.Size = new System.Drawing.Size(97, 27);
            this.utverdil_lbl.TabIndex = 10;
            this.utverdil_lbl.Text = "Утвердил:";
            // 
            // tehKontrVal_lbl
            // 
            this.tehKontrVal_lbl.AutoSize = true;
            this.tehKontrVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.tehKontrVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tehKontrVal_lbl.Location = new System.Drawing.Point(223, 160);
            this.tehKontrVal_lbl.Name = "tehKontrVal_lbl";
            this.tehKontrVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.tehKontrVal_lbl.TabIndex = 13;
            // 
            // normoKontr_lbl
            // 
            this.normoKontr_lbl.AutoSize = true;
            this.normoKontr_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.normoKontr_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.normoKontr_lbl.Location = new System.Drawing.Point(69, 180);
            this.normoKontr_lbl.Name = "normoKontr_lbl";
            this.normoKontr_lbl.Size = new System.Drawing.Size(148, 20);
            this.normoKontr_lbl.TabIndex = 12;
            this.normoKontr_lbl.Text = "Нормоконтроль:";
            // 
            // proverilVal_lbl
            // 
            this.proverilVal_lbl.AutoSize = true;
            this.proverilVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.proverilVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proverilVal_lbl.Location = new System.Drawing.Point(223, 140);
            this.proverilVal_lbl.Name = "proverilVal_lbl";
            this.proverilVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.proverilVal_lbl.TabIndex = 13;
            // 
            // tehKontr_lbl
            // 
            this.tehKontr_lbl.AutoSize = true;
            this.tehKontr_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.tehKontr_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tehKontr_lbl.Location = new System.Drawing.Point(92, 160);
            this.tehKontr_lbl.Name = "tehKontr_lbl";
            this.tehKontr_lbl.Size = new System.Drawing.Size(125, 20);
            this.tehKontr_lbl.TabIndex = 12;
            this.tehKontr_lbl.Text = "Тех.контроль:";
            // 
            // razrabVal_lbl
            // 
            this.razrabVal_lbl.AutoSize = true;
            this.razrabVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.razrabVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.razrabVal_lbl.Location = new System.Drawing.Point(223, 120);
            this.razrabVal_lbl.Name = "razrabVal_lbl";
            this.razrabVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.razrabVal_lbl.TabIndex = 13;
            // 
            // proveril_lbl
            // 
            this.proveril_lbl.AutoSize = true;
            this.proveril_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.proveril_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proveril_lbl.Location = new System.Drawing.Point(120, 140);
            this.proveril_lbl.Name = "proveril_lbl";
            this.proveril_lbl.Size = new System.Drawing.Size(97, 20);
            this.proveril_lbl.TabIndex = 12;
            this.proveril_lbl.Text = "Проверил:";
            // 
            // dateTimeVal_lbl
            // 
            this.dateTimeVal_lbl.AutoSize = true;
            this.dateTimeVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimeVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimeVal_lbl.Location = new System.Drawing.Point(223, 100);
            this.dateTimeVal_lbl.Name = "dateTimeVal_lbl";
            this.dateTimeVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.dateTimeVal_lbl.TabIndex = 13;
            // 
            // razrab_lbl
            // 
            this.razrab_lbl.AutoSize = true;
            this.razrab_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.razrab_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.razrab_lbl.Location = new System.Drawing.Point(103, 120);
            this.razrab_lbl.Name = "razrab_lbl";
            this.razrab_lbl.Size = new System.Drawing.Size(114, 20);
            this.razrab_lbl.TabIndex = 12;
            this.razrab_lbl.Text = "Разработал:";
            // 
            // naimenVal_lbl
            // 
            this.naimenVal_lbl.AutoSize = true;
            this.naimenVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.naimenVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.naimenVal_lbl.Location = new System.Drawing.Point(223, 60);
            this.naimenVal_lbl.Name = "naimenVal_lbl";
            this.naimenVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.naimenVal_lbl.TabIndex = 9;
            // 
            // dateTime_lbl
            // 
            this.dateTime_lbl.AutoSize = true;
            this.dateTime_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.dateTime_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTime_lbl.Location = new System.Drawing.Point(54, 100);
            this.dateTime_lbl.Name = "dateTime_lbl";
            this.dateTime_lbl.Size = new System.Drawing.Size(163, 20);
            this.dateTime_lbl.TabIndex = 12;
            this.dateTime_lbl.Text = "Дата подписания:";
            // 
            // invNum_lbl
            // 
            this.invNum_lbl.AutoSize = true;
            this.invNum_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.invNum_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.invNum_lbl.Location = new System.Drawing.Point(50, 0);
            this.invNum_lbl.Name = "invNum_lbl";
            this.invNum_lbl.Size = new System.Drawing.Size(167, 20);
            this.invNum_lbl.TabIndex = 6;
            this.invNum_lbl.Text = "Инвентарный номер:";
            // 
            // pervPrimenVal_lbl
            // 
            this.pervPrimenVal_lbl.AutoSize = true;
            this.pervPrimenVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.pervPrimenVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pervPrimenVal_lbl.Location = new System.Drawing.Point(223, 80);
            this.pervPrimenVal_lbl.Name = "pervPrimenVal_lbl";
            this.pervPrimenVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.pervPrimenVal_lbl.TabIndex = 9;
            // 
            // naimen_lbl
            // 
            this.naimen_lbl.AutoSize = true;
            this.naimen_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.naimen_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.naimen_lbl.Location = new System.Drawing.Point(91, 60);
            this.naimen_lbl.Name = "naimen_lbl";
            this.naimen_lbl.Size = new System.Drawing.Size(126, 20);
            this.naimen_lbl.TabIndex = 8;
            this.naimen_lbl.Text = "Наименование:";
            // 
            // pervPrimen_lbl
            // 
            this.pervPrimen_lbl.AutoSize = true;
            this.pervPrimen_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.pervPrimen_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pervPrimen_lbl.Location = new System.Drawing.Point(3, 80);
            this.pervPrimen_lbl.Name = "pervPrimen_lbl";
            this.pervPrimen_lbl.Size = new System.Drawing.Size(214, 20);
            this.pervPrimen_lbl.TabIndex = 8;
            this.pervPrimen_lbl.Text = "Первичное применение:";
            // 
            // oboznVal_lbl
            // 
            this.oboznVal_lbl.AutoSize = true;
            this.oboznVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.oboznVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oboznVal_lbl.Location = new System.Drawing.Point(223, 40);
            this.oboznVal_lbl.Name = "oboznVal_lbl";
            this.oboznVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.oboznVal_lbl.TabIndex = 5;
            // 
            // prjNameVal_lbl
            // 
            this.prjNameVal_lbl.AutoSize = true;
            this.prjNameVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prjNameVal_lbl.Location = new System.Drawing.Point(223, 20);
            this.prjNameVal_lbl.Name = "prjNameVal_lbl";
            this.prjNameVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.prjNameVal_lbl.TabIndex = 3;
            // 
            // obozn_lbl
            // 
            this.obozn_lbl.AutoSize = true;
            this.obozn_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.obozn_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.obozn_lbl.Location = new System.Drawing.Point(103, 40);
            this.obozn_lbl.Name = "obozn_lbl";
            this.obozn_lbl.Size = new System.Drawing.Size(114, 20);
            this.obozn_lbl.TabIndex = 4;
            this.obozn_lbl.Text = "Обозначение:";
            // 
            // invNumVal_lbl
            // 
            this.invNumVal_lbl.AutoSize = true;
            this.invNumVal_lbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.invNumVal_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.invNumVal_lbl.Location = new System.Drawing.Point(223, 0);
            this.invNumVal_lbl.Name = "invNumVal_lbl";
            this.invNumVal_lbl.Size = new System.Drawing.Size(0, 20);
            this.invNumVal_lbl.TabIndex = 1;
            // 
            // prjName_lbl
            // 
            this.prjName_lbl.AutoSize = true;
            this.prjName_lbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.prjName_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prjName_lbl.Location = new System.Drawing.Point(148, 20);
            this.prjName_lbl.Name = "prjName_lbl";
            this.prjName_lbl.Size = new System.Drawing.Size(69, 20);
            this.prjName_lbl.TabIndex = 2;
            this.prjName_lbl.Text = "Проект:";
            // 
            // DocumentCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 442);
            this.Controls.Add(this.splitContainer1);
            this.Name = "DocumentCard";
            this.Text = "Карточка документа";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.Resize += new System.EventHandler(this.Form4_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label invNumVal_lbl;
        private System.Windows.Forms.Label oboznVal_lbl;
        private System.Windows.Forms.Label obozn_lbl;
        private System.Windows.Forms.Label invNum_lbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label prjNameVal_lbl;
        private System.Windows.Forms.Label prjName_lbl;
        private System.Windows.Forms.Label naimenVal_lbl;
        private System.Windows.Forms.Label naimen_lbl;
        private System.Windows.Forms.Label pervPrimenVal_lbl;
        private System.Windows.Forms.Label pervPrimen_lbl;
        private System.Windows.Forms.Label utverdilVal_lbl;
        private System.Windows.Forms.Label utverdil_lbl;
        private System.Windows.Forms.Label dateTimeVal_lbl;
        private System.Windows.Forms.Label dateTime_lbl;
        private System.Windows.Forms.Label razrabVal_lbl;
        private System.Windows.Forms.Label razrab_lbl;
        private System.Windows.Forms.Label proverilVal_lbl;
        private System.Windows.Forms.Label proveril_lbl;
        private System.Windows.Forms.Label tehKontrVal_lbl;
        private System.Windows.Forms.Label tehKontr_lbl;
        private System.Windows.Forms.Label normoKontrVal_lbl;
        private System.Windows.Forms.Label normoKontr_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Edit_tSMI;
    }
}