﻿namespace Catalog
    {
    partial class RepositoryEditor
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.PrjNameTab = new System.Windows.Forms.TabPage();
            this.IspolniteliTab = new System.Windows.Forms.TabPage();
            this.LiteralsTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Delete_btn = new System.Windows.Forms.Button();
            this.Rename_btn = new System.Windows.Forms.Button();
            this.Add_btn = new System.Windows.Forms.Button();
            this.Edit_tb = new System.Windows.Forms.TextBox();
            this.tabControl.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.PrjNameTab);
            this.tabControl.Controls.Add(this.IspolniteliTab);
            this.tabControl.Controls.Add(this.LiteralsTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(316, 290);
            this.tabControl.TabIndex = 2;
            this.tabControl.Tag = "Literals";
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // PrjNameTab
            // 
            this.PrjNameTab.Location = new System.Drawing.Point(4, 22);
            this.PrjNameTab.Name = "PrjNameTab";
            this.PrjNameTab.Padding = new System.Windows.Forms.Padding(3);
            this.PrjNameTab.Size = new System.Drawing.Size(308, 264);
            this.PrjNameTab.TabIndex = 0;
            this.PrjNameTab.Tag = "PrjName";
            this.PrjNameTab.Text = "Названия проектов";
            this.PrjNameTab.UseVisualStyleBackColor = true;
            // 
            // IspolniteliTab
            // 
            this.IspolniteliTab.Location = new System.Drawing.Point(4, 22);
            this.IspolniteliTab.Name = "IspolniteliTab";
            this.IspolniteliTab.Padding = new System.Windows.Forms.Padding(3);
            this.IspolniteliTab.Size = new System.Drawing.Size(308, 264);
            this.IspolniteliTab.TabIndex = 1;
            this.IspolniteliTab.Tag = "Ispolniteli";
            this.IspolniteliTab.Text = "Исполнители";
            this.IspolniteliTab.UseVisualStyleBackColor = true;
            // 
            // LiteralsTab
            // 
            this.LiteralsTab.Location = new System.Drawing.Point(4, 22);
            this.LiteralsTab.Name = "LiteralsTab";
            this.LiteralsTab.Size = new System.Drawing.Size(308, 264);
            this.LiteralsTab.TabIndex = 2;
            this.LiteralsTab.Text = "Литеры";
            this.LiteralsTab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tabControl, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Delete_btn, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.Rename_btn, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.Add_btn, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Edit_tb, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(322, 416);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // Delete_btn
            // 
            this.Delete_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Delete_btn.Location = new System.Drawing.Point(3, 389);
            this.Delete_btn.Name = "Delete_btn";
            this.Delete_btn.Size = new System.Drawing.Size(316, 24);
            this.Delete_btn.TabIndex = 4;
            this.Delete_btn.Text = "Удалить";
            this.Delete_btn.UseVisualStyleBackColor = true;
            this.Delete_btn.Click += new System.EventHandler(this.Delete_btn_Click);
            // 
            // Rename_btn
            // 
            this.Rename_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Rename_btn.Location = new System.Drawing.Point(3, 359);
            this.Rename_btn.Name = "Rename_btn";
            this.Rename_btn.Size = new System.Drawing.Size(316, 24);
            this.Rename_btn.TabIndex = 4;
            this.Rename_btn.Text = "Переименовать";
            this.Rename_btn.UseVisualStyleBackColor = true;
            this.Rename_btn.Click += new System.EventHandler(this.Rename_btn_Click);
            // 
            // Add_btn
            // 
            this.Add_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Add_btn.Location = new System.Drawing.Point(3, 329);
            this.Add_btn.Name = "Add_btn";
            this.Add_btn.Size = new System.Drawing.Size(316, 24);
            this.Add_btn.TabIndex = 3;
            this.Add_btn.Text = "Добавить";
            this.Add_btn.UseVisualStyleBackColor = true;
            this.Add_btn.Click += new System.EventHandler(this.Add_btn_Click);
            // 
            // Edit_tb
            // 
            this.Edit_tb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Edit_tb.Location = new System.Drawing.Point(3, 299);
            this.Edit_tb.Name = "Edit_tb";
            this.Edit_tb.Size = new System.Drawing.Size(316, 20);
            this.Edit_tb.TabIndex = 5;
            // 
            // RepositoryEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 416);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "RepositoryEditor";
            this.Text = "Редактор справочников";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.tabControl.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

            }

        #endregion
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage PrjNameTab;
        private System.Windows.Forms.TabPage IspolniteliTab;
        private System.Windows.Forms.TabPage LiteralsTab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button Rename_btn;
        private System.Windows.Forms.Button Add_btn;
        private System.Windows.Forms.Button Delete_btn;
        private System.Windows.Forms.TextBox Edit_tb;
    }
    }