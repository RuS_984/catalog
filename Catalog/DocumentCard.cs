﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.IO;

using DBData.Repository;
using DBData.Entities;

using DataModel.SupportClasses;

namespace Catalog
{
    public partial class DocumentCard : Form
    {
        SqlCeConnection CECon = CommonData.SQLConn;
        DataGridView dgv;

        DataGridViewRow _row = new DataGridViewRow();

        ProjectRepository ProjectRep;
        DocsRepository DocsRep;

        MainForm main;


        BuildNodeTree BFNT;
        BuildNodeTree BANT;

        DocumentsTree BFDT;
        DocumentsTree BADT;


        private Dictionary<string, MemoryStream> Files_ = new Dictionary<string, MemoryStream>();

        public DocumentCard(DataGridView DGV)
        {
            InitializeComponent();

            dgv = DGV;
            ProjectRep = new ProjectRepository(CommonData.ProjectTable);
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            main = this.Owner as MainForm;

            //BuildTreeView BTV = new BuildTreeView(treeView1, CECon);
            //BTV.GetFullTree();

             BFNT = new BuildNodeTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetFullDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );

             BANT = new BuildNodeTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetAsmDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );


            BFDT = new DocumentsTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetFullDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );

             BADT = new DocumentsTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetAsmDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );

            BFDT.BuildTree();
            BFDT.TView = treeView1;

            //BADT.BuildTree();
            //BADT.TView = treeView1;


            TreeNode TN = BFNT.BuildTree();
           string str = BFDT.GetRootTreeJSON();
            string str1 = BFDT.GetChildTreeJSON(treeView1.Nodes[0]);
            string str2 = BFDT.GetChildTreeJSON(treeView1.Nodes[0].Nodes[0]);
            string str3 = BFDT.GetChildTreeJSON(treeView1.Nodes[0].Nodes[0].Nodes[0]);

            string ssssdasfasd = "dfsdfsdf";
            //TreeNode TN = BANT.BuildTree();
            //foreach (TreeNode tn in TN.Nodes)
            foreach (TreeNode tn in BFDT.treeNode.Nodes)
            {
                //TreeNode tn1 = tn;
                //tn.Text = "sdgdf1111";
                //treeView1.Nodes.Add(tn);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string str;
            _row = null;
            Files_ = new Dictionary<string, MemoryStream>();

            main.dataGridView1.ClearSelection();
            foreach (DataGridViewRow row in main.dataGridView1.Rows)
            {
                str = row.Cells[3].Value.ToString();
                if (str == treeView1.SelectedNode.Text)
                {
                    row.Selected = true;
                    _row = row;
                }
            }

            if (_row != null)
            {
                invNumVal_lbl.Text = _row.Cells[1].Value.ToString();

                prjNameVal_lbl.Text = _row.Cells[2].Value.ToString();
                oboznVal_lbl.Text = _row.Cells[3].Value.ToString();
                naimenVal_lbl.Text = _row.Cells[4].Value.ToString();
                pervPrimenVal_lbl.Text = _row.Cells[5].Value.ToString();
                dateTimeVal_lbl.Text =
                        ((DateTime)_row.Cells[6].Value).ToString("dd.MM.yyyy");

                razrabVal_lbl.Text = _row.Cells[8].Value.ToString();
                proverilVal_lbl.Text = _row.Cells[9].Value.ToString();
                tehKontrVal_lbl.Text = _row.Cells[10].Value.ToString();
                normoKontrVal_lbl.Text = _row.Cells[11].Value.ToString();
                utverdilVal_lbl.Text = _row.Cells[12].Value.ToString();

                DocsRep = new DocsRepository(CECon, _row.Cells[3].Value.ToString());
                listBox1.DataSource = DocsRep.GetBindingSource();
                listBox1.DisplayMember = "Name";
                listBox1.ValueMember = "Id";
            }


        }



        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            MemoryStream ms;
            //string index;
            string index = listBox1.SelectedItem.ToString();


            ms = ((Docs)listBox1.SelectedItem).Document;

            SaveFileDialog fsv = new SaveFileDialog();

            FileInfo fi = new FileInfo(((Docs)listBox1.SelectedItem).Name);

            fsv.Filter = string.Format("*{0}|*{0}", fi.Extension);
            fsv.FileName = fi.Name;
            fsv.OverwritePrompt = true;
            fsv.ValidateNames = true;

            if (fsv.ShowDialog() == DialogResult.OK)
            {
                FileStream file_ = new FileStream(fsv.FileName,
                FileMode.OpenOrCreate,
                FileAccess.Write);

                file_.Write(ms.ToArray(), 0, (int)ms.Length);
                file_.Close();
                file_.Dispose();
            }
        }

        private void Form4_Resize(object sender, EventArgs e)
        {
            if (this.Width > 500)
            {
                splitContainer1.SplitterDistance = this.Width - 415;
            }
        }

        private void Edit_tSMI_Click(object sender, EventArgs e)
        {
            int i;
            //DataGridViewRow row = new DataGridViewRow();
            DataGridViewRow row = main.dataGridView1.SelectedRows[0];
            i = row.Index;
            DocumentEdit DocEdit = new DocumentEdit(row);
            DocEdit.Owner = this;
            DocEdit.ShowDialog();
            main.dataGridView1.ClearSelection();
            main.dataGridView1.FirstDisplayedScrollingRowIndex = i;
            main.dataGridView1.Rows[i].Selected = true;
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            string txt = e.Node.Text;
            string str = BFDT.GetChildTreeJSON(NodeText: txt);
            //TreeNode[] node =  ((TreeView)sender).Nodes.Find(txt, true);

            //string strv1 = BFDT.FindNode(118).Text;
            //string strv = BFDT.GetChildTreeJSON(e.Node);

            //TreeNode[] node = treeNode.Nodes.Find(NodeText, true);


           // string strv = BFDT.GetChildTreeJSON(node[0]);


            MessageBox.Show(str);
        }
    }
}
