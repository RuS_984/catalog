﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

using DataModel.SupportClasses;
using System.Reflection;

using DBData;

namespace Catalog

{
    public partial class MainForm : Form
        {

        SqlCeConnection sqlCEConn;

        private CheckedListBox filterCheckedListBox;
        private ToolStripDropDown filterPopup;

        public MainForm()
        {

            InitializeComponent();

            SetDoubleBuffered(dataGridView1, true);

            CommonData.LastFilterParam = "documents.inv_num";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                DataModel.SupportClasses.CommonData.ConString = 
                    DBprocessing.GetConString(Settings.SQLCEDBFileName);

                sqlCEConn = DataModel.SupportClasses.CommonData.SQLConn;
            }
            catch (ArgumentNullException)
            {

                MessageBox.Show("БД не выбрана");
                this.Close();
            }

            CommonData.MainTable.DBRequest = DBprocessing.SQLRequest();
            CommonData.MainTable.OrderedBy = CommonData.LastFilterParam;
            CommonData.MainTable.GetMainTable(IsOrdered: true);

            dataGridView1.DataSource = CommonData.MainTable.MainDBdt.DefaultView;
            dataGridView1.Columns[0].Visible = false;
           dataGridView1.MultiSelect = false;

            CommonData.MainTable.OnDBUpdate += dgvUpdate;

            this.Text = "Каталог " + sqlCEConn.ConnectionString;

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.Activate();

            if (dataGridView1.MultiSelect)
            {
                dataGridView1.MultiSelect = false;
            }
            else
            {
                dataGridView1.MultiSelect = true;
            }

            CheckSelectiontype();
        }

        void filterCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //Исключение столбца id из списка столбцов
            if (dataGridView1.Columns[e.Index + 1].HeaderText != "id")
                { dataGridView1.Columns[e.Index + 1].Visible = (e.NewValue
                                                        == CheckState.Checked); }
        }

        private void ColFilter_Click(object sender, EventArgs e)
        {
            filterCheckedListBox = new CheckedListBox();
            filterCheckedListBox.CheckOnClick = true;
            filterCheckedListBox.Items.Clear();
            filterCheckedListBox.ItemCheck +=
                    new ItemCheckEventHandler(filterCheckedListBox_ItemCheck);

            ToolStripControlHost filterControlHost =
                    new ToolStripControlHost(filterCheckedListBox);
            filterControlHost.Padding = Padding.Empty;
            filterControlHost.Margin = Padding.Empty;
            filterControlHost.AutoSize = true;

            filterPopup = new ToolStripDropDown();
            filterPopup.Padding = Padding.Empty;
            filterPopup.Items.Add(filterControlHost);

            foreach (DataGridViewColumn c in dataGridView1.Columns)
            {
                if (c.HeaderText != "id")
                    { filterCheckedListBox.Items.Add(c.HeaderText, c.Visible); }
            }
            int PreferredHeight = (filterCheckedListBox.Items.Count * 16) + 7;
            filterCheckedListBox.Height = (PreferredHeight < 200) ? PreferredHeight : 270;
            filterCheckedListBox.Width = this.Width;
            filterPopup.Show(new Point(MousePosition.X, MousePosition.Y));
        }

        private void dgvUpdate()
        {
            CommonData.MainTable.OnDBUpdate -= dgvUpdate;
            CommonData.MainTable.GetMainTable(true);
            dataGridView1.DataSource = CommonData.MainTable.MainDBdt;

            dataGridView1.Ispoln = CommonData.IspolniteliTable.GetEntities().ToDictionary(n => n.Name, n => n.Id);
            dataGridView1.Prj = CommonData.ProjectTable.GetEntities().ToDictionary(n => n.Name, n => n.Id);
            dataGridView1.Literal = CommonData.LiteralTable.GetEntities().ToDictionary(n => n.Name, n => n.Id);

            this.dataGridView1.Columns[0].Visible = false;

            //MessageBox.Show("gdfsgsgdf");
            CommonData.MainTable.OnDBUpdate += dgvUpdate;
        }

        /// <summary>
        /// Изменение свойства DoubleBuffered контрола.
        /// </summary>
        /// <param name="c">Ссылка на контрол.</param>
        /// <param name="value">Значение, которое надо установить DoubleBuffered.</param>
        void SetDoubleBuffered(Control c, bool value)
        {
            PropertyInfo pi = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic);
            if (pi != null)
            {
                pi.SetValue(c, value, null);

                MethodInfo mi = typeof(Control).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.NonPublic);
                if (mi != null)
                {
                    mi.Invoke(c, new object[] { ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true });
                }

                mi = typeof(Control).GetMethod("UpdateStyles", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.NonPublic);
                if (mi != null)
                {
                    mi.Invoke(c, null);
                }
            }
        }

        private void CheckSelectiontype()
        {
            if (dataGridView1.MultiSelect == false &
                dataGridView1.SelectionMode == DataGridViewSelectionMode.FullRowSelect
                )
            {
                selMode_tsl.Text = "Выделение одной строки целиком";
            }

            if (dataGridView1.MultiSelect == true &
                dataGridView1.SelectionMode == DataGridViewSelectionMode.FullRowSelect
                )
            {
                selMode_tsl.Text = "Выделение нескольких строк целиком";
            }


            if (dataGridView1.MultiSelect == false &
                dataGridView1.SelectionMode == DataGridViewSelectionMode.CellSelect
                )
            {
                selMode_tsl.Text = "Выделение одной ячейки";
            }

            if (dataGridView1.MultiSelect == true &
                dataGridView1.SelectionMode == DataGridViewSelectionMode.CellSelect
                )
            {
                selMode_tsl.Text = "Выделение нескольких ячеек";
            }
    }

        private void dataGridView1_GroupDataChange(object sender,
            MyControls.MyDataGridViev.GroupChangeEventArgs e)
        {
            if (e.newColParam <0)
            {
                return;
            }


            int[] ids = new int[e.selectedRows.Length];
            for (int i = 0; i < ids.Length; i++)
            {
                ids[i] = (int)dataGridView1.Rows[e.selectedRows[i]].Cells[0].Value;
            }
            string str = DBprocessing.ListToString(ids.ToList());
            int dbColIndex = Array.IndexOf(CommonData.ColumnNames, e.colName);
            string dbColName = CommonData.DBColumnNames[dbColIndex];
            CommonData.MainTable.UpdateRecordByCol(ColName: dbColName, ColValue: e.newColParam, IdRange: str);
        }



        #region Menu

        private void Open_tSMnu_Click(object sender, EventArgs e)
        {
            DBprocessing.OpenDBFile();
            DataModel.SupportClasses.CommonData.ConString = 
                DBprocessing.GetConString(Settings.SQLCEDBFileName);
            sqlCEConn = DataModel.SupportClasses.CommonData.SQLConn;

            CommonData.MainTable.GetMainTable(true);
            dataGridView1.DataSource = CommonData.MainTable.MainDBdt.DefaultView;

            this.Text = "Каталог " + sqlCEConn.ConnectionString;
        }

        private void Create_tSMnu_Click(object sender, EventArgs e)
        {
            DocumentEdit DocumentEditFrm = new DocumentEdit();
            DocumentEditFrm.Owner = this;
            DocumentEditFrm.ShowDialog();
            //dgv_Update(orderedByInvNum);
        }

        private void Update_tSMnu_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                int i;
                DataGridViewRow row = new DataGridViewRow();
                row = this.dataGridView1.SelectedRows[0];
                i = row.Index;
                DocumentEdit DocEdit = new DocumentEdit(row);
                DocEdit.Owner = this;
                DocEdit.ShowDialog();
                //dgv_Update(orderedByInvNum);
                dataGridView1.ClearSelection();
                dataGridView1.FirstDisplayedScrollingRowIndex = i;
                dataGridView1.Rows[i].Selected = true;
            }
        }

        private void Delete_tSMnu_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow == null)
            {
                return;
            }
            DialogResult result = MessageBox.Show(text: "Удалить выбранную строку?",
                            caption: "Удаление строки",
                            buttons: MessageBoxButtons.YesNo);
            if (result.ToString().ToLower() == "yes")
                {
                CommonData.MainTable.DeleteRecordByRow(
                    RowId:(int)dataGridView1.CurrentRow.Cells[0].Value,
                    DocumentId: (int)dataGridView1.CurrentRow.Cells[1].Value);
                MessageBox.Show("Строка удалена");
                }
        }

        private void LibEdit_tSMnu_Click(object sender, EventArgs e)
        {
            RepositoryEditor RepEdit = new RepositoryEditor();
            RepEdit.Owner = this;
            RepEdit.ShowDialog();
        }

        private void LibEdit_Click(object sender, EventArgs e)
        {
            DocumentCard DocCard = new DocumentCard(dataGridView1);
            DocCard.Owner = this;
            DocCard.Show();
        }

        private void XLSExport_tSMnu_Click(object sender, EventArgs e)
        {
            string[] ldc = new string[dataGridView1.Columns.Count];

            int i = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.Visible)
                {
                    ldc[i] = column.Name;
                }
                else
                {
                    ldc[i] = string.Empty;
                }
                i++;
            }
            XLSExport Report = new XLSExport(ldc, dataGridView1.Rows);

            Report.GenerateXLS();
        }

        private void XLSImport_tSMnu_Click(object sender, EventArgs e)
        {
            ImportForm importfrm = new ImportForm();
            importfrm.ShowDialog();

        }

        private void DirImport_tSMnu_Click(object sender, EventArgs e)
        {
            ImportDirForm importDirForm = new ImportDirForm();
            importDirForm.ShowDialog();
        }







        #endregion


        #region TaskBar

        private void multisel_tsb_Click(object sender, EventArgs e)
        {
            if (dataGridView1.MultiSelect)
            {
                dataGridView1.MultiSelect = false;
                //multisel_tsl.Text = "Выбор нескольких строк откл.";
            }
            else
            {
                dataGridView1.MultiSelect = true;
                //multisel_tsl.Text = "Выбор нескольких строк вкл.";
            }
            CheckSelectiontype();
        }

        private void rowSel_tsb_Click(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            CheckSelectiontype();
        }

        private void cellSel_tsb_Click(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.CellSelect;
            CheckSelectiontype();
        }



        #endregion








        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //CommonData.MyLogger.Dispose();
        }
    }
    }
