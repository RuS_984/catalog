﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

using System.Data;
using System.Xml;

using DBData;
using DBData.Repository;
using DataModel.SupportClasses;
using DataModel.DBWrapper;

namespace Catalog
{
    public partial class ImportDirForm : Form
    {
        public ImportDirForm()
        {
            InitializeComponent();
            StartNum_tb.Text = 1000000.ToString();
        }


        int nodeIndex;

        private DataTable dt = new DataTable();
        private DataTable dtRepited = new DataTable();
        private BindingSource bindingSource = new BindingSource();

        DirTreeNode dtn;

        private void ScanDir_bt_Click(object sender, EventArgs e)
        {
            DirTree_tv.Nodes.Clear();
            Dependency_dgv.Rows.Clear();
            if (DirPath_tb.Text == string.Empty) { MessageBox.Show("Путь не указан"); return; }
            nodeIndex = int.Parse(StartNum_tb.Text);
            string[] Filterparams = ListOfFilterParam_tb.Text.Split(';');
            //GetDirTree(DirPath_tb.Text);
            dtn = new DirTreeNode(Filterparams, DirPath_tb.Text, nodeIndex);
            TreeNode root = dtn.BuildNodeTree();
            DirTree_tv.Nodes.Add(root);

        }

        private void FillDGV()
        {
            this.Dependency_dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            this.Dependency_dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            this.Dependency_dgv.ColumnHeadersDefaultCellStyle.Font =
                new Font(Dependency_dgv.Font, FontStyle.Bold);
            this.Dependency_dgv.ColumnHeadersHeightSizeMode =
                DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            bindingSource.DataSource = dt;
            dt.Columns.Add("Dependency Id", typeof(int));
            dt.Columns.Add("Dependency On", typeof(int));
            dt.Columns.Add("Document Name", typeof(string));
            dt.Columns.Add("Dependency Name", typeof(string));
            dt.Columns.Add("Is Root", typeof(bool));
            dt.Columns.Add("Is Folder", typeof(bool));
            dt.Columns.Add("Is ASM", typeof(bool));
            dt.Columns.Add("Oboznachenie", typeof(string));
            dt.Columns.Add("Naimenovanie", typeof(string));

            //dt.Columns.Add("Album", typeof(string));

            //dt.Rows.Add(1, "29", "Revolution 9", false);

            TreeViewToDataTable(DirTree_tv.Nodes);

            DataSet ds = new DataSet("table");
            ds.Tables.Add(dt);
            Dependency_dgv.DataSource = ds.Tables[0].DefaultView;
        }
        //-------------------------------------------------------------------

        private void TreeViewToDataTable(TreeNodeCollection parentNodes)
        {
            foreach (TreeNode node1 in parentNodes)
            {
                nodeDependency nd = ((nodeDependency)node1.Tag);

                //if (!nd.isFolder)
                {
                    dt.Rows.Add(nd.dependencyId, nd.dependensOn, nd.documentName, 
                                nd.dependencyName, nd.isRoot, nd.isFolder, nd.isAsm,
                                nd.oboznachenie, nd. naimenovanie);
                }
                if (node1.Nodes != null)
                {
                    TreeViewToDataTable(node1.Nodes);
                }
                //dt.Rows.Add(nd.dependencyId, node1.Name, nd.dependencyName, nd.isRoot);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FillDGV();
            CheckDuplicates();
            if (ContinueDBNum_chb.Checked)
            {
                UpdateId();
            }
        }

        public void UpdateId(bool newNum = true)
        {
            int invNum = CommonData.MainTable.MaxInvNum;
            //if (invNum == 0) { invNum++; }
            invNum++;
            Dictionary<int, int> newInvNum = new Dictionary<int, int>();
            newInvNum.Add(0, 0);
            foreach (DataRow row in dt.Rows)
            {
                
                if (!newInvNum.ContainsKey((int)row[0]))
                {
                    if ((int)row[0] > 0)
                    {
                        newInvNum.Add((int)row[0], invNum++);
                    }
                    else
                    {
                        //newInvNum.Add(0, 0);
                    }

                }
                //row[0] = 
            }

            foreach (DataRow row in dt.Rows)
            {

                row[0] = newInvNum[(int)row[0]];
                row[1] = newInvNum[(int)row[1]];
            }
            //DataSet ds1 = new DataSet("table");
            //ds1.Tables.Add(dt);
            //dataGridView1.DataSource = ds1.Tables[0].DefaultView;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddToDB();
        }

        public void AddToDB()
        {
            DependencyRepository DR = new DependencyRepository(DataModel.SupportClasses.CommonData.SQLConn, "", -1);

            int prjId;

            

            bindingSource.DataSource = dtRepited;
            //dtRepited.Columns.Add("Dependency Id", typeof(int));
            //dtRepited.Columns.Add("Dependency On", typeof(int));
            //dtRepited.Columns.Add("Document Name", typeof(string));
            //dtRepited.Columns.Add("Dependency Name", typeof(string));
            //dtRepited.Columns.Add("Is Root", typeof(bool));
            //dtRepited.Columns.Add("Is Folder", typeof(bool));
            //dtRepited.Columns.Add("Is ASM", typeof(bool));
            //dtRepited.Columns.Add("Oboznachenie", typeof(string));
            //dtRepited.Columns.Add("Naimenovanie", typeof(string));

            //bindingSource.DataSource = dt;

            progressBar1.Maximum = dt.Rows.Count;
            //progressBar1.Maximum = 100;
            //int i = 
            foreach (DataRow row in dt.Rows)
            {
                //if (!(bool)row[5])
                {
                    if ((bool)row[4]) { prjId = 1; }
                    else { prjId = 0; }

                    MainTableStruct dbStr = new MainTableStruct();
                    dbStr.SetDefaultData();
                    dbStr.InventarnNum = (int)row[0];
                    dbStr.Oboznachenie = (string)row[7];
                    dbStr.Naimenovanie = (string)row[8];
                    dbStr.IsAsm = (bool)row[6];
                    CommonData.MainTable.CreateRecord(dbStr);
                    DR.AddDependencyFromDirStruct(
                        DocumentId: (int)row[0],
                        ParentId: (int)row[1],
                        IsAsm: (bool)row[6],
                        ProjectId: prjId);

                }
                progressBar1.Value++;
            }

            bindingSource.DataSource = dtRepited;
            //Dependency_dgv.Rows.Clear();
            DataSet ds = new DataSet("table");
            ds.Tables.Add(dtRepited);
            Dependency_dgv.DataSource = ds.Tables[0].DefaultView;

            //DataSet ds1 = new DataSet("table");
            //ds1.Tables.Add(dt);
            //dataGridView1.DataSource = ds1.Tables[0].DefaultView;

        }


        public void CheckDuplicates()
        {
            List<string> obozn = new List<string>();
            List<int> indexes = new List<int>();

            OboznachenieRepository OboznachenieRep;
            OboznachenieRep = new OboznachenieRepository(CommonData.OboznachenieTable);
            obozn = OboznachenieRep.GetList();

            dtRepited.Columns.Add("Dependency Id", typeof(int));
            dtRepited.Columns.Add("Dependency On", typeof(int));
            dtRepited.Columns.Add("Document Name", typeof(string));
            dtRepited.Columns.Add("Dependency Name", typeof(string));
            dtRepited.Columns.Add("Is Root", typeof(bool));
            dtRepited.Columns.Add("Is Folder", typeof(bool));
            dtRepited.Columns.Add("Is ASM", typeof(bool));
            dtRepited.Columns.Add("Oboznachenie", typeof(string));
            dtRepited.Columns.Add("Naimenovanie", typeof(string));


            

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (!obozn.Contains((string)dt.Rows[i][7]))
                {
                    obozn.Add((string)dt.Rows[i][7]);
                }
                else
                {
                    dtRepited.Rows.Add(dt.Rows[i].ItemArray);
                    indexes.Add(i);
                    continue;
                }
            }

            indexes.Reverse();

            foreach (int i in indexes)
            {
                dt.Rows.RemoveAt(i);
            }
        }

        private void SetDir_bt_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                DirPath_tb.Text = Path.GetDirectoryName(ofd.FileName);
            }
        }

        private void StartNum_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }
    }








}
