﻿namespace Catalog
{
    using DataModel.SupportClasses;
    using DBData.Entities;
    using DBData.Repository;
    using System;
    using System.Data.SqlServerCe;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="RepositoryEditor" />
    /// </summary>
    public partial class RepositoryEditor : Form
    {
        /// <summary>
        /// Defines the CEcon
        /// </summary>
        internal SqlCeConnection CEcon = DataModel.SupportClasses.CommonData.SQLConn;

        /// <summary>
        /// Defines the ispRep
        /// </summary>
        internal IspolniteliRepository ispRep;

        /// <summary>
        /// Defines the prjRep
        /// </summary>
        internal ProjectRepository prjRep;

        /// <summary>
        /// Defines the litRep
        /// </summary>
        internal LiteralRepository litRep;

        /// <summary>
        /// Defines the tabs
        /// </summary>
        internal string[] tabs = { "Названия проектов", "Исполнители", "Литеры" };

        /// <summary>
        /// Defines the lb
        /// </summary>
        internal ListBox lb = new ListBox();

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryEditor"/> class.
        /// </summary>
        public RepositoryEditor()
        {
            InitializeComponent();

            lb.Click += new System.EventHandler(this.lb_Click);

            this.lb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
        }

        /// <summary>
        /// The MouseHover
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void MouseHover(object sender, EventArgs e)
        {
            lb.Invalidate();
            Console.WriteLine("hover");
        }

        /// <summary>
        /// The lb_MouseMove
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="MouseEventArgs"/></param>
        private void lb_MouseMove(object sender, MouseEventArgs e)
        {
        }

        /// <summary>
        /// The OnDrawItem
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="DrawItemEventArgs"/></param>
        private void OnDrawItem(object sender, DrawItemEventArgs e)
        {
            Console.WriteLine("OnDrawItem");
            Brush BackBrush = new SolidBrush(base.BackColor);

            // Перерисовываем фон всех элементов ListBox.
            //e.DrawBackground();

            //e.DrawFocusRectangle();
            e.Graphics.FillRectangle(BackBrush, this.ClientRectangle);
        }

        /// <summary>
        /// The lb_MouseDown
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="MouseEventArgs"/></param>
        private void lb_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = lb.IndexFromPoint(e.Location);
                {
                    if (index == lb.SelectedIndex)
                    {
                        //MessageBox.Show("Selected item clicked");
                        MessageBox.Show(((Project)lb.SelectedItem).Name);
                    }
                }
            }
        }

        /// <summary>
        /// The Form3_Load
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void Form3_Load(object sender, EventArgs e)
        {

            TabPage Prj_tab = new TabPage("Названия проектов");

            ispRep = new IspolniteliRepository(CommonData.IspolniteliTable);

            prjRep = new ProjectRepository(CommonData.ProjectTable);

            litRep = new LiteralRepository(CommonData.LiteralTable);

            tabControl.SelectTab(2);
            tabControl.SelectTab(0);
        }

        /// <summary>
        /// The UpdateLB
        /// </summary>
        private void UpdateLB()
        {
            //Заполнение ListBox
            if (tabControl.SelectedTab.Text == tabs[0])//== "Названия проектов")
            {
                lb.DataSource = prjRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";
            }

            else if (tabControl.SelectedTab.Text == tabs[1])//== "Исполнители")
            {
                lb.DataSource = ispRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";
            }

            else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
            {
                litRep.GetEntities();
                lb.DataSource = litRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";
            }
        }

        /// <summary>
        /// The tabControl_SelectedIndexChanged
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl.SelectedTab.Controls.Clear();
            Edit_tb.Text = string.Empty;

            lb.DataSource = null;
            lb.DisplayMember = null;
            lb.ValueMember = null;
            lb.Dock = DockStyle.Fill;

            tabControl.SelectedTab.Controls.Add(lb);

            if (tabControl.SelectedTab.Text == tabs[0])//== "Названия проектов")
            {
                //lb.Items.Add("Названия проектов");
                lb.DataSource = prjRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";

            }

            else if (tabControl.SelectedTab.Text == tabs[1])//== "Исполнители")
            {
                //lb.Items.Add("Исполнители");
                lb.DataSource = ispRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";
            }

            else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
            {
                lb.DataSource = litRep.GetBindingSource();
                lb.DisplayMember = "Name";
                lb.ValueMember = "Id";
            }
        }

        /// <summary>
        /// The lb_Click
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void lb_Click(object sender, EventArgs e)
        {

            if (tabControl.SelectedTab.Text == tabs[0])//"Названия проектов")
            {
                Edit_tb.Text = ((Project)lb.SelectedItem).Name;
            }

            else if (tabControl.SelectedTab.Text == tabs[1])//"Исполнители")
            {
                Edit_tb.Text = ((Ispolniteli)lb.SelectedItem).Name;
            }

            else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
            {
                Edit_tb.Text = ((Literal)lb.SelectedItem).Name;
            }
        }

        /// <summary>
        /// The Add_btn_Click
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void Add_btn_Click(object sender, EventArgs e)
        {

            if (Edit_tb.Text == "")
            {
                MessageBox.Show("Новое название не указано");
                return;
            }

            DialogResult result = MessageBox.Show("Добавить строку?",
                "Добавить строку",
                MessageBoxButtons.YesNo);

            if (result.ToString().ToLower() == "yes")
            {
                if (tabControl.SelectedTab.Text == tabs[0])//== "Названия проектов")
                {
                    prjRep.AddToDB(Edit_tb.Text);
                }

                else if (tabControl.SelectedTab.Text == tabs[1])//== "Исполнители")
                {
                    ispRep.AddToDB(Edit_tb.Text);
                }

                else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
                {
                    litRep.AddToDB(Edit_tb.Text);

                }
            }

            UpdateLB();
        }

        /// <summary>
        /// The Rename_btn_Click
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void Rename_btn_Click(object sender, EventArgs e)
        {

            if (Edit_tb.Text == "")
            {
                MessageBox.Show("Новое название не указано");
                return;
            }

            DialogResult result = MessageBox.Show("Переименовать строку?",
                 "Переименовать строку",
                 MessageBoxButtons.YesNo);

            if (result.ToString().ToLower() == "yes")
            {
                if (tabControl.SelectedTab.Text == tabs[0])//== "Названия проектов")
                {
                    prjRep.RenameInDB(oldValue: ((Project)lb.SelectedItem).Name,
                       newValue: Edit_tb.Text);
                }

                else if (tabControl.SelectedTab.Text == tabs[1])//== "Исполнители")
                {
                    ispRep.RenameInDB(oldValue: ((Ispolniteli)lb.SelectedItem).Name,
                        newValue: Edit_tb.Text);
                }

                else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
                {
                    litRep.RenameInDB(oldValue: ((Literal)lb.SelectedItem).Name,
                        newValue: Edit_tb.Text);
                }
            }


            UpdateLB();
        }

        /// <summary>
        /// The Delete_btn_Click
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        private void Delete_btn_Click(object sender, EventArgs e)
        {

            if (Edit_tb.Text == "")
            {
                MessageBox.Show("Значение не указано");
                return;
            }


            DialogResult result = MessageBox.Show("Удалить строку?",
               "Удаление строки",
               MessageBoxButtons.YesNo);

            if (result.ToString().ToLower() == "yes")
            {
                if (tabControl.SelectedTab.Text == tabs[0])//== "Названия проектов")
                {
                    prjRep.DeleteFromDB(((Project)lb.SelectedItem).Name);
                }

                else if (tabControl.SelectedTab.Text == tabs[1])//== "Исполнители")
                {
                    ispRep.DeleteFromDB(((Ispolniteli)lb.SelectedItem).Name);
                }

                else if (tabControl.SelectedTab.Text == tabs[2])//== "Литеры")
                {
                    litRep.DeleteFromDB(((Literal)lb.SelectedItem).Name);
                }
            }

            UpdateLB();
        }
    }
}
