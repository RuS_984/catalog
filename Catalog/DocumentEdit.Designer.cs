﻿namespace Catalog
{
    partial class DocumentEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.isAsm_chb = new System.Windows.Forms.CheckBox();
            this.isAsm_lbl = new System.Windows.Forms.Label();
            this.AddDoc_btn = new System.Windows.Forms.Button();
            this.Cancel_btn = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddFile_btn = new System.Windows.Forms.Button();
            this.DelFile_btn = new System.Windows.Forms.Button();
            this.dateTime_dt_uc = new MyControls.DateTime_uc();
            this.Utverdil_cb_uc = new MyControls.ComboBox_uc();
            this.NormoKontr_cb_uc = new MyControls.ComboBox_uc();
            this.TehKontr_cb_uc = new MyControls.ComboBox_uc();
            this.Proveril_cb_uc = new MyControls.ComboBox_uc();
            this.Razrab_cb_uc = new MyControls.ComboBox_uc();
            this.Prj_name_cb_uc = new MyControls.ComboBox_uc();
            this.PervPrimen_tb_uc = new MyControls.TextBox_uc();
            this.Naimen_tb_uc = new MyControls.TextBox_uc();
            this.Obozn_tb_uc = new MyControls.TextBox_uc();
            this.Inv_num_tb_uc = new MyControls.TextBox_uc();
            this.dependency_lb = new System.Windows.Forms.Label();
            this.dependency_tv = new System.Windows.Forms.TreeView();
            this.Literal_cb_uc = new MyControls.ComboBox_uc();
            this.SuspendLayout();
            // 
            // isAsm_chb
            // 
            this.isAsm_chb.AutoSize = true;
            this.isAsm_chb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.isAsm_chb.Location = new System.Drawing.Point(211, 241);
            this.isAsm_chb.Name = "isAsm_chb";
            this.isAsm_chb.Size = new System.Drawing.Size(15, 14);
            this.isAsm_chb.TabIndex = 7;
            this.isAsm_chb.UseVisualStyleBackColor = true;
            this.isAsm_chb.CheckStateChanged += new System.EventHandler(this.isAsm_chb_CheckStateChanged);
            // 
            // isAsm_lbl
            // 
            this.isAsm_lbl.AutoSize = true;
            this.isAsm_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.isAsm_lbl.Location = new System.Drawing.Point(47, 237);
            this.isAsm_lbl.Name = "isAsm_lbl";
            this.isAsm_lbl.Size = new System.Drawing.Size(158, 20);
            this.isAsm_lbl.TabIndex = 13;
            this.isAsm_lbl.Text = "Сборочный чертеж:";
            // 
            // AddDoc_btn
            // 
            this.AddDoc_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddDoc_btn.Location = new System.Drawing.Point(10, 521);
            this.AddDoc_btn.Name = "AddDoc_btn";
            this.AddDoc_btn.Size = new System.Drawing.Size(189, 34);
            this.AddDoc_btn.TabIndex = 16;
            this.AddDoc_btn.Text = "Добавить/Изменить";
            this.AddDoc_btn.UseVisualStyleBackColor = true;
            this.AddDoc_btn.Click += new System.EventHandler(this.AddDoc_btn_Click);
            // 
            // Cancel_btn
            // 
            this.Cancel_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Cancel_btn.Location = new System.Drawing.Point(267, 521);
            this.Cancel_btn.Name = "Cancel_btn";
            this.Cancel_btn.Size = new System.Drawing.Size(113, 34);
            this.Cancel_btn.TabIndex = 17;
            this.Cancel_btn.Text = "Отмена";
            this.Cancel_btn.UseVisualStyleBackColor = true;
            this.Cancel_btn.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // listBox1
            // 
            this.listBox1.DataSource = this.listBox1.CustomTabOffsets;
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(211, 431);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(269, 84);
            this.listBox1.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 431);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 44);
            this.label1.TabIndex = 30;
            this.label1.Text = "Прикрепленные файлы";
            // 
            // AddFile_btn
            // 
            this.AddFile_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddFile_btn.Location = new System.Drawing.Point(14, 479);
            this.AddFile_btn.Name = "AddFile_btn";
            this.AddFile_btn.Size = new System.Drawing.Size(80, 36);
            this.AddFile_btn.TabIndex = 14;
            this.AddFile_btn.Text = "Добавить";
            this.AddFile_btn.UseVisualStyleBackColor = true;
            this.AddFile_btn.Click += new System.EventHandler(this.AddFile_btn_Click);
            // 
            // DelFile_btn
            // 
            this.DelFile_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DelFile_btn.Location = new System.Drawing.Point(117, 479);
            this.DelFile_btn.Name = "DelFile_btn";
            this.DelFile_btn.Size = new System.Drawing.Size(82, 36);
            this.DelFile_btn.TabIndex = 15;
            this.DelFile_btn.Text = "Удалить";
            this.DelFile_btn.UseVisualStyleBackColor = true;
            this.DelFile_btn.Click += new System.EventHandler(this.DelFile_btn_Click);
            // 
            // dateTime_dt_uc
            // 
            this.dateTime_dt_uc.control_width = 470;
            this.dateTime_dt_uc.dateTimePicker1_char_col = 31;
            this.dateTime_dt_uc.dateTimePicker1_width = 270;
            this.dateTime_dt_uc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTime_dt_uc.Lable_txt = "Дата подписания:";
            this.dateTime_dt_uc.Location = new System.Drawing.Point(10, 174);
            this.dateTime_dt_uc.Name = "dateTime_dt_uc";
            this.dateTime_dt_uc.Size = new System.Drawing.Size(470, 26);
            this.dateTime_dt_uc.TabIndex = 6;
            // 
            // Utverdil_cb_uc
            // 
            this.Utverdil_cb_uc.comboBox1_width = 270;
            this.Utverdil_cb_uc.control_width = 470;
            this.Utverdil_cb_uc.IsStandartAutoComplete = false;
            this.Utverdil_cb_uc.Lable_txt = "Утвердил:";
            this.Utverdil_cb_uc.Location = new System.Drawing.Point(10, 397);
            this.Utverdil_cb_uc.Name = "Utverdil_cb_uc";
            this.Utverdil_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.Utverdil_cb_uc.TabIndex = 12;
            // 
            // NormoKontr_cb_uc
            // 
            this.NormoKontr_cb_uc.comboBox1_width = 270;
            this.NormoKontr_cb_uc.control_width = 470;
            this.NormoKontr_cb_uc.IsStandartAutoComplete = false;
            this.NormoKontr_cb_uc.Lable_txt = "Нормоконтроль:";
            this.NormoKontr_cb_uc.Location = new System.Drawing.Point(10, 363);
            this.NormoKontr_cb_uc.Name = "NormoKontr_cb_uc";
            this.NormoKontr_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.NormoKontr_cb_uc.TabIndex = 11;
            // 
            // TehKontr_cb_uc
            // 
            this.TehKontr_cb_uc.comboBox1_width = 270;
            this.TehKontr_cb_uc.control_width = 470;
            this.TehKontr_cb_uc.IsStandartAutoComplete = false;
            this.TehKontr_cb_uc.Lable_txt = "Тех.контроль:";
            this.TehKontr_cb_uc.Location = new System.Drawing.Point(10, 329);
            this.TehKontr_cb_uc.Name = "TehKontr_cb_uc";
            this.TehKontr_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.TehKontr_cb_uc.TabIndex = 10;
            // 
            // Proveril_cb_uc
            // 
            this.Proveril_cb_uc.comboBox1_width = 270;
            this.Proveril_cb_uc.control_width = 470;
            this.Proveril_cb_uc.IsStandartAutoComplete = false;
            this.Proveril_cb_uc.Lable_txt = "Проверил:";
            this.Proveril_cb_uc.Location = new System.Drawing.Point(10, 295);
            this.Proveril_cb_uc.Name = "Proveril_cb_uc";
            this.Proveril_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.Proveril_cb_uc.TabIndex = 9;
            // 
            // Razrab_cb_uc
            // 
            this.Razrab_cb_uc.comboBox1_width = 270;
            this.Razrab_cb_uc.control_width = 470;
            this.Razrab_cb_uc.IsStandartAutoComplete = false;
            this.Razrab_cb_uc.Lable_txt = "Разработал:";
            this.Razrab_cb_uc.Location = new System.Drawing.Point(10, 261);
            this.Razrab_cb_uc.Name = "Razrab_cb_uc";
            this.Razrab_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.Razrab_cb_uc.TabIndex = 8;
            // 
            // Prj_name_cb_uc
            // 
            this.Prj_name_cb_uc.comboBox1_width = 270;
            this.Prj_name_cb_uc.control_width = 470;
            this.Prj_name_cb_uc.IsStandartAutoComplete = false;
            this.Prj_name_cb_uc.Lable_txt = "Проект:";
            this.Prj_name_cb_uc.Location = new System.Drawing.Point(10, 44);
            this.Prj_name_cb_uc.Name = "Prj_name_cb_uc";
            this.Prj_name_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.Prj_name_cb_uc.TabIndex = 2;
            // 
            // PervPrimen_tb_uc
            // 
            this.PervPrimen_tb_uc.control_width = 470;
            this.PervPrimen_tb_uc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PervPrimen_tb_uc.Is_Digit_only = false;
            this.PervPrimen_tb_uc.IsStandartAutoComplete = false;
            this.PervPrimen_tb_uc.Lable_txt = "Первичное применение:";
            this.PervPrimen_tb_uc.Location = new System.Drawing.Point(10, 142);
            this.PervPrimen_tb_uc.Name = "PervPrimen_tb_uc";
            this.PervPrimen_tb_uc.Size = new System.Drawing.Size(470, 26);
            this.PervPrimen_tb_uc.TabIndex = 5;
            this.PervPrimen_tb_uc.TextBox_txt = "";
            this.PervPrimen_tb_uc.textBox_width = 270;
            // 
            // Naimen_tb_uc
            // 
            this.Naimen_tb_uc.control_width = 470;
            this.Naimen_tb_uc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Naimen_tb_uc.Is_Digit_only = false;
            this.Naimen_tb_uc.IsStandartAutoComplete = false;
            this.Naimen_tb_uc.Lable_txt = "Наименование:";
            this.Naimen_tb_uc.Location = new System.Drawing.Point(10, 110);
            this.Naimen_tb_uc.Name = "Naimen_tb_uc";
            this.Naimen_tb_uc.Size = new System.Drawing.Size(470, 26);
            this.Naimen_tb_uc.TabIndex = 4;
            this.Naimen_tb_uc.TextBox_txt = "";
            this.Naimen_tb_uc.textBox_width = 270;
            // 
            // Obozn_tb_uc
            // 
            this.Obozn_tb_uc.control_width = 470;
            this.Obozn_tb_uc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Obozn_tb_uc.Is_Digit_only = false;
            this.Obozn_tb_uc.IsStandartAutoComplete = false;
            this.Obozn_tb_uc.Lable_txt = "Обозначение:";
            this.Obozn_tb_uc.Location = new System.Drawing.Point(10, 78);
            this.Obozn_tb_uc.Name = "Obozn_tb_uc";
            this.Obozn_tb_uc.Size = new System.Drawing.Size(470, 26);
            this.Obozn_tb_uc.TabIndex = 3;
            this.Obozn_tb_uc.TextBox_txt = "";
            this.Obozn_tb_uc.textBox_width = 270;
            // 
            // Inv_num_tb_uc
            // 
            this.Inv_num_tb_uc.control_width = 470;
            this.Inv_num_tb_uc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inv_num_tb_uc.Is_Digit_only = false;
            this.Inv_num_tb_uc.IsStandartAutoComplete = false;
            this.Inv_num_tb_uc.Lable_txt = "Инвентарный номер:";
            this.Inv_num_tb_uc.Location = new System.Drawing.Point(10, 12);
            this.Inv_num_tb_uc.Name = "Inv_num_tb_uc";
            this.Inv_num_tb_uc.Size = new System.Drawing.Size(470, 26);
            this.Inv_num_tb_uc.TabIndex = 1;
            this.Inv_num_tb_uc.TextBox_txt = "";
            this.Inv_num_tb_uc.textBox_width = 270;
            // 
            // dependency_lb
            // 
            this.dependency_lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dependency_lb.Location = new System.Drawing.Point(488, 12);
            this.dependency_lb.Name = "dependency_lb";
            this.dependency_lb.Size = new System.Drawing.Size(93, 26);
            this.dependency_lb.TabIndex = 31;
            this.dependency_lb.Text = "Входит в:";
            this.dependency_lb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dependency_tv
            // 
            this.dependency_tv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dependency_tv.Location = new System.Drawing.Point(492, 44);
            this.dependency_tv.Name = "dependency_tv";
            this.dependency_tv.Size = new System.Drawing.Size(305, 488);
            this.dependency_tv.TabIndex = 32;
            // 
            // Literal_cb_uc
            // 
            this.Literal_cb_uc.comboBox1_width = 270;
            this.Literal_cb_uc.control_width = 470;
            this.Literal_cb_uc.IsStandartAutoComplete = true;
            this.Literal_cb_uc.Lable_txt = "Литера:";
            this.Literal_cb_uc.Location = new System.Drawing.Point(10, 206);
            this.Literal_cb_uc.Name = "Literal_cb_uc";
            this.Literal_cb_uc.Size = new System.Drawing.Size(470, 28);
            this.Literal_cb_uc.TabIndex = 33;
            // 
            // DocumentEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.Literal_cb_uc);
            this.Controls.Add(this.dependency_tv);
            this.Controls.Add(this.dependency_lb);
            this.Controls.Add(this.DelFile_btn);
            this.Controls.Add(this.AddFile_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.dateTime_dt_uc);
            this.Controls.Add(this.Utverdil_cb_uc);
            this.Controls.Add(this.NormoKontr_cb_uc);
            this.Controls.Add(this.TehKontr_cb_uc);
            this.Controls.Add(this.Proveril_cb_uc);
            this.Controls.Add(this.Razrab_cb_uc);
            this.Controls.Add(this.Prj_name_cb_uc);
            this.Controls.Add(this.PervPrimen_tb_uc);
            this.Controls.Add(this.Naimen_tb_uc);
            this.Controls.Add(this.Obozn_tb_uc);
            this.Controls.Add(this.Inv_num_tb_uc);
            this.Controls.Add(this.Cancel_btn);
            this.Controls.Add(this.AddDoc_btn);
            this.Controls.Add(this.isAsm_lbl);
            this.Controls.Add(this.isAsm_chb);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "DocumentEdit";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox isAsm_chb;
        private System.Windows.Forms.Label isAsm_lbl;
        private System.Windows.Forms.Button AddDoc_btn;
        private System.Windows.Forms.Button Cancel_btn;
        private MyControls.TextBox_uc Inv_num_tb_uc;
        private MyControls.ComboBox_uc Prj_name_cb_uc;
        private MyControls.TextBox_uc Obozn_tb_uc;
        private MyControls.TextBox_uc Naimen_tb_uc;
        private MyControls.TextBox_uc PervPrimen_tb_uc;
        private MyControls.DateTime_uc dateTime_dt_uc;
        private MyControls.ComboBox_uc Razrab_cb_uc;
        private MyControls.ComboBox_uc Proveril_cb_uc;
        private MyControls.ComboBox_uc TehKontr_cb_uc;
        private MyControls.ComboBox_uc NormoKontr_cb_uc;
        private MyControls.ComboBox_uc Utverdil_cb_uc;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddFile_btn;
        private System.Windows.Forms.Button DelFile_btn;
        private System.Windows.Forms.Label dependency_lb;
        private System.Windows.Forms.TreeView dependency_tv;
        private MyControls.ComboBox_uc Literal_cb_uc;
    }
}