﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;
using DataModel.DBWrapper.Factories;


using DataModel.DBWrapper;
using DataModel.AppLogger;

using static DataModel.SupportClasses.CommonData;


namespace Catalog
{
    static class Program
    {

        //Thread newThread = null;

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static void Main()
        {

            //********************************
            //Обработчик событий потока интерфейса
            Application.ThreadException += new ThreadExceptionEventHandler(
                  ExceptionHandler.AppThreadExceptionHandler);

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);


            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(
                ExceptionHandler.AppDomainExceptioHandler);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            #region Инициализация настроек

            Settings.OnSQLCEFileCheck += DBprocessing.OpenMSSQLCEDBFile;
            Settings.Init();
            Settings.Load();

            switch (Settings.ConnectionType)
            {
                case DBType.None:
                    break;
                case DBType.MSSQL:
                    throw new NotImplementedException();
                    break;
                case DBType.MSSQLCE:
                    CommonData.dbSQL = DbProviderFactory.GetProvider(
                        DataModel.DBWrapper.DBType.MSSQLCE,
                        Settings.SQLCEDBConString
                    );
                    break;
                default:
                    break;
            }




            Logger.FileLogName = "FileLog";
            Logger.DBLogName = "DBLog";

            Logger.IsLogEnableb = false;
            Logger.MinimalLogLevel = LogLevel.Error;

            Logger.Log(LogTarget.FileLogger,LogLevel.Info,
                string.Format("Data Base type is {0}", Settings.ConnectionType));

            // Инициализация таблиц
            CommonData.ProjectTable         = CommonData.dbSQL.GetProjectTable();
            CommonData.OboznachenieTable    = CommonData.dbSQL.GetOboznachenieTable();
            CommonData.FirstUsageTable      = CommonData.dbSQL.GetFirstUsageTable();
            CommonData.LiteralTable         = CommonData.dbSQL.GetLiteralTable();
            CommonData.IspolniteliTable     = CommonData.dbSQL.GetIspolniteliTable();
            CommonData.InventarNumberTable  = CommonData.dbSQL.GetInventarNumberTable();
            CommonData.DependencyTable      = CommonData.dbSQL.GetDependencyTable();
            CommonData.MainTable            = CommonData.dbSQL.GetMainTable();



            #endregion


            Application.ApplicationExit += OnApplicationExit;


            Application.Run(new MainForm());

        }


        private static void OnApplicationExit(object sender, EventArgs e)
        {
            // When the application is exiting, write the application data to the
            // user file and close it.
            Logger.Dispose();


        }




    }



}
