﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.IO;


using System.Diagnostics;

using DBData;
using DBData.Repository;
using DBData.Entities;



using DataModel.SupportClasses;

using DataModel.DBWrapper;

namespace Catalog
{
    public partial class DocumentEdit : Form
    {
        SqlCeConnection CECon
        {
            get { return DataModel.SupportClasses.CommonData.SQLConn; }
        }

        DataGridViewRow _Row = new DataGridViewRow();

        ProjectRepository ProjectRep;

        DependencyRepository DependencyRep;

        LiteralRepository LiteralRep;

        List<string> isp_name = new List<string>();
        IspolniteliRepository IspolniteliRep;

        DocsRepository DocsRep;
        List<Docs> DeletedFiles = new List<Docs>();

        List<string> obozn = new List<string>();
        OboznachenieRepository OboznachenieRep;

        FirstUsageRepository PervichnoePrimenenieRep;

        List<string> inv_num_s = new List<string>();
        InventarNumberRepository InventarNumberRep;


        BuildTreeView BTV;

        DocumentsTree BADT;

        string oldinvNum = "0";

        #region Конструкторы

        //Для создания записи
        public DocumentEdit()
        {
            this.Text = "Создание записи";
            InitializeComponent();

            _Row = null;
            DocsRep = new DocsRepository(CECon, "");
            InitRepository(CECon);

            ListBoxInit();

            AddDoc_btn.Text = "Создать";

        }

        //Для изменения записи
        public DocumentEdit(DataGridViewRow Row)
        {
            this.Text = "Изменеие записи";
            InitializeComponent();
            _Row = Row;

            InitRepository(CECon);

            DocsRep = new DocsRepository(CECon, _Row.Cells[3].Value.ToString());

            ListBoxInit();

            AddDoc_btn.Text = "Изменить";
        }

        private void ListBoxInit()
        {
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.listBox1.MeasureItem += lst_MeasureItem;
            this.listBox1.DrawItem += lst_DrawItem;
        }


        void InitRepository(SqlCeConnection SQLCEcon)
        {
            ProjectRep = new ProjectRepository(CommonData.ProjectTable);
            IspolniteliRep = new IspolniteliRepository(CommonData.IspolniteliTable);
            OboznachenieRep = new OboznachenieRepository(CommonData.OboznachenieTable);
            PervichnoePrimenenieRep = new FirstUsageRepository(CommonData.FirstUsageTable);
            InventarNumberRep = new InventarNumberRepository(CommonData.InventarNumberTable);
            LiteralRep = new LiteralRepository(CommonData.LiteralTable);
        }
        #endregion Конструкторы

        #region Методы

        private void Update_Record()
        {
            CommonData.MainTable.UpdateRecord(GetDBStruct());
        }

        private MainTableStruct GetDBStruct()
        {
            MainTableStruct mdbs = new MainTableStruct();
            mdbs.Clear();

            mdbs.InventarnNum = Convert.ToInt32(Inv_num_tb_uc.TextBox_txt);

            mdbs.ProjectID = ProjectRep[Prj_name_cb_uc.ComboBox_txt].Id;

            mdbs.Oboznachenie = Obozn_tb_uc.TextBox_txt;

            mdbs.Naimenovanie = Naimen_tb_uc.TextBox_txt;

            mdbs.Pervichoepreminenie = PervPrimen_tb_uc.TextBox_txt;

            mdbs.Literal = LiteralRep[Literal_cb_uc.ComboBox_txt].Id;

            mdbs.Date = dateTime_dt_uc.dateTime_txt;

            mdbs.IsAsm = isAsm_chb.Checked;

            mdbs.Razrabotal = IspolniteliRep[Razrab_cb_uc.ComboBox_txt].Id;

            mdbs.Proveril = IspolniteliRep[Proveril_cb_uc.ComboBox_txt].Id;

            mdbs.Tehcontrol = IspolniteliRep[TehKontr_cb_uc.ComboBox_txt].Id;

            mdbs.Normokontrol = IspolniteliRep[NormoKontr_cb_uc.ComboBox_txt].Id;

            mdbs.Utverdil = IspolniteliRep[Utverdil_cb_uc.ComboBox_txt].Id;

            if (_Row != null)
            {
                mdbs.Id = (int)_Row.Cells[0].Value;
            }

            return mdbs;
        }

        private void Add_New_Record()
        {

            CommonData.MainTable.CreateRecord(GetDBStruct());

        }

        //Заполнение полей на форме
        private void Fill_data()
        {
            Inv_num_tb_uc.TextBox_txt = _Row.Cells[1].Value.ToString();

            oldinvNum = _Row.Cells[1].Value.ToString();

            Prj_name_cb_uc.Set_by_value(_Row.Cells[2].Value.ToString());
            Obozn_tb_uc.TextBox_txt = _Row.Cells[3].Value.ToString();
            Naimen_tb_uc.TextBox_txt = _Row.Cells[4].Value.ToString();
            Naimen_tb_uc.Disable_AutoComplete();

            PervPrimen_tb_uc.TextBox_txt = _Row.Cells[5].Value.ToString();
            dateTime_dt_uc.Set_Date(_Row.Cells[6].Value.ToString());

            Literal_cb_uc.Set_by_value(_Row.Cells[7].Value.ToString());

            isAsm_chb.CheckState = ((bool)_Row.Cells[8].Value) ? CheckState.Checked : CheckState.Unchecked;

            Razrab_cb_uc.Set_by_value(_Row.Cells[9].Value.ToString());
            Proveril_cb_uc.Set_by_value(_Row.Cells[10].Value.ToString());
            TehKontr_cb_uc.Set_by_value(_Row.Cells[11].Value.ToString());
            NormoKontr_cb_uc.Set_by_value(_Row.Cells[12].Value.ToString());
            Utverdil_cb_uc.Set_by_value(_Row.Cells[13].Value.ToString());

            listBox1.DataSource = DocsRep.GetBindingSource();
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";

            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);

        }

        #endregion

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.SuspendLayout();

            AddDoc_btn.Focus();

            #region Создание списка инвентарных номеров+++

            inv_num_s = InventarNumberRep.GetList();

            if (_Row != null)
            {
                inv_num_s.Remove(_Row.Cells[1].Value.ToString());
            }

            Inv_num_tb_uc.Set_Params(AutoComplete_List: inv_num_s,
                AutoComplete_Enabled: true, Validating: true);

            #endregion Создание списка инвентарных номеров

            #region Создание списка названий проектов++++
            Prj_name_cb_uc.Set_Params(AutoComplete_List: ProjectRep.GetList(), first_val: "",
                AutoComplete_Enabled: true, Validating: false);
            #endregion Создание списка названий проектов

            #region Создание списка обозначений++++
            obozn = OboznachenieRep.GetList();

            if (_Row != null)
            { obozn.Remove(_Row.Cells[3].Value.ToString()); }

            Obozn_tb_uc.Set_Params(AutoComplete_List: obozn,
                AutoComplete_Enabled: true, Validating: true);
            #endregion Создание списка обозначений

            #region Создание списка первичного применения++++
            PervPrimen_tb_uc.Set_Params(AutoComplete_List: PervichnoePrimenenieRep.GetList(),
                AutoComplete_Enabled: true, Validating: false);
            #endregion Создание списка первичного примения

            #region Создание списка исполнителей++++
            isp_name = IspolniteliRep.GetList();

            MyControls.ComboBox_uc[] components_ = {
                    Razrab_cb_uc,       Proveril_cb_uc,     TehKontr_cb_uc,
                    NormoKontr_cb_uc,   Utverdil_cb_uc};

            foreach (MyControls.ComboBox_uc comp in components_)
            {
                comp.Set_Params(AutoComplete_List: isp_name, first_val: "",
                AutoComplete_Enabled: true, Validating: false);
            }
            #endregion Создание списка исполнителей++++

            #region Создание списка литер++++

            Literal_cb_uc.Set_Params(AutoComplete_List: LiteralRep.GetList(), first_val: "",
                AutoComplete_Enabled: false, Validating: false);
            #endregion Создание списка литер

            AddDoc_btn.Enabled = true;

            string oboznTmp = "";

            Inv_num_tb_uc.TextBox_txt = (CommonData.MainTable.MaxInvNum + 1).ToString();

            //BTV = new BuildTreeView(dependency_tv, CECon, oboznTmp );
            //BTV.GetAsmTree();
            dependency_tv.CheckBoxes = true;

            BADT = new DocumentsTree(
                DependencyRep: new DependencyRepository(Table: CommonData.DependencyTable, Oboznachenie: ""),
                DependencyTable: CommonData.DependencyTable.GetAsmDependencyTable(),
                ProjectTable: CommonData.ProjectTable.GetProjectTable()
            );


            BADT.BuildTree();
            BADT.tView = dependency_tv;
            BADT.BuildTree();



            if (_Row != null)
            {
                oboznTmp = _Row.Cells[3].Value.ToString();
                //BTV.CheckDependency(_Row.Cells[3].Value.ToString());
                //BTV.ShowChecked();

                BADT.CheckDependency(oboznTmp);
                BADT.ShowChecked();

                //dependency_tv.Nodes.Clear();

                //for (int i = 0; i < BADT.treeNode.GetNodeCount(false); i++)
                //{
                //    dependency_tv.Nodes.Add(BADT.treeNode.Nodes[i]);
                //}



                Fill_data();
            }
            Inv_num_tb_uc.Set_AutoComplete_List(inv_num_s);

            this.ResumeLayout(false);
            this.PerformLayout();

        } //Form2_Load

        private void AddDoc_btn_Click(object sender, EventArgs e)
        {
            if (!Inv_num_tb_uc.Is_Valid())
            {
                return;
            }

            DocsRep.Oboznachenie = Obozn_tb_uc.TextBox_txt;
            if (_Row == null)
            {
                Add_New_Record();
            }
            else
            {
                //Обновление записи
                Update_Record();

                // Обновление таблицы зависимостей
                //при изменении идентификатора документа
                DependencyRep = new DependencyRepository(CECon, Obozn_tb_uc.TextBox_txt, -1);
                bool isasm = (isAsm_chb.CheckState == CheckState.Checked) ? true : false;
                if (Inv_num_tb_uc.TextBox_txt != oldinvNum)
                {
                    DependencyRep.ChangeDocumentID(
                        oldDocuments_id: Convert.ToInt32(oldinvNum),
                        newDcuments_id: Convert.ToInt32(Inv_num_tb_uc.TextBox_txt),
                        isAsm: isasm
                        );
                }
                else
                {
                    DependencyRep.ChangeDocumentID(
                        oldDocuments_id: Convert.ToInt32(oldinvNum),
                        newDcuments_id: Convert.ToInt32(oldinvNum),
                        isAsm: isasm
                        );
                }

            }

            DocsRep.UpdateDB();

            //BTV.UpdateDB(InvNum: Inv_num_tb_uc.TextBox_txt, isAsm: isAsm_chb.Checked);
            BADT.UpdateDB(InvNum: Inv_num_tb_uc.TextBox_txt, isAsm: isAsm_chb.Checked);

            this.Close();

        }

        private void AddFile_btn_Click(object sender, EventArgs e)
        {
            List<Docs> newfiles = new List<Docs>();
            newfiles = IMAGEProcessing.IMAGEOpenMS3(
                    OpenFilter: "*.JPG|*.jpg| *.DOCX|*.DOCX| *.DOC|*.DOC| Все файлы|*.*",
                    InitDir: @"D:\_SW VS KOMPAS");
            DocsRep.AddList(newfiles);
            listBox1.DataSource = DocsRep.GetBindingSource();
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";
        }

        private void DelFile_btn_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                DeletedFiles.Add(DocsRep[(((Docs)listBox1.SelectedItem).Id)]);
            }
            else
            {
                MessageBox.Show("Null");
            }

            DocsRep.DeleteDoc(DeletedFiles[DeletedFiles.Count - 1]);

            listBox1.DataSource = DocsRep.GetBindingSource();
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";
        }

        private void lst_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)e.Graphics.MeasureString(
                listBox1.Items[e.Index].ToString(),
                listBox1.Font,
                listBox1.Width).Height;
        }

        private void lst_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (listBox1.Items.Count > 0)
            {
                e.DrawBackground();
                e.DrawFocusRectangle();
                //e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds);
                e.Graphics.DrawString(((Entity)listBox1.Items[e.Index]).Name,
                    e.Font, new SolidBrush(e.ForeColor), e.Bounds);
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            //Obozn_tb_uc.TextBox_txt = listBox1.SelectedValue.ToString();

        }

        private void isAsm_chb_CheckStateChanged(object sender, EventArgs e)
        {

        }

    }
}
