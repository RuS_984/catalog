﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataModel.DBWrapper.Providers.Abstract;

namespace Catalog1
{
    public static class CommonData1
    {

        //public static DatabaseProvider dbSQL;

        //public static ProjectTableAbstract ProjectTableSql;

        public static string[] ColumnNames = {"Id",
                "Инвентарный номер", "Название проекта", "Обозначение",
                "Наименование", "Первичное преминение", "Литера", "Дата подписания",
                "Сборка", "Проверил", "Разработал",
                "Тех контроль", "Нормоконтроль", "Утвердил" };
        public static string[] DBColumnNames = {"id",
                "inv_num", "project_id", "oboznachenie",
                "naimenovanie", "pervichprimen", "literal", "date",
                "is_asm", "proveril", "razrabotal",
                "tehcontrol", "normcontrol", "utverdil" };

        public static string rowRepeat = "_ПОВТОР_";
    }
}
