﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlServerCe;



using DataModel.SupportClasses;

using DBData;

using System.IO;

using DBData.Repository;
using DBData.Entities;
using DataModel.DBWrapper;

namespace Catalog
{
    public partial class ImportForm : Form
    {
        
        DataSet dataSet = new DataSet();



        private ToolStripDropDown filterPopup;
        //private TextBox Filter_tb;
        private Panel SearchPanel = new Panel();
        private ListBox Headers_lb;
        private int curColIndex;
        private List<string> usedHeaders = new List<string>();
        private List<int> usedHeadersIndex = new List<int>();
        private int minInvNum = CommonData.MainTable.MinInvNum - 1;

        private XLSImport XLSimport;

        private const string noNameColHeader = "Без названия";



        public ImportForm()
        {
            InitializeComponent();

            dataGridView1.CellMouseClick += new
                DataGridViewCellMouseEventHandler(
                DGV_CellMouseClick);
            ToolStripDropDown_z();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            this.Text += $"MaxInvNum {minInvNum.ToString()}";

            XLSimport = new XLSImport();
        }

        private void OpenXLS_Click(object sender, EventArgs e)
        {
            OpenFileDialog fop = new OpenFileDialog();

            fop.InitialDirectory = Application.StartupPath; //@"c:\";
            fop.Filter = "*.xlsx|*.xlsx";

            fop.Multiselect = false;
            if (fop.ShowDialog() == DialogResult.OK)
            {
                XLSimport.OpenXLS(fop.FileName);
                dataGridView1.DataSource = XLSimport.XLSDataTable;
            }

            this.Text += $"minInvNum {minInvNum.ToString()}" ;
        }

        protected virtual void DGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                //MessageBox.Show(e.ColumnIndex.ToString());
                curColIndex = e.ColumnIndex;
                filterPopup.Show(new Point(MousePosition.X, MousePosition.Y));
                Headers_lb.Focus();
                //Filter_tb.Focus();
            }
        }


        private void ToolStripDropDown_z()
        {
            Headers_lb = new ListBox();

            Headers_lb.Items.AddRange(CommonData.ColumnNames);
            Headers_lb.Items.Remove("Id");
            Headers_lb.Items.Add(noNameColHeader);
            Headers_lb.Sorted = true;

            Headers_lb.SelectedIndexChanged +=
                new EventHandler(listBox1_SelectedIndexChanged);

            Headers_lb.Width += 35;

            SearchPanel.Controls.Add(Headers_lb);

            ToolStripControlHost filterControlHost =
                new ToolStripControlHost(SearchPanel);

            filterControlHost.AutoSize = true;

            filterPopup = new ToolStripDropDown();
            filterPopup.Padding = new Padding(2);
            filterPopup.Items.Add(filterControlHost);
            filterPopup.AutoSize = true;
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (Headers_lb.SelectedIndex != -1)
            {
                string old = dataGridView1.Columns[curColIndex].HeaderText;
                string newheader = Headers_lb.SelectedItem.ToString();

                if (newheader != noNameColHeader)
                {
                    dataGridView1.Columns[curColIndex].HeaderText = newheader;
                }
                else
                {
                    dataGridView1.Columns[curColIndex].HeaderText = curColIndex.ToString();
                }

                filterPopup.Hide();

                if (newheader != noNameColHeader)
                {
                    Headers_lb.Items.Remove(newheader);
                }

                if (CommonData.ColumnNames.Contains(old))
                {
                    Headers_lb.Items.Add(old);
                }

                if (Headers_lb.Items.Count < 5)
                {
                    Headers_lb.Height = Headers_lb.Font.Height * Headers_lb.Items.Count + 1 + 10;
                    SearchPanel.Controls.Add(Headers_lb);
                    filterPopup.Height = Headers_lb.Height;
                }
                else
                {
                    Headers_lb.Height = 100;
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            int tmp;

            SqlCeConnection CECon = DataModel.SupportClasses.CommonData.SQLConn;

            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();

            List<MainTableStruct> lstDB = new List<MainTableStruct>();

            //XLSImport XLSImp11 = new XLSImport();

            usedHeaders.Clear();
            usedHeadersIndex.Clear();

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                if (!int.TryParse(col.HeaderText, out tmp))
               {
                    int i = Array.IndexOf(CommonData.ColumnNames, col.HeaderText);
                    usedHeaders.Add(CommonData.DBColumnNames[i]);
                    usedHeadersIndex.Add(col.Index);
               }
            }

            XLSimport.InitImport(UsedHeaders:usedHeaders, UsedHeadersIndex: usedHeadersIndex);
            lstDB = XLSimport.GenerateListOfSQLCommands(1);

            CommonData.MainTable.CreateRecords(lstDB);

            //XLSimport.CreateRecord();
            this.Close();
            return;

            for (int i = 0; i < usedHeaders.Count; i++)
            {
                if (i == usedHeaders.Count-1)
                {
                    sb1.Append(usedHeaders[i]);
                    sb2.Append("@" + usedHeaders[i]);
                }
                else
                {
                    sb1.Append(usedHeaders[i] + ",");
                    sb2.Append("@" + usedHeaders[i] + ",");
                }
            }

            string sqlCmd = string.Format(
                @"INSERT INTO documents
                ({0})
                 VALUES
                ({1})",
                sb1.ToString(),
                sb2.ToString()
                );

            List<string> strlst1 = new List<string>();
            string str1;
            for (int i = 1; i < dataGridView1.RowCount; i++)
            {
                str1 = string.Empty;

                for (int j = 0; j < usedHeadersIndex.Count; j++)
                {
                    str1 += dataGridView1.Rows[i].Cells[j].Value.ToString() + "\t";
                }
                strlst1.Add(str1);
            }

            Dictionary<string, SqlCeParameter> sqlParametrs = new Dictionary<string, SqlCeParameter>();

            sqlParametrs.Add("project_id", new SqlCeParameter("@project_id", SqlDbType.Int));
            sqlParametrs.Add("inv_num", new SqlCeParameter("@inv_num", SqlDbType.Int));
            sqlParametrs.Add("oboznachenie", new SqlCeParameter("@oboznachenie", SqlDbType.NVarChar));
            sqlParametrs.Add("naimenovanie", new SqlCeParameter("@naimenovanie", SqlDbType.NVarChar));
            sqlParametrs.Add("pervichprimen", new SqlCeParameter("@pervichprimen", SqlDbType.NVarChar));
            sqlParametrs.Add("literal", new SqlCeParameter("@literal", SqlDbType.Int));
            sqlParametrs.Add("date", new SqlCeParameter("@date", SqlDbType.DateTime));
            sqlParametrs.Add("is_asm", new SqlCeParameter("@is_asm", SqlDbType.Bit));
            sqlParametrs.Add("razrabotal", new SqlCeParameter("@razrabotal", SqlDbType.Int));
            sqlParametrs.Add("proveril", new SqlCeParameter("@proveril", SqlDbType.Int));
            sqlParametrs.Add("tehcontrol", new SqlCeParameter("@tehcontrol", SqlDbType.Int));
            sqlParametrs.Add("normcontrol", new SqlCeParameter("@normcontrol", SqlDbType.Int));
            sqlParametrs.Add("utverdil", new SqlCeParameter("@utverdil", SqlDbType.Int));

            /*
             @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)"
            */

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(
               sqlCmd
               , CECon);

            for (int i = 0; i < usedHeaders.Count; i++)
            {
                if (sqlParametrs[usedHeaders[i]].SqlDbType == SqlDbType.Int)
                {
                    if (usedHeaders[i] == "inv_num")
                    {
                        _cmd.Parameters.Add(sqlParametrs[usedHeaders[i]]).Value =
                       Convert.ToInt32(dataGridView1.Rows[1].Cells[usedHeadersIndex[i]].Value);
                    }
                    else
                    {
                        _cmd.Parameters.Add(sqlParametrs[usedHeaders[i]]).Value = 1;
                    }
                }

                if (sqlParametrs[usedHeaders[i]].SqlDbType == SqlDbType.NVarChar)
                {
                    _cmd.Parameters.Add(sqlParametrs[usedHeaders[i]]).Value =
                        (string)dataGridView1.Rows[1].Cells[usedHeadersIndex[i]].Value;
                }
            }

            if (CECon.State == ConnectionState.Closed)
            {
                DataModel.SupportClasses.CommonData.SQLConn.Open();
            }

            try
            {
                _cmd.ExecuteNonQuery();
            }
            finally
            {
                CECon.Close();
            }
            MessageBox.Show("import done");

        }
    }
}
