﻿using System.Windows.Forms;

namespace Catalog
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.File_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.Open_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.Create_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.Update_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.ColFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.LibEdit_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.zzzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьОтчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.XLSExport_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.XLSImport_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.DirImport_tSMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.multisel_tsb = new System.Windows.Forms.ToolStripButton();
            this.rowSel_tsb = new System.Windows.Forms.ToolStripButton();
            this.cellSel_tsb = new System.Windows.Forms.ToolStripButton();
            this.selMode_tsl = new System.Windows.Forms.ToolStripLabel();
            this.dataGridView1 = new MyControls.MyDataGridViev();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File_tSMnu,
            this.Create_tSMnu,
            this.Update_tSMnu,
            this.ColFilter,
            this.Delete_tSMnu,
            this.LibEdit_tSMnu,
            this.zzzToolStripMenuItem,
            this.создатьОтчетToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(832, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // File_tSMnu
            // 
            this.File_tSMnu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Open_tSMnu});
            this.File_tSMnu.Name = "File_tSMnu";
            this.File_tSMnu.Size = new System.Drawing.Size(48, 20);
            this.File_tSMnu.Text = "Файл";
            // 
            // Open_tSMnu
            // 
            this.Open_tSMnu.Name = "Open_tSMnu";
            this.Open_tSMnu.Size = new System.Drawing.Size(121, 22);
            this.Open_tSMnu.Text = "Открыть";
            this.Open_tSMnu.Click += new System.EventHandler(this.Open_tSMnu_Click);
            // 
            // Create_tSMnu
            // 
            this.Create_tSMnu.Name = "Create_tSMnu";
            this.Create_tSMnu.Size = new System.Drawing.Size(62, 20);
            this.Create_tSMnu.Text = "Создать";
            this.Create_tSMnu.Click += new System.EventHandler(this.Create_tSMnu_Click);
            // 
            // Update_tSMnu
            // 
            this.Update_tSMnu.Name = "Update_tSMnu";
            this.Update_tSMnu.Size = new System.Drawing.Size(73, 20);
            this.Update_tSMnu.Text = "Изменить";
            this.Update_tSMnu.Click += new System.EventHandler(this.Update_tSMnu_Click);
            // 
            // ColFilter
            // 
            this.ColFilter.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ColFilter.Name = "ColFilter";
            this.ColFilter.Size = new System.Drawing.Size(115, 20);
            this.ColFilter.Text = "Список столбцов";
            this.ColFilter.Click += new System.EventHandler(this.ColFilter_Click);
            // 
            // Delete_tSMnu
            // 
            this.Delete_tSMnu.Name = "Delete_tSMnu";
            this.Delete_tSMnu.Size = new System.Drawing.Size(63, 20);
            this.Delete_tSMnu.Text = "Удалить";
            this.Delete_tSMnu.Click += new System.EventHandler(this.Delete_tSMnu_Click);
            // 
            // LibEdit_tSMnu
            // 
            this.LibEdit_tSMnu.Name = "LibEdit_tSMnu";
            this.LibEdit_tSMnu.Size = new System.Drawing.Size(151, 20);
            this.LibEdit_tSMnu.Text = "Редактор справочников";
            this.LibEdit_tSMnu.Click += new System.EventHandler(this.LibEdit_tSMnu_Click);
            // 
            // zzzToolStripMenuItem
            // 
            this.zzzToolStripMenuItem.Name = "zzzToolStripMenuItem";
            this.zzzToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.zzzToolStripMenuItem.Text = "Дерево проектов";
            this.zzzToolStripMenuItem.Click += new System.EventHandler(this.LibEdit_Click);
            // 
            // создатьОтчетToolStripMenuItem
            // 
            this.создатьОтчетToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.XLSExport_tSMnu,
            this.XLSImport_tSMnu,
            this.DirImport_tSMnu});
            this.создатьОтчетToolStripMenuItem.Name = "создатьОтчетToolStripMenuItem";
            this.создатьОтчетToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.создатьОтчетToolStripMenuItem.Text = "Отчет";
            // 
            // XLSExport_tSMnu
            // 
            this.XLSExport_tSMnu.Name = "XLSExport_tSMnu";
            this.XLSExport_tSMnu.Size = new System.Drawing.Size(226, 22);
            this.XLSExport_tSMnu.Text = "Экспорт в XLSX";
            this.XLSExport_tSMnu.Click += new System.EventHandler(this.XLSExport_tSMnu_Click);
            // 
            // XLSImport_tSMnu
            // 
            this.XLSImport_tSMnu.Name = "XLSImport_tSMnu";
            this.XLSImport_tSMnu.Size = new System.Drawing.Size(226, 22);
            this.XLSImport_tSMnu.Text = "Импорт из XLSX";
            this.XLSImport_tSMnu.Click += new System.EventHandler(this.XLSImport_tSMnu_Click);
            // 
            // DirImport_tSMnu
            // 
            this.DirImport_tSMnu.Name = "DirImport_tSMnu";
            this.DirImport_tSMnu.Size = new System.Drawing.Size(226, 22);
            this.DirImport_tSMnu.Text = "Испорт структуры из папки";
            this.DirImport_tSMnu.Click += new System.EventHandler(this.DirImport_tSMnu_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multisel_tsb,
            this.rowSel_tsb,
            this.cellSel_tsb,
            this.selMode_tsl});
            this.toolStrip1.Location = new System.Drawing.Point(0, 407);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(832, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // multisel_tsb
            // 
            this.multisel_tsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.multisel_tsb.Image = ((System.Drawing.Image)(resources.GetObject("multisel_tsb.Image")));
            this.multisel_tsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.multisel_tsb.Name = "multisel_tsb";
            this.multisel_tsb.Size = new System.Drawing.Size(23, 22);
            this.multisel_tsb.Text = "toolStripButton1";
            this.multisel_tsb.ToolTipText = "Единичный или множественный выбор";
            this.multisel_tsb.Click += new System.EventHandler(this.multisel_tsb_Click);
            // 
            // rowSel_tsb
            // 
            this.rowSel_tsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rowSel_tsb.Image = ((System.Drawing.Image)(resources.GetObject("rowSel_tsb.Image")));
            this.rowSel_tsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rowSel_tsb.Name = "rowSel_tsb";
            this.rowSel_tsb.Size = new System.Drawing.Size(23, 22);
            this.rowSel_tsb.Text = "toolStripButton1";
            this.rowSel_tsb.ToolTipText = "Выбор строк";
            this.rowSel_tsb.Click += new System.EventHandler(this.rowSel_tsb_Click);
            // 
            // cellSel_tsb
            // 
            this.cellSel_tsb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cellSel_tsb.Image = ((System.Drawing.Image)(resources.GetObject("cellSel_tsb.Image")));
            this.cellSel_tsb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cellSel_tsb.Name = "cellSel_tsb";
            this.cellSel_tsb.Size = new System.Drawing.Size(23, 22);
            this.cellSel_tsb.Text = "toolStripButton1";
            this.cellSel_tsb.ToolTipText = "Выбор ячеек";
            this.cellSel_tsb.Click += new System.EventHandler(this.cellSel_tsb_Click);
            // 
            // selMode_tsl
            // 
            this.selMode_tsl.Name = "selMode_tsl";
            this.selMode_tsl.Size = new System.Drawing.Size(86, 22);
            this.selMode_tsl.Text = "toolStripLabel1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.dataGridView1.Ispoln = null;
            this.dataGridView1.Literal = null;
            this.dataGridView1.Location = new System.Drawing.Point(0, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Prj = null;
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.Size = new System.Drawing.Size(832, 380);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.GroupDataChange += new MyControls.MyDataGridViev.GroupCahge(this.dataGridView1_GroupDataChange);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 432);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Каталог";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public MyControls.MyDataGridViev dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Create_tSMnu;
        private System.Windows.Forms.ToolStripMenuItem Update_tSMnu;
        private System.Windows.Forms.ToolStripMenuItem ColFilter;
        private System.Windows.Forms.ToolStripMenuItem Delete_tSMnu;
        private System.Windows.Forms.ToolStripMenuItem LibEdit_tSMnu;
        private System.Windows.Forms.ToolStripMenuItem zzzToolStripMenuItem;
        private ToolStripMenuItem создатьОтчетToolStripMenuItem;
        private ToolStripMenuItem XLSExport_tSMnu;
        private ToolStripMenuItem XLSImport_tSMnu;
        private ToolStrip toolStrip1;
        private ToolStripButton multisel_tsb;
        private ToolStripButton rowSel_tsb;
        private ToolStripButton cellSel_tsb;
        private ToolStripLabel selMode_tsl;
        private ToolStripMenuItem File_tSMnu;
        private ToolStripMenuItem Open_tSMnu;
        private ToolStripMenuItem DirImport_tSMnu;
    }
}

