﻿namespace Catalog
{
    partial class ImportDirForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DirPath_lbl = new System.Windows.Forms.Label();
            this.DirPath_tb = new System.Windows.Forms.TextBox();
            this.SetDir_bt = new System.Windows.Forms.Button();
            this.ScanDir_bt = new System.Windows.Forms.Button();
            this.DirTree_tv = new System.Windows.Forms.TreeView();
            this.Dependency_dgv = new System.Windows.Forms.DataGridView();
            this.ConvertTree_btn = new System.Windows.Forms.Button();
            this.AddToDB_btn = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.ContinueDBNum_chb = new System.Windows.Forms.CheckBox();
            this.StartNum_lbl = new System.Windows.Forms.Label();
            this.StartNum_tb = new System.Windows.Forms.TextBox();
            this.ListOfFilterParam_lbl = new System.Windows.Forms.Label();
            this.ListOfFilterParam_tb = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dependency_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // DirPath_lbl
            // 
            this.DirPath_lbl.AutoSize = true;
            this.DirPath_lbl.Location = new System.Drawing.Point(12, 9);
            this.DirPath_lbl.Name = "DirPath_lbl";
            this.DirPath_lbl.Size = new System.Drawing.Size(82, 13);
            this.DirPath_lbl.TabIndex = 0;
            this.DirPath_lbl.Text = "Путь до папки:";
            // 
            // DirPath_tb
            // 
            this.DirPath_tb.Location = new System.Drawing.Point(15, 25);
            this.DirPath_tb.Name = "DirPath_tb";
            this.DirPath_tb.Size = new System.Drawing.Size(464, 20);
            this.DirPath_tb.TabIndex = 1;
            // 
            // SetDir_bt
            // 
            this.SetDir_bt.Location = new System.Drawing.Point(494, 22);
            this.SetDir_bt.Name = "SetDir_bt";
            this.SetDir_bt.Size = new System.Drawing.Size(91, 23);
            this.SetDir_bt.TabIndex = 3;
            this.SetDir_bt.Text = "Выбор папки";
            this.SetDir_bt.UseVisualStyleBackColor = true;
            this.SetDir_bt.Click += new System.EventHandler(this.SetDir_bt_Click);
            // 
            // ScanDir_bt
            // 
            this.ScanDir_bt.Location = new System.Drawing.Point(15, 52);
            this.ScanDir_bt.Name = "ScanDir_bt";
            this.ScanDir_bt.Size = new System.Drawing.Size(127, 23);
            this.ScanDir_bt.TabIndex = 4;
            this.ScanDir_bt.Text = "Сканировать папки";
            this.ScanDir_bt.UseVisualStyleBackColor = true;
            this.ScanDir_bt.Click += new System.EventHandler(this.ScanDir_bt_Click);
            // 
            // DirTree_tv
            // 
            this.DirTree_tv.Location = new System.Drawing.Point(15, 82);
            this.DirTree_tv.Name = "DirTree_tv";
            this.DirTree_tv.Size = new System.Drawing.Size(394, 353);
            this.DirTree_tv.TabIndex = 5;
            // 
            // Dependency_dgv
            // 
            this.Dependency_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dependency_dgv.Location = new System.Drawing.Point(429, 82);
            this.Dependency_dgv.Name = "Dependency_dgv";
            this.Dependency_dgv.Size = new System.Drawing.Size(875, 353);
            this.Dependency_dgv.TabIndex = 6;
            // 
            // ConvertTree_btn
            // 
            this.ConvertTree_btn.Location = new System.Drawing.Point(429, 53);
            this.ConvertTree_btn.Name = "ConvertTree_btn";
            this.ConvertTree_btn.Size = new System.Drawing.Size(131, 23);
            this.ConvertTree_btn.TabIndex = 7;
            this.ConvertTree_btn.Text = "Обработать документы";
            this.ConvertTree_btn.UseVisualStyleBackColor = true;
            this.ConvertTree_btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // AddToDB_btn
            // 
            this.AddToDB_btn.Location = new System.Drawing.Point(15, 460);
            this.AddToDB_btn.Name = "AddToDB_btn";
            this.AddToDB_btn.Size = new System.Drawing.Size(140, 23);
            this.AddToDB_btn.TabIndex = 8;
            this.AddToDB_btn.Text = "Добавить в каталог";
            this.AddToDB_btn.UseVisualStyleBackColor = true;
            this.AddToDB_btn.Click += new System.EventHandler(this.button2_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(188, 459);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(438, 23);
            this.progressBar1.TabIndex = 9;
            // 
            // ContinueDBNum_chb
            // 
            this.ContinueDBNum_chb.AutoSize = true;
            this.ContinueDBNum_chb.Location = new System.Drawing.Point(582, 56);
            this.ContinueDBNum_chb.Name = "ContinueDBNum_chb";
            this.ContinueDBNum_chb.Size = new System.Drawing.Size(204, 17);
            this.ContinueDBNum_chb.TabIndex = 10;
            this.ContinueDBNum_chb.Text = "Продолжить нуменрацию каталога";
            this.ContinueDBNum_chb.UseVisualStyleBackColor = true;
            // 
            // StartNum_lbl
            // 
            this.StartNum_lbl.AutoSize = true;
            this.StartNum_lbl.Location = new System.Drawing.Point(592, 31);
            this.StartNum_lbl.Name = "StartNum_lbl";
            this.StartNum_lbl.Size = new System.Drawing.Size(106, 13);
            this.StartNum_lbl.TabIndex = 11;
            this.StartNum_lbl.Text = "Начальный индекс:";
            // 
            // StartNum_tb
            // 
            this.StartNum_tb.Location = new System.Drawing.Point(705, 24);
            this.StartNum_tb.Name = "StartNum_tb";
            this.StartNum_tb.Size = new System.Drawing.Size(141, 20);
            this.StartNum_tb.TabIndex = 12;
            this.StartNum_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StartNum_tb_KeyPress);
            // 
            // ListOfFilterParam_lbl
            // 
            this.ListOfFilterParam_lbl.AutoSize = true;
            this.ListOfFilterParam_lbl.Location = new System.Drawing.Point(861, 31);
            this.ListOfFilterParam_lbl.Name = "ListOfFilterParam_lbl";
            this.ListOfFilterParam_lbl.Size = new System.Drawing.Size(90, 13);
            this.ListOfFilterParam_lbl.TabIndex = 13;
            this.ListOfFilterParam_lbl.Text = "Список шифров:";
            // 
            // ListOfFilterParam_tb
            // 
            this.ListOfFilterParam_tb.Location = new System.Drawing.Point(958, 31);
            this.ListOfFilterParam_tb.Name = "ListOfFilterParam_tb";
            this.ListOfFilterParam_tb.Size = new System.Drawing.Size(218, 20);
            this.ListOfFilterParam_tb.TabIndex = 14;
            // 
            // ImportDirForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1316, 505);
            this.Controls.Add(this.ListOfFilterParam_tb);
            this.Controls.Add(this.ListOfFilterParam_lbl);
            this.Controls.Add(this.StartNum_tb);
            this.Controls.Add(this.StartNum_lbl);
            this.Controls.Add(this.ContinueDBNum_chb);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.AddToDB_btn);
            this.Controls.Add(this.ConvertTree_btn);
            this.Controls.Add(this.Dependency_dgv);
            this.Controls.Add(this.DirTree_tv);
            this.Controls.Add(this.ScanDir_bt);
            this.Controls.Add(this.DirPath_tb);
            this.Controls.Add(this.DirPath_lbl);
            this.Controls.Add(this.SetDir_bt);
            this.Name = "ImportDirForm";
            this.Text = "Добавить в каталог";
            ((System.ComponentModel.ISupportInitialize)(this.Dependency_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DirPath_lbl;
        private System.Windows.Forms.TextBox DirPath_tb;
        private System.Windows.Forms.Button SetDir_bt;
        private System.Windows.Forms.Button ScanDir_bt;
        private System.Windows.Forms.TreeView DirTree_tv;
        private System.Windows.Forms.DataGridView Dependency_dgv;
        private System.Windows.Forms.Button ConvertTree_btn;
        private System.Windows.Forms.Button AddToDB_btn;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox ContinueDBNum_chb;
        private System.Windows.Forms.Label StartNum_lbl;
        private System.Windows.Forms.TextBox StartNum_tb;
        private System.Windows.Forms.Label ListOfFilterParam_lbl;
        private System.Windows.Forms.TextBox ListOfFilterParam_tb;
    }
}