﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataModel.AppLogger
{


    public static class Logger 
    {

        private static ILogger logger = null;

        public static bool IsLogEnableb { get; set; }

        public static LogLevel MinimalLogLevel { get; set; }

        public static string FileLogName { get; set; }

        public static string DBLogName { get; set; }

        private static object locker = new object();

        private static ThreadLog thLog;

        static Logger()
        {

            Debug.WriteLine("static AppLoger()");
            IsLogEnableb = true;
            MinimalLogLevel = LogLevel.Warning;
            thLog = new ThreadLog();
            FileLogName = "FileLogName";
            DBLogName = "DBLog";

        }

        //static AppLogger(string LogName, string FilePath = "")
        //{
        //    string path;
        //    if (FilePath == string.Empty)
        //    {
        //        path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
        //        if (!Directory.Exists(FilePath))
        //        {
        //            Directory.CreateDirectory(path);
        //        }
        //    }
        //    else
        //    {
        //        path = FilePath;
        //    }

        //    logFile = Path.Combine(path, LogName);

        //    sw = new StreamWriter(logFile, true, Encoding.UTF8, 1024);
        //    sw.AutoFlush = true;

        //}

        public static void Log(LogTarget logTarget, LogLevel logLevel, string Message)
        {
            if (!IsLogEnableb)
            {
                return;
            }

            if (((int)MinimalLogLevel == (int)LogLevel.None) || ((int)logLevel > (int)MinimalLogLevel))
            {
                return;
            }

            switch (logTarget)
            {
                case LogTarget.FileLogger:
                    logger = new FileLogger(LogName: FileLogName);
                    logger.Init();
                    lock (locker)
                    {
                        logger.Log(logLevel, Message);
                    }
                    break;
                case LogTarget.DBLogger:
                    logger = new FileLogger(LogName: DBLogName);
                    logger.Log(logLevel, Message);
                    break;
                default:
                    break;
            }

            LogData ld = new LogData();
            ld.logLevel = logLevel;
            ld.logTarget = logTarget;
            ld.Message = Message;

        }

        public static void Dispose()
        {
            if (logger != null)
            {
                
                logger.Dispose();
            }
        }
    }


    //+-+-+-+-+-+-+-+-+-+-+-+-+++++++++++++++++++++++

     class ThreadLog : IDisposable
    {
        private ConcurrentQueue<LogData> mLogQueue;
        private ManualResetEventSlim mEnqueueEvent;
        private Task mTaskLog;
        FileLogger logger;

        public ThreadLog()
        {
            mLogQueue = new ConcurrentQueue<LogData>();
            mEnqueueEvent = new ManualResetEventSlim(false);

            logger = new FileLogger(LogName: "FileLogger");
            //mTaskLog = Task.Factory.StartNew(action: this.LogThread,cancellationToken:null,state: null, 
            //    creationOptions: TaskCreationOptions.LongRunning,scheduler: TaskScheduler.Default);

            mTaskLog = Task.Factory.StartNew(action: this.LogThread);
        }

        public void Add(LogData LD)
        {
            LD.Message += " |ThreadLog Add";
            mLogQueue.Enqueue(LD);
            this.mEnqueueEvent.Set();
        }

        public void LogThread()
        {
            LogData log;

            //if (this.mEnqueueEvent.Wait(100000))
            //{
            //    this.mEnqueueEvent.Reset();
            //}

            if (this.mLogQueue.TryDequeue(out log))
            {
                Debug.WriteLine("this.mLogQueue.TryDequeue(out log)");
                logger.Init();
                do
                {
                    //Debug.WriteLine("this.mLogQueue.TryDequeue(out log) Do___||___" + log.Message);
                    logger.Log(log.logLevel, log.Message);
                    Debug.WriteLine("this.mLogQueue.TryDequeue(out log) Do task");
                    
                } while (this.mLogQueue.TryDequeue(out log));
                logger.Dispose();
            }
        }

        private bool _disposed;

        public void Dispose()
        {
            //Dispose(true);
            //GC.SuppressFinalize(this);

            if (mLogQueue.Count > 0)
            {
                LogThread();

            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    throw new NotImplementedException();
                }

                _disposed = true;
            }
        }
    }
}
