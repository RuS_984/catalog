﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AppLogger
{
    public struct LogData
    {
        public LogTarget logTarget;
        public LogLevel logLevel;
        public string Message;
    }
}
