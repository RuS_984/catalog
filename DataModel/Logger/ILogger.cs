﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AppLogger
{
    public interface ILogger
    {
        void Init();
        void Log(LogLevel logLevel, string Message);
        void Dispose();
    }
}
