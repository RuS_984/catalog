﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AppLogger
{
    public class FileLogger : ILogger
    {


        private static StreamWriter sw;
        private static string logFile;

        private static object locker = new object();

        static FileLogger()
        {
            Debug.WriteLine("ctor Logger");
            Debug.WriteLine("Logger stared");
        }

        public FileLogger(string LogName, string FilePath = "",string LogExtension = "log")
        {
            string path;
            if (FilePath == string.Empty)
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");

            }
            else
            {
                path = FilePath;
            }

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logFile = Path.Combine(path, LogName + "." + LogExtension);
        }

        public void Init()
        {
            if (sw == null)
            {
                sw = new StreamWriter(logFile, true, Encoding.UTF8, 1024);
                sw.AutoFlush = true;
            }
        }

        public void Log(LogLevel logLevel, string Message)
        {
            string str = string.Format("{0} ({1}): {2}",
                logLevel.ToString().PadRight(8),
                DateTime.Now.ToString("yyyy.MM.dd HH:MM:ss"),
                Message
                );
            Debug.WriteLine(str);
            sw.WriteLine(str);
            sw.Dispose();
            sw = null;
            //throw new NotImplementedException();
        }

        public void Dispose()
        {
            lock (locker)
            {
                //sw.WriteLine("-------------------------------------------");
                //sw.Flush();
                //sw.Close();
            }
            //sw.Close();
            //sw = null;
        }
    }
}
