﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;
using System.Windows.Forms;

using DataModel.SupportClasses;

namespace DBData
{
    public static class MainDB1
    {
        static DataTable mainDBdt = new DataTable();
        static DataSet mainDBds = new DataSet();
        static SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter();

        static string db_req = "";
        static string[] colNames;

        const int firstMinInvNum = 0;

        static int minInvNum = -1;

        static SqlCeConnection CECon
        {
            get { return CommonData.SQLConn; }
        }

        public static DataTable MainDBdt
        {
            get
            {
                if (mainDBdt == null)
                {
                    GetMainDB(CommonData.LastFilterParam);
                    return mainDBdt;
                }
                else
                {
                    return mainDBdt;
                }
            }
        }

        public static int MaxInvNum
        {
            get {
                int i = GetMaxInvNum();
                return i; }
        }

        public static int MinInvNum
        {
            get
            {
                int i = GetMinInvNum() ;
                if (i > 0)
                {
                    i = 0;
                }

                return i;
            }
        }

        static MainDB1()
        {
            //mainDBdt = null;
            //mainDBds = null;
        }

        public delegate void DBUpdate();

        public static event DBUpdate OnDBUpdate;

        /// <summary>
        /// Получить полную таблицу из БД
        /// </summary>
        /// <param name="ordered_by">Столбец для упорядочивания</param>
        //++DONE
        //+++++++++++++++++++++
        public static void GetMainDB(string ordered_by)
        {

            mainDBdt.Reset();
            mainDBds.Reset();

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {

                if (ordered_by == null)
                {
                    db_req = DBprocessing.SQLRequest();
                }
                else
                {
                    db_req = DBprocessing.SQLRequest(ordered_by: ordered_by);
                }

                dataAdapter = new SqlCeDataAdapter(db_req, CECon);

                dataAdapter.Fill(mainDBds, "documents");

                mainDBdt = mainDBds.Tables["documents"];

                colNames = new string[mainDBdt.Columns.Count];


                for (int i = 0; i < mainDBdt.Columns.Count; i++)
                {
                    colNames[i] = mainDBdt.Columns[i].Caption;
                }

                for (int i = mainDBdt.Rows.Count - 1; i >= 0; i--)
                {
                    if (mainDBdt.Rows[i][1].ToString() == "0")
                    {
                        mainDBdt.Rows.RemoveAt(i);
                        break;
                    }
                }
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }

            }
            finally
            {

                CECon.Close();
            }

        } //GetMainDB

        /// <summary>
        /// Вополнение комманды SQL
        /// </summary>
        /// <param name="SQLCommand">SQL выражение</param>
        /// <returns>Количество обработанных строк</returns>
        public static int SQLExec11xxx(SqlCeCommand SQLCommand)
        {
            //ceCon = CommonData.SQLConn;

            int i = -1;

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

            }

            finally
            {

                CECon.Close();
            }




            return i;

        }

        public static string SQLRequest1xxx(
                        string ordered_by = "",//documents.inv_num
                        string search_txt = "",
                        string search_col = "")
        {
            string select = "";
            string search = "";
            string order = "";

            if (ordered_by != "")
            {
                order = "ORDER BY " + ordered_by;
            }

            //select = @"SELECT
            //            documents.id,documents.inv_num,project.project_name,documents.oboznachenie,
            //            documents.naimenovanie,documents.pervichprimen,documents.date, documents.is_asm,
            //            iRaz.second_name razrabotal, iProv.second_name proveril,iTehKon.second_name tehcontrol,
            //            iNormKon.second_name normocontrol, iUtverdil.second_name utverdil
            //           FROM documents
            //            JOIN project ON project.project_id=documents.project_id
            //            JOIN ispolniteli iRaz ON documents.razrabotal=iRaz.ispoln_id
            //            JOIN ispolniteli iProv ON documents.proveril=iProv.ispoln_id
            //            JOIN ispolniteli iTehKon ON documents.tehcontrol=iTehKon.ispoln_id
            //            JOIN ispolniteli iNormKon ON documents.normcontrol=iNormKon.ispoln_id
            //            JOIN ispolniteli iUtverdil ON documents.utverdil=iUtverdil.ispoln_id
            //           ORDER BY documents.inv_num";
            select = @"SELECT
                        documents.id, documents.inv_num as [Инвентарный номер],
                        project.project_name as [Название проекта],
                        documents.oboznachenie as [Обозначение],
                        documents.naimenovanie as [Наименование],
                        documents.pervichprimen as [Первичное преминение],
                        documents.date as [Дата подписания],
                        documents.is_asm as [Сборка],
                        iRaz.second_name as [Разработал],
                        iProv.second_name as  [Проверил],
                        iTehKon.second_name as  [Тех контроль],
                        iNormKon.second_name as [Нормоконтроль],
                        iUtverdil.second_name as [Утвердил]
                       FROM documents
                        JOIN project ON project.project_id=documents.project_id
                        JOIN ispolniteli iRaz ON documents.razrabotal=iRaz.ispoln_id
                        JOIN ispolniteli iProv ON documents.proveril=iProv.ispoln_id
                        JOIN ispolniteli iTehKon ON documents.tehcontrol=iTehKon.ispoln_id
                        JOIN ispolniteli iNormKon ON documents.normcontrol=iNormKon.ispoln_id
                        JOIN ispolniteli iUtverdil ON documents.utverdil=iUtverdil.ispoln_id
                        " + order;//ORDER BY documents.inv_num";
            if (search_txt != "" & search_col != "")
            {
                search = String.Format("WHERE documents.{0} LIKE '%{1}%'", search_col, search_txt);

            }

            return select + "\r\n" + search;

        }
        public static int DeleteRecordByRow(DataGridViewRow Row)//++DONE
        {
            int iDocuments = 0;
            int iDependency = 0;

            SqlCeCommand cmdDocuments = new SqlCeCommand();

            cmdDocuments = new SqlCeCommand(
                @"DELETE FROM documents where id = @id",
                CommonData.SQLConn);

            cmdDocuments.Parameters.Add("@id", SqlDbType.Int).Value = (int)Row.Cells[0].Value;

            SqlCeCommand cmdDependency = new SqlCeCommand();
            cmdDependency = new SqlCeCommand(@"DELETE FROM dependency where documents_id = @id", CECon);

            cmdDependency.Parameters.Add("@id", SqlDbType.Int).Value = (int)Row.Cells[1].Value;

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                iDocuments += cmdDocuments.ExecuteNonQuery();
                iDependency += cmdDependency.ExecuteNonQuery();

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            finally
            {

                CECon.Close();
            }

            return iDocuments + iDependency;
        }

        
        /// <summary>
        /// Метод создания новой записи
        /// </summary>
        /// <param name="mdbs"></param>
        /// <returns></returns>
        public static int CreateRecord(MainDBStruct1 mdbs)//++DONE
        {
            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();

            /*
            _cmd = new SqlCeCommand(
                @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", CECon);


            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            */
            _cmd = GetSqlCommand(mdbs);

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }

            }
            finally
            {

                CECon.Close();
            }
            return i;

        }
        public static int CreateRecords(List<MainDBStruct1> lstmdbs)//++DONE
        {
            int i = 0;
            SqlCeCommand _cmd = new SqlCeCommand();
            /*
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(
                @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", CECon);


            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            */

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                for (int j = 0; j < lstmdbs.Count; j++)
                {
                    _cmd = GetSqlCommand(lstmdbs[j]);
                    i += _cmd.ExecuteNonQuery();



                }
            }
            finally
            {
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
                CECon.Close();
            }

            return i;

        }

        static SqlCeCommand GetSqlCommand(MainDBStruct1 mdbs)
        {
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(
                @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", CECon);


            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;

            return _cmd;

        }
        public static int UpdateRecord(MainDBStruct1 mdbs)//++DONE
        {
            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();

            _cmd = new SqlCeCommand(@"UPDATE documents
            SET project_id = @project_id, inv_num = @inv_num,
                oboznachenie = @oboznachenie, naimenovanie = @naimenovanie,
                pervichprimen = @pervichprimen, literal=@literal, date = @date,
                is_asm = @is_asm, razrabotal = @razrabotal,
                proveril = @proveril, tehcontrol = @tehcontrol,
                normcontrol = @normcontrol, utverdil = @utverdil
                where id = @id", CECon);

            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = mdbs.Id;

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            finally
            {
                CECon.Close();
            }
            return i;

        }

        public static int UpdateRecordByCol(
            string ColName, int ColValue, string IdRange)
        {

            int i = -1;
            SqlCeCommand _cmd = new SqlCeCommand();

            _cmd = new SqlCeCommand(@"UPDATE documents
                SET "+ColName+@" = @colval
                where id IN "+IdRange, CECon);

            _cmd.Parameters.Add("@colval", SqlDbType.Int).Value = ColValue;

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }

            }
            finally
            {

                CECon.Close();
            }
            return i;

        }


        public static int GetMaxInvNum()
        {
            int i = 0;

            SqlCeCommand _cmd = new SqlCeCommand();

            _cmd = new SqlCeCommand(@"SELECT MAX(inv_num) FROM documents", CECon);

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                int.TryParse(_cmd.ExecuteScalar().ToString(), out i);

            }
            finally
            {
                CECon.Close();
            }

            return i;

        }


        public static int GetMinInvNum()
        {
            int i = 0;

            SqlCeCommand _cmd = new SqlCeCommand();

            _cmd = new SqlCeCommand(@"SELECT MIN(inv_num) FROM documents", CECon);

            if (CECon.State == ConnectionState.Closed)
            {
                CECon.Open();
            }

            try
            {
                int.TryParse(_cmd.ExecuteScalar().ToString(), out i);

            }
            finally
            {
                CECon.Close();
            }

            return i;

        }

        public struct MainDBStruct1
        {
            public int Id;

            public int InventarnNum;

            public int ProjectID;

            public string Oboznachenie;

            public string Naimenovanie;

            public string Pervichoepreminenie;

            public int Literal;

            public string Date;

            public bool IsAsm;

            public int Razrabotal;

            public int Proveril;

            public int Tehcontrol;

            public int Normokontrol;

            public int Utverdil;

            public void Clear()
            {
                Id = -1;
                InventarnNum = -1;
                ProjectID = -1;
                Oboznachenie = string.Empty;
                Naimenovanie = string.Empty;
                Pervichoepreminenie = string.Empty;
                Literal = -1;
                Date = string.Empty;
                IsAsm = false;
                Razrabotal = -1;
                Proveril = -1;
                Tehcontrol = -1;
                Normokontrol = -1;
                Utverdil = -1;

            }

            public void SetDefaultData()
            {
                Id = -1;
                InventarnNum = -1;
                ProjectID = 1;
                Oboznachenie = string.Empty;
                Naimenovanie = string.Empty;
                Pervichoepreminenie = string.Empty;
                Literal = 1;
                Date = "2016.10.25 00:00:00.000";
                IsAsm = false;
                Razrabotal = 1;
                Proveril = 1;
                Tehcontrol = 1;
                Normokontrol = 1;
                Utverdil = 1;

            }

            public override bool Equals(object obj)
            {
                if (obj != null && obj is MainDBStruct1)
                {
                    string thisDate = DateTime.Parse(((MainDBStruct1)obj).Date).ToString("yyyy.MM.dd");
                    string objDate  = DateTime.Parse(((MainDBStruct1)obj).Date).ToString("yyyy.MM.dd");

                    if (((MainDBStruct1)obj).Id                     == this.Id 
                        && ((MainDBStruct1)obj).InventarnNum        == this.InventarnNum
                        && ((MainDBStruct1)obj).ProjectID           == this.ProjectID
                        && ((MainDBStruct1)obj).Oboznachenie        == this.Oboznachenie
                        && ((MainDBStruct1)obj).Naimenovanie        == this.Naimenovanie
                        && ((MainDBStruct1)obj).Pervichoepreminenie == this.Pervichoepreminenie
                        && ((MainDBStruct1)obj).Literal             == this.Literal
                        &&                     objDate             ==      thisDate 
                        && ((MainDBStruct1)obj).Razrabotal          == this.Razrabotal
                        && ((MainDBStruct1)obj).Proveril            == this.Proveril
                        && ((MainDBStruct1)obj).Tehcontrol          == this.Tehcontrol
                        && ((MainDBStruct1)obj).Normokontrol        == this.Normokontrol
                        && ((MainDBStruct1)obj).Utverdil            == this.Utverdil
                        )
                    {
                        return true;
                    }
                }
                return false;
            }


            public static bool operator ==(MainDBStruct1 p1, MainDBStruct1 p2)
            {
                return p1.Equals(p2);
            }

            public static bool operator !=(MainDBStruct1 p1, MainDBStruct1 p2)
            {
                return !p1.Equals(p2);
            }


        }



    }
}





