﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

namespace DBData.Repository
{
    public class DocsVarRepository : RepositoryBase<Docs>
    {

        #region Внутренние переменные
        private readonly string Obozn;
        #endregion

        #region Внешние переменные
        public string Oboznachenie
        {
            get
            {
                return Obozn;
            }
        }

        public List<Docs> List
        {
            get
            {
                return _lst;
            }
        }
        #endregion

        #region Конструкторы
        public DocsVarRepository()
        {

        }

        public DocsVarRepository(SqlCeConnection SQLCEcon)
        {
            this.ceCon = SQLCEcon;
        }
        #endregion


        #region Методы
        public override void GetEntities()
        {
            if (ceCon.State == ConnectionState.Closed)
            {
                ceCon.Open();
            }

            _lst.Clear();

            ceCmd = new SqlCeCommand("SELECT * FROM docs where oboznachenie = @obozn",
                ceCon);
            ceCmd.Parameters.Add("@obozn", SqlDbType.NVarChar).Value = Obozn;


            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {

                _lst.Add(
                        new Docs(DocId: (int)ceDR[0], prjOboznachenie: (string)ceDR[1],
                                 DocName: (string)ceDR[2], Doc: new MemoryStream((byte[])ceDR[3])
                                )
                        );

            }
            ceDR.Close();

            ceCon.Close();
        }


        public void Add(Docs doc)
        {
            _lst.Add(doc);
        }

        public void Add(DocsVarRepository vardoc)
        {
            foreach (Docs item in vardoc)
            {
                _lst.Add(item);
            }
        }

        public void Add(DocsRepository vardoc)
        {
            foreach (Docs item in vardoc)
            {
                _lst.Add(item);
            }
        }

        public void Clear()
        {
            _lst.Clear();
        }

        public void Delete(int Id)
        {
            var selectedItems = from t in _lst
                                where t.Id == Id
                                select t;
            _lst.Remove(selectedItems.Single());
        }

        public void Delete(string Name)
        {
            var selectedItems = from t in _lst
                                where t.Name == Name
                                select t;
            _lst.Remove(selectedItems.Single());
        }

        public bool AddToDB()
        {
            int i = -1;
            {
                byte[] fileBytes;

                foreach (Docs doc in _lst)
                {
                    fileBytes = doc.Document.ToArray();

                    string cmd = string.Format(
                        @"insert into docs ([oboznachenie],[file_name], [doc])
                      values (@oboznachenie, @file_name, @doc)");

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, ceCon);

                    _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = Obozn;
                    _cmd.Parameters.Add("@file_name", SqlDbType.NVarChar).Value = doc.Name;
                    _cmd.Parameters.AddWithValue("@doc", fileBytes);

                    if (ceCon.State == ConnectionState.Closed)
                    {
                        ceCon.Open();
                    }

                    try
                    {
                        i += SQLExec_(_cmd);//_cmd.ExecuteScalar();
                                            //MessageBox.Show("Картинка добавлена");
                    }
                    catch (SqlCeException ex)
                    {
                        // TODO Протоколировать исключение
                        //MessageBox.Show(ex.Message);
                    }
                    if (ceCon.State == ConnectionState.Closed)
                    {
                        ceCon.Open();
                    }
                }
            }
            return i > 0;
        }
        #endregion
    }
}
