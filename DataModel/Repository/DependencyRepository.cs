﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;


using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;
using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class DependencyRepository : RepositoryBase<Dependency>
    {
        #region Внутренние переменные

        DependencyTableAbstract table;


        private string docId;
        private bool isAsm;
        bool changeAll;

        List<int> forDelete = new List<int>();
        List<int> forAdd = new List<int>();

        List<int> forDeleteRoot = new List<int>();
        List<int> forAddRoot = new List<int>();

        SortByName<Dependency> _sortByName = new SortByName<Dependency>();
        #endregion

        #region Внешние переменные

        public string DocId
        {
            get
            {
                return docId;
            }
            set
            {
                docId = value;
                changeAll = true;
            }
        }

        public List<Dependency> List
        {
            get
            {
                //_lst.Sort(_sortByName);
                return _lst;
            }
        }

        public bool IsAsm
        {
            get
            {
                return isAsm;
            }
            set
            {
                isAsm = value;
                foreach (Dependency item in _lst)
                {
                    item.IsAsm = IsAsm;
                }
                changeAll = true;
            }
        }
        #endregion

        #region Конструкторы
        public DependencyRepository(SqlCeConnection SQLCEcon, string Oboznachenie, int i)
        {
            ceCon = SQLCEcon;
            docId = Oboznachenie;
            GetEntities();
        }

        public DependencyRepository(DependencyTableAbstract Table, string Oboznachenie)
        {
            table = Table;
            docId = Oboznachenie;
            GetEntities();
        }



        #endregion

        #region Методы

        /// <summary>
        /// Получить данные из ДБ
        /// </summary>

        public override void GetEntities()
        {
            //_lst = table.GetEntities();
        }

        [ObsoleteAttribute]
        public void GetEntities1()
        {
            string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc.oboznachenie as doc_name

                ,dependency.parent_id
                ,idoc3.oboznachenie as parent_name

                ,dependency.is_asm

                ,idoc1.project_id
                ,idoc2.project_name

                FROM dependency
                JOIN documents idoc  ON dependency.documents_id = idoc.inv_num
                JOIN documents idoc3 ON dependency.parent_id    = idoc3.inv_num
                JOIN documents idoc1 ON dependency.documents_id = idoc1.inv_num
                JOIN project   idoc2 ON idoc1.project_id     	= idoc2.project_id

                order BY is_asm desc, documents_id";

            _lst.Clear();

            if (ceCon.State == ConnectionState.Closed)
            {
                ceCon.Open();
            }

            ceCmd = new SqlCeCommand(documents_req, ceCon);

            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(
                        new Dependency(
                            dependencyId: (int)ceDR[0],
                            documentID: (int)ceDR[1],
                            documentName: (string)ceDR[2],
                            parentID: (int)ceDR[3],
                            parentName: (string)ceDR[4],
                            isAsm: (bool)ceDR[5],
                            projectId: (int)ceDR[6],
                            projectName: (string)ceDR[7]
                            )
                        );
            }
            ceDR.Close();
            ceCon.Close();
        }

        /// <summary>
        /// Добавть сущность
        /// </summary>
        /// <param name="ParentID"></param>
        /// <param name="IsChild"></param>
        public void Add(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forAddRoot.Add(ParentID);
            }
            else
            {
                forAdd.Add(ParentID);
            }
        }

        public void Delete(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forAddRoot.Remove(ParentID);
            }
            else
            {
                forAdd.Remove(ParentID);
            }
        }

        public void DeleteDep(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forDeleteRoot.Add(ParentID);
            }
            else
            {
                forDelete.Add(ParentID);
            }
        }

        public void RetriveDep(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forDeleteRoot.Remove(ParentID);
            }
            else
            {
                forDelete.Remove(ParentID);
            }
        }

        /// <summary>
        /// Добавить набор сущностей
        /// </summary>
        /// <param name="dependencies">Список сущностей</param>
        public void AddList(IEnumerable<Dependency> dependencies)
        {
            if (dependencies == null)
            {
                throw new ArgumentNullException(nameof(dependencies));
            }

            _lst.AddRange(dependencies);
        }

        public bool UpdateDB2()
        {
            int i = 0;
            int y = 0;

            {//НОВЫЙ ВАРИАНТ
                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }

                foreach (int item in forAdd)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm])
                      values (@documents_id, @parent_id, @is_asm)");
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(docId);
                        _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = Convert.ToInt32(item);
                        _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(isAsm);

                        try
                        {
                            i += SQLExec_(_cmd);

                        }

                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                foreach (int item in forAddRoot)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm], [project_id])
                      values (@documents_id, @parent_id, @is_asm, @project_id)");
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(docId);
                        _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = 0;
                        _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(isAsm);
                        _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = Convert.ToInt32(item);

                        try
                        {
                            i += SQLExec_(_cmd);

                        }
                        finally
                        { }
                    }
                }

                if (forDelete.Count > 0)
                {
                    string cmd = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", docId.ToString(), GetDeleteIds(forDelete));

                    //string cmd1 = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", docId.ToString(), SupportClasses.DBprocessing.ListToString(forDelete));

                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        try
                        {
                            y += SQLExec_(_cmd);
                        }
                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                if (forDeleteRoot.Count > 0)
                {
                    //string cmd = string.Format(@"DELETE FROM dependency where ID in {0}", GetDeleteIds(forDelete));
                    string cmd = string.Format(@"DELETE FROM dependency where documents_id = {0} and project_id in {1}", docId.ToString(), GetDeleteIds(forDeleteRoot));
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        try
                        {
                            y += SQLExec_(_cmd);
                        }
                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                if (ceCon.State == ConnectionState.Open)
                {
                    ceCon.Close();
                }
            }
            return i > 0;
        }






        public bool UpdateDB()
        {
            bool res = table.UpdateDB(
                ForAdd: forAdd, ForDelete: forDelete,
                ForAddRoot: forAddRoot, ForDeleteRoot: forDeleteRoot,
                DocId: docId, IsAsm: isAsm);

            //List<int> ForAdd, List< int > ForDelete,
            //List<int> ForAddRoot, List< int > ForDeleteRoot,
            //string DocId, bool IsAsm

            return res;
        }



        public void ChangeDocumentID(int oldDocuments_id, int newDcuments_id, bool isAsm)
        {
            string cmd = string.Format(@"UPDATE dependency SET documents_id=@newDcuments_id, is_asm=@is_asm
                        WHERE ID in 
                        (select [id] from dependency where documents_id=@oldDocuments_id)");
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
            {
                _cmd.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
                _cmd.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;
                _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = isAsm;

                string cmd2 = string.Format(@"UPDATE dependency SET parent_id=@newDcuments_id WHERE ID in 
                        (select [id] from dependency where parent_id=@oldDocuments_id)");
                using (SqlCeCommand _cmd2 = new SqlCeCommand(cmd2, ceCon))
                {
                    _cmd2.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
                    _cmd2.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;

                    if (ceCon.State == ConnectionState.Closed)
                    {
                        ceCon.Open();
                    }

                    try
                    {
                        _cmd.ExecuteNonQuery();
                        _cmd2.ExecuteNonQuery();
                    }
                    finally
                    {
                        ceCon.Close();
                    }
                }
            }
        }

        public bool AddDependencyFromDirStruct(int DocumentId, int ParentId, bool IsAsm, int ProjectId = 0)
        {
            int i = 0;
            //int y = 0;

            {//НОВЫЙ ВАРИАНТ
                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }
                string cmd = string.Format(
                    @"insert into dependency ([documents_id],[parent_id], [is_asm], [project_id])
                      values (@documents_id, @parent_id, @is_asm, @project_id)");
                using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                {
                    _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = DocumentId;
                    _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = ParentId;
                    _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(IsAsm);
                    _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = ProjectId;

                    try
                    {
                        i += SQLExec_(_cmd);

                    }

                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }
            }
            return i > 0;
        }


        public string GetDeleteIds(List<int> list)
        {
            string _str = "(";
            foreach (int item in list)
            {
                _str += item + ", ";
            }

            _str = _str.Substring(0, _str.Length - 2) + ")";

            return _str;
        }

        public Dependency GetDepByDocId(int Id)
        {
            var selectedItems = from t in _lst
                                where t.DocumentID == Id
                                select t;
            return selectedItems.Single();

        }


        public DataTable GetDependency1Table(string obooboznachenie)
        {
            return table.GetDependency1Table(obooboznachenie);  
        }

        public DataTable GetDependency2Table(string obooboznachenie)
        {
            return table.GetDependency2Table(obooboznachenie);
        }



        #endregion


        public override List<string> GetList()
        {
            var selectedItems = from t in _lst
                                orderby t.Id
                                select "ID: " + t.Id.ToString() + "  DocId:" + t.DocumentID + "  ParentID:" + t.ParentID + "  IsAsm:" + t.IsAsm;
            return selectedItems.ToList<string>();
        }
    }



    public class DependencyRepository_Table : RepositoryBase<Dependency>
    {
        #region Внутренние переменные

        DependencyTableAbstract table;


        private string docId;
        private bool isAsm;
        bool changeAll;

        List<int> forDelete = new List<int>();
        List<int> forAdd = new List<int>();

        List<int> forDeleteRoot = new List<int>();
        List<int> forAddRoot = new List<int>();

        SortByName<Dependency> _sortByName = new SortByName<Dependency>();
        #endregion

        #region Внешние переменные

        public string DocId
        {
            get
            {
                return docId;
            }
            set
            {
                docId = value;
                changeAll = true;
            }
        }

        public List<Dependency> List
        {
            get
            {
                //_lst.Sort(_sortByName);
                return _lst;
            }
        }

        public bool IsAsm
        {
            get
            {
                return isAsm;
            }
            set
            {
                isAsm = value;
                foreach (Dependency item in _lst)
                {
                    item.IsAsm = IsAsm;
                }
                changeAll = true;
            }
        }
        #endregion

        #region Конструкторы

        public DependencyRepository_Table(DependencyTableAbstract Table, string Oboznachenie)
        {
            table = Table;
            docId = Oboznachenie;
            GetEntities();
        }



        #endregion

        #region Методы

        /// <summary>
        /// Получить данные из ДБ
        /// </summary>

        public override void GetEntities()
        {
            _lst = table.GetEntities();
        }

        [ObsoleteAttribute]
        public void GetEntities1()
        {
            string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc.oboznachenie as doc_name

                ,dependency.parent_id
                ,idoc3.oboznachenie as parent_name

                ,dependency.is_asm

                ,idoc1.project_id
                ,idoc2.project_name

                FROM dependency
                JOIN documents idoc  ON dependency.documents_id = idoc.inv_num
                JOIN documents idoc3 ON dependency.parent_id    = idoc3.inv_num
                JOIN documents idoc1 ON dependency.documents_id = idoc1.inv_num
                JOIN project   idoc2 ON idoc1.project_id     	= idoc2.project_id

                order BY is_asm desc, documents_id";

            _lst.Clear();

            if (ceCon.State == ConnectionState.Closed)
            {
                ceCon.Open();
            }

            ceCmd = new SqlCeCommand(documents_req, ceCon);

            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(
                        new Dependency(
                            dependencyId: (int)ceDR[0],
                            documentID: (int)ceDR[1],
                            documentName: (string)ceDR[2],
                            parentID: (int)ceDR[3],
                            parentName: (string)ceDR[4],
                            isAsm: (bool)ceDR[5],
                            projectId: (int)ceDR[6],
                            projectName: (string)ceDR[7]
                            )
                        );
            }
            ceDR.Close();
            ceCon.Close();
        }

        /// <summary>
        /// Добавть сущность
        /// </summary>
        /// <param name="ParentID"></param>
        /// <param name="IsChild"></param>
        public void Add(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forAddRoot.Add(ParentID);
            }
            else
            {
                forAdd.Add(ParentID);
            }
        }

        public void Delete(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forAddRoot.Remove(ParentID);
            }
            else
            {
                forAdd.Remove(ParentID);
            }
        }

        public void DeleteDep(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forDeleteRoot.Add(ParentID);
            }
            else
            {
                forDelete.Add(ParentID);
            }
        }

        public void RetriveDep(int ParentID, bool IsChild)
        {
            if (!IsChild)
            {
                forDeleteRoot.Remove(ParentID);
            }
            else
            {
                forDelete.Remove(ParentID);
            }
        }

        /// <summary>
        /// Добавить набор сущностей
        /// </summary>
        /// <param name="dependencies">Список сущностей</param>
        public void AddList(IEnumerable<Dependency> dependencies)
        {
            if (dependencies == null)
            {
                throw new ArgumentNullException(nameof(dependencies));
            }

            _lst.AddRange(dependencies);
        }

        public bool UpdateDB1()
        {
            int i = 0;
            int y = 0;

            {//НОВЫЙ ВАРИАНТ
                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }

                foreach (int item in forAdd)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm])
                      values (@documents_id, @parent_id, @is_asm)");
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(docId);
                        _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = Convert.ToInt32(item);
                        _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(isAsm);

                        try
                        {
                            i += SQLExec_(_cmd);

                        }

                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                foreach (int item in forAddRoot)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm], [project_id])
                      values (@documents_id, @parent_id, @is_asm, @project_id)");
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(docId);
                        _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = 0;
                        _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(isAsm);
                        _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = Convert.ToInt32(item);

                        try
                        {
                            i += SQLExec_(_cmd);

                        }
                        finally
                        { }
                    }
                }

                if (forDelete.Count > 0)
                {
                    string cmd = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", docId.ToString(), GetDeleteIds(forDelete));

                    //string cmd1 = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", docId.ToString(), SupportClasses.DBprocessing.ListToString(forDelete));

                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        try
                        {
                            y += SQLExec_(_cmd);
                        }
                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                if (forDeleteRoot.Count > 0)
                {
                    //string cmd = string.Format(@"DELETE FROM dependency where ID in {0}", GetDeleteIds(forDelete));
                    string cmd = string.Format(@"DELETE FROM dependency where documents_id = {0} and project_id in {1}", docId.ToString(), GetDeleteIds(forDeleteRoot));
                    using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                    {
                        try
                        {
                            y += SQLExec_(_cmd);
                        }
                        //catch (SqlCeException ex)
                        //{
                        //    throw ex;

                        //}
                        finally
                        { }
                    }
                }

                if (ceCon.State == ConnectionState.Open)
                {
                    ceCon.Close();
                }
            }
            return i > 0;
        }


        public bool UpdateDB()
        {
            bool res = table.UpdateDB(
                ForAdd: forAdd, ForDelete: forDelete,
                ForAddRoot: forAddRoot, ForDeleteRoot: forDeleteRoot,
                DocId: docId, IsAsm: isAsm);

            //List<int> ForAdd, List< int > ForDelete,
            //List<int> ForAddRoot, List< int > ForDeleteRoot,
            //string DocId, bool IsAsm

            return res;
        }



        public void ChangeDocumentID(int oldDocuments_id, int newDcuments_id, bool isAsm)
        {
            table.ChangeDocumentID(oldDocuments_id, newDcuments_id, isAsm);

            /*
           string cmd = string.Format(@"UPDATE dependency SET documents_id=@newDcuments_id, is_asm=@is_asm
                       WHERE ID in 
                       (select [id] from dependency where documents_id=@oldDocuments_id)");
           using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
           {
               _cmd.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
               _cmd.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;
               _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = isAsm;

               string cmd2 = string.Format(@"UPDATE dependency SET parent_id=@newDcuments_id WHERE ID in 
                       (select [id] from dependency where parent_id=@oldDocuments_id)");
               using (SqlCeCommand _cmd2 = new SqlCeCommand(cmd2, ceCon))
               {
                   _cmd2.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
                   _cmd2.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;

                   if (ceCon.State == ConnectionState.Closed)
                   {
                       ceCon.Open();
                   }

                   try
                   {
                       _cmd.ExecuteNonQuery();
                       _cmd2.ExecuteNonQuery();
                   }
                   finally
                   {
                       ceCon.Close();      
                   }
               }



           }
           */
        }

        public bool AddDependencyFromDirStruct(int DocumentId, int ParentId, bool IsAsm, int ProjectId = 0)
        {
            int i = 0;
            //int y = 0;

            {//НОВЫЙ ВАРИАНТ
                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }
                string cmd = string.Format(
                    @"insert into dependency ([documents_id],[parent_id], [is_asm], [project_id])
                      values (@documents_id, @parent_id, @is_asm, @project_id)");
                using (SqlCeCommand _cmd = new SqlCeCommand(cmd, ceCon))
                {
                    _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = DocumentId;
                    _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = ParentId;
                    _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(IsAsm);
                    _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = ProjectId;

                    try
                    {
                        i += SQLExec_(_cmd);


                    }

                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }
            }
            return i > 0;
        }


        public string GetDeleteIds(List<int> list)
        {
            string _str = "(";
            foreach (int item in list)
            {
                _str += item + ", ";
            }

            _str = _str.Substring(0, _str.Length - 2) + ")";

            return _str;
        }

        public Dependency GetDepByDocId(int Id)
        {
            var selectedItems = from t in _lst
                                where t.DocumentID == Id
                                select t;
            return selectedItems.Single();

        }


        #endregion


        public override List<string> GetList()
        {
            var selectedItems = from t in _lst
                                orderby t.Id
                                select "ID: " + t.Id.ToString() + "  DocId:" + t.DocumentID + "  ParentID:" + t.ParentID + "  IsAsm:" + t.IsAsm;
            return selectedItems.ToList<string>();
        }
    }






}
