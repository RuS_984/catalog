﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;



//using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class ProjectRepository : RepositoryBase<Project>
    {
        ProjectTableAbstract table;

        public ProjectRepository(ProjectTableAbstract Table)
        {
            table = Table;
            base.OnUpdate += GetEntities;
            GetEntities();
        }

        public override void GetEntities()
        {
            _lst = table.GetEntities();
        }


        public  bool AddToDB(string value)
        {
            bool res =  table.AddToDB(value);

            GetEntities();

            return res;
        }

        public bool DeleteFromDB(string value)
        {
            int _id = GetIdByValue(value);

            bool res = table.DeleteFromDB(value, _id);

            GetEntities();

            return res;
        }

        public bool RenameInDB(string oldValue, string newValue)
        {
            int _id = GetIdByValue(oldValue);

            bool res = table.RenameInDB(oldValue,newValue, _id);

            GetEntities();

            return res;
        }
    }
}
