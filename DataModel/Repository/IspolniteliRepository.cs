﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class IspolniteliRepository : RepositoryBase<Ispolniteli>//, IRepository<Ispolniteli>
    {
        IspolniteliTableAbstract table;

        public IspolniteliRepository(IspolniteliTableAbstract Table)
        {
            //ceCon = SQLCEcon;
            table = Table;
            GetEntities();
        }

        public override void GetEntities()
        {
            _lst = table.GetEntities();
        }

        public bool AddToDB(string text)
        {
            bool res;

            res = table.AddToDB(text);

            GetEntities();

            return res;
        }
        public bool DeleteFromDB(string value)
        {
            bool res;

            int _id = GetIdByValue(value);

            res = table.DeleteFromDB(value, _id);

            GetEntities();

            return res;
        }

        public bool RenameInDB(string oldValue, string newValue)
        {
            bool res;

            int _id = GetIdByValue(oldValue);

            res = table.RenameInDB(oldValue, newValue, _id);

            GetEntities();

            return res;
        }
    }
}
