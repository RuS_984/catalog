﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;
using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class InventarNumberRepository : RepositoryBase<InventarNumber>
    {
        #region Внутренние переменные
        InventarNumberTableAbstract table;
        #endregion

        #region Внешние переменные

        #endregion

        #region Конструкторы
        public InventarNumberRepository(InventarNumberTableAbstract Table)
        {
            table = Table;
            GetEntities();
        }
        #endregion

        public override void GetEntities()
        {
            _lst = table.GetEntities();
            /*
            if (ceCon.State == ConnectionState.Closed)
            {
                ceCon.Open();
            }

            _lst.Clear();

            ceCmd = new SqlCeCommand("SELECT id, inv_num FROM documents", ceCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {

                _lst.Add(new InventarNumber((int)ceDR[0], (int)ceDR[1]));

            }
            ceDR.Close();
            ceCon.Close();
            */
        }

        public override List<string> GetList()
        {
            var selectedItems = from t in _lst
                                orderby t.Name
                                select t.Name.ToString();
            return selectedItems.ToList<string>();
        }

        private bool AddToDB(string value)
        {
            return true;
        }

        private bool DeleteFromDB(string value)
        {
            return true;
        }

        private bool RenameInDB(string oldValue, string newValue)
        {
            return true;
        }
    }
}
