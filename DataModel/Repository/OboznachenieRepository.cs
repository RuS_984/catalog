﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class OboznachenieRepository : RepositoryBase<Oboznachenie>//, IRepository<Oboznachenie>
    {
        OboznachenieTableAbstract table;

        public OboznachenieRepository(OboznachenieTableAbstract Table)
        {
            table = Table;
            GetEntities();
        }

        public override void GetEntities()
        {
            _lst = table.GetEntities();
        }

        private new bool AddToDB(string value)
        {
            return false;
        }

        private new bool DeleteFromDB(string value)
        {
            return false;
        }

        private new bool RenameInDB(string oldValue, string newValue)
        {
            return false;
        }
    }
}
