﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;
//using DataModel.SupportClasses;
using DataModel.DBWrapper.Providers.Abstract;

namespace DBData.Repository
{
    public class FirstUsageRepository : RepositoryBase<FirstUsage>//, IRepository<PervichnoePrimenenie>
    {
        FirstUsageTableAbstract table;

        public FirstUsageRepository(FirstUsageTableAbstract Table)
        {
            table = Table;
            GetEntities();
        }

        public override void GetEntities()
        {
            _lst = table.GetEntities();
        }

        private bool AddToDB(string value)
        {
            return true;
        }

        private bool DeleteFromDB(string value)
        {
            return true;
        }

        private bool RenameInDB(string oldValue, string newValue)
        {
            return true;
        }
    }
}
