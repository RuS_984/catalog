﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;


using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

namespace DBData.Repository
{
    /// <summary>
    /// Репозиторий приложений
    /// </summary>
    public class DocsRepository : RepositoryBase<Docs>
    {
        #region Внутренние переменные
        private string Obozn;
        List<int> forDelete = new List<int>();

        SortByName<Docs> _sortByName = new SortByName<Docs>();
        #endregion


        #region Внешние переменные
        /// <summary>
        /// Обозначение документа
        /// </summary>
        public string Oboznachenie
        {
            get
            {
                return Obozn;
            }
            set
            {
                Obozn = value;
            }
        }

        public List<Docs> List
        {
            get
            {
                _lst.Sort(_sortByName);
                return _lst;
            }
        }
        #endregion


        #region Конструкторы
        public DocsRepository(SqlCeConnection SQLCEcon, string Oboznachenie)
        {
            ceCon = SQLCEcon;
            Obozn = Oboznachenie;
            GetEntities();
        }
        #endregion


        #region Методы
        /// <summary>
        /// Получить данные из ДБ
        /// </summary>
        public override void GetEntities()
        {
            _lst.Clear();

            if (Obozn != "")
            {
                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }

                ceCmd = new SqlCeCommand("SELECT * FROM docs where oboznachenie = @obozn",
                    ceCon);
                ceCmd.Parameters.Add("@obozn", SqlDbType.NVarChar).Value = Obozn;

                ceDR = ceCmd.ExecuteReader();
                while (ceDR.Read())
                {

                    _lst.Add(
                            new Docs(DocId: (int)ceDR[0], prjOboznachenie: (string)ceDR[1],
                                     DocName: (string)ceDR[2], Doc: new MemoryStream((byte[])ceDR[3])
                                    )
                            );
                }
                ceDR.Close();

                ceCon.Close();
            }
        }

        /// <summary>
        /// Добавть сущность
        /// </summary>
        /// <param name="doc">Экземпляр сущности</param>
        public void Add(Docs doc)
        {
            _lst.Add(doc);
        }

        /// <summary>
        /// Добавить набор сущностей
        /// </summary>
        /// <param name="doc">Список сущностей</param>
        public void AddList(List<Docs> doc)
        {
            _lst.AddRange(doc);
       }

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="doc">Экземпляр сущности</param>
        public void DeleteDoc(Docs doc)
        {
            if (!doc.IsNew)
            {
                forDelete.Add(doc.Id);
            }
            _lst.Remove(doc);
        }

        //*----------------------------------DBProcess
        public bool UpdateDB()
        {
            int i = 0;
            int y = 0;

            {//НОВЫЙ ВАРИАНТ
                byte[] fileBytes;

                if (ceCon.State == ConnectionState.Closed)
                {
                    ceCon.Open();
                }

                foreach (Docs doc in _lst)
                {
                    if (doc.IsNew == false)
                    {
                        continue;
                    }

                    fileBytes = doc.Document.ToArray();

                    string cmd = string.Format(
                        @"insert into docs ([oboznachenie],[file_name], [doc])
                      values (@oboznachenie, @file_name, @doc)");

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, ceCon);

                    _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = Obozn;
                    _cmd.Parameters.Add("@file_name", SqlDbType.NVarChar).Value = doc.Name;
                    _cmd.Parameters.AddWithValue("@doc", fileBytes);

                    try
                    {
                        i += SQLExec_(_cmd);

                    }
                    catch (SqlCeException ex)
                    {
                        throw ex;

                    }
                }

                if (forDelete.Count > 0)
                {
                    string cmd = string.Format(@"DELETE FROM docs where ID in {0}", GetDeleteIds());

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, ceCon);
                    try
                    {
                        y += SQLExec_(_cmd);
                    }
                    catch (SqlCeException ex)
                    {
                        throw ex;
                    }
                }

                if (ceCon.State == ConnectionState.Open)
                {
                    ceCon.Close();
                }
            }
            return i > 0;
        }

        public string GetDeleteIds()
        {
            string _str = "(";
            foreach (int item in forDelete)
            {
                _str += item + ", ";
            }

            _str = _str.Substring(0, _str.Length - 2) + ")";

            return _str;
        }
        #endregion

    }
}
