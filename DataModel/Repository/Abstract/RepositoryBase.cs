﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data;

using System.Windows.Forms;

using System.Runtime.InteropServices;


using DBData.Entities;

namespace DBData.Repository
{
    /// <summary>
    /// Абстрактный класс репозитория
    /// </summary>
    /// <typeparam name="T">Класс типа Entity</typeparam>
    public abstract class RepositoryBase<T> : IDisposable
        where T : Entity
    {
        #region Внутренние переменные
        protected SqlCeConnection ceCon;
        protected SqlCeCommand ceCmd = new SqlCeCommand();
        protected SqlCeDataReader ceDR;
        protected SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter();

        protected List<T> _lst = new List<T>();
        #endregion

        #region Внешние переменные
        /// <summary>
        /// Количество элементов репозитория
        /// </summary>
        public int Count
        {
            get
            {
                return _lst.Count;
            }
        }
        #endregion

        #region События

        public delegate void Onupdate();

        /// <summary>
        /// Возникает при обновлении репозитория
        /// </summary>
        public event Onupdate OnUpdate;

        #endregion

        #region Конструкторы

        #endregion

        #region Методы

        /// <summary>
        /// Вополнение комманды SQL
        /// </summary>
        /// <param name="SQLCommand">SQL выражение</param>
        /// <returns>Количество обработанных строк</returns>
        protected int SQLExec_(SqlCeCommand SQLCommand)
        {
            int i = -1;

            if (ceCon.State == ConnectionState.Closed)
            {
                ceCon.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

                if (OnUpdate != null)
                {
                    OnUpdate();
                }
            }
            //catch (SqlCeException ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    throw ex;
            //}
            finally
            {
                ceCon.Close();
            }
            return i;
        }

        /// <summary>
        /// Получение списка наименований из репозитория
        /// </summary>
        /// <returns>Список элементов репозитория</returns>
        public virtual List<string> GetList()
        {
            if (OnUpdate != null)
            {
                OnUpdate();
            }

            var selectedItems = from t in _lst
                                orderby t.Name
                                select t.Name;
            return selectedItems.ToList<string>();
        }

        /// <summary>
        /// Добаление сущности в базу
        /// </summary>
        /// <returns></returns>
        private bool AddToDB(string value) { return false; }

        /// <summary>
        /// Удаление сущности по идентификатору
        /// </summary>
        private bool DeleteFromDB(string value) { return false; }

        /// <summary>
        /// Переименование сущности по идентификатору
        /// </summary>
        private bool RenameInDB(string oldValue, string newValue) { return false; }

        /// <summary>
        /// Обновление списка из базы
        /// </summary
        public abstract void GetEntities();// { }

        /// <summary>
        /// Получение идентификатора по названию
        /// </summary>
        /// <param name="IspName"></param>
        /// <returns></returns>
        public int GetIdByValue(string IspName)
        {
            var selectedItems = from t in _lst
                                where t.Name.ToUpper() == IspName.ToUpper()
                                select t.Id;
            return selectedItems.FirstOrDefault();

        }


        public BindingSource GetBindingSource()
        {

            _lst.Sort(new SortByName<T>());

            BindingSource bs = new BindingSource();
            bs.DataSource = _lst;
            return bs;

        }

        public bool ContaisValue(string Value)
        {
            //var selectedItems = from t in _lst
            //                    where t.Name.ToUpper() == Value.ToUpper()
            //                    select t.Id;
            return _lst.Count(p => p.Name.ToUpper() == Value.ToUpper()) > 0;

        }

        #endregion

        #region Итераторы и индексаторы

        public T this[string Name]
        {
            //set { }
            get
            {
                var selectedItems = from   t in _lst
                                    where  t.Name.ToUpper() == Name.ToUpper()
                                    select t;
                return selectedItems.Single();
            }
        }

        public T this[int Id]
        {
            //set { }
            get
            {
                var selectedItems = from t in _lst
                                    where t.Id == Id
                                    select t;
                return selectedItems.Single();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (T entity in _lst)
            {
                yield return entity;
            }
        }


        public bool ContainsID(int Id)
        {

            //var selectedItems = from t in _lst
            //                    where t.Id == Id
            //                    select t.Id;
            int number = _lst.Count(p => p.Id == Id);
            return number > 0;
            //turn false;
        }


        public bool ContainsName(string Name)
        {
            int number = _lst.Count(p => p.Name.ToLower() == Name.ToLower());
            return number > 0;
        }

        #endregion

        public void Dispose()
        {
            ((IDisposable)ceCon).Dispose();
        }
    }

    /// <summary>
    /// Сортировка по имени
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class SortByName<T> : IComparer<T> where T : Entity

    {
        public int Compare(T x, T y)
        {
            return SortTypes.StrCmpLogicalW(x.Name, y.Name);

        }
    }
    /// <summary>
    /// Сотрировка по идентификатору
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class SortById<T> : IComparer<T> where T : Entity
    {
        // Реализуем интерфейс IComparer<T>
        public int Compare(T x, T y)
        {
            if (x.Id < y.Id)
            {
                return 1;
            }

            if (x.Id > y.Id)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }

    static class SortTypes
    {

        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
        public static extern int StrCmpLogicalW(string psz1, string psz2);
    }


}
