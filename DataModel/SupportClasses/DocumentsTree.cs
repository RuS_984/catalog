﻿using System;
using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;


using DBData.Repository;
using DBData.Entities;
using System.Drawing;

using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DataModel.SupportClasses
{

    public class DocumentsTree
    {

        #region private
        DataTable dependencyTable;
        DataTable projectTable;
        string currentOboznachenie;
        DependencyRepository dependencyRep;

        public TreeNode treeNode = new TreeNode();
        TreeNode currentNode;

        MyNode myNode;
        MyNode myNodedoc;


        DataSet documents_ds1 = new DataSet();
        DataTable documents_dt1 = new DataTable();
        SqlCeDataAdapter dataAdapter;


        public TreeView tView;// = new TreeView();
        private TreeViewCancelEventHandler checkForCheckedChildren;

        List<string> checkedNodes;

        #endregion


        #region public
        public TreeView TView
        {
            //private get { };
            set
            {
                treeNode.Nodes.Clear();
                if (treeNode.Nodes.Count == 0)
                {
                    BuildTree();
                }

                foreach (TreeNode tn in treeNode.Nodes)
                {
                    //tn.Text = "from DocumentsTree";
                    value.Nodes.Add(tn);
                }

                checkForCheckedChildren =
                    new TreeViewCancelEventHandler(CheckForCheckedChildrenHandler);

            }
        }
        #endregion


        #region CTOR
        public DocumentsTree(DependencyRepository DependencyRep,
                DataTable DependencyTable, DataTable ProjectTable,
                string CurrentOboznachenie = "")
        {
            this.dependencyTable = DependencyTable;
            this.projectTable = ProjectTable;
            currentOboznachenie = CurrentOboznachenie;

            dependencyRep = DependencyRep;

        }


        #endregion


        public void SetAsm(bool state)
        {
            dependencyRep.IsAsm = state;
        }

        public void SetObozn(string obozn)
        {
            dependencyRep.DocId = obozn;
        }

        #region TreeNode Generation
        /// <summary>
        ///  Заполнение дерева из БД
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="dt"></param>
        public void BuildTree()
        {
            treeNode.Nodes.Clear();
            List<string> nname = new List<string>();

            foreach (DataRow prjrow in projectTable.Rows)
            {
                TreeNode project_node = new TreeNode(prjrow["project_name"].ToString());
                project_node.Name = project_node.Text;

                myNode.dependencyId = (int)prjrow["project_id"];
                myNode.documentId = (int)prjrow["project_id"];
                myNode.IsChild = false;
                project_node.Tag = myNode;

                DataRow[] documents_rows = dependencyTable.Select(
                string.Format("project_id = {0}", ((MyNode)project_node.Tag).documentId)
                    );
                foreach (DataRow docrow in documents_rows)
                {
                    if (docrow["oboznachenie"].ToString() == currentOboznachenie)
                    {
                        continue;
                    }

                    TreeNode node = new TreeNode(docrow["oboznachenie"].ToString());
                    node.Name = node.Text;
                    myNodedoc.dependencyId = (int)docrow["ID"];
                    myNodedoc.documentId = (int)docrow["documents_id"];
                    myNodedoc.IsChild = true;
                    node.Tag = myNodedoc;
                    //Debug.WriteLine("node" + node.Text + "  IsChild:" + myNodedoc.IsChild);

                    node.Name = docrow["oboznachenie"].ToString();

                    if (!nname.Contains(prjrow["project_name"].ToString()))
                    {
                        project_node.Nodes.Add(node);
                        treeNode.Nodes.Add(project_node);
                        nname.Add(prjrow["project_name"].ToString());
                    }
                    else
                    {
                        project_node.Nodes.Add(node);
                    }

                    FillNode(node, dependencyTable);
                    node = null;
                }
                if (documents_rows.Length == 0)
                {
                    treeNode.Nodes.Add(project_node);
                }
            }
            foreach (TreeNode tn in treeNode.Nodes)
            {
                //tn.Text = "from DocumentsTree";
                //tView.Nodes.Add(tn);
            }
            //return treeNode;
        }

        /// <summary>
        ///  Добавление к узлу наследников
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="dt"></param>
        private void FillNode(TreeNode parentNode, DataTable dt)
        {
            DataRow[] rows = dt.Select(string.Format("Parent_Id = {0}", ((MyNode)parentNode.Tag).documentId));
            foreach (DataRow row in rows)
            {
                if (row["oboznachenie"].ToString() == currentOboznachenie)
                {
                    continue;
                }

                TreeNode tn = new TreeNode(row["oboznachenie"].ToString());
                tn.Name = tn.Text;
                tn.Tag = row["documents_id"];

                myNode.dependencyId = (int)row["ID"];
                myNode.documentId = (int)row["documents_id"];
                myNode.IsChild = true;

                tn.Tag = myNode;

                //Debug.WriteLine("treeNode" + tn.Text + "  IsChild:" + myNode.IsChild.ToString());

                tn.Name = row["oboznachenie"].ToString();
                parentNode.Nodes.Add(tn);
                FillNode(tn, dt);
            }
        }





        #endregion


        #region External TreeViev



        #region CheckDependency
        public void CheckDependency(string obooboznachenie)
        {

            checkedNodes = new List<string>();
            //this.tView.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);

            currentNode = new TreeNode(obooboznachenie);
            //_-----------------------
            /*string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.oboznachenie as parent_name


                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num

                JOIN documents idoc2 ON dependency.parent_id=idoc2.inv_num

                where idoc.oboznachenie = @oboznachenie  and idoc2.oboznachenie <> 'корень'
                order BY oboznachenie";

            SqlCeCommand _cmd = new SqlCeCommand();
            SqlCeConnection ceCon = new SqlCeConnection(null);//(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(documents_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");
            documents_dt1 = documents_ds1.Tables["documents"];   */
            //_--------------------------------------
            documents_dt1 = dependencyRep.GetDependency1Table(obooboznachenie);


            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 0);
            }

            //_------------------------------------

            /*string project_req =
                @" SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.project_name as parent_name

                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num
                JOIN project idoc2 ON dependency.project_id=idoc2.project_id
                
                where idoc.oboznachenie = @oboznachenie
                order BY project_name";

            documents_dt1.Clear();
            documents_ds1.Clear();

            _cmd = new SqlCeCommand();
            ceCon = new SqlCeConnection(null);//(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(project_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");

            documents_dt1 = documents_ds1.Tables["documents"];*/
            //_------------------------------------
            documents_dt1 = dependencyRep.GetDependency2Table(obooboznachenie);


            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 1);
            }



            //this.tView.Nodes.Clear();

            //foreach (TreeNode tn in treeNode.Nodes)
            //{
            //    //int i = 0;
            //    //tn.Text = "from DocumentsTree";
            //    this.tView.Nodes.Add(tn);
            //}

            //tView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);
        }

        private void FindNodeAndCheck(string _Name, int id = 0)
        {
            //foreach (TreeNode node in tView.Nodes)
            foreach (TreeNode node in treeNode.Nodes)
            {
                if (id == 0)
                {
                    TreeNode[] _tn;
                    _tn = treeNode.Nodes.Find(_Name, true);

                    foreach (TreeNode item in _tn)
                    {
                        //myNode.dependencyId = id;
                        //myNode.documentId = ((MyNode)(item.Tag)).documentId;
                        //item.Tag = myNode;
                        item.Checked = true;
                        item.BackColor = System.Drawing.Color.LightGray;

                        MyNode mn = (MyNode)item.Tag;
                        mn.isChecked = true;

                        item.Tag = mn;
                    }
                }
                else
                {
                    if (node.Text == _Name)
                    {
                        node.Checked = true;
                        node.BackColor = System.Drawing.Color.LightGray;
                    }
                }
            }
        }
        #endregion

        #region ShowChecked

        public void ShowChecked()
        {

            if (tView == null) return;

            this.tView.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);

            this.tView.Nodes.Clear();

            foreach (TreeNode tn in treeNode.Nodes)
            {
                //int i = 0;
                //tn.Text = "from DocumentsTree";
                this.tView.Nodes.Add(tn);
            }

            tView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);


            // Collapse all nodes of treeView1.
            tView.CollapseAll();

            // Add the checkForCheckedChildren event handler to the BeforeExpand event.
            tView.BeforeExpand += CheckForCheckedChildrenHandler;//checkForCheckedChildren;

            // Expand all nodes of treeView1. Nodes without checked children are
            // prevented from expanding by the checkForCheckedChildren event handler.
            tView.ExpandAll();

            // Remove the checkForCheckedChildren event handler from the BeforeExpand
            // event so manual node expansion will work correctly.
            tView.BeforeExpand -= CheckForCheckedChildrenHandler; //checkForCheckedChildren;

        }

        // Prevent expansion of a node that does not have any checked child nodes.
        private void CheckForCheckedChildrenHandler(object sender,
            TreeViewCancelEventArgs e)
        {
            if (!HasCheckedChildNodes(e.Node)) e.Cancel = true;
        }

        // Returns a value indicating whether the specified
        // TreeNode has checked child nodes.
        private bool HasCheckedChildNodes(TreeNode node)
        {
            if (node.Nodes.Count == 0)
            {
                return false;
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                if (childNode.Checked)
                {
                    return true;
                }
                // Recursively check the child ren of the current child node.
                if (HasCheckedChildNodes(childNode))
                {
                    return true;
                }
            }
            return false;
        }



        private void AfterCheck(object sender, TreeViewEventArgs e)
        {
            System.Drawing.Color clr = new System.Drawing.Color();
            //Dependency dep;

            #region Некорневые узлы

            if (e.Node.BackColor == System.Drawing.Color.LightGray && !e.Node.Checked)
            {
                e.Node.BackColor = System.Drawing.Color.LightCoral;

                //depRep.DeleteDep(depRep[(int)e.Node.Tag]);
                //depRep.DeleteDep(dep: depRep[
                //((MyNode)e.Node.Tag).dependencyId
                //],
                //IsChild: ((MyNode)e.Node.Tag).IsChild
                //);

                //if (((MyNode)e.Node.Tag).IsChild)
                {
                    dependencyRep.DeleteDep(ParentID: //depRep[
                    ((MyNode)e.Node.Tag).documentId,
                    IsChild: ((MyNode)e.Node.Tag).IsChild
                    );
                }
            }
            else if (e.Node.BackColor == System.Drawing.Color.LightCoral && e.Node.Checked)
            {
                {
                    e.Node.BackColor = System.Drawing.Color.LightGray;
                    dependencyRep.RetriveDep(ParentID: //depRep[
                        ((MyNode)e.Node.Tag).documentId
                        //]
                        ,
                        IsChild: ((MyNode)e.Node.Tag).IsChild
                        );
                }
            }
            //***----------------------------
            else if (e.Node.BackColor.Name == System.Drawing.Color.FromName("0").Name
               && e.Node.Checked)
            {
                clr = e.Node.BackColor;

                e.Node.BackColor = System.Drawing.Color.LightGreen;

                dependencyRep.Add(
                ParentID: ((MyNode)e.Node.Tag).documentId
                , IsChild: ((MyNode)e.Node.Tag).IsChild
                );
            }
            else if (e.Node.BackColor == System.Drawing.Color.LightGreen
               && !e.Node.Checked)
            {
                e.Node.BackColor = clr;

                if (((MyNode)e.Node.Tag).IsChild)
                {
                    dependencyRep.Delete(
                        ParentID: ((MyNode)e.Node.Tag).dependencyId
                        , IsChild: ((MyNode)e.Node.Tag).IsChild
                        );
                }
            }
            #endregion

            #region Корневые узлы
            if (e.Node.Parent == null)
            {

            }
            #endregion
        }




        #endregion


        public void UpdateDB(string InvNum, bool isAsm)
        {
            dependencyRep.DocId = InvNum;
            dependencyRep.IsAsm = isAsm;
            dependencyRep.UpdateDB();
        }


        #endregion



        public string GetRootTreeJSON()
        {



            //string json = JsonConvert.SerializeObject(employee, Formatting.Indented, new KeysJsonConverter(typeof(Employee)));

            //string json1 = JsonConvert.SerializeObject(treeNode, Formatting.Indented, new KeysJsonConverter(typeof(TreeNode)));




            string json3 = JsonConvert.SerializeObject(treeNode, Formatting.Indented);


            List<MyJSONNode> Nodes = new List<MyJSONNode>();

            foreach (TreeNode item in treeNode.Nodes)
            {
                MyJSONNode jn = new MyJSONNode();


                jn.text = item.Text;
                jn.documentId = ((MyNode)item.Tag).documentId;
                jn.hasChild = item.Nodes.Count > 0;
                jn.isRoot = true;
                jn.isChecked = item.Checked;

                Nodes.Add(jn);
            }


            string json4 = JsonConvert.SerializeObject(Nodes);



            return json4;
        }


        public string GetChildTreeJSON(string NodeText)
        {
            TreeNode[] node = treeNode.Nodes.Find(NodeText, true);


            return GetChildTreeJSON(node[0]);
        }



        public string GetChildTreeJSON(TreeNode node)
        {

            /*
              List<MyJSONNode> Nodes = new List<MyJSONNode>();

              foreach (TreeNode item in node.Nodes)
              {
                  MyJSONNode jn = new MyJSONNode();


                  jn.text = item.Text;
                  jn.documentId = ((MyNode)item.Tag).documentId;
                  jn.hasChild = item.Nodes.Count > 0;

                  Nodes.Add(jn);
              }


              string json4 = JsonConvert.SerializeObject(Nodes, Formatting.Indented);

              */

            MyJSONNodes Nodes = new MyJSONNodes();
            Nodes.SubNodes = new List<MyJSONNode>();

            Nodes.Count = node.Nodes.Count;


            if ((node.Parent != null) && (!string.IsNullOrEmpty(node.Parent.Text)))
            {
                MyJSONNode jn1 = new MyJSONNode();

                jn1.text = node.Parent.Text;
                jn1.documentId = ((MyNode)node.Parent.Tag).documentId;
                jn1.hasChild = node.Parent.Nodes.Count > 0;



                Nodes.ParentNode = jn1;
            }

            MyJSONNode jn2 = new MyJSONNode();

            jn2.text = node.Text;
            jn2.documentId = ((MyNode)node.Tag).documentId;
            jn2.hasChild = node.Nodes.Count > 0;
            jn2.isChecked = node.Checked;


            Nodes.CurrentNode = jn2;



            foreach (TreeNode item in node.Nodes)
            {
                MyJSONNode jn = new MyJSONNode();


                jn.text = item.Text;
                jn.documentId = ((MyNode)item.Tag).documentId;
                jn.hasChild = item.Nodes.Count > 0;
                jn.isChecked = item.Checked;

                Nodes.SubNodes.Add(jn);
            }


            string json4 = JsonConvert.SerializeObject(Nodes, Formatting.Indented);

            return json4;
        }





        public string GetAsmDependentTreeJSON()
        {

            List<string> res = new List<string>();
            List<string> temp = new List<string>();

            foreach (TreeNode item in treeNode.Nodes)
            {

                if (item.Nodes.Count > 0)
                {
                    temp = CheckNode(item);
                    res.Add(item.Text + "|1");
                    res.AddRange(temp);
                    continue;

                }

                if (item.Checked)
                {
                    res.Add(item.Text + "|0");
                }
            }



            List<MyJSONNode> Nodes = new List<MyJSONNode>();

            foreach (TreeNode item in treeNode.Nodes)
            {
                MyJSONNode jn = new MyJSONNode();


                jn.text = item.Text;
                jn.documentId = ((MyNode)item.Tag).documentId;
                jn.hasChild = item.Nodes.Count > 0;
                jn.isRoot = true;
                jn.isChecked = item.Checked;

                Nodes.Add(jn);
            }

            List<string> res1 = new List<string>();
            string[] s;
            for (int i = 0; i < res.Count; i++)
            {
                s = res[i].Split('|');

                if (s[1] == "1")
                {
                    res1.Add(s[0]);
                }


            }

            MyJSONRootNodes rootNodes = new MyJSONRootNodes();

            rootNodes.rootNodes = Nodes;
            rootNodes.opendList = res1;


            //string json4 = JsonConvert.SerializeObject(Nodes);

            string json4 = JsonConvert.SerializeObject(rootNodes);



            return json4;
        }



        List<string> CheckNode(TreeNode TN)
        {
            List<string> res = new List<string>();
            List<string> temp = new List<string>();

            foreach (TreeNode item in TN.Nodes)
            {
                //if (item.Checked)
                //{
                //    res.Add(item.Text);
                //}
                //if (item.Nodes.Count > 0)
                //{
                //    res.AddRange(CheckNode(item));
                //}


                if (item.Nodes.Count > 0)
                {
                    temp = CheckNode(item);
                    
                    if (temp.Count > 0)
                    {
                        res.Add(item.Text+"|1");
                        res.AddRange(temp);
                        continue;
                    }

                    if (item.Checked)
                    {
                        res.Add(item.Text + "|01");
                    }


                }

               if (item.Checked)
               {
                    res.Add(item.Text + "|0");
               }




            }

            return res;
        }





        public TreeNode FindNode(int documentId)
        {
            TreeNode tn;
            string text = dependencyRep.GetDepByDocId(documentId).Name;
            TreeNode[] node = treeNode.Nodes.Find(text, true);

            return node[0];
        }


        struct MyNode
        {
            public int dependencyId;
            public int documentId;
            public bool isChild;
            public bool isChecked;


            public bool IsChild
            {
                get { return isChild; }
                set
                {
                    isChild = value;
                    //Debug.WriteLine("isChild changed:"+isChild.ToString());
                }
            }
        }



        struct MyJSONNode
        {
            public string text;
            //public int dependencyId;
            public int documentId;
            public bool hasChild;
            public bool isRoot;
            public bool isChecked;



        }

        struct MyJSONRootNodes
        {
            public List<MyJSONNode> rootNodes;
            public List<string> opendList;


        }


        struct MyJSONNodes
        {
            public int Count;
            public MyJSONNode ParentNode;
            public MyJSONNode CurrentNode;
            public List<MyJSONNode> SubNodes;


        }


    }






}
