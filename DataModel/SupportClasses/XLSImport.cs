﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Data.SqlServerCe;

using DBData.Repository;
using DBData.Entities;
using System.Data;

using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

using System.Diagnostics;

using DBData;
using DataModel.DBWrapper;

namespace DataModel.SupportClasses
{
    public class XLSImport
    {
        SqlCeConnection CEcon = CommonData.SQLConn;

        ProjectRepository ProjectRep;

        LiteralRepository LiteralRep;

        IspolniteliRepository IspolniteliRep;
        InventarNumberRepository InventarNumberRep;

        OboznachenieRepository OboznachenieRep;

        XSSFWorkbook xssfworkbook;
        DataSet dataSet = new DataSet();
        DataTable dataTable = new DataTable();

        Dictionary<string, SqlCeParameter> sqlParametrs = new Dictionary<string, SqlCeParameter>();

        SqlCeConnection CECon = CommonData.SQLConn;

        private List<string> usedHeaders;
        private List<int> usedHeadersIndex;

        List<string> newOboznachenie = new List<string>();

        SqlCeCommand SQLcmd;

        private int minInvNum;

        private List<int> usedInvNum = new List<int>();

        const string RecDateStr = "2016-10-25 00:00:00.000";

        public DataTable XLSDataTable
        {
            get
            {
                return dataTable;
            }
        }

        public XLSImport()
        {
            LiteralRep = new LiteralRepository(CommonData.LiteralTable);
            IspolniteliRep = new IspolniteliRepository(CommonData.IspolniteliTable);
            InventarNumberRep = new InventarNumberRepository(CommonData.InventarNumberTable);
            OboznachenieRep = new OboznachenieRepository(CommonData.OboznachenieTable);

            sqlParametrs.Add("project_id", new SqlCeParameter("@project_id", SqlDbType.Int));
            sqlParametrs.Add("inv_num", new SqlCeParameter("@inv_num", SqlDbType.Int));
            sqlParametrs.Add("oboznachenie", new SqlCeParameter("@oboznachenie", SqlDbType.NVarChar));
            sqlParametrs.Add("naimenovanie", new SqlCeParameter("@naimenovanie", SqlDbType.NVarChar));
            sqlParametrs.Add("pervichprimen", new SqlCeParameter("@pervichprimen", SqlDbType.NVarChar));
            sqlParametrs.Add("literal", new SqlCeParameter("@literal", SqlDbType.Int));
            sqlParametrs.Add("date", new SqlCeParameter("@date", SqlDbType.DateTime));
            sqlParametrs.Add("is_asm", new SqlCeParameter("@is_asm", SqlDbType.Bit));
            sqlParametrs.Add("razrabotal", new SqlCeParameter("@razrabotal", SqlDbType.Int));
            sqlParametrs.Add("proveril", new SqlCeParameter("@proveril", SqlDbType.Int));
            sqlParametrs.Add("tehcontrol", new SqlCeParameter("@tehcontrol", SqlDbType.Int));
            sqlParametrs.Add("normcontrol", new SqlCeParameter("@normcontrol", SqlDbType.Int));
            sqlParametrs.Add("utverdil", new SqlCeParameter("@utverdil", SqlDbType.Int));

            minInvNum = CommonData.MainTable.MinInvNum - 1;//MainDB.MinInvNum - 1;
        }

        public void OpenXLS(string fileName)
        {
            FileStream FS = new FileStream(fileName, FileMode.Open, FileAccess.Read);

            try
            {
                xssfworkbook = new XSSFWorkbook(FS);
            }
            catch (Exception)
            {
                throw;
            }

            dataSet = ConvertToDataTable();
            dataTable = dataSet.Tables[0];
        }

        DataSet ConvertToDataTable()
        {
            DataSet dstmp = new DataSet();

            ISheet sheet = xssfworkbook.GetSheetAt(0);
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            DataTable dt = new DataTable();

            int collNum = sheet.GetRow(0).Cells.Count;

            for (int j = 0; j < collNum; j++)
            {
                dt.Columns.Add(j.ToString());
            }

            while (rows.MoveNext())
            {
                IRow row = (XSSFRow)rows.Current;
                DataRow dr = dt.NewRow();

                for (int i = 0; i < row.LastCellNum; i++)
                {
                    ICell cell = row.GetCell(i);

                    if (cell == null)
                    {
                        dr[i] = null;
                    }
                    else
                    {
                        dr[i] = cell.ToString();
                    }
                }
                dt.Rows.Add(dr);
            }
            dstmp.Tables.Add(dt);

            return dstmp;
        }

        public void InitImport(List<string> UsedHeaders, List<int> UsedHeadersIndex)
        {
            usedHeaders = UsedHeaders;
            usedHeadersIndex = UsedHeadersIndex;
            SQLcmd = GetSQLRequest();
            newOboznachenie.Clear();
        }

        public List<MainTableStruct> GenerateListOfSQLCommands(int StartRowNum)
        {
            List<MainTableStruct> tmpDBStruct = new List<MainTableStruct>();
            tmpDBStruct.Clear();
            MainTableStruct mainDBStruct = new MainTableStruct();

            usedInvNum.Clear();

            for (int i = StartRowNum; i < dataTable.Rows.Count; i++)
            {
                mainDBStruct = new MainTableStruct();
                mainDBStruct = CreateCommand(dataTable.Rows[i]);
                tmpDBStruct.Add(mainDBStruct);
            }
            return tmpDBStruct;
        }

        public MainTableStruct CreateCommand(DataRow dr)
        {
            MainTableStruct DBStruct = new MainTableStruct();
            DBStruct.SetDefaultData();

            for (int j = 0; j < usedHeaders.Count; j++)
            {
                string head = usedHeaders[j];
                string val = dr[usedHeadersIndex[j]].ToString();

                switch (head)
                {
                    case "inv_num":
                        if (val == string.Empty
                            || InventarNumberRep.ContainsName(val)
                            || usedInvNum.Contains(Convert.ToInt32(val))
                            )
                        {
                            DBStruct.InventarnNum = minInvNum--;
                        }
                        else
                        {
                            DBStruct.InventarnNum = Convert.ToInt32(val);
                            usedInvNum.Add(Convert.ToInt32(val));
                        }
                        Console.WriteLine("inv_num val={0} DBStruct.InventarnNum={1}",
                            val, DBStruct.InventarnNum);
                        break;
                    case "project_id":
                        if (ProjectRep.ContainsName(val))
                        {
                            DBStruct.ProjectID = ProjectRep[val].Id;
                        }
                        Console.WriteLine("project_id {0} DBStruct.ProjectID={1}",
                            val, DBStruct.ProjectID);
                        break;
                    case "oboznachenie":

                        if (val == string.Empty)
                        {
                            DBStruct.Oboznachenie = ("___" + DBprocessing.GetRandomString(15));
                        }
                        else if (OboznachenieRep.ContainsName(val) | newOboznachenie.Contains(val))
                        {
                            DBStruct.Oboznachenie = val + CommonData.rowRepeat + DBprocessing.GetRandomString(3);
                        }
                        else
                        {
                            DBStruct.Oboznachenie = val;
                            newOboznachenie.Add(val);
                        }
                        Console.WriteLine("oboznachenie {0} DBStruct.Oboznachenie={1}",
                            val, DBStruct.Oboznachenie);
                        break;
                    case "naimenovanie":
                        if (val == string.Empty)
                        {
                            DBStruct.Naimenovanie = string.Empty;
                        }
                        else
                        {
                            DBStruct.Naimenovanie = val;
                        }
                        Console.WriteLine("naimenovanie {0} DBStruct.Naimenovanie={1}",
                            val, DBStruct.Naimenovanie);
                        break;
                    case "pervichprimen":
                        if (val == string.Empty)
                        {
                            DBStruct.Pervichoepreminenie = string.Empty;
                        }
                        else
                        {
                            DBStruct.Pervichoepreminenie = val;
                        }
                        Console.WriteLine("pervichprimen {0} DBStruct.Pervichoepreminenie={1}",
                            val, DBStruct.Pervichoepreminenie);
                        break;
                    case "literal":
                        if (LiteralRep.ContainsName(val))
                        {
                            DBStruct.Literal = LiteralRep[val].Id;
                        }
                        Console.WriteLine("literal {0} DBStruct.Literal={1}",
                            val, DBStruct.Literal);
                        break;
                    case "date":
                        if (val != string.Empty)
                        {
                            DBStruct.Date = val;
                        }
                        Console.WriteLine("date {0} DBStruct.Date={1}",
                            val, DBStruct.Date);
                        break;
                    case "is_asm":
                        if (val != string.Empty)
                        {
                            DBStruct.IsAsm = Convert.ToBoolean(val);
                        }
                        Console.WriteLine("is_asm {0} DBStruct.IsAsm={1}",
                            val, DBStruct.IsAsm.ToString());
                        break;
                    case "proveril":
                        if (IspolniteliRep.ContainsName(val))
                        {
                            DBStruct.Proveril = IspolniteliRep[val].Id;
                        }
                        Console.WriteLine("proveril {0} DBStruct.Proveril={1}",
                            val, DBStruct.Proveril);
                        break;
                    case "razrabotal":
                        if (IspolniteliRep.ContainsName(val))
                        {
                            DBStruct.Razrabotal = IspolniteliRep[val].Id;
                        }
                        Console.WriteLine("razrabotal {0} DBStruct.Razrabotal={1}",
                            val, DBStruct.Razrabotal);
                        break;
                    case "tehcontrol":
                        if (IspolniteliRep.ContainsName(val))
                        {
                            DBStruct.Tehcontrol = IspolniteliRep[val].Id;
                        }
                        Console.WriteLine("tehcontrol {0} DBStruct.Tehcontrol={1}",
                            val, DBStruct.Tehcontrol);
                        break;
                    case "normcontrol":
                        if (IspolniteliRep.ContainsName(val))
                        {
                            DBStruct.Normokontrol = IspolniteliRep[val].Id;
                        }
                        Console.WriteLine("normcontrol {0} DBStruct.Normokontrol={1}",
                            val, DBStruct.Normokontrol);
                        break;
                    case "utverdil":
                        if (IspolniteliRep.ContainsName(val))
                        {
                            DBStruct.Utverdil = IspolniteliRep[val].Id;
                        }
                        Console.WriteLine("utverdil {0} DBStruct.Utverdil={1}",
                            val, DBStruct.Utverdil);
                        break;
                }

                if (DBStruct.InventarnNum == -1)
                {
                    DBStruct.InventarnNum = minInvNum--;
                }

                if (DBStruct.Oboznachenie == string.Empty)
                {
                    DBStruct.Oboznachenie = DBprocessing.GetRandomString();
                }
            }
            Console.WriteLine("---------------{0}------------------------");
            return DBStruct;
        }

        private SqlCeCommand GetSQLRequest()
        {
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();

            for (int i = 0; i < usedHeaders.Count; i++)
            {
                if (i == usedHeaders.Count - 1)
                {
                    sb1.Append(usedHeaders[i]);
                    sb2.Append("@" + usedHeaders[i]);
                }
                else
                {
                    sb1.Append(usedHeaders[i] + ",");
                    sb2.Append("@" + usedHeaders[i] + ",");
                }
            }

            string sqlCmd = string.Format(
                @"INSERT INTO documents
                ({0})
                 VALUES
                ({1})",
                sb1.ToString(),
                sb2.ToString()
                );
            SqlCeCommand _cmd = new SqlCeCommand(
                sqlCmd
                , CECon);
            return _cmd;
        }
    }
}
