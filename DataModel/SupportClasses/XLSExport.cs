﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

using NPOI;
using NPOI.XSSF.UserModel;

using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.POIFS.FileSystem;




using System.IO;

namespace DataModel.SupportClasses
{
    public class XLSExport
    {
        string[] columns;
        DataGridViewRowCollection rows;
        List<int> colIndex = new List<int>();

        XSSFWorkbook workbookReport;
        ISheet sheetReport;

        public XLSExport(string[] Columns, DataGridViewRowCollection Rows)
        {
            columns = Columns;
            rows = Rows;
        }

        public void GenerateXLS()
        {
            workbookReport = new XSSFWorkbook();
            sheetReport = workbookReport.CreateSheet("1");
            sheetReport = workbookReport.GetSheetAt(0);

            ICellStyle stylegreen = workbookReport.CreateCellStyle();
            stylegreen.FillForegroundColor = IndexedColors.LightGreen.Index;
            stylegreen.FillPattern = FillPattern.SolidForeground;
            stylegreen.FillBackgroundColor = IndexedColors.LightGreen.Index;
            stylegreen.WrapText = true;

            int rowIndex = 0;

            IRow XLSrow = sheetReport.CreateRow(0);

            for (int j = 0; j < columns.Length; j++)
            {
                if (columns[j] != string.Empty)
                {
                    XLSrow.CreateCell(rowIndex).SetCellValue(columns[j]);
                    sheetReport.AutoSizeColumn(rowIndex);
                    colIndex.Add(j);
                    rowIndex++;
                }
            }

            rowIndex = 0;
            int i = 0;

            for (int j = 0; j < rows.Count; j++)
            {
                rowIndex++;
                XLSrow = sheetReport.CreateRow(j+1);

                for (int k = 0; k < colIndex.Count; k++)
                {
                    XLSrow.CreateCell(k).SetCellValue( //"hfgdhgdf"
                        rows[j].Cells[colIndex[k]].Value.ToString()
                        );
                    i++;
                }
            }

            for (int j = 0; j < colIndex.Count; j++)
            {
                sheetReport.AutoSizeColumn(j);
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.InitialDirectory = Application.StartupPath;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                workbookReport.Write(new FileStream(saveFileDialog1.FileName
                    , FileMode.Create));
                MessageBox.Show("Done");
            }
        }
    }
}
