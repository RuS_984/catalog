﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.Data;
using System.IO;
using System.Windows.Forms;



using DBData.Entities;

namespace DataModel.SupportClasses
{
        /// <summary>
        /// Класс работы с БД
        /// </summary>
        [System.Runtime.InteropServices.Guid("41D9B562-E700-487A-8AF1-B620B2C95BBC")]
        public class DBRecord11rr

           {

            SqlCeConnection CEcon;
            SqlCeCommand CEcmd = new SqlCeCommand();
            //SqlCeDataReader CEdr;
            SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter();

            private string db_req = "";

            public DataTable Full_DB_dt = new DataTable();
           DataSet Full_DB_ds = new DataSet();

            string[] colNames;


       public DBRecord11rr(SqlCeConnection SQLCEcon)
                {
                  CEcon = SQLCEcon;
                  Update();
                }

        /// <summary>
        /// Получить полную таблицу из БД
        /// </summary>
        /// <param name="ordered_by">Столбец для упорядочивания</param>
        public void Get_Full_DB(string ordered_by =  "documents.inv_num")
        {

            Full_DB_dt.Reset();
            Full_DB_ds.Reset();

            if (CEcon.State == ConnectionState.Closed)
            {
                CEcon.Open();
            }

            if (ordered_by == null)
            {
                db_req = DBprocessing.SQLRequest();
            }
            else
            {
            db_req = DBprocessing.SQLRequest(ordered_by: ordered_by);
            }


            dataAdapter = new SqlCeDataAdapter(db_req, CEcon);

            dataAdapter.Fill(Full_DB_ds, "documents");

            Full_DB_dt = Full_DB_ds.Tables["documents"];

            colNames = new string[Full_DB_dt.Columns.Count];


            for (int i = 0; i < Full_DB_dt.Columns.Count; i++)
            {
                colNames[i] = Full_DB_dt.Columns[i].Caption;
            }



            for (int i = Full_DB_dt.Rows.Count - 1; i >= 0; i--)
            {
                if (Full_DB_dt.Rows[i][1].ToString() == "0")
                {
                    Full_DB_dt.Rows.RemoveAt(i);
                    break;
                }
            }

            CEcon.Close();

            //dataAdapter = null;
            //Full_DB_ds = null;
            //Full_DB_dt = null;
        } //Get_Full_DB()

        public void DeleteRecord(DataGridViewRow Row)
        {

            //DataGridViewRow row = new DataGridViewRow();
            //row = Row;//this.dataGridView1.CurrentRow;

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"DELETE FROM documents  where id = @id", CEcon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = (int)Row.Cells[0].Value;

            //SqlCeCommand _cmd2 = new SqlCeCommand();
            //_cmd2 = new SqlCeCommand(@"DELETE FROM documents  where oboznachenie = @oboznachenie", CEcon);

            //_cmd2.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = Row.Cells[3].Value.ToString();

            SqlCeCommand _cmd2 = new SqlCeCommand();
            _cmd2 = new SqlCeCommand(@"DELETE FROM dependency  where documents_id = @id", CEcon);

            _cmd2.Parameters.Add("@id", SqlDbType.Int).Value = (int)Row.Cells[1].Value;



            if (CEcon.State == ConnectionState.Closed)
            {
                CEcon.Open();
            }

            try
            {
                _cmd.ExecuteNonQuery();
                _cmd2.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                // Протоколировать исключение
                MessageBox.Show(ex.Message);
            }

            CEcon.Close();

        }

        public string[] GetColumnNames()
        {
            return colNames;

        }

        /// <summary>
        /// Обновление всех данных из БД
        /// </summary>
        public void Update()
            {
            Get_Full_DB();

        }
            }




}