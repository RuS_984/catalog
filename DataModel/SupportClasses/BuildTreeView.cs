﻿using System;
using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;


using DBData.Repository;
using DBData.Entities;
using System.Drawing;


namespace DataModel.SupportClasses
{
    public class BuildTreeView
    {
        readonly string documents_req = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie

            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            JOIN documents idoc1 ON dependency.documents_id=idoc1.inv_num
            order BY is_asm desc, documents_id";


        string documents_req2 = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie
                
            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            where dependency.is_asm = 1
            order BY is_asm desc, documents_id";


        DataSet documents_ds = new DataSet();
        DataTable documents_dt = new DataTable();


        DataSet documents_ds1 = new DataSet();
        DataTable documents_dt1 = new DataTable();


        string projects_req = @"SELECT * FROM project
                                order BY project_name ";

        DataSet projects_ds = new DataSet();
        DataTable projects_dt = new DataTable();

        SqlCeDataAdapter dataAdapter;

        TreeView TView;

        TreeNode currentNode;

        DependencyRepository depRep;

        SqlCeConnection sqlceCon;

        private TreeViewCancelEventHandler checkForCheckedChildren;

        MyNode myNode;
        MyNode myNodedoc;

        string currentOboznachenie;

        public BuildTreeView(TreeView treeView, SqlCeConnection SQLCEcon, 
            string CurrentOboznachenie = "")
        {
            this.TView = treeView;
            this.sqlceCon = SQLCEcon;
            currentOboznachenie = CurrentOboznachenie;

            depRep = new DependencyRepository(sqlceCon,"", -1);


            checkForCheckedChildren =
            new TreeViewCancelEventHandler(CheckForCheckedChildrenHandler);


            this.TView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);



            //this.TView.DrawMode = TreeViewDrawMode.OwnerDrawText;
            //this.TView.DrawNode +=
            //    new DrawTreeNodeEventHandler(myTreeView_DrawNode);


        }


        public void SetAsm(bool state)
        {
            depRep.IsAsm = state;
        }

        public void SetObozn(string obozn)
        {
            depRep.DocId = obozn;
        }

        /// <summary>
        /// Генерация полного дерева
        /// </summary>
        public void GetFullTree()
        {
            dataAdapter = new SqlCeDataAdapter(documents_req, sqlceCon.ConnectionString);
            dataAdapter.Fill(documents_ds, "documents");
            documents_dt = documents_ds.Tables["documents"];

            dataAdapter = new SqlCeDataAdapter(projects_req, sqlceCon.ConnectionString);
            dataAdapter.Fill(projects_ds, "documents");
            projects_dt = projects_ds.Tables["documents"];


            BuildTree();


        }

        /// <summary>
        /// Генерация дерева сборочных единиц
        /// </summary>
        public void GetAsmTree()
        {

            dataAdapter = new SqlCeDataAdapter(documents_req2, sqlceCon.ConnectionString);
            dataAdapter.Fill(documents_ds, "documents");
            documents_dt = documents_ds.Tables["documents"];

            dataAdapter = new SqlCeDataAdapter(projects_req, sqlceCon.ConnectionString);
            dataAdapter.Fill(projects_ds, "documents");
            projects_dt = projects_ds.Tables["documents"];


            BuildTree();


        }



        #region BuildTree

        /// <summary>
        ///  Заполнение дерева из БД
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="dt"></param>
        public void BuildTree()
        {

            TView.Nodes.Clear();
            List<string> nname = new List<string>();

            foreach (DataRow prjrow in projects_dt.Rows)
            {
                TreeNode project_node = new TreeNode(prjrow["project_name"].ToString());
                myNode.dependencyId = (int)prjrow["project_id"];
                myNode.documentId = (int)prjrow["project_id"];
                myNode.IsChild = false;
                project_node.Tag = myNode;
                //Debug.WriteLine("project_node:"+project_node.Text + "  IsChild:" + myNode.IsChild);


                DataRow[] documents_rows = documents_dt.Select(
                    string.Format("project_id = {0}", ((MyNode)project_node.Tag).documentId)
                    );
                foreach (DataRow docrow in documents_rows)
                {

                    if (docrow["oboznachenie"].ToString() == currentOboznachenie)
                    {
                        continue;
                    }

                    TreeNode node = new TreeNode(docrow["oboznachenie"].ToString());
                    //node.Tag = docrow["documents_id"];
                    //node.Text = docrow["oboznachenie"].ToString() + Environment.NewLine + docrow["naimenovanie"].ToString();
                    //Debug.WriteLine(docrow["oboznachenie"].ToString() + Environment.NewLine + docrow["naimenovanie"].ToString());
                    myNodedoc.dependencyId = (int)docrow["ID"];
                    myNodedoc.documentId = (int)docrow["documents_id"];
                    myNodedoc.IsChild = true;
                    node.Tag = myNodedoc;
                    Debug.WriteLine("node"+node.Text + "  IsChild:" + myNodedoc.IsChild);

                    //node.Text += "  " + myNodedoc.IsChild;

                    node.Name = docrow["oboznachenie"].ToString();


                    if (!nname.Contains(prjrow["project_name"].ToString()))
                    {
                        project_node.Nodes.Add(node);
                        TView.Nodes.Add(project_node);
                        nname.Add(prjrow["project_name"].ToString());
                    }
                    else
                    {
                        project_node.Nodes.Add(node);

                    }


                    FillNode(node, documents_dt);
                    node = null;
                }
                if (documents_rows.Length == 0)
                {
                    TView.Nodes.Add(project_node);
                }
            }





            }





        /// <summary>
        ///  Добавление к узлу наследников
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="dt"></param>
        private void FillNode(TreeNode parentNode, DataTable dt)
        {
            DataRow[] rows = dt.Select(string.Format("Parent_Id = {0}", ((MyNode)parentNode.Tag).documentId));
            foreach (DataRow row in rows)
            {
                if (row["oboznachenie"].ToString() == currentOboznachenie)
                {
                    continue;
                }

                TreeNode treeNode = new TreeNode(row["oboznachenie"].ToString());
                treeNode.Tag = row["documents_id"];

                myNode.dependencyId = (int)row["ID"];
                myNode.documentId = (int)row["documents_id"];
                myNode.IsChild = true;

                //treeNode.Text += "  "+myNode.IsChild.ToString();

                treeNode.Tag = myNode;

                Debug.WriteLine("treeNode"+treeNode.Text + "  IsChild:" + myNode.IsChild.ToString());

                //treeNode.Tag = row["ID"];
                treeNode.Name = row["oboznachenie"].ToString();
                parentNode.Nodes.Add(treeNode);
                FillNode(treeNode, dt);
            }
        }

        #endregion


        #region CheckDependency

        public void CheckDependency(string obooboznachenie)
        {

            this.TView.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);

            currentNode = new TreeNode(obooboznachenie);

            string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.oboznachenie as parent_name


                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num

                JOIN documents idoc2 ON dependency.parent_id=idoc2.inv_num

                where idoc.oboznachenie = @oboznachenie  and idoc2.oboznachenie <> 'корень'
                order BY oboznachenie";

            SqlCeCommand _cmd = new SqlCeCommand();
            SqlCeConnection ceCon = new SqlCeConnection(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(documents_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");
            documents_dt1 = documents_ds1.Tables["documents"];

            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 0);
            }

            string project_req =
                @" SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.project_name as parent_name

                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num
                JOIN project idoc2 ON dependency.project_id=idoc2.project_id
                
                where idoc.oboznachenie = @oboznachenie
                order BY project_name";

            documents_dt1.Clear();
            documents_ds1.Clear();

            _cmd = new SqlCeCommand();
            ceCon = new SqlCeConnection(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(project_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");

            documents_dt1 = documents_ds1.Tables["documents"];

            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 1);
            }




            this.TView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);
        }


        private void FindNodeAndCheck(string _Name, int id =0)
        {
            foreach (TreeNode node in TView.Nodes)
            {

                if (id == 0)
                {
                    TreeNode[] _tn;
                    _tn = TView.Nodes.Find(_Name, true);

                    foreach (TreeNode item in _tn)
                    {
                        //myNode.dependencyId = id;
                        //myNode.documentId = ((MyNode)(item.Tag)).documentId;
                        //item.Tag = myNode;
                        item.Checked = true;
                        item.BackColor = System.Drawing.Color.LightGray;
                    }
                }
                else
                {
                    if (node.Text == _Name)
                    {
                        node.Checked = true;
                        node.BackColor = System.Drawing.Color.LightGray;
                    }
                }



            }
        }

        #endregion


        #region ShowChecked

       public void ShowChecked()
        {
            // Collapse all nodes of treeView1.
            TView.CollapseAll();

            // Add the checkForCheckedChildren event handler to the BeforeExpand event.
            TView.BeforeExpand += checkForCheckedChildren;

            // Expand all nodes of treeView1. Nodes without checked children are
            // prevented from expanding by the checkForCheckedChildren event handler.
            TView.ExpandAll();

            // Remove the checkForCheckedChildren event handler from the BeforeExpand
            // event so manual node expansion will work correctly.
            TView.BeforeExpand -= checkForCheckedChildren;

        }

        // Prevent expansion of a node that does not have any checked child nodes.
        private void CheckForCheckedChildrenHandler(object sender,
            TreeViewCancelEventArgs e)
        {
            if (!HasCheckedChildNodes(e.Node)) e.Cancel = true;
        }

        // Returns a value indicating whether the specified
        // TreeNode has checked child nodes.
        private bool HasCheckedChildNodes(TreeNode node)
        {
            if (node.Nodes.Count == 0)
            {
                return false;
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                if (childNode.Checked)
                {
                    return true;
                }
                // Recursively check the children of the current child node.
                if (HasCheckedChildNodes(childNode))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion



        private void AfterCheck(object sender, TreeViewEventArgs e)
        {
            System.Drawing.Color clr = new System.Drawing.Color();
            //Dependency dep;

          #region Некорневые узлы
            //if ( ((MyNode)e.Node.Tag).IsChild )
            //{


                if (e.Node.BackColor == System.Drawing.Color.LightGray && !e.Node.Checked)
                {
                    //e.Node.Checked = false;
                    e.Node.BackColor = System.Drawing.Color.LightCoral;


                //depRep.DeleteDep(depRep[(int)e.Node.Tag]);
                //depRep.DeleteDep(dep: depRep[
                //((MyNode)e.Node.Tag).dependencyId
                //],
                //IsChild: ((MyNode)e.Node.Tag).IsChild
                //);

                //if (((MyNode)e.Node.Tag).IsChild)
                    {
                        depRep.DeleteDep(ParentID: //depRep[
                        ((MyNode)e.Node.Tag).documentId
                        //]
,
                        IsChild: ((MyNode)e.Node.Tag).IsChild
                        );
                    }





                }
                else if (e.Node.BackColor == System.Drawing.Color.LightCoral && e.Node.Checked)
                {


                    //e.Node.BackColor = System.Drawing.Color.LightGray;
                    //depRep.RetriveDep( ParentID: //depRep[
                    //    ((MyNode)e.Node.Tag).documentId
                    //    //]
                    //    ,
                    //    IsChild: ((MyNode)e.Node.Tag).IsChild
                    //    );
                    //if (((MyNode)e.Node.Tag).IsChild)
                    {
                        e.Node.BackColor = System.Drawing.Color.LightGray;
                        depRep.RetriveDep(ParentID: //depRep[
                            ((MyNode)e.Node.Tag).documentId
                            //]
                            ,
                            IsChild: ((MyNode)e.Node.Tag).IsChild
                            );
                    }
                    //else
                    //{
                    //    e.Node.BackColor = System.Drawing.Color.LightGray;
                    //    depRep.RetriveDep(ParentID: //depRep[
                    //        ((MyNode)e.Node.Tag).documentId
                    //        //]
                    //        ,
                    //        IsChild: ((MyNode)e.Node.Tag).IsChild
                    //        );
                    //}

                }
                //***----------------------------
                else if (e.Node.BackColor.Name == System.Drawing.Color.FromName("0").Name
                   && e.Node.Checked)
                {

                    clr = e.Node.BackColor;

                    e.Node.BackColor = System.Drawing.Color.LightGreen;

                //depRep.Add(
                //    ParentID: ((MyNode)e.Node.Tag).documentId
                //    , IsChild: ((MyNode)e.Node.Tag).IsChild
                //    );

                //if (((MyNode)e.Node.Tag).IsChild)
                    {
                        depRep.Add(
                        ParentID: ((MyNode)e.Node.Tag).documentId
                        , IsChild: ((MyNode)e.Node.Tag).IsChild
                        );

                    }


                }
                else if (e.Node.BackColor == System.Drawing.Color.LightGreen
                   && !e.Node.Checked)
                {

                    e.Node.BackColor = clr;

                if (((MyNode)e.Node.Tag).IsChild)
                    {
                        depRep.Delete(
                            ParentID: ((MyNode)e.Node.Tag).dependencyId
                            , IsChild: ((MyNode)e.Node.Tag).IsChild
                            );

                    }



            }

           // }
            #endregion

          #region Корневые узлы
            if (e.Node.Parent == null)
            {

            }
          #endregion






        }

        public void UpdateDB(string InvNum, bool isAsm)
        {
            depRep.DocId = InvNum;
            depRep.IsAsm = isAsm;
            depRep.UpdateDB();
        }

        struct MyNode
        {
            public int dependencyId;
            public int documentId;
            bool isChild;

            public bool IsChild
            {
                get { return isChild; }
                set { isChild = value;
                      //Debug.WriteLine("isChild changed:"+isChild.ToString());
                    }
            }
        }


        Font tagFont = new Font("Helvetica", 8, FontStyle.Bold);


        /*

        private void myTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            Graphics g1 = this.TView.CreateGraphics();
            SizeF boundSize1 = g1.MeasureString
                    (e.Node.Tag.ToString(), tagFont);

            e.Graphics.DrawString(e.Node.Text.ToString(), e.Node.NodeFont,
                Brushes.Red, new RectangleF(
                    new Point(e.Node.Bounds.X, e.Node.Bounds.Y),
                    new Size((int)boundSize1.Width, (int)boundSize1.Height + 50)
                    )
                );

            // Draw the background and node text for a selected node.
            if ((e.State & TreeNodeStates.Selected) != 0)
            {
                // Draw the background of the selected node. The NodeBounds
                // method makes the highlight rectangle large enough to
                // include the text of a node tag, if one is present.
                //e.Graphics.FillRectangle(Brushes.DimGray, NodeBounds(e.Node));
                e.Graphics.FillRectangle(Brushes.DimGray, e.Node.Bounds);

                // Retrieve the node font. If the node font has not been set,
                // use the TreeView font.
                Font nodeFont = e.Node.NodeFont;
                if (nodeFont == null)
                {
                    nodeFont = ((TreeView)sender).Font;
                }

                // Draw the node text.
                //e.Graphics.DrawString(e.Node.Text, nodeFont, Brushes.Yellow,
                //    Rectangle.Inflate(e.Bounds, 20, 0));
                Graphics g = this.TView.CreateGraphics();
                SizeF boundSize = g.MeasureString
                        (e.Node.Tag.ToString(), tagFont);

                e.Graphics.DrawString(e.Node.Text.ToString(), nodeFont,
                    Brushes.Red, new RectangleF(
                        new Point(e.Node.Bounds.X, e.Node.Bounds.Y),
                        new Size((int)boundSize.Width, (int)boundSize.Height+50)
                        )
                    );

            }

            // Use the default background and node text.
            else
            {
                e.DrawDefault = true;
            }

            // If a node tag is present, draw its string representation
            // to the right of the label text.
            if (e.Node.Tag != null)
            {
                e.Graphics.DrawString(e.Node.Text.ToString(), tagFont,
                    Brushes.Red, e.Bounds.Right + 2, e.Bounds.Top);
            }


            // If the node has focus, draw the focus rectangle large, making
            // it large enough to include the text of the node tag, if present.
            if ((e.State & TreeNodeStates.Focused) != 0)
            {
                using (Pen focusPen = new Pen(Color.Black))
                {
                    focusPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    Rectangle focusBounds = NodeBounds(e.Node);
                    focusBounds.Size = new Size(focusBounds.Width - 1,
                    focusBounds.Height - 1);
                    e.Graphics.DrawRectangle(focusPen, focusBounds);
                }
            }

        }


        private Rectangle NodeBounds(TreeNode node)
        {
            // Set the return value to the normal node bounds.
            Rectangle bounds = node.Bounds;
            if (node.Text != null)
            {
                // Retrieve a Graphics object from the TreeView handle
                // and use it to calculate the display width of the tag.
                Graphics g = this.TView.CreateGraphics();

                SizeF boundSize = g.MeasureString
                    (node.Tag.ToString(), tagFont);

                //int tagWidth = (int)g.MeasureString
                //    (node.Tag.ToString(), tagFont).Width + 6;

                int tagWidth = (int)boundSize.Width + 6;
                int tagHeight = (int)boundSize.Height + 6;

                // Adjust the node bounds using the calculated value.
                bounds.Offset(100, 0);
                //bounds = Rectangle.Inflate(bounds, tagWidth / 2, 0);
                g.Dispose();
            }

            return bounds;

        }



    */


        private void myTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {

            e.Graphics.FillRectangle(Brushes.Green,e.Node.Bounds);

            e.Graphics.DrawString(e.Node.Text, ((TreeView)sender).Font, Brushes.Red,
                Rectangle.Inflate(e.Bounds, 2, 20));

            e.Graphics.DrawString(e.Node.Text.ToString(), ((TreeView)sender).Font,
                Brushes.Black, e.Bounds.Right + 2, e.Bounds.Top);


            //e.DrawDefault = true;
        }


















        }



    public class BuildNodeTree
    {

        #region from BuildTreeView
        readonly string documents_req_ = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie

            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            JOIN documents idoc1 ON dependency.documents_id=idoc1.inv_num
            order BY is_asm desc, documents_id";


        string documents_req2_ = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie
                
            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            where dependency.is_asm = 1
            order BY is_asm desc, documents_id";

        string projects_req_ = @"SELECT * FROM project
                                order BY project_name ";


        DataSet documents_ds_ = new DataSet();
        DataTable documents_dt_ = new DataTable();


        DataSet documents_ds11_ = new DataSet();
        DataTable documents_dt1_ = new DataTable();




        DataSet projects_ds_ = new DataSet();
        DataTable projects_dt_ = new DataTable();

        SqlCeDataAdapter dataAdapter_;

        TreeView TView_;

        TreeNode rootnode_;

        TreeNode currentNode_;

        

        SqlCeConnection sqlceCon_;

        private TreeViewCancelEventHandler checkForCheckedChildren_;

        #endregion



        #region For BuildNodeTree
        DataTable dependencyTable;
        DataTable asmDependencyTable;
        DataTable projectTable;

        DependencyRepository dependencyRep;

        TreeNode treeNode = new TreeNode();
        TreeNode currentNode;

        TreeView TView = new TreeView();

        MyNode myNode;
        MyNode myNodedoc;

        string currentOboznachenie;

        private TreeViewCancelEventHandler checkForCheckedChildren;


        DataSet documents_ds1 = new DataSet();
        DataTable documents_dt1 = new DataTable();
        SqlCeDataAdapter dataAdapter;


        #endregion


        public BuildNodeTree(TreeView treeView, DependencyRepository DependencyRep,
            DataTable DependencyTable, DataTable ProjectTable, 
            string CurrentOboznachenie = "")
        {
            this.TView = treeView;
            //this.sqlceCon = SQLCEcon;
            currentOboznachenie = CurrentOboznachenie;

            dependencyRep = DependencyRep;

            checkForCheckedChildren =
            new TreeViewCancelEventHandler(CheckForCheckedChildrenHandler);

            this.TView.AfterCheck += new TreeViewEventHandler(this.AfterCheck);
        }


        public BuildNodeTree(DependencyRepository DependencyRep,
            DataTable DependencyTable, DataTable ProjectTable, 
            string CurrentOboznachenie = "")
        {
            this.dependencyTable = DependencyTable;
            this.projectTable = ProjectTable;
            currentOboznachenie = CurrentOboznachenie;

            dependencyRep = DependencyRep;

        }


        public void SetAsm(bool state)
        {
            dependencyRep.IsAsm = state;
        }

        public void SetObozn(string obozn)
        {
            dependencyRep.DocId = obozn;
        }


        #region BuildTree
        /// <summary>
        ///  Заполнение дерева из БД
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="dt"></param>
        public TreeNode BuildTree()
        {
            treeNode.Nodes.Clear();
            List<string> nname = new List<string>();

            foreach (DataRow prjrow in projectTable.Rows)
            {
                TreeNode project_node = new TreeNode(prjrow["project_name"].ToString());
                myNode.dependencyId = (int)prjrow["project_id"];
                myNode.documentId = (int)prjrow["project_id"];
                myNode.IsChild = false;
                project_node.Tag = myNode;

                DataRow[] documents_rows = dependencyTable.Select(
                string.Format("project_id = {0}", ((MyNode)project_node.Tag).documentId)
                    );
                foreach (DataRow docrow in documents_rows)
                {
                    if (docrow["oboznachenie"].ToString() == currentOboznachenie)
                    {
                        continue;
                    }

                    TreeNode node = new TreeNode(docrow["oboznachenie"].ToString());
                    myNodedoc.dependencyId = (int)docrow["ID"];
                    myNodedoc.documentId = (int)docrow["documents_id"];
                    myNodedoc.IsChild = true;
                    node.Tag = myNodedoc;
                    Debug.WriteLine("node" + node.Text + "  IsChild:" + myNodedoc.IsChild);

                    node.Name = docrow["oboznachenie"].ToString();

                    if (!nname.Contains(prjrow["project_name"].ToString()))
                    {
                        project_node.Nodes.Add(node);
                        treeNode.Nodes.Add(project_node);
                        nname.Add(prjrow["project_name"].ToString());
                    }
                    else
                    {
                        project_node.Nodes.Add(node);
                    }

                    FillNode(node, dependencyTable);
                    node = null;
                }
                if (documents_rows.Length == 0)
                {
                    treeNode.Nodes.Add(project_node);
                }
            }
            return treeNode;
        }

        /// <summary>
        ///  Добавление к узлу наследников
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="dt"></param>
        private void FillNode(TreeNode parentNode, DataTable dt)
        {
            DataRow[] rows = dt.Select(string.Format("Parent_Id = {0}", ((MyNode)parentNode.Tag).documentId));
            foreach (DataRow row in rows)
            {
                if (row["oboznachenie"].ToString() == currentOboznachenie)
                {
                    continue;
                }

                TreeNode treeNode = new TreeNode(row["oboznachenie"].ToString());
                treeNode.Tag = row["documents_id"];

                myNode.dependencyId = (int)row["ID"];
                myNode.documentId = (int)row["documents_id"];
                myNode.IsChild = true;

                treeNode.Tag = myNode;

                Debug.WriteLine("treeNode" + treeNode.Text + "  IsChild:" + myNode.IsChild.ToString());

                treeNode.Name = row["oboznachenie"].ToString();
                parentNode.Nodes.Add(treeNode);
                FillNode(treeNode, dt); 
            }
        }
        #endregion

        #region CheckDependency
        public void CheckDependency(string obooboznachenie)
        {
            this.TView.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);

            currentNode = new TreeNode(obooboznachenie);

            string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.oboznachenie as parent_name


                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num

                JOIN documents idoc2 ON dependency.parent_id=idoc2.inv_num

                where idoc.oboznachenie = @oboznachenie  and idoc2.oboznachenie <> 'корень'
                order BY oboznachenie";

            SqlCeCommand _cmd = new SqlCeCommand();
            SqlCeConnection ceCon = new SqlCeConnection(null);//(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(documents_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");
            documents_dt1 = documents_ds1.Tables["documents"];

            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 0);
            }

            string project_req =
                @" SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.project_name as parent_name

                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num
                JOIN project idoc2 ON dependency.project_id=idoc2.project_id
                
                where idoc.oboznachenie = @oboznachenie
                order BY project_name";

            documents_dt1.Clear();
            documents_ds1.Clear();

            _cmd = new SqlCeCommand();
            ceCon = new SqlCeConnection(null);//(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(project_req, ceCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds1, "documents");

            documents_dt1 = documents_ds1.Tables["documents"];

            foreach (DataRow row in documents_dt1.Rows)
            {
                FindNodeAndCheck(row["parent_name"].ToString(), 1);
            }

            this.TView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.AfterCheck);
        }

        private void FindNodeAndCheck(string _Name, int id = 0)
        {
            foreach (TreeNode node in TView.Nodes)
            {
                if (id == 0)
                {
                    TreeNode[] _tn;
                    _tn = TView.Nodes.Find(_Name, true);

                    foreach (TreeNode item in _tn)
                    {
                        //myNode.dependencyId = id;
                        //myNode.documentId = ((MyNode)(item.Tag)).documentId;
                        //item.Tag = myNode;
                        item.Checked = true;
                        item.BackColor = System.Drawing.Color.LightGray;
                    }
                }
                else
                {
                    if (node.Text == _Name)
                    {
                        node.Checked = true;
                        node.BackColor = System.Drawing.Color.LightGray;
                    }
                }
            }
        }
        #endregion
        #region ShowChecked

        public void ShowChecked()
        {
            // Collapse all nodes of treeView1.
            TView.CollapseAll();

            // Add the checkForCheckedChildren event handler to the BeforeExpand event.
            TView.BeforeExpand += checkForCheckedChildren;

            // Expand all nodes of treeView1. Nodes without checked children are
            // prevented from expanding by the checkForCheckedChildren event handler.
            TView.ExpandAll();

            // Remove the checkForCheckedChildren event handler from the BeforeExpand
            // event so manual node expansion will work correctly.
            TView.BeforeExpand -= checkForCheckedChildren;

        }

        // Prevent expansion of a node that does not have any checked child nodes.
        private void CheckForCheckedChildrenHandler(object sender,
            TreeViewCancelEventArgs e)
        {
            if (!HasCheckedChildNodes(e.Node)) e.Cancel = true;
        }

        // Returns a value indicating whether the specified
        // TreeNode has checked child nodes.
        private bool HasCheckedChildNodes(TreeNode node)
        {
            if (node.Nodes.Count == 0)
            {
                return false;
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                if (childNode.Checked)
                {
                    return true;
                }
                // Recursively check the children of the current child node.
                if (HasCheckedChildNodes(childNode))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        private void AfterCheck(object sender, TreeViewEventArgs e)
        {
            System.Drawing.Color clr = new System.Drawing.Color();
            //Dependency dep;

            #region Некорневые узлы

            if (e.Node.BackColor == System.Drawing.Color.LightGray && !e.Node.Checked)
            {
                e.Node.BackColor = System.Drawing.Color.LightCoral;

                //depRep.DeleteDep(depRep[(int)e.Node.Tag]);
                //depRep.DeleteDep(dep: depRep[
                //((MyNode)e.Node.Tag).dependencyId
                //],
                //IsChild: ((MyNode)e.Node.Tag).IsChild
                //);

                //if (((MyNode)e.Node.Tag).IsChild)
                {
                    dependencyRep.DeleteDep(ParentID: //depRep[
                    ((MyNode)e.Node.Tag).documentId,
                    IsChild: ((MyNode)e.Node.Tag).IsChild
                    );
                }
            }
            else if (e.Node.BackColor == System.Drawing.Color.LightCoral && e.Node.Checked)
            {
                {
                    e.Node.BackColor = System.Drawing.Color.LightGray;
                    dependencyRep.RetriveDep(ParentID: //depRep[
                        ((MyNode)e.Node.Tag).documentId
                        //]
                        ,
                        IsChild: ((MyNode)e.Node.Tag).IsChild
                        );
                }
            }
            //***----------------------------
            else if (e.Node.BackColor.Name == System.Drawing.Color.FromName("0").Name
               && e.Node.Checked)
            {
                clr = e.Node.BackColor;

                e.Node.BackColor = System.Drawing.Color.LightGreen;

                dependencyRep.Add(
                ParentID: ((MyNode)e.Node.Tag).documentId
                , IsChild: ((MyNode)e.Node.Tag).IsChild
                );
            }
            else if (e.Node.BackColor == System.Drawing.Color.LightGreen
               && !e.Node.Checked)
            {
                e.Node.BackColor = clr;

                if (((MyNode)e.Node.Tag).IsChild)
                {
                    dependencyRep.Delete(
                        ParentID: ((MyNode)e.Node.Tag).dependencyId
                        , IsChild: ((MyNode)e.Node.Tag).IsChild
                        );
                }
            }
            #endregion

            #region Корневые узлы
            if (e.Node.Parent == null)
            {

            }
            #endregion
        }

        public void UpdateDB1(string InvNum, bool isAsm)
        {
            dependencyRep.DocId = InvNum;
            dependencyRep.IsAsm = isAsm;
            dependencyRep.UpdateDB();
        }

        struct MyNode
        {
            public int dependencyId;
            public int documentId;
            bool isChild;

            public bool IsChild
            {
                get { return isChild; }
                set
                {
                    isChild = value;
                    //Debug.WriteLine("isChild changed:"+isChild.ToString());
                }
            }
        }

        Font tagFont = new Font("Helvetica", 8, FontStyle.Bold);

        private void myTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.Green, e.Node.Bounds);

            e.Graphics.DrawString(e.Node.Text, ((TreeView)sender).Font, Brushes.Red,
                Rectangle.Inflate(e.Bounds, 2, 20));

            e.Graphics.DrawString(e.Node.Text.ToString(), ((TreeView)sender).Font,
                Brushes.Black, e.Bounds.Right + 2, e.Bounds.Top);
        }
    }






     











}

