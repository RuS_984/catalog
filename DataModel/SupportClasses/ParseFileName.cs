﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace DataModel.SupportClasses
{

    #region ForDelete



    public static class ParseFileName211
    {
        public static FileName ParseName(string Name)
        {
            string name;
            FileName fileName;
            FileInfo file = new FileInfo(Name);
            string ext = file.Extension;
            name = Path.GetFileNameWithoutExtension(Name);

            string[] str;

            fileName.Oboznachenie = Name;
            fileName.Naimenovanie = "none";

            switch (ext)
            {
                case ".m3d": fileName.DocType = DocumentType.Part; break;
                default: fileName.DocType = DocumentType.None; break;
            }

            if (name.Length < 17)
            { return fileName; }

            int offSet = 0;

            if (name.Substring(0, 15 + 1).Contains("-") == true)
            {
                offSet = 3;
            }

            if (file.Extension.ToLower() == ".a3d")
            {
                fileName.DocType = DocumentType.Assemble;
                str = ParseByType(name, "СБ", offSet);
                fileName.Oboznachenie = str[0];
                fileName.Naimenovanie = str[1];
            }
            else if (file.Extension.ToLower() == ".m3d")
            {
                fileName.Oboznachenie = name.Substring(0, 15 + offSet);
                fileName.Naimenovanie = name.Substring(16 + offSet);
                fileName.DocType = DocumentType.Part;
            }
            else if (file.Extension.ToLower() == ".spw")
            {
                fileName.DocType = DocumentType.Specification;
                str = ParseByType(name, "СП", offSet);
                fileName.Oboznachenie = str[0];
                fileName.Naimenovanie = str[1];
            }

            return fileName;
        }

        private static string[] ParseByType(string Name, string DocType, int OffSet)
        {
            string name = Name;
            int offSet = OffSet;
            string[] res = new string[2];

            if (name.Length < 23)
            {
                res[0] = name;
                res[1] = "none";
                return res;
            }

            if (name.Substring(15 + offSet, 4).ToUpper().Contains(" " + DocType + " "))
            {
                res[0] = name.Substring(0, 15 + offSet) + " " + DocType;
                res[1] = name.Substring(19 + offSet);
            }
            else if (name.Substring(15 + offSet, 3).ToUpper().Contains(DocType))
            {
                res[0] = name.Substring(0, 15 + offSet) + " " + DocType;
                res[1] = name.Substring(18 + offSet);
            }
            else
            {
                res[0] = name.Substring(0, 15 + offSet) + " " + DocType;
                res[1] = name.Substring(16 + offSet);
            }
            return res;
        }
    }


    #endregion


    public static class ParseFileName
    {
        public static FileName ParseName(string Name)
        {
            string name;
            FileName fileName;
            FileInfo file = new FileInfo(Name);
            string ext = file.Extension;
            name = Path.GetFileNameWithoutExtension(Name);

            string[] str;

            fileName.Oboznachenie = Name;
            fileName.Naimenovanie = "none";

            switch (ext)
            {
                case ".m3d": fileName.DocType = DocumentType.Part; break;
                default: fileName.DocType = DocumentType.None; break;
            }

            if (file.Extension.ToLower() == ".a3d")
            {
                fileName.DocType = DocumentType.Assemble;
                str = ParseByType(name, DocumentType.Assemble);
                fileName.Oboznachenie = str[0];
                fileName.Naimenovanie = str[1];
            }
            else if (file.Extension.ToLower() == ".m3d")
            {
                fileName.DocType = DocumentType.Part;
                str = ParseByType(name, DocumentType.Part);
                fileName.Oboznachenie = str[0];
                fileName.Naimenovanie = str[1];
            }
            else if (file.Extension.ToLower() == ".spw")
            {
                fileName.DocType = DocumentType.Specification;
                str = ParseByType(name, DocumentType.Specification);
                fileName.Oboznachenie = str[0];
                fileName.Naimenovanie = str[1];
            }
            return fileName;
        }

        private static string[] ParseByType(string Name, DocumentType DocType)
        {
            string[] res = new string[2];
            string regexpr = string.Empty;

            if (Name.Length < 23)
            {
                res[0] = Name;
                res[1] = "none";
                return res;
            }

            if (DocType == DocumentType.Specification)
            {
                regexpr = @"((?:\D{4})\.(?:\d{6}\.\d{3})(?:(?:-)(?:\d+))?\s+?(?:СП)?)\s?(.+)";
                Match match = Regex.Match(Name,regexpr,RegexOptions.CultureInvariant);

                res[0] = match.Groups[1].Value;
                res[1] = match.Groups[2].Value;
                if (!res[0].Contains(" СП"))
                {
                    res[0] = res[0].Trim();
                    res[0] += " СП";
                }
            }

            if (DocType == DocumentType.Assemble)
            {
                regexpr = @"((?:\D{4})\.(?:\d{6}\.\d{3})(?:(?:-)(?:\d+))?\s+?(?:СБ)?)\s?(.+)";
                Match match = Regex.Match(Name,regexpr,RegexOptions.CultureInvariant);

                res[0] = match.Groups[1].Value;
                res[1] = match.Groups[2].Value;
                if (!(res[0].Contains(" СБ")))
                {
                    res[0] = res[0].Trim();
                    res[0] += " СБ";
                }
            }

            if (DocType == DocumentType.Part)
            {
                regexpr = @"((?:\D{4})\.(?:\d{6}\.\d{3})(?:(?:-)(?:\d+))?)\s+?(.+)";
                Match match = Regex.Match(Name,regexpr,RegexOptions.CultureInvariant);

                res[0] = match.Groups[1].Value.Trim();
                res[1] = match.Groups[2].Value;
            }
            return res;
        }
    }

    public struct FileName
    {
        public string Oboznachenie;
        public string Naimenovanie;
        public DocumentType DocType;

        public static bool operator ==(FileName x, FileName y)
        {
            return x.Oboznachenie == y.Oboznachenie
                    && x.Naimenovanie == y.Naimenovanie
                    && x.DocType == y.DocType;
        }

        public static bool operator !=(FileName x, FileName y)
        {
            return !(x == y);
        }
    }

    public enum DocumentType
    {
        None,
        Assemble,
        Part,
        Specification
    }
}
