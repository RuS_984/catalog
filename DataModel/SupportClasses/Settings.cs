﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

using DataModel.DBWrapper;

namespace DataModel.SupportClasses
{
    public static class Settings
    {
        static string settingFileName = "settings.cfg";

        static string SQLCEdbFileName;
        static string SQLCEdbConString;

        static string SQLdbConString;

        static string passWord;

        static bool passIsChanged;

        static DBType connType;


        #region События
        public delegate void OnSQLCEfilecheck();

        /// <summary>
        /// Возникает при проверке подключения к SQL CE DB
        /// </summary>
        public static event OnSQLCEfilecheck OnSQLCEFileCheck;
        #endregion

        static public string SQLCEDBFileName
        {
            get { return SQLCEdbFileName; }
            set { SQLCEdbFileName = value;
                 SQLCEdbConString = string.Format("Data Source = {0}", SQLCEdbFileName);
            }
        }

        static public string SQLCEDBConString
        {
            get {
                SQLCEdbConString = string.Format("Data Source = {0}", SQLCEdbFileName);
                  return SQLCEdbConString; }
        }

        static public string SQLDBConString
        {
            get { return SQLdbConString; }
            set
            {
                SQLdbConString = value;
            }
        }

        static public string ConnectionString
        {
            get
            {
                if (connType == DBType.MSSQLCE)
                {
                    return SQLCEDBConString;
                }
                else if (connType == DBType.MSSQL)
                {
                    return SQLDBConString;
                }
                else
                {
                    return string.Empty;
                }
            }

        }

        static public string PassWord
        {
            get { return passWord; }
            set { passWord = value; }
        }

        static public bool PassIsChanged
        {
            get { return passIsChanged; }
            set { passIsChanged = value; }
        }

        static public DBType ConnectionType
        {
            get { return connType; }
            set { connType = value; }
        }

        static MySettings MS;

        static Byte[] EncIV = System.Text.Encoding.ASCII.GetBytes("DpuTAhpfSqc6ddy7");
        static Byte[] EncKey = System.Text.Encoding.ASCII.GetBytes("B2q9c0k0K54Fskwr");

        static Settings()
        {
            //int loadres;
            //MS.Clear();
            //loadres = Load();

            //if (loadres < 0)
            //{
            //    Save(newfile: true);
            //    SQLCEdbFileName = MS.SQLCEdbFileName_;
            //    SQLdbConString = MS.SQLCEdbConString_;
            //    connType = MS.ConnType;
            //    passWord = MS.password;
            //}

            //if (!File.Exists(SQLCEdbFileName) && (OnFileCheck != null))
            //{
            //    OnFileCheck();
            //}

        }


        /// <summary>
        /// Инициализация открытия файла настроек
        /// </summary>
        static public void Init()
        {
            int loadres;
            MS.Clear();
            loadres = Load();

            if (loadres < 0)
            {
                Save(newfile: true);
                SQLCEdbFileName = MS.SQLCEdbFileName_;
                SQLdbConString = MS.SQLCEdbConString_;
                connType = MS.ConnType;
                passWord = MS.password;
            }

            // Проверка события при инициализации
            if (!File.Exists(SQLCEdbFileName) & (OnSQLCEFileCheck != null))
            {
                OnSQLCEFileCheck();
            }
        }

        static public int Load()
        {
            if (!File.Exists(settingFileName))
            {
                return -1;
            }

            using (FileStream file = new FileStream(settingFileName, FileMode.Open, FileAccess.Read))
            {
                IFormatter BinFormatter = new BinaryFormatter();

                Rijndael alg = Rijndael.Create();
                alg.IV = EncIV;
                alg.Key = EncKey;

                MemoryStream ms = new MemoryStream();

                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);

                ms.Position = 0; // rewind

                byte[] benc = ms.ToArray();

                MemoryStream bencr = new MemoryStream(benc);

                CryptoStream cr = new CryptoStream(bencr, alg.CreateDecryptor(), CryptoStreamMode.Read);

                byte[] buf = new byte[(int)ms.Length];
                cr.Read(buf, 0, (int)ms.Length);

                MemoryStream unenc = new MemoryStream(buf);
                unenc.Position = 0;

                MS = (MySettings)BinFormatter.Deserialize(unenc);

                SQLCEdbFileName = MS.SQLCEdbFileName_;
                SQLdbConString = MS.SQLCEdbConString_;
                connType = MS.ConnType;
                passWord = MS.password;

                return 0;
            }
        }

        static public void Save(bool newfile = false)
        {
            if (newfile)
            {
                MS.Clear();
            }
            else
            {
                MS.SQLCEdbFileName_ = SQLCEdbFileName;
                MS.SQLCEdbConString_ = SQLdbConString;
                MS.ConnType = connType;
                MS.password = passWord;
            }

            IFormatter BinFormatter = new BinaryFormatter();

            Rijndael alg = Rijndael.Create();
            alg.IV = EncIV;
            alg.Key = EncKey;

            Console.Write(alg.Key.ToString() + "\n");

            char[] chr = System.Text.Encoding.UTF8.GetString(alg.Key, 0, alg.Key.Length).ToCharArray();

            Console.Write("\n");

            //---------------------------
            MemoryStream ms = new MemoryStream();
            BinFormatter.Serialize(ms, (object)MS);
            ms.Position = 0;
            //---------------------
            MemoryStream enc = new MemoryStream();
            CryptoStream cw = new CryptoStream(enc, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cw.Write(ms.ToArray(), 0, (int)ms.Length);
            cw.FlushFinalBlock();
            int len;
            len = (int)ms.Length;

            enc.Position = 0;

            byte[] benc = enc.ToArray();

            using (FileStream file = new FileStream(settingFileName, FileMode.Create, FileAccess.Write))
            {
                enc.WriteTo(file);
            }
        }

        [Serializable]
        struct MySettings
        {
            public string SQLCEdbFileName_;
            public string SQLCEdbConString_;
            public string password;
            public DBType ConnType;

            public void Clear()
            {
                password = "0000";
                ConnType = DBType.None;
                SQLCEdbFileName_ = string.Empty;
                SQLCEdbConString_ = string.Empty;
            }
        }
    }
}
