﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlServerCe;
using System.Collections;
using DataModel.DBWrapper;

namespace DataModel.SupportClasses
{
    //TODO Проработать обработку исключений
    public static class ExceptionHandler
    {
        public static void AppThreadExceptionHandler(object sender, ThreadExceptionEventArgs t)
        {
            MainExceptioHandler(t.Exception);
        }

        public static void AppDomainExceptioHandler(object sender, UnhandledExceptionEventArgs args)
        {
            MainExceptioHandler((Exception)args.ExceptionObject);
        }

        static void MainExceptioHandler(Exception ex)
        {
            if (ex is SqlCeException)
            {
                switch (((SqlCeException)ex).NativeError)
                {
                    case 25011:
                        {
                            //Catalog.Properties.Settings.Default.BDFile = "";
                            //Catalog.Properties.Settings.Default.Save();
                            MessageBox.Show("Несоответсвующий файл БД");
                            break;
                        }
                    case 25017:
                        {
                            MessageBox.Show("Файл БД поврежден");
                            break;
                        }
                    case 25035:
                        {
                            MessageBox.Show("Файл занят другим процессом");
                            break;
                        }
                    default:
                        {
                            MessageBox.Show("MainExceptionHandle SqlCeException\n"
                                                            + "Message: " + ex.Message
                                                            + "NativeError: \n" + ((SqlCeException)ex).NativeError);
                            break;
                        }
                }
            }
            else if (ex is SQLEntityException)
            {
                SQLEntityExceptionHandler((SQLEntityException)ex);
            }
            else if (ex is ICSharpCode.SharpZipLib.SharpZipBaseException)
            {
                MessageBox.Show("Файл поврежден");
            }
            else if (ex is Exception)
            {
                MessageBox.Show("MainExceptionHandle Exception\n" + ex.Message +
                    "\n\n" + ex.StackTrace);
            }
        }

        static void SQLEntityExceptionHandler(SQLEntityException ex)
        {
            string mes = string.Empty;// "Невозможно удалить выбраный проект { 0}";

            switch (ex.SQLNativeError)
            {
                //Изменения в БД противоречащие FOREIGN KEY Constraint
                //Невозможно удалить первичный ключ, так как еще существуют ссылки на этот ключ
                //Foreign key constraint
                case 25025:
                    {
                        if (ex.SQLTableName == DBTableName.Project)
                        { mes = string.Format("Невозможно удалить выбраный проект {0}", ex.SQLEntityName); }
                        else if (ex.SQLTableName == DBTableName.Ispolniteli)
                        { { mes = string.Format("Невозможно удалить выбраного исполнителя  {0}", ex.SQLEntityName); } }

                        MessageBox.Show(//"MainExceptionHandle SQLEntityException\n" + ex.Message + "\n"
                                 "mes:" + mes + "\n"
                                + "SQLCENativeError:" + ex.SQLNativeError + "\n"
                                + "SQLTableName:" + ex.SQLTableName + "\n"
                                + "SQLEntityID:" + ex.SQLEntityID + "\n"
                                + "SQLEntityName:" + ex.SQLEntityName
                                + "OperationType:" + ex.OperationType.ToString()
                                );
                        break;
                    }

                //Повторяющееся значение невозможно вставить в уникальный индекс.
                case 25016:
                    {
                        if (ex.SQLTableName == DBTableName.Project)
                        { mes = string.Format("Невозможно создать или переименовать проект, т.к. проект с именем {0} уже существует", ex.SQLEntityName); }
                        else if (ex.SQLTableName == DBTableName.Ispolniteli)
                        { { mes = string.Format("Невозможно создать или переименовать  исполнителя, т.к. исполнитель с именем {0} уже существует", ex.SQLEntityName); } }

                        MessageBox.Show("MainExceptionHandle SQLEntityException\n" + ex.Message + "\n"
                                + "mes:" + mes + "\n"
                                + "SQLCENativeError:" + ex.SQLNativeError + "\n"
                                + "SQLTableName:" + ex.SQLTableName + "\n"
                                + "SQLEntityID:" + ex.SQLEntityID + "\n"
                                + "SQLEntityName:" + ex.SQLEntityName + "\n"
                                + "OperationType:" + ex.OperationType.ToString()
                                );
                        break;
                    }

                    //Файл не является файлом формата базы данных SQL Server Compact.
            }
        }

        [Serializable]
        public class SQLEntityException : Exception
        {
            public SQLEntityException() { }
            public SQLEntityException(string message) : base(message) { }
            public SQLEntityException(string message, Exception ex) : base(message, ex) { }
            // Конструктор для обработки сериализации типа
            protected SQLEntityException(System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext contex)
                : base(info, contex) { }

            public DBType DBtype { get; set; }
            public int SQLNativeError { get; set; }
            public DBTableName SQLTableName { get; set; }
            public int SQLEntityID { get; set; }
            public string SQLEntityName { get; set; }
            public DBOperationType OperationType { get; set; }
        }
        public enum DBOperationType
        {
            GetEntities,
            AddToDB,
            DeleteFromDB,
            RenameInDB
        }

        public enum DBTableName
        {
            Dependency,
            FirstUsage,
            InventarNumber,
            Ispolniteli,
            Literal,
            MainTable,
            Oboznachenie,
            Project
        }
    }
}
