﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataModel.SupportClasses
{
    public class DirTreeNode
    {
        int nodeIndex;
        string[] startWhith;
        string rootDir;

        public DirTreeNode(string[] StartWhith, string RootDir, int StartIndex)
        {
            startWhith = StartWhith;
            nodeIndex = StartIndex;
            rootDir = RootDir;
        }



        public TreeNode BuildNodeTree()
        {
            TreeNode rootNode;

            nodeDependency rootDepend;
                                  
            FileName fn;

            DirectoryInfo dirRoot = new DirectoryInfo(rootDir);
            rootNode = new TreeNode(dirRoot.Name);
            if (dirRoot.Exists)
            {
                rootNode = new TreeNode(nodeIndex.ToString() + "___" + dirRoot.Name + "<---" + dirRoot.Name);

                rootDepend.dependencyId = nodeIndex;
                rootDepend.dependensOn = 0;
                rootDepend.documentName = dirRoot.Name;
                rootDepend.dependencyName = dirRoot.Name;
                rootDepend.isRoot = true;
                rootDepend.isFolder = true;
                rootDepend.isAsm = true;

                fn = ParseFileName.ParseName(dirRoot.Name + ".a3d");

                rootDepend.naimenovanie = fn.Naimenovanie;
                rootDepend.oboznachenie = fn.Oboznachenie;

                rootNode.Tag = rootDepend;

                GetSubDir(dirRoot.GetDirectories(), rootNode);

                TreeNode tmp = new TreeNode();
                GetFiles(dirRoot, rootNode);
            }
            return rootNode;
        }


        private void GetSubDir(DirectoryInfo[] subDirs, TreeNode nodeToAddTo)
        {
            TreeNode subNode;

            DirectoryInfo[] subSubDirs;

            nodeDependency dirDepend;

            int idir = -1;
            int ifile = -1;

            FileName fn;

            foreach (DirectoryInfo subDir in subDirs)
            {


                idir = ++nodeIndex;
                string str = idir.ToString() + "___" + subDir.Name + "<---" + ((nodeDependency)nodeToAddTo.Tag).dependencyName;
                subNode = new TreeNode(str, 0, 0);

                dirDepend.dependencyId = idir;
                dirDepend.dependensOn = ((nodeDependency)nodeToAddTo.Tag).dependencyId;
                dirDepend.documentName = subDir.Name;
                dirDepend.dependencyName = ((nodeDependency)nodeToAddTo.Tag).dependencyName;
                dirDepend.isRoot = false;
                dirDepend.isFolder = true;
                dirDepend.isAsm = true;

                fn = ParseFileName.ParseName(subDir.Name + ".a3d");

                dirDepend.naimenovanie = fn.Naimenovanie;
                dirDepend.oboznachenie = fn.Oboznachenie;

                subNode.Tag = dirDepend;//nodeIndex + 1;

                subSubDirs = subDir.GetDirectories();
                if (subSubDirs.Length != 0)
                {
                    GetSubDir(subSubDirs, subNode);
                }
                nodeToAddTo.Nodes.Add(GetFiles(subDir, subNode));
            }
        }

        private TreeNode GetFiles(DirectoryInfo subDir, TreeNode subNode)
        {
            TreeNode parentNode = subNode.Parent;

            //nodeDependency nndd = (nodeDependency)parentNode.Tag;

            int ifile = -1;

            nodeDependency fileDepend;

            TreeNode fileNode;

            FileName fn;

            string dependencyName;

            int dependensOn;

            foreach (FileInfo file in subDir.GetFiles())
            {
                string subNodedocName = ((nodeDependency)subNode.Tag).documentName.Substring(0, 15);
                string fileDocName = "";
                if ((file.Extension == ".dwg"
                    | file.Extension == ".a3d"
                    | file.Extension == ".m3d"
                    | file.Extension == ".spw")
                    & CheckNameStartPattern(file.Name)//(Path.GetFileNameWithoutExtension(file.Name).ToLower().StartsWith("глрт"))
                    )
                {
                    fileDocName = Path.GetFileNameWithoutExtension(file.Name);//.Substring(0, 15);
                }

                if (file.Extension == ".a3d"
                    & subNodedocName == fileDocName
                    )
                {
                    continue;
                }
                else
                {
                    ifile = nodeIndex++;
                }

                if ((file.Extension == ".dwg"
                   | file.Extension == ".a3d"
                   | file.Extension == ".m3d"
                   | file.Extension == ".spw")
                   & CheckNameStartPattern(file.Name) //& (Path.GetFileNameWithoutExtension(file.Name).ToLower().StartsWith("глрт"))
                                                      //| file.Extension == ".spw"
                   )
                {
                    ifile = nodeIndex;
                    string str2 = ifile.ToString() + "___" + file.Name;
                    fileNode = new TreeNode(str2, 0, 0);

                    if (file.Extension == ".a3d"
                        | file.Name.Contains("СБ "))
                    {
                        dependensOn = ((nodeDependency)subNode.Tag).dependensOn;

                        if (subNodedocName == fileDocName)
                        {
                            dependensOn = ((nodeDependency)subNode.Tag).dependensOn;
                        }
                        else
                        {
                            dependensOn = ((nodeDependency)subNode.Tag).dependencyId;
                        }
                        if (dependensOn == 1)
                        {
                            dependencyName = ((nodeDependency)subNode.Tag).dependencyName;
                        }
                        else
                        {
                            dependencyName = ((nodeDependency)subNode.Tag).documentName;
                        }
                        fileDepend.isAsm = true;
                    }
                    else
                    {
                        dependencyName = ((nodeDependency)subNode.Tag).documentName;
                        dependensOn = ((nodeDependency)subNode.Tag).dependencyId;
                        fileDepend.isAsm = false;
                    }

                    fileDepend.dependencyId = ifile;
                    fileDepend.dependensOn = dependensOn;
                    fileDepend.documentName = file.Name;
                    fileDepend.dependencyName = dependencyName;
                    fileDepend.isRoot = false;
                    fileDepend.isFolder = false;


                    fn = ParseFileName.ParseName(file.Name);

                    fileDepend.naimenovanie = fn.Naimenovanie;
                    fileDepend.oboznachenie = fn.Oboznachenie;


                    fileNode.Tag = fileDepend;

                    fileNode.ImageKey = "fle";
                    subNode.Nodes.Add(fileNode);
                }
            }
            return subNode;
        }



        private bool CheckNameStartPattern(string Name)
        {
            bool res = false;

            foreach (string item in startWhith)
            {
                res = res | (Name.ToLower().StartsWith(item, StringComparison.InvariantCulture));
            }

            return res;
        }











    }
    public struct nodeDependency
    {
        public int dependencyId;
        public int dependensOn;
        public string documentName;
        public string dependencyName;
        public bool isRoot;
        public bool isFolder;
        public bool isAsm;

        public string naimenovanie;
        public string oboznachenie;
    }
}
