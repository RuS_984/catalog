﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyControls
{
    public enum Win32_Messages
    {


        /// <summary>
        /// Сообщение <b>WM_ NULL</b> не выполняет никакого действия. 
        /// Приложение отправляет сообщение WM_NULL, если оно желает посылать сообщение, 
        /// которое окно получатель должно проигнорировать.
        /// </summary>
        WM_NULL = 0x0000,

        /// <summary>
        /// Уведомляет окно, что оно создано и должна быть выполнена инициализация.
        /// <para>
        /// Паpаметpы:
        /// </para>
        /// <para>
        /// wParam Не используется
        /// </para>
        /// <para>
        /// lParam Указывает на стpуктуpу TCreateStruct, котоpая содеpжит инфоpмацию, пеpедаваемую в функцию CreateWindow.
        /// </para>        
        /// <returns>Возвpащаемое значение: Не используется.</returns>
        /// </summary>
        /// <remarks>
        /// Комментаpии: Это сообщение посылается окну во вpемя вызова функции CreateWindow пеpед откpытием окна.
        /// </remarks>
        WM_CREATE = 0x0001,

        /// <summary>
        /// Уведомляет окно о том, что оно будет pазpушено.
        /// Паpаметpы:
        /// wParam: Не используется.
        /// lParam: Не используется.
        /// Возвpащаемое значение: Не используется.
        /// Комментаpии: Любое окно, находящееся в цепочке буфеpа выpезанного изобpажения,
        /// должно удалять само себя из этой цепочки с помощью функции ChangeClipboardChain
        /// пеpед тем, как она веpнется из сообщения wm_DestroyWindow. Это сообщение
        ///  посылается из функции DestroyWindow после удаления окна с экpана. Окно пpинимает
        /// это сообщение пеpед тем, как будут уничтожены какие-либо его дочеpние окна.
        /// </summary>
        WM_DESTROY                = 0x0002, //Звкрытие окна

        /// <summary>
        ///Сообщение WM_MOVE передается после того, когда окно будет перемещено. Снтаксис
        ///WM_MOVE
        ///xPos = (int)LOWORD(lParam);    // позиция по горизонтали 
        ///yPos = (int)HIWORD(lParam);    // позиция по вертикали 
        ///Параметры
        ///xPos
        ///Значение младшего байта слова lParam. Устанавливает x-координату левого верхнего угла рабочей области окна.
        /// yPos
        ///Значение старшего слова lParam. Устанавливает y-координату левого верхнего угла рабочей области окна.
        ///Возвращаемые значения
        ///Если программа обрабатывает это сообщение, она должна возвратить ноль.
        ///Замечания
        /// xPos и yPos параметры даны в экранных координатах для перекрывающих и выскакивающих окон и в координатах пользователя - родителя для дочерних окон. Прикладная программа может использовать макрокоманду MAKEPOINTS, чтобы преобразовать параметр lParam для структуры POINTS. 
        /// </summary>
        WM_MOVE                   = 0x0003,

        /// <summary>
        /// Сообщение WM_SIZE посылается окну после того, как его размер изменился.
        /// Синтаксис
        /// WM_SIZE
        /// fwSizeType = wParam;      // флажок изменения размеров 
        /// nWidth = LOWORD(lParam);  // ширина рабочей области 
        /// nHeight = HIWORD(lParam); // высота рабочей области 
        /// Параметры
        /// fwSizeType
        /// Значение wParam. Определяет тип запрошенного изменения размеров. Этот параметр может принимать одно из следующих значений:
        /// SIZE_MAXHIDE - Сообщение посылается всем выскакивающим окнам, когда развернуто некоторое другое окно.
        /// SIZE_MAXIMIZED - Окно было развернуто.
        /// SIZE_MAXSHOW - Сообщение посылается всем выскакивающим окнам, когда некоторое другое окно было восстановлено в его прежних размерах.
        /// SIZE_MINIMIZED - Окно было свернуто(минимизировано).
        /// SIZE_RESTORED - Окно было изменено, но ни одно значение SIZE_MINIMIZED ни SIZE_MAXIMIZED не применяется. 
        /// nWidth
        /// Значение младшего слова lParam. Устанавливает новую ширину рабочей области.
        /// nHeight
        /// Значение старшего слова lParam. Устанавливает новую высоту рабочей области.
        /// Возвращаемые значения
        /// Если программа обрабатывает это сообщение, она должна возвратить ноль.
        /// Замечания
        /// Если функция SetScrollPos или MoveWindow вызвана для дочернего окна в результате сообщения WM_SIZE, параметр bRedraw должен отличаться от нуля, чтобы заставить окно быть перекрашенным. Хотя ширина и высота окна - 32-разрядные значения, параметры nWidth и nHeight сообщения WM_SIZE содержат только младшие 16 битов.
        /// </summary>
        WM_SIZE                   = 0x0005,

        /// <summary>
        /// Сообщение WM_ACTIVATE посылается, когда окно активизируется или деактивизируется. Это сообщение посылается сначала оконной процедуре деактивизируемого окна верхнего уровня; затем оно посылается оконной процедуре активизируемого окна верхнего уровня.
        /// Синтаксис
        /// WM_ACTIVATE
        /// fActive = LOWORD(wParam);               // флажок активизации 
        /// fMinimized = (BOOL)HIWORD(wParam);  // флажок минимизации 
        /// hwndPrevious = (HWND)lParam;        // дескриптор окна 
        /// Параметры
        /// fActive
        /// Значение младшего байта слова wParam. Устанавливает, активизируется ли окно или оно деактивизируется. Этот параметр может быть одним из следующих значений:
        /// WA_ACTIVE - Активизировано некоторым методом другим, чем щелчок мыши (например, обращением к функции SetActiveWindow или использованием интерфейса клавиатуры для выбора окна).
        /// WA_CLICKACTIVE - Активизировано щелчком мыши.
        /// WA_INACTIVE - Деактивизировано. 
        /// fMinimized
        /// Значение старшего байта слова wParam. Устанавливает свернутое состояние окна, активизируемого или деактивизируемого. Значение, отличающееся от нуля, указывает, что окно свернуто (минимизировано).
        /// hwndPrevious
        /// Значение lParam. Идентифицирует окно, активизируемое или деактивизируемое, в зависимости от значения параметра fActive. Если значение fActive - WA_INACTIVE, hwndPrevious - дескриптор активизируемого окна. Если значение fActive - WA_ACTIVE или WA_CLICKACTIVE, hwndPrevious - дескриптор деактивизируемого окна. Этот дескриптор может быть ПУСТО (NULL).
        /// Возвращаемые значения
        /// Если программа обработала это сообщение, она должна возвратить нуль.
        /// Действие по умолчанию
        /// Если окно активизируется и не свернуто (минимизировано), функция DefWindowProc устанавливает фокус клавиатуры в окно.
        /// Замечания
        /// Если окно активизировано щелчком мыши, оно также принимает сообщение WM_MOUSEACTIVATE. 
        /// </summary>
        WM_ACTIVATE               = 0x0006,


        WM_SETFOCUS               = 0x0007,
        WM_KILLFOCUS              = 0x0008,

        /// <summary>
        /// Сообщение WM_ENABLE посылается тогда, когда прикладная программа изменяет включенное состояние окна. Оно посылается окну, чьё включенное состояние изменяется. Это сообщение посылается перед возвращением значения функцией EnableWindow, но только после того, как изменилось включенное состояние (бит стиля WS_DISABLED) окна.
        /// Синтаксис
        /// WM_ENABLE
        /// fEnabled = (BOOL)wParam;   // флажок включить/заблокировать
        /// Параметры
        /// fEnabled
        /// Значение wParam. Устанавливает, было ли окно включено или заблокировано. Этот параметр был бы ИСТИНА (TRUE), если бы окно было включено или ЛОЖЬ(FALSE), если бы окно было заблокировано.
        /// Возвращаемые значения
        /// Если программа обрабатывает это сообщение, она должна возвратить ноль. 
        /// </summary>
        WM_ENABLE = 0x000A,
        WM_SETREDRAW              = 0x000B,
        WM_SETTEXT                = 0x000C,
        WM_GETTEXT                = 0x000D,
        WM_GETTEXTLENGTH          = 0x000E,
        WM_PAINT                  = 0x000F,

        /// <summary>
        /// Сообщение WM_CLOSE посылается как сигнал, по которому окно или прикладная программа должны завершить свою работу.
        /// Синтаксис
        /// WM_CLOSE
        /// Параметры
        /// У этого сообщения нет параметров.
        /// Возвращаемые значения
        /// Если приложение обрабатывает это сообщение, оно должно возвратить ноль.
        /// Действие по умолчанию
        /// Функция DefWindowProc обращается к функции DestroyWindow, чтобы уничтожить окно.
        /// Примечания
        /// Прикладная программа может запросить пользователя о подтверждении, до разрушения окна, в ходе обработки сообщение WM_CLOSE и вызывает функцию DestroyWindow только тогда, если пользователь подтверждает выбор. 
        /// </summary>
        WM_CLOSE = 0x0010,

        WM_QUERYENDSESSION        = 0x0011,
        WM_QUIT                   = 0x0012,
        WM_QUERYOPEN              = 0x0013,
        WM_ERASEBKGND             = 0x0014,
        WM_SYSCOLORCHANGE         = 0x0015,
        WM_ENDSESSION             = 0x0016,
        WM_SHOWWINDOW             = 0x0018,
        WM_WININICHANGE           = 0x001A,
        WM_SETTINGCHANGE          = 0x001A,
        WM_DEVMODECHANGE          = 0x001B,

        /// <summary>
        /// Сообщение WM_ACTIVATEAPP посылается тогда, когда окно, принадлежащее другой прикладной программе, кроме активного окна, собирается быть активизированным. Сообщение посылается прикладной программе, чье окно активизируется и прикладной программе, чье окно деактивируется.
        /// Синтаксис
        /// WM_ACTIVATEAPP
        /// fActive = (BOOL)wParam;             // активизация флажка
        /// dwThreadID = (DWORD)lParam: 		// идентификатор потока 
        /// Параметры
        /// fActive
        /// Значение wParam. Устанавливает, активизируется ли или деактивизируется окно. Этот параметр - ИСТИНА (TRUE), если окно активизируется; ЛОЖЬ(FALSE), если окно деактивизируется.
        /// dwThreadID
        /// Значение lParam. Определяет идентификатор потока. Если параметр fActive - ИСТИНА (TRUE), dwThreadID - идентификатор потока, который владеет деактивизируемым окном. Если fActive - ЛОЖЬ(FALSE), dwThreadID - идентификатор потока, который владеет активизируемым окном.
        /// Возвращаемые значения
        /// Если прикладная программа обрабатывает это сообщение, она должна возвратить нуль.
        /// </summary>
        WM_ACTIVATEAPP = 0x001C,

        WM_FONTCHANGE             = 0x001D,
        WM_TIMECHANGE             = 0x001E,

        /// <summary>
        /// Сообщение WM_CANCELMODE посылается окну фокуса, когда диалоговое окно или окно сообщений отображаются на экране; оно дает возможность окну фокуса отменить режимы, типа сбора данных от мыши.
        /// Синтаксис
        /// WM_CANCELMODE
        /// Параметры
        /// У этого сообщения нет параметров.
        /// Возвращаемые значения
        /// Если приложение обрабатывает это сообщение, оно должно возвратить нуль.
        /// Действие по умолчанию
        /// Функция DefWindowProc отменяет внутреннюю обработку стандартного ввода линейки прокрутки, отменяет внутреннюю обработку меню и отказывается от сбора данных от мыши. 
        /// </summary>
        WM_CANCELMODE = 0x001F,

        WM_SETCURSOR              = 0x0020,
        WM_MOUSEACTIVATE          = 0x0021,




        /// <summary>
        /// Сообщение WM_CHILDACTIVATE посылается дочернему окну многодокументной среды (MDI) тогда, когда пользователь щелкает мышью по области заголовка окна или, когда окно активизировано, перемещено или установлено по размеру.
        /// Синтаксис
        /// WM_CHILDACTIVATE
        ///  Параметры
        /// У этого сообщения нет параметров.
        /// Возвращаемые значения
        /// Если приложение обрабатывает сообщение, оно должно возвратить ноль. 
        /// </summary>
        WM_CHILDACTIVATE = 0x0022,

        WM_QUEUESYNC              = 0x0023,
        WM_GETMINMAXINFO          = 0x0024,
        WM_PAINTICON              = 0x0026,
        WM_ICONERASEBKGND         = 0x0027,
        WM_NEXTDLGCTL             = 0x0028,
        WM_SPOOLERSTATUS          = 0x002A,
        WM_DRAWITEM               = 0x002B,
        WM_MEASUREITEM            = 0x002C,
        WM_DELETEITEM             = 0x002D,
        WM_VKEYTOITEM             = 0x002E,
        WM_CHARTOITEM             = 0x002F,
        WM_SETFONT                = 0x0030,
        WM_GETFONT                = 0x0031,
        WM_SETHOTKEY              = 0x0032,
        WM_GETHOTKEY              = 0x0033,
        WM_QUERYDRAGICON          = 0x0037,
        WM_COMPAREITEM            = 0x0039,
        WM_GETOBJECT              = 0x003D,


        /// <summary>
        /// Сообщение WM_COMPACTING посылается всем окнам верхнего уровня, когда Windows обнаруживает больше чем 12.5 процентов от системного времени в течение от 30 - до 60-секундного интервала, тратится на уплотнение памяти. Это указывает на то, что недостаточно системной памяти.
        /// Синтаксис
        /// WM_COMPACTING
        /// wCompactRatio = wParam; // степень сжатия 
        /// Параметры
        /// wCompactRatio
        /// Значение wParam. Устанавливает коэффициент текущего времени центрального процессора (ЦП), потраченного Windows на уплотнение памяти, к текущему времени ЦП, потраченному Windows на выполнение других действий. Например, 0x8000 представляет 50 процентов от потраченного ПРОЦЕССОРНОГО ВРЕМЕНИ на уплотнение памяти.
        /// Возвращаемые значения
        /// Если приложение обрабатывает это сообщение, оно должно возвратить ноль.
        /// Замечания
        /// Когда прикладная программа принимает это сообщение, она должна освободить так много памяти, сколько это возможно, принимая во внимание текущий уровень действия прикладной про-граммы и общее количество прикладных программ, запущенных в Windows. 
        /// </summary>
        WM_COMPACTING = 0x0041,

        WM_COMMNOTIFY             = 0x0044 ,
        WM_WINDOWPOSCHANGING      = 0x0046,
        WM_WINDOWPOSCHANGED       = 0x0047,
        WM_POWER                  = 0x0048,


        /// <summary>
        /// Сообщение WM_COPYDATA передается тогда, когда одна программа пересылает данные в другую программу.

        /// Синтаксис

        /// WM_COPYDATA
        /// wParam = (WPARAM)(HWND)hwnd;                    // дескриптор передающего окна
        /// lParam = (LPARAM)(PCOPYDATASTRUCT)pcds;         // указатель на структуру с данными
        /// Параметры
        /// hwnd
        /// Идентифицирует окно, которое передает данные.
        /// pcds
        /// Указывает на структуру COPYDATASTRUCT, которая содержит данные для передачи.
        /// Возвращаемые значения
        /// Если принимающая программа обрабатывает это сообщение, она должна возвратить значение ИСТИНА (TRUE); в противном случае она должна возвратить - ЛОЖЬ (FALSE).
        /// Замечания
        /// Для передачи этого сообщения программа должна использовать функцию SendMessage, а не функцию PostMessage. Данные, предназначенные для передачи, не должны содержать указателей или других ссылок на объекты, не доступные для программы, принимающей эти данные.
        /// До тех пор, пока это сообщение действует, вызванные данные не должны быть изменены другим по-током процесса пересылки. Принимающая программа должна принимать во внимание данные только для чтения. Параметр pcds правилен только в течение обработки сообщения. Принимающая программа не должна освобождать память, вызванную pcds. Если принимающая программа обратилась к данным после возврата значения функцией SendMessage, она должно копировать данные в локальный буфер. 
        /// </summary>
        WM_COPYDATA = 0x004A,

        WM_CANCELJOURNAL          = 0x004B,
        WM_NOTIFY                 = 0x004E,
        WM_INPUTLANGCHANGEREQUEST = 0x0050,
        WM_INPUTLANGCHANGE        = 0x0051,
        WM_TCARD                  = 0x0052,
        WM_HELP                   = 0x0053,
        WM_USERCHANGED            = 0x0054,
        WM_NOTIFYFORMAT           = 0x0055,
        WM_CONTEXTMENU            = 0x007B,
        WM_STYLECHANGING          = 0x007C,
        WM_STYLECHANGED           = 0x007D,
        WM_DISPLAYCHANGE          = 0x007E,
        WM_GETICON                = 0x007F,
        WM_SETICON                = 0x0080,
        WM_NCCREATE               = 0x0081,
        WM_NCDESTROY              = 0x0082,
        WM_NCCALCSIZE             = 0x0083,
        WM_NCHITTEST              = 0x0084,
        WM_NCPAINT                = 0x0085,
        WM_NCACTIVATE             = 0x0086,
        WM_GETDLGCODE             = 0x0087,
        WM_SYNCPAINT              = 0x0088,
        WM_NCMOUSEMOVE            = 0x00A0,
        WM_NCLBUTTONDOWN          = 0x00A1,
        WM_NCLBUTTONUP            = 0x00A2,
        WM_NCLBUTTONDBLCLK        = 0x00A3,
        WM_NCRBUTTONDOWN          = 0x00A4,
        WM_NCRBUTTONUP            = 0x00A5,
        WM_NCRBUTTONDBLCLK        = 0x00A6,
        WM_NCMBUTTONDOWN          = 0x00A7,
        WM_NCMBUTTONUP            = 0x00A8,
        WM_NCMBUTTONDBLCLK        = 0x00A9,
        WM_NCXBUTTONDOWN          = 0x00AB,
        WM_NCXBUTTONUP            = 0x00AC,
        WM_KEYDOWN                = 0x0100,
        WM_KEYUP                  = 0x0101,
        WM_CHAR                   = 0x0102,
        WM_DEADCHAR               = 0x0103,
        WM_SYSKEYDOWN             = 0x0104,
        WM_SYSKEYUP               = 0x0105,
        WM_SYSCHAR                = 0x0106,
        WM_SYSDEADCHAR            = 0x0107,
        WM_KEYLAST                = 0x0108,
        WM_IME_STARTCOMPOSITION   = 0x010D,
        WM_IME_ENDCOMPOSITION     = 0x010E,
        WM_IME_COMPOSITION        = 0x010F,
        WM_IME_KEYLAST            = 0x010F,
        WM_INITDIALOG             = 0x0110,
        WM_COMMAND                = 0x0111,
        WM_SYSCOMMAND             = 0x0112,
        WM_TIMER                  = 0x0113,
        WM_HSCROLL                = 0x0114,
        WM_VSCROLL                = 0x0115,
        WM_INITMENU               = 0x0116,
        WM_INITMENUPOPUP          = 0x0117,
        WM_MENUSELECT             = 0x011F,
        WM_MENUCHAR               = 0x0120,
        WM_ENTERIDLE              = 0x0121,
        WM_MENURBUTTONUP          = 0x0122,
        WM_MENUDRAG               = 0x0123,
        WM_MENUGETOBJECT          = 0x0124,
        WM_UNINITMENUPOPUP        = 0x0125,
        WM_MENUCOMMAND            = 0x0126,
        WM_CTLCOLORMSGBOX         = 0x0132,
        WM_CTLCOLOREDIT           = 0x0133,
        WM_CTLCOLORLISTBOX        = 0x0134,
        WM_CTLCOLORBTN            = 0x0135,
        WM_CTLCOLORDLG            = 0x0136,
        WM_CTLCOLORSCROLLBAR      = 0x0137,
        WM_CTLCOLORSTATIC         = 0x0138,
        WM_MOUSEMOVE              = 0x0200,
        WM_LBUTTONDOWN            = 0x0201, //Нажатие левой кнопки мыши
        WM_LBUTTONUP              = 0x0202,
        WM_LBUTTONDBLCLK          = 0x0203,
        WM_RBUTTONDOWN            = 0x0204,
        WM_RBUTTONUP              = 0x0205,
        WM_RBUTTONDBLCLK          = 0x0206,
        WM_MBUTTONDOWN            = 0x0207,
        WM_MBUTTONUP              = 0x0208,
        WM_MBUTTONDBLCLK          = 0x0209,
        WM_MOUSEWHEEL             = 0x020A,
        WM_XBUTTONDOWN            = 0x020B,
        WM_XBUTTONUP              = 0x020C,
        WM_XBUTTONDBLCLK          = 0x020D,
        WM_PARENTNOTIFY           = 0x0210,
        WM_ENTERMENULOOP          = 0x0211,
        WM_EXITMENULOOP           = 0x0212,
        WM_NEXTMENU               = 0x0213,
        WM_SIZING                 = 0x0214,
        WM_CAPTURECHANGED         = 0x0215,
        WM_MOVING                 = 0x0216,
        WM_DEVICECHANGE           = 0x0219,
        WM_MDICREATE              = 0x0220,
        WM_MDIDESTROY             = 0x0221,
        WM_MDIACTIVATE            = 0x0222,
        WM_MDIRESTORE             = 0x0223,
        WM_MDINEXT                = 0x0224,
        WM_MDIMAXIMIZE            = 0x0225,
        WM_MDITILE                = 0x0226,
        WM_MDICASCADE             = 0x0227,
        WM_MDIICONARRANGE         = 0x0228,
        WM_MDIGETACTIVE           = 0x0229,
        WM_MDISETMENU             = 0x0230,




        /// <summary>
        /// Сообщение WM_ENTERSIZEMOVE посылается только один раз окну, когда оно вводит режим установки размеров или перемещения. Окно вводит режим перемещения или установки размеров, когда пользователь щелкает мышью по области заголовка окна или устанавливает размеры рамки окна, или когда окно передает сообщение WM_SYSCOMMAND в функцию DefWindowProc, а параметр wParam сообщения устанавливает значение SC_MOVE или SC_SIZE. Windows посылает сообщение WM_ENTERSIZEMOVE независимо от того, включено ли перемещение полных окон.
        /// Синтаксис
        /// WM_ENTERSIZEMOVE
        /// wParam = 0;  // не используется, должен быть ноль
        /// lParam = 0;  // не используется, должен быть ноль
        /// Параметры
        /// Это сообщение не имеет параметров.
        /// Возвращаемые значения
        /// Программа должна возвратить ноль, если она обрабатывает это приложение. 
        /// </summary>
        WM_ENTERSIZEMOVE = 0x0231,




        WM_EXITSIZEMOVE           = 0x0232,
        WM_DROPFILES              = 0x0233,
        WM_MDIREFRESHMENU         = 0x0234,
        WM_IME_SETCONTEXT         = 0x0281,
        WM_IME_NOTIFY             = 0x0282,
        WM_IME_CONTROL            = 0x0283,
        WM_IME_COMPOSITIONFULL    = 0x0284,
        WM_IME_SELECT             = 0x0285,
        WM_IME_CHAR               = 0x0286,
        WM_IME_REQUEST            = 0x0288,
        WM_IME_KEYDOWN            = 0x0290,
        WM_IME_KEYUP              = 0x0291,
        WM_MOUSEHOVER             = 0x02A1,
        WM_MOUSELEAVE             = 0x02A3,
        WM_CUT                    = 0x0300,
        WM_COPY                   = 0x0301,
        WM_PASTE                  = 0x0302,
        WM_CLEAR                  = 0x0303,
        WM_UNDO                   = 0x0304,
        WM_RENDERFORMAT           = 0x0305,
        WM_RENDERALLFORMATS       = 0x0306,
        WM_DESTROYCLIPBOARD       = 0x0307,
        WM_DRAWCLIPBOARD          = 0x0308,
        WM_PAINTCLIPBOARD         = 0x0309,
        WM_VSCROLLCLIPBOARD       = 0x030A,
        WM_SIZECLIPBOARD          = 0x030B,
        WM_ASKCBFORMATNAME        = 0x030C,
        WM_CHANGECBCHAIN          = 0x030D,
        WM_HSCROLLCLIPBOARD       = 0x030E,
        WM_QUERYNEWPALETTE        = 0x030F,
        WM_PALETTEISCHANGING      = 0x0310,
        WM_PALETTECHANGED         = 0x0311,
        WM_HOTKEY                 = 0x0312,
        WM_PRINT                  = 0x0317,
        WM_PRINTCLIENT            = 0x0318,
        WM_HANDHELDFIRST          = 0x0358,
        WM_HANDHELDLAST           = 0x035F,
        WM_AFXFIRST               = 0x0360,
        WM_AFXLAST                = 0x037F,
        WM_PENWINFIRST            = 0x0380,
        WM_PENWINLAST             = 0x038F,
        WM_APP                    = 0x8000,
        WM_USER                   = 0x0400,
    }
}

