﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlServerCe;
using System.Data.SqlClient;
using DataModel.DBWrapper.Providers.Abstract;



namespace DataModel.SupportClasses
{
    public static class CommonData
    {
        static SqlCeConnection sqlConn = null;
        static string conString = string.Empty;

        public static DatabaseProvider dbSQL;

        public static ProjectTableAbstract ProjectTable;
        public static OboznachenieTableAbstract OboznachenieTable;
        public static FirstUsageTableAbstract FirstUsageTable;
        public static LiteralTableAbstract LiteralTable;
        public static IspolniteliTableAbstract IspolniteliTable;
        public static InventarNumberTableAbstract InventarNumberTable;
        public static DependencyTableAbstract DependencyTable;

        public static MainTableAbstract MainTable;

        public static string LastFilterParam;

        public static string ConString
        {
            get
            {
                return conString;
            }
            set
            {
                if (value != null)
                {
                    sqlConn = new SqlCeConnection(value);
                    conString = value;
                }
                else
                {
                    throw new ArgumentNullException(value);
                }
           }
        }



        public static SqlCeConnection SQLConn
        {
            get
            {
                return sqlConn;
            }
        }

        public static string[] ColumnNames = {"Id",
                "Инвентарный номер", "Название проекта", "Обозначение",
                "Наименование", "Первичное преминение", "Литера", "Дата подписания",
                "Сборка", "Проверил", "Разработал",
                "Тех контроль", "Нормоконтроль", "Утвердил" };
        public static string[] DBColumnNames = {"id",
                "inv_num", "project_id", "oboznachenie",
                "naimenovanie", "pervichprimen", "literal", "date",
                "is_asm", "proveril", "razrabotal",
                "tehcontrol", "normcontrol", "utverdil" };

        public static string rowRepeat = "_ПОВТОР_";

        public static string GetDBColumnName1(string ColumnName)
        {
            int i = ColumnNames.ToList<string>().IndexOf(ColumnName);

            return DBColumnNames[i];

        }

    }
}
