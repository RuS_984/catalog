﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DBData.Entities;
using DBData.Repository;

namespace DataModel.SupportClasses
{

    public class IMAGEProcessing
    {
        public static Image Get_Thumbnail(Image img, int Width, int Height)
        {
            float aspect;
            int img_Width = Width, img_Height = Height;
            aspect = (float)img.Width / (float)img.Height;
            if (img.Width > img.Height)
            {
                img_Height = Convert.ToInt16(Math.Round(Width / aspect));
            }
            if (img.Height > img.Width)
            {
                img_Width = Convert.ToInt16(Math.Round(Height * aspect));
            }
            return img.GetThumbnailImage(img_Width, img_Height, null, IntPtr.Zero);
        }

        public static List<FileStream> IMAGEOpenFS1(List<FileStream> FS_List)
        {
            List<FileStream> FS_List_ = new List<FileStream>();
            OpenFileDialog fop = new OpenFileDialog();
            fop.InitialDirectory = @"c:\";
            fop.Filter = "[JPG,JPEG]|*.jpg";
            fop.Multiselect = true;
            if (fop.ShowDialog() == DialogResult.OK)
            {
                foreach (String filename in fop.FileNames)
                {
                    FileStream FS = new FileStream(@filename, FileMode.Open, FileAccess.Read);
                    byte[] img = new byte[FS.Length];
                    FS.Read(img, 0, Convert.ToInt32(FS.Length));
                    FS_List.Add(FS);
                }
                return FS_List;
            }
            else
            {
                MessageBox.Show("Please Select a Image to save!!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        public static Dictionary<string, MemoryStream> IMAGEOpenMS(
            Dictionary<string, MemoryStream> MS_Dict,
            string OpenFilter, string InitDir)
        {
            Dictionary<string, MemoryStream> MS_Dict_ = new Dictionary<string, MemoryStream>();
            OpenFileDialog fop = new OpenFileDialog();
            FileInfo fi;
            fop.InitialDirectory = InitDir; //@"c:\";
            fop.Filter = OpenFilter;//"*.JPG|*.jpg| *.DOCX|*.DOCX| *.DOC|*.DOC| Все файлы|*.*";

            fop.Multiselect = true;
            if (fop.ShowDialog() == DialogResult.OK)
            {
                foreach (String filename in fop.FileNames)
                {
                    FileStream FS = new FileStream(@filename, FileMode.Open, FileAccess.Read);
                    fi = new FileInfo(filename);
                    MS_Dict_.Add(fi.Name, FStreamToMStream(FS));
                }
                return MS_Dict_;
            }
            else
            {
                MessageBox.Show("Please Select a Image to save!!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        public static DocsVarRepository IMAGEOpenMS2(string OpenFilter, string InitDir)
        {
            DocsVarRepository _DocsLst = new DocsVarRepository();

            OpenFileDialog fop = new OpenFileDialog();
            FileInfo fi;
            fop.InitialDirectory = InitDir; //@"c:\";
            fop.Filter = OpenFilter;//"*.JPG|*.jpg| *.DOCX|*.DOCX| *.DOC|*.DOC| Все файлы|*.*";

            fop.Multiselect = true;
            if (fop.ShowDialog() == DialogResult.OK)
            {
                foreach (String filename in fop.FileNames)
                {
                    FileStream FS = new FileStream(@filename, FileMode.Open, FileAccess.Read);
                    MemoryStream MS = FStreamToMStream(FS);

                    fi = new FileInfo(filename);
                    _DocsLst.Add(new Docs(DocId: 0, DocName: fi.Name, prjOboznachenie: "", Doc: MS, IsNew: true));
                }

                return _DocsLst;
            }
            else
            {
                MessageBox.Show("Please Select a Image to save!!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        public static List<Docs> IMAGEOpenMS3(string OpenFilter, string InitDir)
        {
            List<Docs> _DocsLst = new List<Docs>();

            OpenFileDialog fop = new OpenFileDialog();
            FileInfo fi;
            fop.InitialDirectory = InitDir; //@"c:\";
            fop.Filter = OpenFilter;//"*.JPG|*.jpg| *.DOCX|*.DOCX| *.DOC|*.DOC| Все файлы|*.*";

            fop.Multiselect = true;
            if (fop.ShowDialog() == DialogResult.OK)
            {
                foreach (String filename in fop.FileNames)
                {
                    FileStream FS = new FileStream(@filename, FileMode.Open, FileAccess.Read);
                    MemoryStream MS = FStreamToMStream(FS);

                    fi = new FileInfo(filename);
                    _DocsLst.Add(new Docs(DocId: 0, DocName: fi.Name, prjOboznachenie: "", Doc: MS, IsNew: true));
                }

                return _DocsLst;
            }
            else
            {
                MessageBox.Show("Please Select a Image to save!!", "Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        public static MemoryStream FStreamToMStream(FileStream file)
        {
            //byte[] data = new Byte[file.Length]; ;
            MemoryStream stream = new MemoryStream();

            stream.SetLength(file.Length);
            file.Read(stream.GetBuffer(), 0, (int)file.Length);

            return stream;
        }
    }
}
