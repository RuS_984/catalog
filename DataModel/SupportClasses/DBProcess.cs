﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;

using System.IO;

using DBData.Entities;
using DBData.Repository;

//using ProgrammSettings;

using DataModel.DBWrapper;

namespace DataModel.SupportClasses
{
    public static class DBprocessing
    {

        /// <summary>
        /// Генерация пути к файлу БД
        /// </summary>
        public static string GetConString(string SQLCEDBFileName)
        {
            string file = SQLCEDBFileName;

            if (File.Exists(file))
            {
                return string.Format(
                       @"Data Source = {0}",
                       file);
            }
            else
            {
                OpenFileDialog fop = new OpenFileDialog();
                fop.InitialDirectory = Application.ExecutablePath;
                fop.Filter = "[SDF]|*.SDF";

                if (fop.ShowDialog() == DialogResult.OK)
                {
                    file = fop.FileName;
                    Settings.SQLCEDBFileName = file;
                    Settings.Save();
                    return string.Format(
                    @"Data Source = {0}",
                    file);
                }
                else
                {
                    return null;
                }
            }
        }

        public static void OpenDBFile()
        {
            SqlCeConnection CECon = null;
            string file;
            OpenFileDialog fop = new OpenFileDialog();
            fop.InitialDirectory = Application.ExecutablePath;
            fop.Filter = "[SDF]|*.SDF";

            if (fop.ShowDialog() == DialogResult.OK)
            {
                file = fop.FileName;
                try
                {
                    CECon = new SqlCeConnection(
                        string.Format(@"Data Source = {0}", file));
                    CECon.Open();
                }
                finally
                {
                    CECon.Close();
                }
                Settings.SQLCEDBFileName = file;
                Settings.ConnectionType = DBType.MSSQLCE;
                Settings.Save();
            }
        }

        public static void OpenMSSQLCEDBFile()
        {
            SqlCeConnection CECon = null;
            string file;
            OpenFileDialog fop = new OpenFileDialog();
            fop.InitialDirectory = Application.ExecutablePath;
            fop.Filter = "[SDF]|*.SDF";

            if (fop.ShowDialog() == DialogResult.OK)
            {
                file = fop.FileName;
                try
                {
                    CECon = new SqlCeConnection(
                        string.Format(@"Data Source = {0}", file));
                    CECon.Open();
                }
                finally
                {
                    CECon.Close();
                }
                Settings.SQLCEDBFileName = file;
                Settings.ConnectionType = DBType.MSSQLCE;
                Settings.Save();
            }

        }

        public static string SQLRequest(
            string ordered_by = "",//documents.inv_num
            string search_txt = "",
            string search_col = "")
        {
            string select = "";
            string search = "";
            string order = "";

            if (ordered_by != "")
            {
                order = "ORDER BY " + ordered_by;
            }

            //select = @"SELECT
            //            documents.id,documents.inv_num,project.project_name,documents.oboznachenie,
            //            documents.naimenovanie,documents.pervichprimen,documents.date, documents.is_asm,
            //            iRaz.second_name razrabotal, iProv.second_name proveril,iTehKon.second_name tehcontrol,
            //            iNormKon.second_name normocontrol, iUtverdil.second_name utverdil
            //           FROM documents
            //            JOIN project ON project.project_id=documents.project_id
            //            JOIN ispolniteli iRaz ON documents.razrabotal=iRaz.ispoln_id
            //            JOIN ispolniteli iProv ON documents.proveril=iProv.ispoln_id
            //            JOIN ispolniteli iTehKon ON documents.tehcontrol=iTehKon.ispoln_id
            //            JOIN ispolniteli iNormKon ON documents.normcontrol=iNormKon.ispoln_id
            //            JOIN ispolniteli iUtverdil ON documents.utverdil=iUtverdil.ispoln_id
            //           ORDER BY documents.inv_num";
            select = @"SELECT
                        documents.id, documents.inv_num as [Инвентарный номер],
                        project.project_name as [Название проекта],
                        documents.oboznachenie as [Обозначение],
                        documents.naimenovanie as [Наименование],
                        documents.pervichprimen as [Первичное преминение],
                        documents.date as [Дата подписания],
                        literals.literal_name as [Литера],
                        documents.is_asm as [Сборка],
                        iRaz.second_name as [Разработал],
                        iProv.second_name as  [Проверил],
                        iTehKon.second_name as  [Тех контроль],
                        iNormKon.second_name as [Нормоконтроль],
                        iUtverdil.second_name as [Утвердил]
                       FROM documents
                        JOIN project ON project.project_id=documents.project_id
                        JOIN literals ON literals.literal_id=documents.literal
                        JOIN ispolniteli iRaz ON documents.razrabotal=iRaz.ispoln_id
                        JOIN ispolniteli iProv ON documents.proveril=iProv.ispoln_id
                        JOIN ispolniteli iTehKon ON documents.tehcontrol=iTehKon.ispoln_id
                        JOIN ispolniteli iNormKon ON documents.normcontrol=iNormKon.ispoln_id
                        JOIN ispolniteli iUtverdil ON documents.utverdil=iUtverdil.ispoln_id
                        " + order;//ORDER BY documents.inv_num";
            if (search_txt != "" & search_col != "")
            {
                search = String.Format("WHERE documents.{0} LIKE '%{1}%'", search_col, search_txt);
            }

            return select + "\r\n" + search;
        }

        /// <summary>
        /// Получение ключа в словаре по его значению
        /// </summary>
        /// <param name="dic">Словарь в котором производится поиск</param>
        /// <param name="val">Значение для которого ищется ключ</param>
        public static int GetKeyByVal(Dictionary<int, string> dic, string val)
        {
            return dic.FirstOrDefault(x => x.Value == val).Key;
        }

        public static string ListToString(List<int> list)
        {
            string _str = "(";
            foreach (int item in list)
            {
                _str += item + ", ";
            }

            _str = _str.Substring(0, _str.Length - 2) + ")";

            return _str;

        }

        public static string GetRandomString(int Len = 10)
        {
            if (Len > 25)
            {
                Len = 25;
            }
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, Len);

        }
    }
}
