﻿using System;

namespace DataModel.DBWrapper
{
    public struct MainTableStruct
    {
        public int Id;

        public int InventarnNum;

        public int ProjectID;

        public string Oboznachenie;

        public string Naimenovanie;

        public string Pervichoepreminenie;

        public int Literal;

        public string Date;

        public bool IsAsm;

        public int Razrabotal;

        public int Proveril;

        public int Tehcontrol;

        public int Normokontrol;

        public int Utverdil;

        public void Clear()
        {
            Id = -1;
            InventarnNum = -1;
            ProjectID = -1;
            Oboznachenie = string.Empty;
            Naimenovanie = string.Empty;
            Pervichoepreminenie = string.Empty;
            Literal = -1;
            Date = string.Empty;
            IsAsm = false;
            Razrabotal = -1;
            Proveril = -1;
            Tehcontrol = -1;
            Normokontrol = -1;
            Utverdil = -1;
        }

        public void SetDefaultData()
        {
            Id = -1;
            InventarnNum = -1;
            ProjectID = 1;
            Oboznachenie = string.Empty;
            Naimenovanie = string.Empty;
            Pervichoepreminenie = string.Empty;
            Literal = 1;
            Date = "2016.10.25 00:00:00.000";
            IsAsm = false;
            Razrabotal = 1;
            Proveril = 1;
            Tehcontrol = 1;
            Normokontrol = 1;
            Utverdil = 1;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is MainTableStruct)
            {
                string thisDate = DateTime.Parse(((MainTableStruct)obj).Date).ToString("yyyy.MM.dd");
                string objDate = DateTime.Parse(((MainTableStruct)obj).Date).ToString("yyyy.MM.dd");

                if (((MainTableStruct)obj).Id == this.Id
                    && ((MainTableStruct)obj).InventarnNum == this.InventarnNum
                    && ((MainTableStruct)obj).ProjectID == this.ProjectID
                    && ((MainTableStruct)obj).Oboznachenie == this.Oboznachenie
                    && ((MainTableStruct)obj).Naimenovanie == this.Naimenovanie
                    && ((MainTableStruct)obj).Pervichoepreminenie == this.Pervichoepreminenie
                    && ((MainTableStruct)obj).Literal == this.Literal
                    && objDate == thisDate
                    && ((MainTableStruct)obj).Razrabotal == this.Razrabotal
                    && ((MainTableStruct)obj).Proveril == this.Proveril
                    && ((MainTableStruct)obj).Tehcontrol == this.Tehcontrol
                    && ((MainTableStruct)obj).Normokontrol == this.Normokontrol
                    && ((MainTableStruct)obj).Utverdil == this.Utverdil
                    )
                {
                    return true;
                }
            }
            return false;
        }

        public static bool operator ==(MainTableStruct p1, MainTableStruct p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(MainTableStruct p1, MainTableStruct p2)
        {
            return !p1.Equals(p2);
        }
    }
}