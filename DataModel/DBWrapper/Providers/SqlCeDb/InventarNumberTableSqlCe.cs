﻿using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBData.Entities;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class InventarNumberTableSqlCe : InventarNumberTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        public InventarNumberTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;
        }

        public override List<InventarNumber> GetEntities()
        {
            List<InventarNumber> _lst = new List<InventarNumber>();

            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            _lst.Clear();

            SqlCeCommand ceCmd;
            SqlCeDataReader ceDR;

            ceCmd = new SqlCeCommand("SELECT id, inv_num FROM documents", SQLCeCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(new InventarNumber((int)ceDR[0], (int)ceDR[1]));
            }
            ceDR.Close();
            SQLCeCon.Close();

            return _lst;
        }
    }
}
