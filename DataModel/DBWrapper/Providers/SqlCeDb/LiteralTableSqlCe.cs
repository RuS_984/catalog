﻿using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBData.Entities;
using DataModel.SupportClasses;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class LiteralTableSqlCe : LiteralTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        public LiteralTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;
        }

        public override List<Literal> GetEntities()
        {
            List<Literal> _lst = new List<Literal>();

            SqlCeCommand ceCmd;
            SqlCeDataReader ceDR;


            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            _lst.Clear();

            ceCmd = new SqlCeCommand("SELECT literal_id, literal_name FROM literals", SQLCeCon);
            try
            {
                ceDR = ceCmd.ExecuteReader();
                while (ceDR.Read())
                {
                    _lst.Add(new Literal((int)ceDR[0], (string)ceDR[1]));
                }
                ceDR.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            SQLCeCon.Close();

            return _lst;
        }

        public override bool AddToDB(string value)
        {
            int i = -1;

            string cmd = string.Format(@"INSERT Into literals (literal_name) Values (@literal_name)");

            //SqlCeCommand _cmd = new SqlCeCommand();
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@literal_name", SqlDbType.NVarChar).Value =
                    value;

                try
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                }
                catch (Exception ex)
                {

                    if (ex is SqlCeException)
                    {
                        ExceptionHandler.SQLEntityException exc =
                            new ExceptionHandler.SQLEntityException("AddLiteral", ex);
                        exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                        exc.SQLTableName = ExceptionHandler.DBTableName.Literal;
                        exc.SQLEntityID = -1;
                        exc.SQLEntityName = value;
                        exc.OperationType = ExceptionHandler.DBOperationType.AddToDB;
                        throw exc;
                    }
                    else
                    { throw ex; }
                }
            }

            GetEntities();

            return i > 0;
        }

        public override bool DeleteFromDB(string value, int id)
        {
            string cmd = string.Format(@"DELETE FROM literals where literal_id = @id");

            int i = -1;

            //SqlCeCommand _cmd = new SqlCeCommand();
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

                try
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);// SQLExec_(_cmd);
                }
                catch (Exception ex)
                {
                    if (ex is SqlCeException)
                    {
                        ExceptionHandler.SQLEntityException exc =
                            new ExceptionHandler.SQLEntityException("DeleteLiteral", ex);
                        // "Невозможно удалить выбраный проект " + value);

                        //exc.Data.Add("project_id", _id);
                        exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                        exc.SQLTableName = ExceptionHandler.DBTableName.Literal;
                        exc.SQLEntityID = id;
                        exc.SQLEntityName = value;
                        exc.OperationType = ExceptionHandler.DBOperationType.DeleteFromDB;
                        throw exc;
                    }
                    else
                    { throw ex; }
                }
            }

            GetEntities();
            return i > 0;
        }

        public override bool RenameInDB(string oldValue, string newValue, int oldValueId)
        {
            string cmd = string.Format(@"UPDATE literals SET literal_name = @name WHERE literal_id = @id");

            int i = -1;

            //SqlCeCommand _cmd = new SqlCeCommand();
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@id", SqlDbType.Int).Value = oldValueId;
                _cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = newValue;

                try
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);// SQLExec_(_cmd);
                }
                catch (Exception ex)
                {
                    if (ex is SqlCeException)
                    {
                        ExceptionHandler.SQLEntityException exc =
                        new ExceptionHandler.SQLEntityException("RenameLiteral", ex);
                        exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                        exc.SQLTableName = ExceptionHandler.DBTableName.Literal;
                        exc.SQLEntityID = oldValueId;
                        exc.SQLEntityName = newValue;
                        exc.OperationType = ExceptionHandler.DBOperationType.RenameInDB;
                        throw exc;
                    }
                    else
                    { throw ex; }
                }
            }

            GetEntities();
            return i > 0;
        }

        public override bool ExecuteTestCommand(string[] value)
        {
            //bool res;
            int i = -1;

            foreach (var item in value)
            {
                //SqlCeCommand _cmd = new SqlCeCommand();

                using (SqlCeCommand _cmd = new SqlCeCommand(item, SQLCeCon))
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);// SQLExec_(_cmd);
                }
            }

            return i > 0;
        }

        public override bool ExecuteBatchCommand(string[] value)
        {
            bool res = SQLCommon.SQLBatchCommandExec(
                    value, SQLCeCon, new SqlCeCommand()
                );

            return res;
        }
    }
}
