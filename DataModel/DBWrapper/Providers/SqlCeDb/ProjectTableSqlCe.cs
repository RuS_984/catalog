﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlServerCe;

using DBData.Entities;
using DBData.Repository;
using DataModel.SupportClasses;

using DataModel.DBWrapper.Providers.Abstract;


namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class ProjectTableSqlCe : ProjectTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        SqlCeCommand ceCmd;
        SqlCeDataReader ceDR;
        SqlCeDataAdapter ceDA;

        const string Ordered_Project_Request = 
            @"SELECT * FROM project order BY project_name ";

        const string Project_Request =
            @"SELECT project_id, project_name FROM project";

        public ProjectTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            //if (SQLCeCon != null)
            //{
                SQLCeCon = (SqlCeConnection)CNN;
            //}
            //else
            //{
            //    throw new ArgumentNullException("Connection" );
            //}
        }

        public override List<Project> GetEntities()
        {
            List<Project> _lst = new List<Project>();

            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            _lst.Clear();

            ceCmd = new SqlCeCommand(Project_Request, SQLCeCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(new Project((int)ceDR[0], (string)ceDR[1]));
            }
            ceDR.Close();
            CNN.Close();

            return _lst;
        }



        public override bool AddToDB(string value)
        {
            int i = -1;

            string cmd = string.Format(@"INSERT Into project (project_name) Values (@prj_name)");

            //SqlCeCommand _cmd = new SqlCeCommand();   
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@prj_name", SqlDbType.NVarChar).Value =
                    value;

                try
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                }
                catch (Exception ex)
                {

                    if (ex is SqlCeException)
                    {
                        ExceptionHandler.SQLEntityException exc =
                            new ExceptionHandler.SQLEntityException("AddProject", ex);
                        exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                        exc.SQLTableName = ExceptionHandler.DBTableName.Project;
                        exc.SQLEntityID = -1;
                        exc.SQLEntityName = value;
                        exc.OperationType = ExceptionHandler.DBOperationType.AddToDB;
                        throw exc;
                    }
                    else
                    { throw ex; }
                }
            }

            GetEntities();

            return i > 0;
        }

        public override bool DeleteFromDB(string value, int id)
        {
            string cmd = string.Format(@"DELETE FROM project where project_id = @id");

            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            try
            {
                i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
            }
            catch (Exception ex)
            {
                if (ex is SqlCeException)
                {
                    ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException("DeleteProject", ex);
                    exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                    exc.SQLTableName = ExceptionHandler.DBTableName.Project;
                    exc.OperationType = ExceptionHandler.DBOperationType.DeleteFromDB;
                    exc.SQLEntityID = id;
                    exc.SQLEntityName = value;
                    
                    throw exc;
                }
                else
                { throw ex; }
            }
            return i > 0;
        }

        public override bool RenameInDB(string oldValue, string newValue, int oldValueId)
        {
            string cmd = string.Format(@"UPDATE project SET project_name = @name WHERE project_id = @id");

            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);
            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = oldValueId;
            _cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = newValue;

            try
            {
                SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
            }
            catch (Exception ex)
            {

                if (ex is SqlCeException)
                {
                    ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException("Rename", ex);
                    exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                    exc.SQLTableName = ExceptionHandler.DBTableName.Project;
                    exc.OperationType = ExceptionHandler.DBOperationType.RenameInDB;
                    exc.SQLEntityID = oldValueId;
                    exc.SQLEntityName = newValue;
                    throw exc;
                }
                else
                { throw ex; }
            }

            GetEntities();

            return i > 0;
        }

        public override DataTable GetProjectTable()
        {
            DataSet documents_ds = new DataSet();

            ceDA = new SqlCeDataAdapter(Ordered_Project_Request, SQLCeCon);
            ceDA.Fill(documents_ds, "documents");
            return documents_ds.Tables["documents"];

        }

        protected int SQLExec1(SqlCeCommand SQLCommand)
        {
            int i = -1;

            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

            //    if (OnUpdate != null)
            //    {
            //        OnUpdate();
            //    }
            }
            //catch (SqlCeException ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    throw ex;
            //}
            finally
            {
                CNN.Close();
            }
            return i;

        }
    }
}
