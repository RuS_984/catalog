﻿using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBData.Entities;
using DataModel.SupportClasses;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class IspolniteliTableSqlCe : IspolniteliTableAbstract
    {
        SqlCeConnection SQLCeCon;

        public IspolniteliTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;
        }

        public override List<Ispolniteli> GetEntities()
        {
            List<Ispolniteli> _lst = new List<Ispolniteli>();


            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            _lst.Clear();

            SqlCeCommand ceCmd;
            SqlCeDataReader ceDR;

            ceCmd = new SqlCeCommand("SELECT ispoln_id, second_name FROM ispolniteli",
                SQLCeCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(new Ispolniteli((int)ceDR[0], (string)ceDR[1]));
            }
            ceDR.Close();

            SQLCeCon.Close();

            return _lst;
        }




        public override bool AddToDB(string text)
        {
            int i = -1;
            string cmd = string.Format(@"SELEct MAX(ispoln_id) from ispolniteli");

            SqlCeCommand ceCmd = new SqlCeCommand();

            ceCmd = new SqlCeCommand(cmd, SQLCeCon);

            cmd = string.Format(@"INSERT Into ispolniteli (second_name) Values (@second_name)");

            //SqlCeCommand _cmd = new SqlCeCommand();
            //_cmd = new SqlCeCommand(cmd, SQLCeCon);

            //_cmd.Parameters.Add("@second_name", SqlDbType.NVarChar).Value =
            //    text;

            //++----------------------------------------
            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@second_name", SqlDbType.NVarChar).Value =
                text;

                try
                {
                    i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                }
                catch (Exception ex)
                {

                    if (ex is SqlCeException)
                    {
                        ExceptionHandler.SQLEntityException exc =
                            new ExceptionHandler.SQLEntityException("AddIspolniteli", ex);
                        exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                        exc.SQLTableName = ExceptionHandler.DBTableName.Ispolniteli;
                        exc.SQLEntityID = -1;
                        exc.SQLEntityName = text;
                        exc.OperationType = ExceptionHandler.DBOperationType.AddToDB;
                        throw exc;
                    }
                    else
                    { throw ex; }
                }
            }

            //++----------------------------------------

            GetEntities();

            return i > 0;
        }

        public override bool DeleteFromDB(string value, int id)
        {
            string cmd = string.Format(@"DELETE FROM ispolniteli where ispoln_id = @id");

            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            try
            {
                //i = SQLExec(_cmd);
                i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
            }
            catch (SqlCeException ex)
            {
                ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException("DeleteIsp", ex);
                exc.SQLNativeError = ex.NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.Ispolniteli;
                exc.SQLEntityID = id;
                exc.SQLEntityName = value;

                throw exc;
            }

            GetEntities();

            return i > 0;
        }



        public override bool RenameInDB(string oldValue, string newValue, int oldValueId)
        {
            string cmd = string.Format(@"UPDATE ispolniteli SET second_name = @name WHERE ispoln_id = @id");

            int i = -1;

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = oldValueId;
            _cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = newValue;

            try
            {
                //i = SQLExec(_cmd);
                i = SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
            }
            catch (Exception ex)
            {
                if (ex is SqlCeException)
                {
                    ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException("RenameIsp", ex);
                    exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                    exc.SQLTableName = ExceptionHandler.DBTableName.Ispolniteli;
                    exc.SQLEntityID = oldValueId;
                    exc.SQLEntityName = newValue;
                    exc.OperationType = ExceptionHandler.DBOperationType.RenameInDB;
                    throw exc;
                }
                else
                {
                    throw ex;
                }

            }

            GetEntities();

            return i > 0;
        }

        protected int SQLExec1(SqlCeCommand SQLCommand)
        {
            int i = -1;

            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

                //    if (OnUpdate != null)
                //    {
                //        OnUpdate();
                //    }
            }
            //catch (SqlCeException ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    throw ex;
            //}
            finally
            {
                CNN.Close();
            }
            return i;
        }
    }
}
