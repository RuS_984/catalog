﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;

using DataModel.DBWrapper.Providers.Abstract;
using System.Data;


namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class SqlCeDbProvider : DatabaseProvider
    {
        public SqlCeDbProvider(string connectStr)
            : base(connectStr)
        {
            CNN = new SqlCeConnection(CONNECT_STR);
        }

        public override ProjectTableAbstract GetProjectTable()
        {

            return new ProjectTableSqlCe(CNN);
        }


        public override FirstUsageTableAbstract GetFirstUsageTable()
        {
            return new FirstUsageTableSqlCe(CNN);
        }

        public override OboznachenieTableAbstract GetOboznachenieTable()
        {
            return new OboznachenieTableSqlCe(CNN);
        }

        public override LiteralTableAbstract GetLiteralTable()
        {
            return new LiteralTableSqlCe(CNN);
        }

        public override IspolniteliTableAbstract GetIspolniteliTable()
        {
            return new IspolniteliTableSqlCe(CNN);
        }

        public override InventarNumberTableAbstract GetInventarNumberTable()
        {
            return new InventarNumberTableSqlCe(CNN);
        }

        public override MainTableAbstract GetMainTable()
        {
            return new MainTableSqlCe(CNN, new SqlCeCommand());
        }

        //public override DependencyTableAbstract GetDependencyTable()
        //{
        //    throw new NotImplementedException();
        //}

        public override DependencyTableAbstract GetDependencyTable()
        {
            return new DependencyTableSqlCe(CNN);
        }
    }
}
