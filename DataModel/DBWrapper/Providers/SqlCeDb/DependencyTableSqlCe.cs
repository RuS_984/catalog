﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataModel.DBWrapper.Providers.Abstract;
using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class DependencyTableSqlCe : DependencyTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        public DependencyTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;
        }

        SqlCeCommand ceCmd;
        SqlCeDataReader ceDR;
        SqlCeDataAdapter ceDA;


        #region Requests

        const string Full_Dependency_Request = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie

            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            JOIN documents idoc1 ON dependency.documents_id=idoc1.inv_num
            order BY is_asm desc, documents_id";


        const string Asm_Dependency_Request = @"SELECT dependency.ID

                ,dependency.documents_id
                ,dependency.parent_id
                ,dependency.is_asm
                ,idoc.oboznachenie
                ,dependency.project_id
                ,idoc.naimenovanie
                
            FROM dependency
            JOIN documents idoc ON dependency.documents_id=idoc.inv_num
            where dependency.is_asm = 1
            order BY is_asm desc, documents_id";

        const string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc.oboznachenie as doc_name

                ,dependency.parent_id
                ,idoc3.oboznachenie as parent_name

                ,dependency.is_asm

                ,idoc1.project_id
                ,idoc2.project_name

                FROM dependency
                JOIN documents idoc  ON dependency.documents_id = idoc.inv_num
                JOIN documents idoc3 ON dependency.parent_id    = idoc3.inv_num
                JOIN documents idoc1 ON dependency.documents_id = idoc1.inv_num
                JOIN project   idoc2 ON idoc1.project_id     	= idoc2.project_id

                order BY is_asm desc, documents_id";


        #endregion










        public override List<Dependency> GetEntities()
        {
            List<Dependency> _lst = new List<Dependency>();

            _lst.Clear();

            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            ceCmd = new SqlCeCommand(documents_req, SQLCeCon);

            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(
                        new Dependency(
                            dependencyId: (int)ceDR[0],
                            documentID: (int)ceDR[1],
                            documentName: (string)ceDR[2],
                            parentID: (int)ceDR[3],
                            parentName: (string)ceDR[4],
                            isAsm: (bool)ceDR[5],
                            projectId: (int)ceDR[6],
                            projectName: (string)ceDR[7]
                            )
                        );
            }
            ceDR.Close();
            SQLCeCon.Close();

            return _lst;
        }


        public override DataTable GetFullDependencyTable()
        {
            DataSet documents_ds = new DataSet();

            ceDA = new SqlCeDataAdapter(Full_Dependency_Request, SQLCeCon);
            ceDA.Fill(documents_ds, "documents");
            return documents_ds.Tables["documents"];

        }


        public override DataTable GetAsmDependencyTable()
        {
            DataSet documents_ds = new DataSet();

            ceDA = new SqlCeDataAdapter(Asm_Dependency_Request, SQLCeCon);
            ceDA.Fill(documents_ds, "documents");
            return documents_ds.Tables["documents"];
        }

        public override bool UpdateDB(List<int> ForAdd, List<int> ForDelete,
            List<int> ForAddRoot, List<int> ForDeleteRoot,
            string DocId, bool IsAsm)
        {
            int i = 0;
            int y = 0;

            {//НОВЫЙ ВАРИАНТ
                if (CNN.State == ConnectionState.Closed)
                {
                    CNN.Open();
                }

                foreach (int item in ForAdd)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm])
                      values (@documents_id, @parent_id, @is_asm)");

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, SQLCeCon);

                    _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(DocId);
                    _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = Convert.ToInt32(item);
                    _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(IsAsm);

                    try
                    {
                        i += SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);

                    }

                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }

                foreach (int item in ForAddRoot)
                {
                    string cmd = string.Format(
                        @"insert into dependency ([documents_id],[parent_id], [is_asm], [project_id])
                      values (@documents_id, @parent_id, @is_asm, @project_id)");

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, SQLCeCon);

                    _cmd.Parameters.Add("@documents_id", SqlDbType.Int).Value = Convert.ToInt32(DocId);
                    _cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = 0;
                    _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = Convert.ToInt32(IsAsm);
                    _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = Convert.ToInt32(item);

                    try
                    {
                        i += SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);

                    }
                    finally
                    { }
                }

                if (ForDelete.Count > 0)
                {
                    string cmd = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", DocId.ToString(), GetDeleteIds(ForDelete));

                    //string cmd1 = string.Format(@"DELETE FROM dependency where documents_id = {0} and parent_id in {1}", docId.ToString(), SupportClasses.DBprocessing.ListToString(forDelete));

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, SQLCeCon);

                    try
                    {
                        y += SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                    }
                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }

                if (ForDeleteRoot.Count > 0)
                {
                    //string cmd = string.Format(@"DELETE FROM dependency where ID in {0}", GetDeleteIds(forDelete));
                    string cmd = string.Format(
                        @"DELETE FROM dependency where documents_id = {0} and project_id in {1}",
                        DocId.ToString(), GetDeleteIds(ForDeleteRoot));

                    SqlCeCommand _cmd = new SqlCeCommand();
                    _cmd = new SqlCeCommand(cmd, SQLCeCon);

                    try
                    {
                        y += SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                    }
                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }

                if (SQLCeCon.State == ConnectionState.Open)
                {
                    SQLCeCon.Close();
                }
            }
            GetEntities();
            return i > 0;
        }


        public override void ChangeDocumentID(int oldDocuments_id, int newDcuments_id, bool isAsm)
        {
            string cmd = string.Format(
                @"UPDATE dependency SET documents_id=@newDcuments_id, is_asm=@is_asm
                  WHERE ID in 
                  (select [id] from dependency where documents_id=@oldDocuments_id)"
            );

            using (SqlCeCommand _cmd = new SqlCeCommand(cmd, SQLCeCon))
            {
                _cmd.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
                _cmd.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;
                _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = isAsm;

                string cmd2 = string.Format(
                    @"UPDATE dependency SET parent_id=@newDcuments_id WHERE ID in 
                      (select [id] from dependency where parent_id=@oldDocuments_id)"
                );

                using (SqlCeCommand _cmd2 = new SqlCeCommand(cmd2, SQLCeCon))
                {
                    _cmd2.Parameters.Add("@oldDocuments_id", SqlDbType.Int).Value = oldDocuments_id;
                    _cmd2.Parameters.Add("@newDcuments_id", SqlDbType.Int).Value = newDcuments_id;

                    try
                    {
                        SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd);
                        SQLCommon.SQLSingleCommandExec(SQLCeCon, _cmd2);
                    }
                    //catch (SqlCeException ex)
                    //{
                    //    throw ex;

                    //}
                    finally
                    { }
                }
            }
        }


        public override DataTable GetDependency1Table(string obooboznachenie)
        {
            DataSet documents_ds = new DataSet();
            SqlCeDataAdapter dataAdapter;

            string documents_req =
                @"SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.oboznachenie as parent_name


                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num

                JOIN documents idoc2 ON dependency.parent_id=idoc2.inv_num

                where idoc.oboznachenie = @oboznachenie  and idoc2.oboznachenie <> 'корень'
                order BY oboznachenie";

            SqlCeCommand _cmd = new SqlCeCommand();
            SqlCeConnection ceCon = new SqlCeConnection(null);//(sqlceCon.ConnectionString);

            _cmd = new SqlCeCommand(documents_req, SQLCeCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds, "documents");

            return documents_ds.Tables["documents"];

        }


        public override DataTable GetDependency2Table(string obooboznachenie)
        {
            DataSet documents_ds = new DataSet();
            SqlCeDataAdapter dataAdapter;

            string project_req =
                @" SELECT dependency.ID
                ,dependency.documents_id
                ,idoc2.project_name as parent_name

                FROM dependency
                JOIN documents idoc ON dependency.documents_id=idoc.inv_num
                JOIN project idoc2 ON dependency.project_id=idoc2.project_id
                
                where idoc.oboznachenie = @oboznachenie
                order BY project_name";


            SqlCeCommand _cmd = new SqlCeCommand(project_req, SQLCeCon);

            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = obooboznachenie;

            dataAdapter = new SqlCeDataAdapter(_cmd);
            dataAdapter.Fill(documents_ds, "documents");

            return documents_ds.Tables["documents"];

        }



        public string GetDeleteIds(List<int> list)
        {
            string _str = "(";
            foreach (int item in list)
            {
                _str += item + ", ";
            }

            _str = _str.Substring(0, _str.Length - 2) + ")";

            return _str;
        }

        protected int SQLExec1545(SqlCeCommand SQLCommand)
        {
            int i = -1;

            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

                //    if (OnUpdate != null)
                //    {
                //        OnUpdate();
                //    }
            }
            //catch (SqlCeException ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    throw ex;
            //}
            finally
            {
                CNN.Close();
            }
            return i;
        }

        //public override bool UpdateDB()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
