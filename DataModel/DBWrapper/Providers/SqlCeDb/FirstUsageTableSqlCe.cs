﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataModel.DBWrapper.Providers.Abstract;
using System.Data.SqlServerCe;
using System.Data;

using DBData.Entities;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class FirstUsageTableSqlCe : FirstUsageTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        public FirstUsageTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;
        }

        SqlCeCommand ceCmd;
        SqlCeDataReader ceDR;

        public override List<FirstUsage> GetEntities()
        {
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            List<FirstUsage> _lst = new List<FirstUsage>();

            _lst.Clear();

            ceCmd = new SqlCeCommand("SELECT id, oboznachenie  FROM documents WHERE (is_asm = 1)", SQLCeCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {
                _lst.Add(new FirstUsage((int)ceDR[0], (string)ceDR[1]));
            }
            ceDR.Close();

            SQLCeCon.Close();

            return _lst;
        }


    }
}
