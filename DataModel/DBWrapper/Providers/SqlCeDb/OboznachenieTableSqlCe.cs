﻿using DBData.Entities;
using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    public class OboznachenieTableSqlCe : OboznachenieTableAbstract
    {
        static SqlCeConnection SQLCeCon;

        public OboznachenieTableSqlCe(IDbConnection Connection) : base(Connection)
        {
            SQLCeCon = (SqlCeConnection)CNN;

        }

        public override List<Oboznachenie> GetEntities()
        {
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            List<Oboznachenie> _lst = new List<Oboznachenie>();

            _lst.Clear();

            SqlCeCommand ceCmd;
            SqlCeDataReader ceDR;

            ceCmd = new SqlCeCommand("SELECT Id, oboznachenie FROM documents", SQLCeCon);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {

                _lst.Add(new Oboznachenie((int)ceDR[0], (string)ceDR[1]));

            }
            ceDR.Close();
            SQLCeCon.Close();

            return _lst;
        }
    }
}
