﻿namespace DataModel.DBWrapper.Providers.SqlCeDb
{
    using DataModel.DBWrapper.Providers.Abstract;
    using DataModel.SupportClasses;
    using DBData;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlServerCe;

    /// <summary>
    /// Defines the <see cref=MainTableSqlCe />
    /// </summary>
    public class MainTableSqlCe : MainTableAbstract
    {
        /// <summary>
        /// Defines the dataAdapter
        /// </summary>
        internal static SqlCeDataAdapter dataAdapter;

        /// <summary>
        /// Defines the SQLCeCon
        /// </summary>
        private SqlCeConnection SQLCeCon;

        public MainTableSqlCe(IDbConnection Connection, IDbCommand Command) : base(Connection, Command)
        {
            SQLCeCon = (SqlCeConnection)CNN;
            //SQLCeCom1 = new SqlCeCommand();
        }

        public override int CreateRecord(MainTableStruct mdbs)
        {
            int i = -1;
            SqlCeCommand _cmd = new SqlCeCommand();
            /*
            _cmd = new SqlCeCommand(
                @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", CECon);


            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            */
            _cmd = (SqlCeCommand)GetSqlCommand(mdbs);
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("CreateRecord", ex);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.AddToDB;
                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
            return i;
        }

        public override int CreateRecords(List<MainTableStruct> lstmdbs)
        {
            int i = 0;
            SqlCeCommand _cmd = new SqlCeCommand();
            /*
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(
                @"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", CECon);


            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            */
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                for (int j = 0; j < lstmdbs.Count; j++)
                {
                    _cmd = (SqlCeCommand)GetSqlCommand(lstmdbs[j]);
                    i += _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("CreateRecords", ex);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.AddToDB;
                throw exc;
            }
            finally
            {
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }

                SQLCeCon.Close();
            }
            return i;
        }
        //++DONE
        public override int DeleteRecordByRow(int RowId, int DocumentId)
        {
            int iDocuments = 0;
            int iDependency = 0;
            SqlCeCommand cmdDocuments = new SqlCeCommand();
            cmdDocuments = new SqlCeCommand(@"DELETE FROM documents where id = @id", SQLCeCon);
            cmdDocuments.Parameters.Add("@id", SqlDbType.Int).Value = RowId;
            SqlCeCommand cmdDependency = new SqlCeCommand();
            cmdDependency = new SqlCeCommand(@"DELETE FROM dependency where documents_id = @id", SQLCeCon);
            cmdDependency.Parameters.Add("@id", SqlDbType.Int).Value = DocumentId;
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                iDocuments += cmdDocuments.ExecuteNonQuery();
                iDependency += cmdDependency.ExecuteNonQuery();
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("DeleteRecord", ex);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.DeleteFromDB;
                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
            return iDocuments + iDependency;
        }

        //++DONE
        public override void GetMainTable1(string ordered_by)
        {
            mainDBdt.Reset();
            mainDBds.Reset();
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                if (ordered_by == null)
                {
                    db_req = DBprocessing.SQLRequest();
                }
                else
                {
                    db_req = DBprocessing.SQLRequest(ordered_by: ordered_by);
                }

                dataAdapter = new SqlCeDataAdapter(db_req, SQLCeCon);
                dataAdapter.Fill(mainDBds, "documents");
                mainDBdt = mainDBds.Tables["documents"];
                colNames = new string[mainDBdt.Columns.Count];
                for (int i = 0; i < mainDBdt.Columns.Count; i++)
                {
                    colNames[i] = mainDBdt.Columns[i].Caption;
                }

                for (int i = mainDBdt.Rows.Count - 1; i >= 0; i--)
                {
                    if (mainDBdt.Rows[i][1].ToString() == "0")
                    {
                        mainDBdt.Rows.RemoveAt(i);
                        break;
                    }
                }

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("GetMainTable1", ex);
                // "Невозможно удалить выбраный проект " + value);
                //exc.Data.Add("project_id", _id);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.GetEntities;
                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
        } //GetMainDB

        //++DONE
        public override void GetMainTable(bool IsOrdered)
        {
            mainDBdt.Reset();
            mainDBds.Reset();
            string dbrq = dbRequest;
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                if (IsOrdered)
                {
                    dbrq += " ORDER BY " + orderedBy;
                }

                dataAdapter = new SqlCeDataAdapter(dbrq, SQLCeCon);
                dataAdapter.Fill(mainDBds, "documents");
                mainDBdt = mainDBds.Tables["documents"];
                colNames = new string[mainDBdt.Columns.Count];
                for (int i = 0; i < mainDBdt.Columns.Count; i++)
                {
                    colNames[i] = mainDBdt.Columns[i].Caption;
                }

                for (int i = mainDBdt.Rows.Count - 1; i >= 0; i--)
                {
                    if (mainDBdt.Rows[i][1].ToString() == "0")
                    {
                        mainDBdt.Rows.RemoveAt(i);
                        break;
                    }
                }

                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("GetMainTable", ex);
                // "Невозможно удалить выбраный проект " + value);
                //exc.Data.Add("project_id", _id);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.GetEntities;
                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
        } //GetMainDB

        public override int UpdateRecord(MainTableStruct mdbs)
        {
            int i = -1;
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"UPDATE documents
            SET project_id = @project_id, inv_num = @inv_num,
                oboznachenie = @oboznachenie, naimenovanie = @naimenovanie,
                pervichprimen = @pervichprimen, literal=@literal, date = @date,
                is_asm = @is_asm, razrabotal = @razrabotal,
                proveril = @proveril, tehcontrol = @tehcontrol,
                normcontrol = @normcontrol, utverdil = @utverdil
                where id = @id", SQLCeCon);
            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = mdbs.Id;
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("UpdateRecord", ex);
                // "Невозможно удалить выбраный проект " + value);
                //exc.Data.Add("project_id", _id);
                //exc. ;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = mdbs.InventarnNum.ToString();
                exc.OperationType = ExceptionHandler.DBOperationType.RenameInDB;

                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
            return i;
        }

        public override int UpdateRecordByCol(string ColName, int ColValue, string IdRange)
        {
            int i = -1;
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"UPDATE documents
                SET " + ColName + @" = @colval
                where id IN " + IdRange, SQLCeCon);
            _cmd.Parameters.Add("@colval", SqlDbType.Int).Value = ColValue;
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                i = _cmd.ExecuteNonQuery();
                if (OnDBUpdate != null)
                {
                    OnDBUpdate();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.SQLEntityException exc = new ExceptionHandler.SQLEntityException("UpdateRecord", ex);
                // "Невозможно удалить выбраный проект " + value);
                //exc.Data.Add("project_id", _id);
                exc.DBtype = DBType.MSSQLCE;
                exc.SQLNativeError = ((SqlCeException)ex).NativeError;
                exc.SQLTableName = ExceptionHandler.DBTableName.MainTable;
                exc.SQLEntityID = -1;
                exc.SQLEntityName = "value";
                exc.OperationType = ExceptionHandler.DBOperationType.RenameInDB;
                throw exc;
            }
            finally
            {
                SQLCeCon.Close();
            }
            return i;
        }

        protected override int GetMaxInvNum()
        {
            int i = 0;
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"SELECT MAX(inv_num) FROM documents", SQLCeCon);
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                int.TryParse(_cmd.ExecuteScalar().ToString(), out i);
            }
            finally
            {
                SQLCeCon.Close();
            }
            return i;
        }

        protected override int GetMinInvNum()
        {
            int i = 0;
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"SELECT MIN(inv_num) FROM documents", SQLCeCon);
            if (SQLCeCon.State == ConnectionState.Closed)
            {
                SQLCeCon.Open();
            }

            try
            {
                int.TryParse(_cmd.ExecuteScalar().ToString(), out i);
            }
            finally
            {
                SQLCeCon.Close();
            }
            return i;
        }

        protected override IDbCommand GetSqlCommand(MainTableStruct mdbs)
        {
            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(@"INSERT INTO documents
                (project_id, inv_num, oboznachenie, naimenovanie, pervichprimen, literal, date,is_asm,
                 razrabotal, proveril, tehcontrol, normcontrol, utverdil)
                 VALUES
                (@project_id, @inv_num, @oboznachenie, @naimenovanie,@pervichprimen, @literal, @date, @is_asm,
                 @razrabotal, @proveril, @tehcontrol, @normcontrol, @utverdil)", SQLCeCon);
            _cmd.Parameters.Add("@project_id", SqlDbType.Int).Value = mdbs.ProjectID;
            _cmd.Parameters.Add("@inv_num", SqlDbType.Int).Value = mdbs.InventarnNum;
            _cmd.Parameters.Add("@oboznachenie", SqlDbType.NVarChar).Value = mdbs.Oboznachenie.ToUpper();
            _cmd.Parameters.Add("@naimenovanie", SqlDbType.NVarChar).Value = mdbs.Naimenovanie;
            _cmd.Parameters.Add("@pervichprimen", SqlDbType.NVarChar).Value = mdbs.Pervichoepreminenie.ToUpper();
            _cmd.Parameters.Add("@literal", SqlDbType.Int).Value = mdbs.Literal;
            _cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = mdbs.Date;
            _cmd.Parameters.Add("@is_asm", SqlDbType.Bit).Value = mdbs.IsAsm;
            _cmd.Parameters.Add("@razrabotal", SqlDbType.Int).Value = mdbs.Razrabotal;
            _cmd.Parameters.Add("@proveril", SqlDbType.Int).Value = mdbs.Proveril;
            _cmd.Parameters.Add("@tehcontrol", SqlDbType.Int).Value = mdbs.Tehcontrol;
            _cmd.Parameters.Add("@normcontrol", SqlDbType.Int).Value = mdbs.Normokontrol;
            _cmd.Parameters.Add("@utverdil", SqlDbType.Int).Value = mdbs.Utverdil;
            return _cmd;
        }
        public override event DBUpdate OnDBUpdate;
    }
}