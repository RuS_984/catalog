﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.Common;

namespace DataModel.DBWrapper.Providers
{
    public static class SQLCommon
    {
        public static int SQLSingleCommandExec(IDbConnection CNN, DbCommand SQLCommand)
        {
            if (SQLCommand == null)
            {
                throw new ArgumentNullException(nameof(SQLCommand));
            }

            int i = -1;

            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();
            }

            finally
            {
                CNN.Close();
            }
            return i;
        }

        public static bool SQLBatchCommandExec(IEnumerable<string> commands, IDbConnection CNN, DbCommand SQLCommand)
        {
            int i = -1;

            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                foreach (var item in commands)
                {
                    SQLCommand.Connection = (DbConnection)CNN;
                    SQLCommand.CommandText = item;

                    i = SQLSingleCommandExec(CNN, SQLCommand);
                }
            }

            finally
            {
                CNN.Close();
            }
            return i > 0;
        }
    }
}
