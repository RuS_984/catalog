﻿using DBData.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class DependencyTableAbstract
    {
        protected IDbConnection CNN = null;

        public DependencyTableAbstract(IDbConnection Connection)
        {
            CNN = Connection;
        }

        /// <summary>
        ///  Получить список зависимостей
        /// </summary>
        /// <returns></returns>
        public abstract List<Dependency> GetEntities();

        /// <summary>
        /// Получить таблицу зависимостей для всех документов
        /// </summary>
        /// <returns></returns>
        public abstract DataTable GetFullDependencyTable();

        /// <summary>
        /// Получить таблицу зависимостей только для сборочников
        /// </summary>
        /// <returns></returns>
        public abstract DataTable GetAsmDependencyTable();

        //public abstract bool UpdateDB();
        public abstract bool UpdateDB(
            List<int> ForAdd, List<int> ForDelete,
            List<int> ForAddRoot, List<int> ForDeleteRoot,
            string DocId, bool IsAsm
        );


        public abstract DataTable GetDependency1Table(string obooboznachenie);
        public abstract DataTable GetDependency2Table(string obooboznachenie);


        public abstract void ChangeDocumentID(int oldDocuments_id, int newDcuments_id, bool isAsm);



    }
}