﻿using DBData.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class IspolniteliTableAbstract
    {
        protected IDbConnection CNN = null;

        public IspolniteliTableAbstract(IDbConnection Connection)
        {
            CNN = Connection;
        }

        public abstract List<Ispolniteli> GetEntities();

        public abstract bool AddToDB(string text);

        public abstract bool DeleteFromDB(string value, int id);

        public abstract bool RenameInDB(string oldValue, string newValue, int oldValueId);
    }
}
