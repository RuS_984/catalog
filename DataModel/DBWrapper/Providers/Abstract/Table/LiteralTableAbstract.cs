﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DBData.Entities;

namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class LiteralTableAbstract
    {
        protected IDbConnection CNN = null;

        public LiteralTableAbstract(IDbConnection Connection)
        {
            CNN = Connection;
        }

        public abstract List<Literal> GetEntities();
        public abstract bool AddToDB(string value);
        public abstract bool DeleteFromDB(string value, int id);
        public abstract bool RenameInDB(string oldValue, string newValue, int oldValueId);
        public abstract bool ExecuteTestCommand(string[] value);
        public abstract bool ExecuteBatchCommand(string[] value);
    }
}
