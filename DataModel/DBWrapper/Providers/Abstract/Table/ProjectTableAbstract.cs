﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using DBData.Entities;


namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class ProjectTableAbstract
    {
        protected IDbConnection CNN = null;
        

        public ProjectTableAbstract(IDbConnection Connection)
        {
            CNN = Connection;
        }

        public abstract List<Project> GetEntities();

        public abstract DataTable GetProjectTable();

        public abstract bool AddToDB(string value);

        public abstract bool DeleteFromDB(string value, int id);

        public abstract bool RenameInDB(string oldValue, string newValue, int oldValueId);


    }
}
