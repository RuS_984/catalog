﻿using DBData.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class FirstUsageTableAbstract
    {
        protected IDbConnection CNN = null;

        public FirstUsageTableAbstract(IDbConnection Connection)
        {
            CNN = Connection;
        }

        public abstract List<FirstUsage> GetEntities();

        
    }
}
