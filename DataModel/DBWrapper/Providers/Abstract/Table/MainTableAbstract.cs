﻿
namespace DataModel.DBWrapper.Providers.Abstract
{
    using DataModel.SupportClasses;
    using DBData;
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// Defines the <see cref=MainTableAbstract />
    /// </summary>
    public abstract class MainTableAbstract
    {
        /// <summary>
        /// Defines the firstMinInvNum = 0
        /// </summary>
        internal const int firstMinInvNum = 0;

        /// <summary>
        /// Defines the minInvNum = -1
        /// </summary>
        internal static int minInvNum = -1;

        /// <summary>
        /// Defines the CNN = null
        /// </summary>
        protected IDbConnection CNN = null;

        /// <summary>
        /// Defines the colNames
        /// </summary>
        protected string[] colNames;

        //protected static IDbDataAdapter dataAdapter;
        protected string db_req = "";

        /// <summary>
        /// Defines the DBCom = null
        /// </summary>
        protected IDbCommand DBCom = null;

        /// <summary>
        /// Defines the dbRequest = ""
        /// </summary>
        protected string dbRequest = "";

        /// <summary>
        /// Defines the isOrdered = false
        /// </summary>
        protected bool isOrdered = false;

        /// <summary>
        /// Defines the mainDBds = new DataSet()
        /// </summary>
        protected static DataSet mainDBds = new DataSet();

        /// <summary>
        /// Defines the mainDBdt = new DataTable()
        /// </summary>
        protected static DataTable mainDBdt = new DataTable();

        /// <summary>
        /// Defines the orderedBy = ""
        /// </summary>
        protected string orderedBy = "";

        public MainTableAbstract(IDbConnection Connection, IDbCommand Command)
        {
            CNN = Connection;
            DBCom = Command;
        }

        /// <summary>
        /// Gets or sets the DBRequest
        /// </summary>
        public string DBRequest
        {
            get
            {
                return dbRequest;
            }

            set
            {
                dbRequest = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsOrdered
        /// </summary>
        public bool IsOrdered
        {
            get
            {
                return isOrdered;
            }

            set
            {
                isOrdered = value;
            }
        }

        /// <summary>
        /// Gets the MainDBdt
        /// </summary>
        public DataTable MainDBdt
        {
            get
            {
                if (mainDBdt == null)
                {
                    GetMainTable(isOrdered);
                    
                    return mainDBdt;
                }
                else
                {
                    return mainDBdt;
                }
            }
        }

        /// <summary>
        /// Gets the MaxInvNum
        /// </summary>
        public int MaxInvNum
        {
            get
            {
                int i = GetMaxInvNum();
                //if (i < firstMaxInvNum)
                //{
                //    i = firstMaxInvNum;
                //}
                return i;
            }
        }

        /// <summary>
        /// Gets the MinInvNum
        /// </summary>
        public int MinInvNum
        {
            get
            {
                int i = GetMinInvNum();
                if (i > 0)
                {
                    i = 0;
                }
                return i;
            }
        }

        /// <summary>
        /// Gets or sets the OrderedBy
        /// </summary>
        public string OrderedBy
        {
            get
            {
                return orderedBy;
            }

            set
            {
                orderedBy = value;
            }
        }

        /// <summary>
        /// Gets the CECon
        /// </summary>
        internal IDbConnection CECon
        {
            get
            {
                return CNN;
            }
        }

        public abstract int CreateRecord(MainTableStruct mdbs);

        public abstract int CreateRecords(List<MainTableStruct> lstmdbs);

        public abstract int DeleteRecordByRow(int RowId, int DocumentId);

        /// <summary>
        /// Получить полную таблицу из БД
        /// </summary>
        /// <param name = "ordered_by">Столбец для упорядочивания</param>
        public abstract void GetMainTable1(string ordered_by);

        public abstract void GetMainTable(bool IsOrdered);

        public MainTableStruct GetRowByIndex(int index)
        {
            MainTableStruct mdb;
            //mdb.SetDefaultData();
            mdb.Id = (int)MainDBdt.Rows[index][0];
            mdb.ProjectID = (int)MainDBdt.Rows[index][1];
            mdb.InventarnNum = (int)MainDBdt.Rows[index][2];
            mdb.Oboznachenie = (string)MainDBdt.Rows[index][3];
            mdb.Naimenovanie = (string)MainDBdt.Rows[index][4];
            mdb.Pervichoepreminenie = (string)MainDBdt.Rows[index][5];
            mdb.Literal = (int)MainDBdt.Rows[index][6];
            mdb.Date = ((DateTime)MainDBdt.Rows[index][7]).ToString();
            mdb.IsAsm = (bool)MainDBdt.Rows[index][8];
            mdb.Razrabotal = (int)MainDBdt.Rows[index][9];
            mdb.Proveril = (int)MainDBdt.Rows[index][10];
            mdb.Tehcontrol = (int)MainDBdt.Rows[index][11];
            mdb.Normokontrol = (int)MainDBdt.Rows[index][12];
            mdb.Utverdil = (int)MainDBdt.Rows[index][6];
            return mdb;
        }

        public abstract int UpdateRecord(MainTableStruct mdbs);

        public abstract int UpdateRecordByCol(string ColName, int ColValue, string IdRange);

        protected abstract int GetMaxInvNum();

        protected abstract int GetMinInvNum();

        protected abstract IDbCommand GetSqlCommand(MainTableStruct mdbs);
        public delegate void DBUpdate();
        public virtual  event DBUpdate OnDBUpdate;

        public void InvokeOnDBUpdate()
        {
            var handler = OnDBUpdate;
            if (handler != null)
                handler();
        }
    }
}