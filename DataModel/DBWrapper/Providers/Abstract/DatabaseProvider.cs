﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;


namespace DataModel.DBWrapper.Providers.Abstract
{
    public abstract class DatabaseProvider
    {
        protected string CONNECT_STR = string.Empty;
        protected IDbConnection CNN = null;

        public DatabaseProvider(string connectStr)
        {
            CONNECT_STR = connectStr;
        }

        public abstract ProjectTableAbstract GetProjectTable();

        public abstract FirstUsageTableAbstract GetFirstUsageTable();

        public abstract OboznachenieTableAbstract GetOboznachenieTable();

        public abstract LiteralTableAbstract GetLiteralTable();

        public abstract IspolniteliTableAbstract GetIspolniteliTable();

        public abstract InventarNumberTableAbstract GetInventarNumberTable();

        public abstract DependencyTableAbstract GetDependencyTable();

        public abstract MainTableAbstract GetMainTable();
    }
}
