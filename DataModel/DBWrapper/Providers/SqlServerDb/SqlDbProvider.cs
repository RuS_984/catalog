﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;

using DataModel.DBWrapper.Providers.Abstract;


namespace DataModel.DBWrapper.Providers.SqlServerDb
{
    class SqlDbProvider : DatabaseProvider
    {
        public SqlDbProvider(string connectStr)
            : base(connectStr)
        {
            CNN = new SqlConnection(CONNECT_STR);
        }

        public override OboznachenieTableAbstract GetOboznachenieTable()
        {
            throw new NotImplementedException();
        }

        public override FirstUsageTableAbstract GetFirstUsageTable()
        {
            throw new NotImplementedException();
        }

        public override ProjectTableAbstract GetProjectTable()
        {
            return new ProjectTableSqlServer(CNN);
        }

        public override LiteralTableAbstract GetLiteralTable()
        {
            throw new NotImplementedException();
        }

        public override IspolniteliTableAbstract GetIspolniteliTable()
        {
            throw new NotImplementedException();
        }

        public override InventarNumberTableAbstract GetInventarNumberTable()
        {
            throw new NotImplementedException();
        }

        public override MainTableAbstract GetMainTable()
        {
            throw new NotImplementedException();
        }

        public override DependencyTableAbstract GetDependencyTable()
        {
            throw new NotImplementedException();
        }
    }
}
