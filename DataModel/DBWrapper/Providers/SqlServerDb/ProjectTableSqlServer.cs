﻿using DataModel.DBWrapper.Providers.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using DBData.Entities;
using DBData.Repository;
using DataModel.SupportClasses;


namespace DataModel.DBWrapper.Providers.SqlServerDb
{
    class ProjectTableSqlServer : ProjectTableAbstract
    {
        static SqlConnection SQLServerCon;

        public ProjectTableSqlServer(IDbConnection Connection) : base(Connection)
        {
            SQLServerCon = (SqlConnection)CNN;

        }

        public override List<Project> GetEntities()
        {
            List<Project> _lst = new List<Project>();

            if (SQLServerCon.State == ConnectionState.Closed)
            {
                SQLServerCon.Open();
            }

            _lst.Clear();

            SqlCommand ceCmd;
            SqlDataReader ceDR;

            ceCmd = new SqlCommand("SELECT project_id, project_name FROM project", (SqlConnection)CNN);
            ceDR = ceCmd.ExecuteReader();
            while (ceDR.Read())
            {

                _lst.Add(new Project((int)ceDR[0], (string)ceDR[1]));

            }
            ceDR.Close();

            CNN.Close();

            return _lst;
        }

        public override bool AddToDB(string value)
        {
            int i = -1;
           
            string cmd = string.Format(@"INSERT Into project (project_name) Values (@prj_name)");
            using (SqlCommand _cmd = new SqlCommand(cmd, SQLServerCon))
            {
                _cmd.Parameters.Add("@prj_name", SqlDbType.NVarChar).Value =
                    value;

                //TODO Продумать работу с БД
                i = SQLCommon.SQLSingleCommandExec(SQLServerCon, _cmd);
            }

            GetEntities();
            
            return i > 0;
        }

        public override bool DeleteFromDB(string value, int id)
        {
            string cmd = string.Format(@"DELETE FROM project where project_id = @id");


            int i = -1;

            #region
            /*

            SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            try
            {
                i = SQLExec_(_cmd);
            }
            catch (Exception ex)
            {

                if (ex is SqlCeException)
                {
                    ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException();
                    // "Невозможно удалить выбраный проект " + value);

                    //exc.Data.Add("project_id", _id);
                    exc.SQLCENativeError = ((SqlCeException)ex).NativeError;
                    exc.SQLTableName = "project";
                    exc.SQLEntityID = id;
                    exc.SQLEntityName = value;
                    throw exc;
                }
                else
                { throw ex; }

            }
            */
            #endregion

            return i > 0;
        }

        public override bool RenameInDB(string oldValue, string newValue, int oldValueId)
        {
            string cmd = string.Format(@"UPDATE project SET project_name = @name WHERE project_id = @id");


            int i = -1;

            #region
            /*
                          SqlCeCommand _cmd = new SqlCeCommand();
            _cmd = new SqlCeCommand(cmd, SQLCeCon);

            _cmd.Parameters.Add("@id", SqlDbType.Int).Value = oldValueId;
            _cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = newValue;

            //i = SQLExec_(_cmd);

            try
            {
                i = SQLExec_(_cmd);
            }
            catch (Exception ex)
            {

                if (ex is SqlCeException)
                {
                    ExceptionHandler.SQLEntityException exc =
                    new ExceptionHandler.SQLEntityException();
                    // "Невозможно удалить выбраный проект " + value);

                    //exc.Data.Add("project_id", _id);
                    exc.SQLCENativeError = ((SqlCeException)ex).NativeError;
                    exc.SQLTableName = "project";
                    exc.SQLEntityID = oldValueId;
                    exc.SQLEntityName = newValue;
                    throw exc;
                }
                else
                { throw ex; }


            }






            GetEntities();

            */
            #endregion

            return i > 0;
        }



        protected int SQLExec(SqlCommand SQLCommand)
        {
            int i = -1;

            #region
            /*
            if (CNN.State == ConnectionState.Closed)
            {
                CNN.Open();
            }

            try
            {
                i = SQLCommand.ExecuteNonQuery();

            //    if (OnUpdate != null)
            //    {
            //        OnUpdate();
            //    }
            }
            //catch (SqlCeException ex)
            //{
            //    //MessageBox.Show(ex.Message);
            //    throw ex;
            //}
            finally
            {
                CNN.Close();
            }
            */
            #endregion

            return i;

        }

        public override DataTable GetProjectTable()
        {
            throw new NotImplementedException();
        }
    }
}
