﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using DataModel.DBWrapper;
using DataModel.DBWrapper.Providers.Abstract;

using DataModel.DBWrapper.Providers.SqlCeDb;
using DataModel.DBWrapper.Providers.SqlServerDb;
using DataModel.AppLogger;

namespace DataModel.DBWrapper.Factories
{
    public static class DbProviderFactory
    {
        /// <summary>
        /// Возвращает провайдер базы данных изходя из типа базы данных
        /// </summary>
        /// <param name="dbType">Тип базы данных</param>
        /// <param name="ConnectStr">Строка соединния</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static DatabaseProvider GetProvider(DBType dbType, string ConnectStr)
        {
            DatabaseProvider provider = null;
            //string extension = Path.GetExtension(fileName);
            if (string.IsNullOrEmpty(ConnectStr))
            {
                throw new ArgumentNullException(nameof(ConnectStr), "Строка соединения не указана");
            }

            switch (dbType)
            {
                case DBType.MSSQL:
                    provider = new SqlDbProvider(ConnectStr);
                    
                    break;
                case DBType.MSSQLCE:
                    
                    
                    provider = new SqlCeDbProvider(ConnectStr);
                    Logger.Log(LogTarget.FileLogger, LogLevel.Info, 
                        string.Format("SqlCeDbProvider is init"));
                    break;
                default:
                    throw new ArgumentException("Wrong database type!");
            }
            return provider;
        }
    }
}
