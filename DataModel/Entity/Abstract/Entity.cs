﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public abstract class Entity
    {
        protected Entity()
        {
            Id = -1;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsNew()
        {
            return Id == -1;
        }
    }
}
