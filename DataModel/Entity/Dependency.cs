﻿namespace DBData.Entities
{
    /// <summary>
    /// Defines the <see cref="Dependency" />
    /// </summary>
    public class Dependency : Entity
    {
        //public int Id { get; private set; }
        //public string Name { get; set; }
        /// <summary>
        /// Идентификатор документа
        /// </summary>
        ///
        public int DocumentID { get; private set; }

        /// <summary>
        /// Идентификатор документа "родителя"
        /// </summary>
        public int ParentID { get; private set; }

        /// <summary>
        /// Название документа "родителя"
        /// </summary>
        public string ParentName { get; private set; }

        /// <summary>
        /// Является ли документ сборкой
        /// </summary>
        public bool IsAsm { get; set; }

        /// <summary>
        /// Идентификатор проектва в который входит документ
        /// </summary>
        public int ProjectId { get; private set; }

        /// <summary>
        /// Назване проекта
        /// </summary>
        public string ProjectName { get; private set; }

        /// <summary>
        ///Объект зависимости
        /// </summary>
        /// <param name="dependencyId">Идентификатор зависимости в БД</param>
        /// <param name="documentID">Идентификатор документа</param>
        /// <param name="documentName">Название документа</param>
        /// <param name="parentID">Идентификатор документа "родителя"</param>
        /// <param name="parentName">Название документа "родителя"</param>
        /// <param name="isAsm">Является ли документ сборкой</param>
        /// <param name="projectId">Идентификатор проектва в который входит документ</param>
        /// <param name="projectName">Назване проекта</param>
        public Dependency(int dependencyId,
                            int documentID, string documentName,
                            int parentID, string parentName,
                            bool isAsm,
                            int projectId, string projectName
                           )
        {
            this.Id = dependencyId;
            this.DocumentID = documentID;
            this.Name = documentName;
            this.ParentID = parentID;
            this.ParentName = parentName;
            this.IsAsm = isAsm;
            this.ProjectId = projectId;
            this.ProjectName = projectName;
        }

        /// <summary>
        ///Объект зависимости
        /// </summary>
        /// <param name="documentID">Идентификатор документа</param>
        /// <param name="parentID">>Идентификатор документа "родителя"</param>
        /// <param name="isAsm">Является ли документ сборкой</param>
        public Dependency(
                    int documentID,
                    int parentID,
                    bool isAsm
                   )
        {
            this.Id = 0;
            this.DocumentID = documentID;
            this.ParentID = parentID;
            this.IsAsm = isAsm;
        }
    }
}
