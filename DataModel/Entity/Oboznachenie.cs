﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public class Oboznachenie : Entity
    {
        /// <summary>
        /// Обозначение
        /// </summary>
        /// <param name="ProjectId">Идентификатор обозначения в БД</param>
        /// <param name="Oboznachenie">Обозначение</param>
        public Oboznachenie(int ProjectId, string Oboznachenie)
        {
            this.Id = ProjectId;
            this.Name = Oboznachenie;
        }
    }
}
