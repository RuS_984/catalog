﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public class FirstUsage : Entity
    {
        /// <summary>
        /// Первичное применение
        /// </summary>
        /// <param name="ProjectId">Идентификатор в БД</param>
        /// <param name="Oboznachenie">Первичное применение</param>
        public FirstUsage(int ProjectId, string Oboznachenie)
        {
            this.Id = ProjectId;
            this.Name = Oboznachenie;
        }
    }
}
