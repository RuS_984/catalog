﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public class Project : Entity
    {
        /// <summary>
        /// Проект
        /// </summary>
        /// <param name="ProjectId">Идентификатор проекта в БД</param>
        /// <param name="ProjectName">Название проекта</param>
        public Project(int ProjectId, string ProjectName)
        {
            this.Id = ProjectId;
            this.Name = ProjectName;
        }
    }
}
