﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DBData.Entities
{
    [DebuggerDisplay("Id:{Id}; Name:{Name}")]
    public class InventarNumber : Entity
    {
        /// <summary>
        /// Инвентарный номер
        /// </summary>
        /// <param name="ProjectId">Первичный ключ в БД</param>
        /// <param name="InventarNumber">Инвентарный номер</param>
        public InventarNumber(int ProjectId, int InventarNumber)
        {
            this.Id = ProjectId;
            this.Name = InventarNumber.ToString();
        }
    }
}
