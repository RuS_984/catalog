﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public class Ispolniteli : Entity
    {
        /// <summary>
        /// Исполнитель
        /// </summary>
        /// <param name="IspolniteliId">Идентификатор исполнителя в БД</param>
        /// <param name="IspolniteliName">Имя сполнителя</param>
        public Ispolniteli(int IspolniteliId, string IspolniteliName)
        {
            this.Id = IspolniteliId;
            this.Name = IspolniteliName;
        }
    }
}
