﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBData.Entities
{
    public class Literal : Entity
    {
        /// <summary>
        /// Литерал
        /// </summary>
        /// <param name="LiteralId">Идентификатор литерала в БД</param>
        /// <param name="LiteralName">Литерал</param>
        public Literal(int LiteralId, string LiteralName)
        {
            this.Id = LiteralId;
            this.Name = LiteralName;
        }
    }
}
