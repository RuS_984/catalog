﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace DBData.Entities
{
    [DebuggerDisplay("Id:{Id}; Name:{Name}; Oboznachenie:{Oboznachenie}, IsNew:{IsNew}")]
    public class Docs : Entity
    {
        public string Oboznachenie { get; set; }
        public MemoryStream Document { get; set; }
        public bool IsNew { get; set; }

        /// <summary>
        /// Приложение к документу
        /// </summary>
        /// <param name="DocId">Идентификатор документа в БД</param>
        /// <param name="DocName">Дазвание приложения</param>
        /// <param name="prjOboznachenie">Обозначение документа</param>
        /// <param name="Doc">Файл приложения</param>
        public Docs(int DocId, string DocName, string prjOboznachenie, MemoryStream Doc)
        {
            this.Id = DocId;
            this.Name = DocName;
            this.Oboznachenie = prjOboznachenie;
            this.Document = Doc;
        }

        /// <summary>
        /// Приложение к документу
        /// </summary>
        /// <param name="DocId">Идентификатор документа в таблице</param>
        /// <param name="DocName">Дазвание приложения</param>
        /// <param name="prjOboznachenie">Обозначение документа</param>
        /// <param name="Doc">Файл приложения</param>
        /// <param name="IsNew">Является ли приложение новым</param>
        public Docs(int DocId, string DocName, string prjOboznachenie, MemoryStream Doc, bool IsNew)
        {
            this.Id = DocId;
            this.Name = DocName;
            this.Oboznachenie = prjOboznachenie;
            this.Document = Doc;
            this.IsNew = IsNew;
        }

    }
}
