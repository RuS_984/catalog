﻿/*


function GetRootTree() {
    var xx = $("#RootTree").val();
    $.ajax({
        url: xx,


        success: function (result) {
            //RepositoryEditorViewModel.Init(result);

            //$('#RepositoryEditor').ready(function () {

            //    $(".Rerository-List__Entity").hover(
            //        function () {
            //            $(this).find('.Rerository-List__Entity-Buttons').css("display", "block");
            //        }, function () {
            //            $(this).find('.Rerository-List__Entity-Buttons').css("display", "none");
            //        }
            //    );
            //});

            //alert(result);

            //for (var i = 0; i < result.; i++) {
            //
            //} 

            //var res = '';
            //
            //res += '<ul style="list-style-type:none">'
            //
            //for (i in result) {
            //    res += treeNode(result[i]);
            //}
            //
            //res += '</ul>'
            //
            //$(".tree-content" ).after( res );


            var ul = document.createElement('ul');

            ul.style['list-style-type'] = 'none';
            for (i in result) {
                ul.appendChild(treeNode(result[i]));
            }


            $(".tree-content").after(ul);

            leftsidebar.Open();

        },

        HttpMethod: "GET"
    })
}


function GetSubTree(node) {
    var xx = $("#SubTree").val();
    $.ajax({
        url: xx,

        data: {
            "node": node,
        },

        success: function (result) {

            var ul = document.createElement('ul');

            ul.style['list-style-type'] = 'none';
            for (i in result) {
                ul.appendChild(treeNode(result[i]));
            }


            node.after(ul);



        },

        HttpMethod: "GET"
    })
}

function treeNode(nodeObj) {
    //var str = '<li>';
    //
    //if (nodeObj.hasChild) {
    //    str += '[+]  ';
    //
    //}
    //else {
    //    str += '[-]  ';
    //};
    //
    //str += nodeObj.text;//+ "<br />";
    //
    //str += '</li>';

    var li = document.createElement('LI');
    var iHtml = '';
    var nodeText = nodeObj.text;

    if (nodeObj.hasChild) {
        iHtml += '[+]  ';
        li.className = 'hasChild';

        var a = document.createElement('A');

        //a.onclick = function () { 'GetChildNodes(' + nodeText + ')'; };
        a.setAttribute('href', '#');
        a.setAttribute('onClick', "GetSubTree(" + nodeText + ")");


    }
    else {
        iHtml += '[-]  ';
    };

    iHtml += nodeText;

    li.innerHTML += iHtml;
    if (a) {
        li.appendChild(a);
    }



    return li;
}


*/

//////////////////////////////////////////




function MyTree(param) {
    var elem = param.Element;
    var sidebar = param.SideBar;
    var isAsm = param.IsAsm;
    var dependency = param.Dependency;

    var addedNodes = {};
    var deletedNodes = {};

    var openedlst = [];
    var checkedlst = [];


    var pause = 0;

    function getElem() {

        checkedlst = [];

        if (!isAsm) {

            $(".asmdoc-tree-content").html("");

            if ($(elem).has('ul').length === 0) {
                render();
            }
            else {
                sidebar.Open();

            }
            $('.sidebar-left .close').removeClass('invisible');
            $('.fulldoc-tree-content').removeClass('invisible');
        }
        else {

            $(".asmdoc-tree-content").html("");

            $('.fulldoc-tree-content').addClass('invisible');
            $('.sidebar-left .close').addClass('invisible');
            renderasm();
            sidebar.Open();

        }


    }


    function setDependency(dep) {
        dependency = dep;
    }




    function render() {

        GetRootTree();

        elem.onmousedown = function () {
            return false;
        };

        elem.onclick = function (event) {
            if (event.target.classList.contains('.hasChild')) {
                toggle();
            }


        }
    }

    function GetRootTree() {
        var xx = $("#RootTree").val();
        $.ajax({
            url: xx,

            success: function (result) {

                var ul = document.createElement('ul');

                ul.style['list-style-type'] = 'none';
                ul.classList.add('tree-rootnode');

                for (i in result) {
                    ul.appendChild(treeNode(result[i]));
                }

                ul.onmousedown = function () {
                    return false;
                };

                ul.onclick = function (event) {
                    if (event.target.parentElement.classList.contains('hasChild')) {
                        toggle(event.target.parentElement);
                    }

                }

                elem.append(ul);

                sidebar.Open();
                alert(checkedlst);
            },
            HttpMethod: "POST"
        })
    }


    function GetSubTree(node) {
        var xx = $("#SubTree").val();
        $.ajax({
            url: xx,

            beforeSend: function () {
                node.classList.add('loading');
            },

            data: {
                "node": node.dataset.name,
            },

            success: function (result) {

                node.classList.remove('loading');

                var ul = document.createElement('ul');

                ul.classList.add('tree-subnode');

                ul.style['list-style-type'] = 'none';
                for (i in result.SubNodes) {
                    ul.appendChild(treeNode(result.SubNodes[i]));
                }

                node.append(ul);



            },

            HttpMethod: "POST"
        })
    }



    function renderasm() {

        GetAsmRootTree(dependency);

        //elem.onmousedown = function () {
        //    return false;
        //};

        //elem.onclick = function (event) {
        //    if (event.target.classList.contains('.hasChild')) {
        //        toggle();
        //    }
        //
        //}

    }


    function GetAsmRootTree() {
        var xx = $("#AsmRootTree").val();
        $.ajax({
            url: xx,

            data: {
                "Dependency": dependency
            },


            success: function (result) {
                openedlst = result.opendList;
                //openedlst.push('');

                var ul = document.createElement('ul');

                ul.style['list-style-type'] = 'none';
                ul.classList.add('tree-rootnode');

                for (i in result.rootNodes) {
                    var tn1 = treeNode(result.rootNodes[i]);
                    ul.appendChild(tn1);
                    if (openedlst.indexOf(result.rootNodes[i].text) > -1) {
                        open(tn1);
                    }

                }

                ul.onmousedown = function () {
                    return false;
                };

                ul.onclick = function (event) {
                    if (event.target.parentElement.classList.contains('hasChild')) {
                        toggle(event.target.parentElement);
                    }
                    if (event.target.classList.contains('chbox')) {
                        alert(event.target.parentElement.innerText + 'checkedlst:' + checkedlst);
                    }

                }



                elem.append(ul);

                sidebar.Open();
                //alert('GetAsmRootTree:  ' + checkedlst);
            },
            HttpMethod: "POST"
        })
    }


    function GetAsmSubTree(node) {
        var xx = $("#AsmSubTree").val();
        var dep = "";

        if ("text" in node) {
            dep = node.text;
        }
        else {
            dep = node.dataset.name
        }


        $.ajax({
            url: xx,

            beforeSend: function () {
                node.classList.add('loading');
            },

            data: {
                "node": dep,
                "Dependency": dependency
            },

            success: function (result) {

                node.classList.remove('loading');

                var ul = document.createElement('ul');

                ul.classList.add('tree-subnode');

                ul.style['list-style-type'] = 'none';
                for (i in result.SubNodes) {
                    var tn1 = treeNode(result.SubNodes[i]);

                    ul.appendChild(tn1);


                    if (openedlst.indexOf(result.SubNodes[i].text) > -1) {
                        open(tn1);
                    }

                }

                node.append(ul);


                //alert('GetAsmSubTree:  ' + checkedlst);
            },

            HttpMethod: "POST"
        })
    }




    function treeNode(nodeObj) {

        var li = document.createElement('LI');
        var iHtml = '';
        var nodeText = nodeObj.text;

        if (nodeObj.hasChild) {

            //li.className = 'hasChild';
            li.classList.add('hasChild');
        }

        li.classList.add('leaf');

        iHtml += nodeText;


        var span = document.createElement('span');

        if (isAsm) {

            var inp = document.createElement('input');
            inp.classList.add('chbox');
            inp.setAttribute("type", "checkbox");

            if (nodeObj.isChecked) {

                inp.setAttribute("checked", "checked");
                checkedlst.push(nodeObj.text);
                //alert('treeNode:  ' + checkedlst);
                //li.classList.add('open');

                if (nodeObj.hasChild) {
                    //GetAsmSubTree(nodeObj)
                    //open(nodeObj);
                }

            }


            span.appendChild(inp);
        }



        span.innerHTML += iHtml;
        //span.classList.add('hasChild');

        li.appendChild(span);

        li.dataset.name = nodeText;

        return li;
    }


    function remove1(array, element) {
        const index = array.indexOf(element);

        if (index !== -1) {
            array.splice(index, 1);
        }
    }


    function open(elem_) {
        if ($(elem_).has('ul').length === 0) {
            if (!isAsm) {
                GetSubTree(elem_);
            }
            else {
                GetAsmSubTree(elem_);
            }
        }
        elem_.classList.add('open');
    };

    function close(elem_) {
        elem_.classList.remove('open');
    };

    function toggle(elem_) {
        if (elem_.classList.contains('open')) close(elem_);
        else open(elem_);
        //alert('toggle')
    };

    function closeTree() {
        sidebar.Close();
    };

    function getChecked() {
        return checkedlst;
    }

    this.GetElem = getElem;
    this.SetDependency = setDependency;
    this.CloseTree = closeTree;
    this.GetChecked = getChecked;



}