﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDGV
{
    public partial class DGVColumnFilter : UserControl
    {
        string columnName;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }

        public DGVColumnFilter(DataGridViewColumn dgvColumn, DataView dataView)
        {
            InitializeComponent();

            DataView dv = dataView;

            columnName = dgvColumn.DataPropertyName.Split('\n')[0];

            DataTable DistinctDataTable = dv.ToTable(true, new string[] { columnName });
            DistinctDataTable.DefaultView.Sort = columnName;
            comboBox2.DataSource = DistinctDataTable;

            comboBox2.DisplayMember = dgvColumn.DataPropertyName;// "Инвентарный номер";
            comboBox2.ValueMember = dgvColumn.DataPropertyName;

            comboBox2.SelectedValue = FilterParams.GetFilterParam(dgvColumn.DataPropertyName);

            comboBox1.Items.AddRange(new string[] { "=", "<>" });
            comboBox1.SelectedItem = FilterParams.GetFilterOperator(dgvColumn.DataPropertyName);

            this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((comboBox2.SelectedIndex != -1)
                & (comboBox1.SelectedIndex != -1)
                )
            {
                string filterStr = string.Empty;

                if (comboBox1.SelectedItem.ToString() == "=")
                {
                    filterStr = "[" + columnName + "] like " + "'%" +
                        comboBox2.SelectedValue.ToString() + "%'";
                }

                if (comboBox1.SelectedItem.ToString() == "<>")
                {
                    filterStr = "[" + columnName + "] not like " + "'%" +
                        comboBox2.SelectedValue.ToString() + "%'";
                }

                FilterParams.SetFilterParam(columnName,
                    comboBox2.SelectedValue.ToString(),
                    filterStr,
                    comboBox1.SelectedItem.ToString()
                    );
            }
        }
    }
}
