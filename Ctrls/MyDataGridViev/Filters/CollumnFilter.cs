﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDGV
{
    class ColumnFilter
    {
        private readonly PopUpBase myPopUp;

        public ColumnFilter(int ColumnIndex, DataGridViewColumn dgvColumn, DataView dataView)
        {
            myPopUp = new PopUpBase();

            string[] checkBoxFilterCols = {
                "Разработал", "Проверил", "Тех контроль",
                "Нормоконтроль", "Утвердил", "Название проекта",
                "Литера"
            };

            string[] numericalFilterCols = {
                "Инвентарный номер"
            };

            string[] textboxlFilterCols = {
                "Обозначение", "Наименование", "Первичное преминение"
            };

            string[] dateFilterCols = {
                "Дата подписания"
            };

            string[] boolFilterCols = {
                "Сборка"
            };

            if (checkBoxFilterCols.Contains(dgvColumn.HeaderText.Split('\n')[0]))
            {
                DGVColumnFilter dgvcol = new DGVColumnFilter(dgvColumn, dataView);
                myPopUp.AddControl(dgvcol, dgvcol.ColumnName);
            }

            if (numericalFilterCols.Contains(dgvColumn.HeaderText.Split('\n')[0]))
            {
                DGVNumericColumnFilter dgvcol = new DGVNumericColumnFilter(dgvColumn, dataView);
                myPopUp.AddControl(dgvcol, dgvcol.ColumnName);
            }


            if (textboxlFilterCols.Contains(dgvColumn.HeaderText.Split('\n')[0]))
            {
                DGVTextBoxColumnFilter dgvcol = new DGVTextBoxColumnFilter(dgvColumn, dataView);
                myPopUp.AddControl(dgvcol, dgvcol.ColumnName);
            }

            if (dateFilterCols.Contains(dgvColumn.HeaderText.Split('\n')[0]))
            {
                DGVDateColumnFilter dgvcol = new DGVDateColumnFilter(dgvColumn, dataView);
                myPopUp.AddControl(dgvcol, dgvcol.ColumnName);
            }

            if (boolFilterCols.Contains(dgvColumn.HeaderText.Split('\n')[0]))
            {
                DGVBoolColumnFilter dgvcol = new DGVBoolColumnFilter(dgvColumn);
                myPopUp.AddControl(dgvcol, dgvcol.ColumnName);
            }
        }


        public void ShowPopUp(Point p)
        {
            myPopUp.PopUp.Show(new Point(p.X, p.Y+15));
            myPopUp.Focus();
        }



    }
}
