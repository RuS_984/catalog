﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using MyControls;

namespace MyDGV
{
    public partial class DGVTextBoxColumnFilter : UserControl
    {
        private MyAutoCompletePopUp ACpopUp;

        string columnName;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }

        public DGVTextBoxColumnFilter(DataGridViewColumn dgvColumn, DataView dataView)
        {
            InitializeComponent();

            DataView dv = dataView;

            columnName = dgvColumn.DataPropertyName.Split('\n')[0];

            DataTable DistinctDataTable = dv.ToTable(true, new string[] { columnName });
            DistinctDataTable.DefaultView.Sort = columnName;

            comboBox1.Items.AddRange(new string[] { "=", "<>" });
            comboBox1.SelectedItem = FilterParams.GetFilterOperator(dgvColumn.DataPropertyName);

            textBox1.Text = FilterParams.GetFilterParam(dgvColumn.DataPropertyName).Split('\n')[0];

            this.comboBox1.SelectedValueChanged +=
                new System.EventHandler(this.comboBox2_SelectedValueChanged);

            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);

            List<string> ACList = new List<string>();

            foreach (DataRow dr in DistinctDataTable.Rows)
            {
                ACList.Add(dr[0].ToString());
            }

            ACpopUp = new MyAutoCompletePopUp(textBox1, ACList);
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        void UpdateFilter()
        {
            if ((comboBox1.SelectedIndex != -1)
                & (textBox1.Text != string.Empty)
                )
            {
                string filterStr = string.Empty;

                if (comboBox1.SelectedItem.ToString() == "=")
                {
                    filterStr = "[" + columnName + "] like " + "'%" + textBox1.Text + "%'";
                }

                if (comboBox1.SelectedItem.ToString() == "<>")
                {
                    filterStr = "[" + columnName + "] not like " + "'%" + textBox1.Text + "%'";
                }

                FilterParams.SetFilterParam(
                    ParamName: columnName,
                    ParamValue: textBox1.Text,
                    FilterString: filterStr,
                    Operation: comboBox1.SelectedItem.ToString()
                    );
            }

            if ((comboBox1.SelectedIndex != -1)
            & (textBox1.Text == string.Empty)
            )
            {
                FilterParams.SetFilterParam(
                    ParamName: columnName,
                    ParamValue: string.Empty,
                    FilterString: string.Empty,
                    Operation: comboBox1.SelectedItem.ToString()
                    );
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ACpopUp.IsActive = true;
            Rectangle r = RectangleToScreen(this.ClientRectangle);
            Point p = new Point(
                      r.X + textBox1.Location.X + 8, //Коордната X в системе координат экрана
                      r.Y + textBox1.Height);       //Коордната Y в системе координат экрана

            if (ACpopUp != null)
            {
                ACpopUp.popUpCoord = p;
                ACpopUp.UpdateAndShow();
            }

            UpdateFilter();
        }


        public void HideAutoComplete()
        {
            ((ToolStripDropDown)ACpopUp).AutoClose = true;
            ACpopUp.Hide();

        }
    }
}
