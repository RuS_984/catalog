﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;



namespace MyDGV
{
    public partial class PopUpBase : UserControl
    {
        private ToolStripDropDown myPopUp;
        private ToolStripControlHost ControlHost;
        private Control ctrl;
        int lastCtrlRow;

        [DebuggerStepThrough]
        public PopUpBase()
        {
            InitializeComponent();
            myPopUp = new ToolStripDropDown();
            myPopUp.AutoSize = false;
        }

        public ToolStripDropDown PopUp
        {
            get
            {
                    ControlHost = new ToolStripControlHost(this);
                    ControlHost.AutoSize = false;

                myPopUp.Padding = Padding.Empty;
                    myPopUp.Items.Add(ControlHost);
                    myPopUp.Region = this.Region;

                    myPopUp.Closed +=
                        new ToolStripDropDownClosedEventHandler(myPopUp_Closed);
                return myPopUp;
            }
        }

        public void AddControl(Control Ctrl, string ColumnName)
        {
            lastCtrlRow = 1;

            ctrl = Ctrl;

            this.tableLayoutPanel1.Controls.Add(ctrl, 0, lastCtrlRow);

            this.tableLayoutPanel1.RowStyles[1].SizeType = SizeType.AutoSize;

            this.tableLayoutPanel1.RowStyles[1].Height = ctrl.Height+10;


            this.tableLayoutPanel1.ColumnStyles[0].SizeType = SizeType.AutoSize;


            this.tableLayoutPanel1.AutoSize = true;

            this.Width = ctrl.Width + 10;
            this.Height = this.tableLayoutPanel1.Height;

            myPopUp.Width = this.tableLayoutPanel1.Width;
            myPopUp.Height = this.tableLayoutPanel1.Height;

            this.label1.Text = ColumnName;

            this.Focus();
        }

        public void AddNewControl()
        {
            TextBox ctrl = new TextBox();

            this.tableLayoutPanel1.Controls.Add(new CheckBox(), 0, lastCtrlRow);

            this.tableLayoutPanel1.RowStyles[lastCtrlRow].SizeType = SizeType.Absolute;

            this.tableLayoutPanel1.RowStyles[lastCtrlRow].Height = ctrl.Height + 10;

            this.tableLayoutPanel1.AutoSize = true;

            Debug.WriteLine(lastCtrlRow.ToString() + "_________" + this.tableLayoutPanel1.RowCount.ToString());

            lastCtrlRow += 1;
            this.tableLayoutPanel1.RowCount += 1;

            this.Focus();
            this.Invalidate();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FilterParams.ClearFilters();
            this.PopUp.Hide();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FilterParams.ClearCurrentFilters(label1.Text);
            this.PopUp.Hide();
        }

        void myPopUp_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            try
            {
                ((DGVTextBoxColumnFilter)ctrl).HideAutoComplete();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
