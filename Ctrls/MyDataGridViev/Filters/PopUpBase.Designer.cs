﻿using System.Diagnostics;

namespace MyDGV
{
    partial class PopUpBase
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        //[DebuggerHidden]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PopUpBase));
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ClearCurFilter = new System.Windows.Forms.ToolStripButton();
            this.ClearAllFilter = new System.Windows.Forms.ToolStripButton();
            this.DeleteFilter_ts = new System.Windows.Forms.ToolStripButton();
            this.AddFilter_ts = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.toolStrip1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(392, 72);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClearCurFilter,
            this.ClearAllFilter,
            this.DeleteFilter_ts,
            this.AddFilter_ts});
            this.toolStrip1.Location = new System.Drawing.Point(0, 47);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(392, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ClearCurFilter
            // 
            this.ClearCurFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearCurFilter.Image = ((System.Drawing.Image)(resources.GetObject("ClearCurFilter.Image")));
            this.ClearCurFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearCurFilter.Name = "ClearCurFilter";
            this.ClearCurFilter.Size = new System.Drawing.Size(23, 22);
            this.ClearCurFilter.Text = "Удалить фильтр";
            this.ClearCurFilter.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // ClearAllFilter
            // 
            this.ClearAllFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearAllFilter.Image = ((System.Drawing.Image)(resources.GetObject("ClearAllFilter.Image")));
            this.ClearAllFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearAllFilter.Name = "ClearAllFilter";
            this.ClearAllFilter.Size = new System.Drawing.Size(23, 22);
            this.ClearAllFilter.Text = "Удалить все фильтры";
            this.ClearAllFilter.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // DeleteFilter_ts
            // 
            this.DeleteFilter_ts.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.DeleteFilter_ts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteFilter_ts.Image = ((System.Drawing.Image)(resources.GetObject("DeleteFilter_ts.Image")));
            this.DeleteFilter_ts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteFilter_ts.Name = "DeleteFilter_ts";
            this.DeleteFilter_ts.Size = new System.Drawing.Size(23, 22);
            this.DeleteFilter_ts.Text = "toolStripButton3";
            this.DeleteFilter_ts.Visible = false;
            // 
            // AddFilter_ts
            // 
            this.AddFilter_ts.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.AddFilter_ts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddFilter_ts.Image = ((System.Drawing.Image)(resources.GetObject("AddFilter_ts.Image")));
            this.AddFilter_ts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddFilter_ts.Name = "AddFilter_ts";
            this.AddFilter_ts.Size = new System.Drawing.Size(23, 22);
            this.AddFilter_ts.Text = "toolStripButton4";
            this.AddFilter_ts.Visible = false;
            // 
            // PopUpBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "PopUpBase";
            this.Size = new System.Drawing.Size(392, 71);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ClearCurFilter;
        private System.Windows.Forms.ToolStripButton ClearAllFilter;
        private System.Windows.Forms.ToolStripButton DeleteFilter_ts;
        private System.Windows.Forms.ToolStripButton AddFilter_ts;
    }
}
