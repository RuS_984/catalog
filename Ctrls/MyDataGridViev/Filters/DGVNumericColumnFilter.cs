﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDGV
{
    public partial class DGVNumericColumnFilter : UserControl
    {
        string columnName;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }

        public DGVNumericColumnFilter(DataGridViewColumn dgvColumn, DataView dataView)
        {
            InitializeComponent();

            DataView dv = dataView;

            columnName = dgvColumn.DataPropertyName.Split('\n')[0];

            DataTable DistinctDataTable = dv.ToTable(true, new string[] { columnName });
            DistinctDataTable.DefaultView.Sort = columnName;

            comboBox1.Items.AddRange(new string[] { "=", "<>", "<", ">", "X..Y" });
            comboBox1.SelectedItem = FilterParams.GetFilterOperator(dgvColumn.DataPropertyName);

            textBox1.Text = FilterParams.GetFilterParam(dgvColumn.DataPropertyName).Split('\n')[0];

            if (comboBox1.SelectedItem.ToString() != "X..Y")
            {
                textBox2.Visible = false;
            }
            else
            {
                textBox2.Visible = true;
                textBox2.Text = FilterParams.GetFilterParam(dgvColumn.DataPropertyName).Split('\n')[1];
            }

            this.comboBox1.SelectedValueChanged +=
                new System.EventHandler(this.comboBox2_SelectedValueChanged);

            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() != "X..Y")
            {
                textBox2.Visible = false;
                UpdateFilter();
            }
            else
            {
                textBox2.Visible = true;
            }
        }

        void UpdateFilter()
        {
            if (textBox1.Text == string.Empty & comboBox1.SelectedItem.ToString() != "X..Y")
            {
                FilterParams.SetFilterParam(columnName,
                    textBox1.Text,
                    "",
                    comboBox1.SelectedItem.ToString()
                 );
            }

            if ((textBox1.Text != string.Empty) &
                (comboBox1.SelectedItem.ToString() != "X..Y")
                )
            {
                string filterStr = string.Empty;

                if (comboBox1.SelectedItem.ToString() == "=")
                {
                    filterStr = "[" + columnName + "] = " + textBox1.Text;
                }

                if (comboBox1.SelectedItem.ToString() == "<>")
                {
                    filterStr = "[" + columnName + "] <> " + textBox1.Text;
                }

                if (comboBox1.SelectedItem.ToString() == ">")
                {
                    filterStr = "[" + columnName + "] > " + textBox1.Text;
                }

                if (comboBox1.SelectedItem.ToString() == "<")
                {
                    filterStr = "[" + columnName + "] < " + textBox1.Text;
                }

                FilterParams.SetFilterParam(columnName,
                    textBox1.Text,
                    filterStr,
                    comboBox1.SelectedItem.ToString()
                    );
            }

            if ((textBox1.Text != string.Empty) &
                (textBox2.Text != string.Empty) &
                (comboBox1.SelectedItem.ToString() == "X..Y")
                )
            {
                string filterStr = string.Empty;

                filterStr = "[" + columnName + "] >= " + textBox1.Text + " AND " +
                    "[" + columnName + "] <= " + textBox2.Text;

                FilterParams.SetFilterParam(columnName,
                    textBox1.Text + "\n" + textBox2.Text,
                    filterStr,
                    comboBox1.SelectedItem.ToString()
                    );
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }
    }
}
