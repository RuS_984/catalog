﻿namespace MyDGV

{
    partial class DGVBoolColumnFilter
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Yes_cb = new System.Windows.Forms.CheckBox();
            this.No_cb = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Yes_cb
            // 
            this.Yes_cb.AutoSize = true;
            this.Yes_cb.Location = new System.Drawing.Point(4, 4);
            this.Yes_cb.Name = "Yes_cb";
            this.Yes_cb.Size = new System.Drawing.Size(125, 17);
            this.Yes_cb.TabIndex = 0;
            this.Yes_cb.Text = "Сборочная единица";
            this.Yes_cb.UseVisualStyleBackColor = true;
            this.Yes_cb.Click += new System.EventHandler(this.cb_Click);
            // 
            // No_cb
            // 
            this.No_cb.AutoSize = true;
            this.No_cb.Location = new System.Drawing.Point(4, 27);
            this.No_cb.Name = "No_cb";
            this.No_cb.Size = new System.Drawing.Size(141, 17);
            this.No_cb.TabIndex = 1;
            this.No_cb.Text = "Не сборочная единица";
            this.No_cb.UseVisualStyleBackColor = true;
            this.No_cb.Click += new System.EventHandler(this.cb_Click);
            // 
            // DGVBoolColumnFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.No_cb);
            this.Controls.Add(this.Yes_cb);
            this.Name = "DGVBoolColumnFilter";
            this.Size = new System.Drawing.Size(239, 60);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox Yes_cb;
        private System.Windows.Forms.CheckBox No_cb;
    }
}
