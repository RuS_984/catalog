﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDGV
{
    public partial class DGVBoolColumnFilter : UserControl
    {
        string columnName;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }

        public DGVBoolColumnFilter(DataGridViewColumn dgvColumn)
        {
            InitializeComponent();

            columnName = dgvColumn.DataPropertyName.Split('\n')[0];
            string res = FilterParams.GetFilterParam(columnName);

            if (res == "1")
            {
                Yes_cb.Checked = true;
                No_cb.Checked = false;
            }
            if (res == "0")
            {
                Yes_cb.Checked = false;
                No_cb.Checked = true;

            }
            if (res == "")
            {
                Yes_cb.Checked = false;
                No_cb.Checked = false;
            }
        }

        private void cb_Click(object sender, EventArgs e)
        {
            Yes_cb.Checked = false;
            No_cb.Checked = false;

            ((CheckBox)sender).Checked = true;

            UpdateFilter();
        }

        /// <summary>
        /// Обновление фильтра
        /// </summary>
        void UpdateFilter()
        {
            string filterStr = string.Empty;

            if (Yes_cb.Checked)
            {
                filterStr = "[" + columnName + "] = 1";
                FilterParams.SetFilterParam(
                    ParamName: columnName,
                    ParamValue: "1",
                    FilterString: filterStr,
                    Operation: "="
                    );
            }
            else
            {
                filterStr = "[" + columnName + "] = 0";
                FilterParams.SetFilterParam(
                    ParamName: columnName,
                    ParamValue: "0",
                    FilterString: filterStr,
                    Operation: "="
                    );
            }

        }

    }
}
