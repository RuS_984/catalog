﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyControls;

namespace MyDGV
{
    public partial class DGVDateColumnFilter : UserControl
    {

        string columnName;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
        }

        public DGVDateColumnFilter(DataGridViewColumn dgvColumn, DataView dataView)
        {
            InitializeComponent();

            DataView dv = dataView;

            columnName = dgvColumn.DataPropertyName.Split('\n')[0];

            DataTable DistinctDataTable = dv.ToTable(true, new string[] { columnName });
            DistinctDataTable.DefaultView.Sort = columnName;

            comboBox1.Items.AddRange(new string[] { "=", "<>", "<", ">", "X..Y" });
            comboBox1.SelectedItem = FilterParams.GetFilterOperator(dgvColumn.DataPropertyName);

            dateTimePicker1.Text = FilterParams.GetFilterParam(dgvColumn.DataPropertyName).Split('\n')[0];

            if (comboBox1.SelectedItem.ToString() != "X..Y")
            {
                dateTimePicker2.Visible = false;
            }
            else
            {
                dateTimePicker2.Visible = true;
                dateTimePicker2.Text = FilterParams.GetFilterParam(dgvColumn.DataPropertyName).Split('\n')[1];
            }

            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);

            this.dateTimePicker1.TextChanged += new System.EventHandler(this.dateTime_TextChanged);
            this.dateTimePicker2.TextChanged += new System.EventHandler(this.dateTime_TextChanged);

        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(((ComboBox)sender).SelectedValue.ToString());
            if (comboBox1.SelectedItem.ToString() != "X..Y")
            {
                dateTimePicker2.Visible = false;
                UpdateFilter();
            }
            else
            {
                dateTimePicker2.Visible = true;
            }

        }

        void UpdateFilter()
        {
            string startDate;
            string endDate;

            startDate = "'" + dateTimePicker1.Value.ToString("yyyy'-'MM'-'dd") + "'";
            endDate = "'" + dateTimePicker2.Value.ToString("yyyy'-'MM'-'dd") + "'";

            if (//(textBox1.Text != string.Empty) &
                (comboBox1.SelectedItem.ToString() != "X..Y")
                )
            {
                string filterStr = string.Empty;

                if (comboBox1.SelectedItem.ToString() == "=")
                {
                    filterStr = "[" + columnName + "] = " + startDate;
                }

                if (comboBox1.SelectedItem.ToString() == "<>")
                {
                    filterStr = "[" + columnName + "] <> " + startDate;
                }

                if (comboBox1.SelectedItem.ToString() == ">")
                {
                    filterStr = "[" + columnName + "] >= " + startDate;
                }

                if (comboBox1.SelectedItem.ToString() == "<")
                {
                    filterStr = "[" + columnName + "] <= " + startDate;
                }

                FilterParams.SetFilterParam(
                    ParamName: columnName,
                    ParamValue: dateTimePicker1.Text,
                    FilterString: filterStr,
                    Operation: comboBox1.SelectedItem.ToString()
                    );
            }

            if (
                (comboBox1.SelectedItem.ToString() == "X..Y")
                )
            {
                string filterStr = string.Empty;

                filterStr = "[" + columnName + "] >= " + startDate + " AND " +
                    "[" + columnName + "] <= " + endDate;

                FilterParams.SetFilterParam(columnName,
                    dateTimePicker1.Text + "\n" + dateTimePicker2.Text,
                    filterStr,
                    comboBox1.SelectedItem.ToString()
                    );
            }
        }

        private void dateTime_TextChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }
    }
}
