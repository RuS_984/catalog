﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using DBData.Repository;
//using DBData.Entities;

//using SupportClasses;

namespace MyControls
{
    public partial class GroupChange : Form
    {
        Dictionary<string, int> repData =new  Dictionary<string, int>();
        string repositoryName;

        private int selectedID = -1;
        public int SelectedID
        {
            get
            {
                return selectedID;
            }
            //set
            //{
            //    numUpDownVal = value;
            //}
        }

        public GroupChange(Dictionary<string, int> RepData)
        {
            InitializeComponent();
            //IspolniteliRep = new IspolniteliRepository(CommonData.SQLConn);
            repData = RepData;
        }

        private void GroupChange_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(repData.Keys.ToArray());

            comboBox1.SelectedIndex = 0;
        }

        private void Cancel_btn_Click(object sender, EventArgs e)
        {
            //selectedID = IspolniteliRep[comboBox1.SelectedText].Id;
            selectedID = -1;
            this.Close();
        }

        private void OK_btn_Click(object sender, EventArgs e)
        {
            selectedID = repData[comboBox1.SelectedItem.ToString()];

            //selectedID = IspolniteliRep[comboBox1.SelectedItem.ToString()].Id;

            //if (repositoryName == "Project")
            //{
            //    selectedID = ProjectRep[comboBox1.SelectedItem.ToString()].Id;
            //}
            //if (repositoryName == "Ispolniteli")
            //{
            //    selectedID = IspolniteliRep[comboBox1.SelectedItem.ToString()].Id;
            //}
            //if (repositoryName == "Literal")
            //{
            //    selectedID = LiteralRep[comboBox1.SelectedItem.ToString()].Id;
            //}

            this.Close();
        }
    }
}
