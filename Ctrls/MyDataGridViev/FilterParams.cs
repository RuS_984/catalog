﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDGV
{
    public static class FilterParams
    {

        static ObservableCollection<string> filters = new ObservableCollection<string>();
        static List<string> filterString = new List<string>();

        static List<string> operators = new List<string>();
        static List<string> headers = new List<string>();
        static bool isHeaderSet = false;

        public delegate void StateHandler();

        public static event StateHandler FilterChanged;

        /// <summary>
        /// Список заголовков
        /// </summary>
        static public List<string> Headers
        {
            get
            {
                return headers;
            }
        }

        /// <summary>
        /// Определны ли заголовки 
        /// </summary>
        static public bool IsHeaderSet
        {
            get
            {
                return isHeaderSet;
            }
        }

        static FilterParams()
        {
            filters.Clear();
            filters.CollectionChanged += filters_CollectionChanged;
        }

        /// <summary>
        /// Установить заголовки
        /// </summary>
        /// <param name="Headers">Список заголовков</param>
        static public void SetHeaders(List<string> Headers)
        {
            headers = Headers;
            filters.CollectionChanged -= filters_CollectionChanged;

            if (filters.Count == 0)
            {
                AddRange(headers, false);
            }

            filters.CollectionChanged += filters_CollectionChanged;
            isHeaderSet = true;
        }

        /// <summary>
        /// AddRange
        /// </summary>
        /// <param name="Enumeration"></param>
        /// <param name="data"></param>
        static void AddRange(IEnumerable<string> Enumeration, bool data = true)
        {
            foreach (string item in Enumeration)
            {
                if (data)
                {
                    filters.Add(item);
                    operators.Add(item);
                    filterString.Add(item);
                }
                else
                {
                    filters.Add(string.Empty);
                    filterString.Add(string.Empty);
                    operators.Add("=");
                }
            }
        }

        static public void SetFilterParam(string ParamName, string ParamValue,
            string FilterString,  string Operation)
        {
            int index = headers.IndexOf(ParamName);
            operators[index] = Operation;
            filterString[index] = FilterString;
            filters[index] = ParamValue;
        }

        static public string GetFilterParam(string ParamName)
        {
            int index = headers.IndexOf(ParamName);
            return filters[index];
        }

        static public string GetFilterOperator(string ParamName)
        {
            int index = headers.IndexOf(ParamName);
            return operators[index];
        }

        private static void filters_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Replace: // если замена

                    if (FilterChanged != null)
                    {
                        FilterChanged();
                    }
                    break;
            }
        }

        static public string GetFilterString()
        {
            string tmp = string.Empty;
            bool first = true;
            string oper = "";

            for (int i = 0; i < filters.Count; i++)
            {
                if (filters[i] != string.Empty)
                {
                    if (operators[i] == "=")
                    {
                        oper = "like";
                    }
                    if (operators[i] == "<>")
                    {
                        oper = "not like";
                    }


                    if (first)
                    {
                        tmp +=
                            filterString[i];
                        first = false;
                    }
                    else
                    {
                        tmp +=
                            "and " + filterString[i];
                    }
                }
            }

            return tmp;
        }

        static public void ClearFilters()
        {

            filters.CollectionChanged -= filters_CollectionChanged;
            for (int i = 0; i < filters.Count; i++)
            {
                filters[i] = string.Empty;
                operators[i] = "=";
            }
            filters.CollectionChanged += filters_CollectionChanged;
            FilterChanged();
        }

        static public void ClearCurrentFilters(string ParamName)
        {
            filters.CollectionChanged -= filters_CollectionChanged;

            int index = headers.IndexOf(ParamName);

            if (ParamName != "Сборка")
            {
                filters[index] = string.Empty;
            }
            else
            {
                filters[index] = "";
            }

            operators[index] = "=";

            filters.CollectionChanged += filters_CollectionChanged;
            FilterChanged();
        }
    }
}
