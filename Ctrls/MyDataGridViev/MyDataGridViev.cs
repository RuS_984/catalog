﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyDGV;

namespace MyControls
{
    public class MyDataGridViev : DataGridView

    {

        bool markAsm;
        bool setPopUp;

        ColumnFilter myColumnFilter ;

        public delegate void GroupCahge(object sender, GroupChangeEventArgs e);

        [Category("Property Changed"), Description("")]
        public event GroupCahge GroupDataChange;

        public Dictionary<string, int> Ispoln { get; set; }

        public Dictionary<string, int> Prj { get; set; }

        public Dictionary<string, int> Literal { get; set; }

        [Category("Params"), Description("Выделять цветом сборочные изделия"), DefaultValue(true)]
        public bool MarkAsm
        {
            get { return markAsm; }
            set
            {
                if (value)
                {
                    markAsm = value;
                    //this.RowPrePaint += new
                    //    System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dataGridView1_RowPrePaint);
                    Invalidate();
                }
                else
                {
                    markAsm = value;
                    //this.RowPrePaint -= 
                    //    new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dataGridView1_RowPrePaint);
                    Invalidate();
                }
            }
        }

        public MyDataGridViev()
        {

            markAsm = true;
            this.CellMouseClick += new DataGridViewCellMouseEventHandler(DataGridView_CellMouseClick);

            this.BackgroundColor = Color.White;

            FilterParams.FilterChanged += FilterDGV;

            this.RowPrePaint += new
                DataGridViewRowPrePaintEventHandler(this.dataGridView1_RowPrePaint);

        }

        /// <summary>
        /// Получение заголовков колонок DataGridView
        /// </summary>
        /// <returns></returns>
        private List<string> GetHeaders()
        {
            List<string> headers = new List<string>();

            foreach (DataGridViewColumn col in this.Columns)
            {
                headers.Add(col.HeaderText);
            }
            return headers;
        }

        private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

            //Выделение цветом сборок
            if (Convert.ToBoolean(this.Rows[e.RowIndex].Cells["Сборка"].Value) == true
                & markAsm)
            {
                this.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
            }

            //Выделение цветом повторяющихся элементов
            if (this.Rows[e.RowIndex].Cells["Обозначение"].Value.ToString().Contains("_ПОВТОР_"))
            {
                this.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.OrangeRed;
            }

        }

        protected virtual void DataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!FilterParams.IsHeaderSet)
            {
                FilterParams.SetHeaders(GetHeaders());
            }
            //Отображение фильтра
            if (e.Button == MouseButtons.Right & e.RowIndex == -1 & e.ColumnIndex > -1)
            {
                //MessageBox.Show(e.ColumnIndex.ToString());

                DataGridViewColumn dgvColumn = this.Columns[e.ColumnIndex];

                //DataTable dt = (DataTable)this.DataSource;

                DataView dv = (DataView)this.DataSource;

                myColumnFilter = new ColumnFilter(e.ColumnIndex, dgvColumn, dv);
                //myColumnFilter = new ColumnFilter(e.ColumnIndex, dgvColumn, dt);
                Point p = new Point(MousePosition.X, MousePosition.Y);
                myColumnFilter.ShowPopUp(p);

            }

            //Групповое изменение значения исполнителя
            if (e.Button == MouseButtons.Right & e.RowIndex > -1 & e.ColumnIndex >= 9 & e.ColumnIndex <= 13
                & GetSelectedColsIndexes().Length == 1)
            {

                //MessageBox.Show(e.ColumnIndex.ToString());
                if (GroupDataChange != null)
                {

                    GroupChange gp = new GroupChange(Ispoln);

                    gp.ShowDialog();

                    GroupDataChange(this, new GroupChangeEventArgs(
                         _colname: this.Columns[e.ColumnIndex].HeaderText,
                        _selrows: GetSelectedRowsIndexes(),
                        _colindex: e.ColumnIndex,
                        _newColParam: gp.SelectedID
                        )
                    );
                }

            }

            //Групповое изменение значения проекта
            if (e.Button == MouseButtons.Right & e.RowIndex > -1 & e.ColumnIndex == 2
                & GetSelectedColsIndexes().Length == 1)
            {
                //MessageBox.Show(e.ColumnIndex.ToString());
                if (GroupDataChange != null)
                {
                    GroupChange gp = new GroupChange(Prj);

                    gp.ShowDialog();

                    GroupDataChange(this, new GroupChangeEventArgs(
                         _colname: this.Columns[e.ColumnIndex].HeaderText,
                        _selrows: GetSelectedRowsIndexes(),
                        _colindex: e.ColumnIndex,
                        _newColParam: gp.SelectedID
                        )
                        );

                }

            }

            //Групповое изменение значения литеры
            if (e.Button == MouseButtons.Right & e.RowIndex > -1 & e.ColumnIndex == 7
                & GetSelectedColsIndexes().Length == 1)
            {
                //MessageBox.Show(e.ColumnIndex.ToString());
                if (GroupDataChange != null)
                {

                    GroupChange gp = new GroupChange(Literal);

                    gp.ShowDialog();

                    GroupDataChange(this, new GroupChangeEventArgs(
                         _colname: this.Columns[e.ColumnIndex].HeaderText,
                        _selrows: GetSelectedRowsIndexes(),
                        _colindex: e.ColumnIndex,
                        _newColParam: gp.SelectedID
                        )
                        );
                }
            }
        }

        /// <summary>
        /// Фильтрация данных
        /// </summary>
        public void FilterDGV()
        {
            DataView dv = (DataView)this.DataSource;
            string _textFilters;

            _textFilters = FilterParams.GetFilterString();

            dv.RowFilter = _textFilters;

            foreach (string header in FilterParams.Headers)
            {
                this.Columns[header].HeaderText = header;

                if (FilterParams.GetFilterParam(header) != string.Empty)
                {
                    if (FilterParams.GetFilterOperator(header) != "X..Y")
                    {
                        this.Columns[header].HeaderText += "\n"
                            + FilterParams.GetFilterOperator(header) + " "
                            + FilterParams.GetFilterParam(header);
                    }
                    else
                    {
                        this.Columns[header].HeaderText += "\n" +
                            FilterParams.GetFilterOperator(header)
                            .Replace("X", FilterParams.GetFilterParam(header).Split('\n')[0])
                            .Replace("Y", FilterParams.GetFilterParam(header).Split('\n')[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Получение индексов выделенных строк
        /// </summary>
        /// <returns>Массив индксов</returns>
        public int[] GetSelectedRowsIndexes()
        {
            int[] rowIndexes = (from sc in this.SelectedCells.Cast<DataGridViewCell>()
                                select sc.RowIndex).Distinct().ToArray();
            return rowIndexes;

        }

        /// <summary>
        /// Получение индексов выделенных столбцов
        /// </summary>
        /// <returns>Массив индксов</returns>
        public int[] GetSelectedColsIndexes()
        {
            int[] colIndexes = (from sc in this.SelectedCells.Cast<DataGridViewCell>()
                                select sc.ColumnIndex).Distinct().ToArray();
            return colIndexes;
        }

        public class GroupChangeEventArgs
        {
            public string colName;
            public int colIndex;
            public int[] selectedRows;
            public int newColParam;

            public GroupChangeEventArgs(string _colname, int[] _selrows, int _colindex, int _newColParam)
            {
                colName = _colname;
                selectedRows = _selrows;
                colIndex = _colindex;
                newColParam = _newColParam;
            }
        }

    }
}
