﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

using System.Diagnostics;


namespace MyControls
{
    public class TextBoxAutoComplete1 : TextBox
    {
        #region Переменные
        private ToolStripControlHost host;

        private ListBox AutoComplite_lsb = new ListBox();

        private List<string> autoCompleteVar = new List<string>();

        ToolStripDropDown popUp = new ToolStripDropDown();

        Font _font;

        WinHook hook;
        #endregion

        #region Конструктор
        public TextBoxAutoComplete1()
        {
            host = new ToolStripControlHost(AutoComplite_lsb);

            host.Padding = Padding.Empty;
            host.Margin = Padding.Empty;
            host.AutoSize = false;

            popUp.AutoClose = false;
            popUp.Items.Add(host);

            this.TextChanged += TextBox_Changed;

            AutoComplite_lsb.DrawItem += lstBox_DrawItem;
            AutoComplite_lsb.MeasureItem += listBox_MeasureItem;
            AutoComplite_lsb.DrawMode = DrawMode.OwnerDrawVariable;
            AutoComplite_lsb.SelectedIndexChanged += LstBoxItemSelect;
        }
        #endregion

        #region Методы
        [Browsable(false)]
        public List<string> AutoCompleteVar
        {
            get { return autoCompleteVar; }
            set
            {
                autoCompleteVar = value;
            }
        }

        public override Font Font
        {
            get { return base.Font; }
            set
            {
                _font = base.Font = value;
                AutoComplite_lsb.Font = _font;
            }
        }
        #endregion

        #region События
        /// <summary>
        /// Выборка вариантов автодополнения при введении текста
        /// </summary>
        private void TextBox_Changed(object sender, EventArgs e)
        {
            #region Выборка из вариантов автодополнения
            //Выбираются строки из autoCompleteVar в которые воходит
            //текст вводимый в текстовое поле.
            var selectedTeams = from t in autoCompleteVar // определяем каждый объект из teams как t
                                where t.ToUpper().Contains(this.Text.ToUpper()) //фильтрация по критерию
                                orderby t  // упорядочиваем по возрастанию
                                select t;

            //Выбранные варианты записывются в AutoComplite_lsb (ListBox)
            AutoComplite_lsb.Items.Clear();

            popUp.AutoClose = true;
            popUp.Hide();

            foreach (string item in selectedTeams)
            {
                if (item != null)
                {
                    AutoComplite_lsb.Items.Add(item);
                }
            }
            #endregion

            if (AutoComplite_lsb.Items.Count > 0)
            {
                popUp.AutoClose = false;
            }

            if (selectedTeams.Count() > 0 & this.Text != "")
            {
                int str_height;
                str_height = this.Font.Height * AutoComplite_lsb.Items.Count;

                if (str_height > 100)
                {
                    //Форморование элемента автодополнения при количестве вариантов
                    //больше 6ти
                    AutoComplite_lsb.Size = new Size(this.Width - 5, this.Font.Height * 6 + 5);
                    AutoComplite_lsb.Region = new Region(
                           new Rectangle(0, 0, this.Width + 0, this.Font.Height * 6 + 5));
                    popUp.Region = AutoComplite_lsb.Region;
                }
                else
                {
                    //Форморование элемента автодополнения при количестве вариантов
                    //меньше 6ти
                    AutoComplite_lsb.Size = new Size(this.Width - 5, this.Font.Height * 6 + 5);
                    AutoComplite_lsb.Region = new Region(
                       new Rectangle(0, 0, this.Width, str_height + 5));
                    popUp.Region = AutoComplite_lsb.Region;
                }
                //Получение области контрола на экране
                Rectangle r = RectangleToScreen(this.ClientRectangle);

                if (this.hook == null)
                {
                    this.hook = new WinHook(this, popUp);
                    this.hook.AssignHandle(this.FindForm().Handle);
                }

                popUp.Show(new Point(
                   r.X + 2, //Коордната X в системе координат экрана
                   r.Y + this.Height));       //Коордната Y в системе координат экрана
            }
            else
            {
                popUp.AutoClose = true;
                popUp.Hide();
            }
        }

        private void lstBox_DrawItem(object sender,
                 DrawItemEventArgs e)
        {
            ListBox lsb = new ListBox();

            lsb = sender as ListBox;

            SolidBrush MyBrushBGeven = new SolidBrush(Color.FromArgb(200, 200, 200));
            SolidBrush MyBrushBGodd = new SolidBrush(Color.White);
            SolidBrush MyBrushFG = new SolidBrush(Color.Black);

            e.DrawBackground();
            bool selected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            int index = e.Index;
            if (index >= 0 && index < lsb.Items.Count)
            {
                string text = lsb.Items[index].ToString();
                Graphics g = e.Graphics;

                SolidBrush backgroundBrush;
                if ((index % 2) == 0)
                {
                    backgroundBrush = MyBrushBGeven;
                }
                else
                {
                    backgroundBrush = MyBrushBGodd;
                }

                g.FillRectangle(backgroundBrush, e.Bounds);
                g.DrawString(text, _font, MyBrushFG, AutoComplite_lsb.GetItemRectangle(index).Location);
            }
            e.DrawFocusRectangle();
        }

        private void listBox_MeasureItem(object sender,
               System.Windows.Forms.MeasureItemEventArgs e)
        {
            e.ItemHeight = _font.Height;
        }

        /// <summary>
        /// Ввод выбранного значения в текстовое поле
        /// </summary>
        private void LstBoxItemSelect(object sender, EventArgs e)
        {
            if (AutoComplite_lsb.SelectedItem != null)
            {
                this.Text = AutoComplite_lsb.SelectedItem.ToString();
            }
            popUp.AutoClose = true;
            popUp.Hide();
        }
        #endregion

        private class WinHook : NativeWindow
        {
            private ToolStripDropDown lb;
            private TextBoxAutoComplete1 tb;

            public WinHook(TextBoxAutoComplete1 tbox, ToolStripDropDown lbox)
            {
                this.tb = tbox;
                this.lb = lbox;
            }

            protected override void WndProc(ref Message m)
            {
                switch (m.Msg)
                {


                    case (int)Win32_Messages.WM_LBUTTONDOWN:
                    case (int)Win32_Messages.WM_LBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_MBUTTONDOWN:
                    case (int)Win32_Messages.WM_MBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_RBUTTONDOWN:
                    case (int)Win32_Messages.WM_RBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_NCLBUTTONDOWN:
                    case (int)Win32_Messages.WM_NCMBUTTONDOWN:
                    case (int)Win32_Messages.WM_NCRBUTTONDOWN:
                    case (int)Win32_Messages.WM_MOUSEACTIVATE:
                    case (int)Win32_Messages.WM_MOVE:
                    case (int)Win32_Messages.WM_SIZE:
                        {
                            Form form = tb.FindForm();
                            Point p = form.PointToScreen(new Point((int)m.LParam));
                            Point p2 = tb.PointToScreen(new Point(0, 0));
                            Rectangle rect = new Rectangle(p2, lb.Size);
                            if (!rect.Contains(p) && lb.Visible)
                            {
                                lb.AutoClose = true;
                                lb.Hide();
                                lb.AutoClose = false;
                                lb.AutoClose = false;

                                //Debug.WriteLine("X1");
                            }
                            //Debug.WriteLine("X1_1_");
                        }
                        break;
                    // Закрытие автодополнения при потере фокуса окна
                    case (int)Win32_Messages.WM_ACTIVATEAPP:
                        if (((int)m.WParam == 0))
                        {
                            lb.AutoClose = true;
                            lb.Hide();
                            lb.AutoClose = false;
                            Debug.WriteLine("X3");
                            base.WndProc(ref m);
                        }
                        break;
                }
                base.WndProc(ref m);
            }
        }

        protected virtual void HideList()
        {
            if (this.hook != null)
            {
                this.hook.ReleaseHandle();
            }

            this.hook = null;
            popUp.Hide();
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            AutoComplite_lsb.Top = Top + Height - 3;
            AutoComplite_lsb.Left = Left + 3;
        }

    }
}
