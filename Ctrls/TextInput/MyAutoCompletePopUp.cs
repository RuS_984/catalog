﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

using System.Diagnostics;

namespace MyControls
{
    class MyAutoCompletePopUp : ToolStripDropDown
    {
        #region Переменные

        private Control inputControl;

        private ToolStripControlHost host;
        public Point popUpCoord;

        private ListBox AutoComplite_lsb = new ListBox();

        public List<string> autoCompleteVar = new List<string>();

        Font _font;

        WinHook hook;

        bool isActive;

        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ResumeLayout(false);
        }

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        #region Конструкторы
        /// <summary>
        ///  Конструктор
        /// </summary>
        /// <param name="InputControl">Контрол к которому привязывается автодополнение</param>
        /// <param name="AutoCompleteVar">Список вариантов автодополнения</param>
        public MyAutoCompletePopUp(Control InputControl, List<string> AutoCompleteVar)
        {
            this.inputControl = InputControl;

            this.autoCompleteVar = AutoCompleteVar;

            this.host = new ToolStripControlHost(AutoComplite_lsb);

            this.host.Padding = Padding.Empty;
            this.host.Margin = new Padding(5);
            this.host.AutoSize = false;

            this.Items.Add(host);

            AutoComplite_lsb.DrawItem += lstBox_DrawItem;
            AutoComplite_lsb.MeasureItem += listBox1_MeasureItem;
            AutoComplite_lsb.DrawMode = DrawMode.OwnerDrawVariable;
            AutoComplite_lsb.SelectedIndexChanged += LstBoxItemSelect;

            _font = inputControl.Font;

            inputControl.Disposed += delegate (object sender, EventArgs e)
            {
                inputControl = null;
                Dispose(true);
            };
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обноление списка автодополнения и отображение его
        /// </summary>
        public void UpdateAndShow()
        {
            if (!isActive)
            {
                //return;
            }

            #region Выборка из вариантов автодополнения
            //Выбираются строки из autoCompleteVar в которые воходит
            //текст вводимый в текстовое поле.
            var selectedItems = from t in autoCompleteVar // определяем каждый объект из teams как t
                                where t.ToUpper().Contains(inputControl.Text.ToUpper()) //фильтрация по критерию
                                orderby t  // упорядочиваем по возрастанию
                                select t;
            #endregion

            //Выбранные варианты записывются в AutoComplite_lsb (ListBox)
            AutoComplite_lsb.Items.Clear();

            this.AutoClose = true;
            this.Hide();

            foreach (string item in selectedItems)
            {
                if (item != null)
                {
                    AutoComplite_lsb.Items.Add(item);
                }
            }

            if (AutoComplite_lsb.Items.Count > 0)
            {
                this.AutoClose = false;
            }

            if ((selectedItems.Count() > 0)
                & inputControl.Text != ""
                )
            {
                int str_height;
                str_height = _font.Height * AutoComplite_lsb.Items.Count;

                if (str_height > 100)
                {
                    //Форморование элемента автодополнения при количестве вариантов
                    //больше 6ти
                    AutoComplite_lsb.Size = new Size(inputControl.Width - 15, _font.Height * 6 + 5);
                    //AutoComplite_lsb.Region = new Region(
                    //       new Rectangle(0, 0, inputControl.Width + 0, _font.Height * 6 + 5));
                    //this.Region = AutoComplite_lsb.Region;
                }
                else
                {
                    //Форморование элемента автодополнения при количестве вариантов
                    //меньше 6ти
                    AutoComplite_lsb.Size = new Size(inputControl.Width - 15,
                        (_font.Height * AutoComplite_lsb.Items.Count + 1) + 10);
                    //AutoComplite_lsb.Region = new Region(
                    //   new Rectangle(0, 0, inputControl.Width, str_height + 5));
                    //this.Region = AutoComplite_lsb.Region;
                    //this.Height = AutoComplite_lsb.Height+100;
                }

                this.Show(popUpCoord);

                if (this.hook == null)
                {
                    this.hook = new WinHook(inputControl, this);

                    //if (inputControl.FindForm().Handle != IntPtr.Zero)
                    //{
                    //    this.hook.AssignHandle(inputControl.FindForm().Handle);
                    //}
                    //else
                    //{
                    //    this.hook.AssignHandle(inputControl.Handle);
                    //}

                    try
                    {
                        this.hook.AssignHandle(inputControl.FindForm().Handle);
                    }
                    catch (Exception)
                    {
                        //throw;
                    }

                    try
                    {
                        this.hook.AssignHandle(inputControl.Handle);
                    }
                    catch (Exception)
                    {
                        //throw;
                    }
                }
            }
        }


        private class WinHook : NativeWindow
        {
            private ToolStripDropDown lb;
            private Control tb;

            public WinHook(Control tbox, ToolStripDropDown lbox)
            {
                this.tb = tbox;
                this.lb = lbox;
            }

            protected override void WndProc(ref Message m)
            {
                switch (m.Msg)
                {
                    case (int)Win32_Messages.WM_LBUTTONDOWN:
                    case (int)Win32_Messages.WM_LBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_MBUTTONDOWN:
                    case (int)Win32_Messages.WM_MBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_RBUTTONDOWN:
                    case (int)Win32_Messages.WM_RBUTTONDBLCLK:
                    case (int)Win32_Messages.WM_NCLBUTTONDOWN:
                    case (int)Win32_Messages.WM_NCMBUTTONDOWN:
                    case (int)Win32_Messages.WM_NCRBUTTONDOWN:
                    case (int)Win32_Messages.WM_MOUSEACTIVATE:
                    case (int)Win32_Messages.WM_MOVE:
                    case (int)Win32_Messages.WM_SIZE:
                        {
                            Form form = tb.FindForm();
                            Point p = form.PointToScreen(new Point((int)m.LParam));
                            Point p2 = tb.PointToScreen(new Point(0, 0));
                            Rectangle rect = new Rectangle(p2, lb.Size);
                            if (!rect.Contains(p) && lb.Visible)
                            {
                                lb.AutoClose = true;
                                lb.Hide();
                                lb.AutoClose = false;

                                //Debug.WriteLine("X1");
                            }
                            //Debug.WriteLine("X1_1_");
                        }
                        break;

                    // Закрытие автодополнения при потере фокуса окна
                    case (int)Win32_Messages.WM_ACTIVATEAPP:

                        if (((int)m.WParam == 0))
                        {
                            lb.AutoClose = true;
                            lb.Hide();
                            lb.AutoClose = false;
                            //Debug.WriteLine("X3");
                            base.WndProc(ref m);
                        }
                        break;
                }
                base.WndProc(ref m);
            }
        }
        #endregion

        #region События
        private void lstBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox lsb = new ListBox();

            lsb = sender as ListBox;

            SolidBrush MyBrushBGeven = new SolidBrush(Color.FromArgb(219, 219, 219));
            SolidBrush MyBrushBGodd = new SolidBrush(Color.White);
            SolidBrush MyBrushFG = new SolidBrush(Color.Black);

            e.DrawBackground();

            bool selected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            int index = e.Index;
            if (index >= 0 && index < lsb.Items.Count)
            {
                string text = lsb.Items[index].ToString();
                Graphics g = e.Graphics;

                SolidBrush backgroundBrush;
                if ((index % 2) == 0)
                {
                    backgroundBrush = MyBrushBGeven;
                }
                else
                {
                    backgroundBrush = MyBrushBGodd;
                }

                g.FillRectangle(backgroundBrush, e.Bounds);

                g.DrawString(text, inputControl.Font, MyBrushFG,
                             AutoComplite_lsb.GetItemRectangle(index).Location);
            }
            e.DrawFocusRectangle();
        }

        private void listBox1_MeasureItem(object sender,
               System.Windows.Forms.MeasureItemEventArgs e)
        {
            e.ItemHeight = inputControl.Font.Height;
        }

        private void LstBoxItemSelect(object sender, EventArgs e)
        {
            if (AutoComplite_lsb.SelectedItem != null)
            {
                this.inputControl.Text = AutoComplite_lsb.SelectedItem.ToString();
            }

            this.AutoClose = true;
            this.Hide();
        }
        #endregion
    }
}



