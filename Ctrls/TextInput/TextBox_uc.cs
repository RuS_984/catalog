﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Reflection;

using System.Runtime.InteropServices;

using System.Diagnostics;
//using System.Windows.Forms.Design.Behavior;

namespace MyControls
{
    public partial class TextBox_uc : UserControl
    {
        private MyAutoCompletePopUp popUp;

        #region Конструкторы
        public TextBox_uc()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);

            InitializeComponent();

            this.textBox2.Text = "textBox2";

            //r1 = RectangleToScreen(textBox2.ClientRectangle);
            //r1.X += textBox2.Location.X;
        }
        #endregion

        #region Переменные
        public List<string> autoCompleteVar = new List<string>();
        Font _font ;
        bool is_Digit_only = false;
        bool is_AutoComplited = false;
        bool isStandartAutoComplete = true;
        bool isValid = true;
        #endregion Переменные

        #region Параметры
        //[Category("Params"), Description("Шрифт контрола")]
        public override Font Font
        {
            get { return base.Font; }
            set
            {
                _font = base.Font = value;

                label2.Font = _font;
                textBox2.Font = _font;

                Invalidate();
                redraw();
            }
        }

        [Category("Params"), Description("Текст Lable"),
         DisplayName("Lable text")]
        public string Lable_txt
        {
            get { return label2.Text; }
            set
            {
                label2.Text = value;
                if ((textBox2.Width + label2.Width + 5) > this.Width)
                { this.Width = textBox2.Width + label2.Width + 5; }
                redraw();
            }
        }

        [Category("Params"), Description("Разрешить вводить только цифры"),
         DisplayName("Digit only")]
        public bool Is_Digit_only
        {
            get { return is_Digit_only; }
            set
            {
                is_Digit_only = value;
                if (is_Digit_only)
                {
                    this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(
                        this.textBox2_KeyDown);
                }
                else
                {
                    this.textBox2.KeyDown -= new System.Windows.Forms.KeyEventHandler(
                        this.textBox2_KeyDown);
                }
            }
        }

        [Category("Params"), Description("Ширина поля ввода"),
         DisplayName("TextBox width")]
        public int textBox_width
        {
            get { return textBox2.Width; }

            set
            {
                textBox2.Width = value;
                redraw();
            }

        }

        [Category("Params"), Description("Полная ширина конторола"),
         DisplayName("Control_widt")]
        public int control_width
        {
            get { return this.Width; }
            set
            {
                this.Width = value;
                redraw();
            }
        }

        //[Category("Params"), Description("Название")]
        [Browsable(false)]
        public string TextBox_txt
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }

        [Category("Params"), Description("Вариант автодополнения"),
        DisplayName("IsStandartAutoComplete")]
        public bool IsStandartAutoComplete
        {
            get { return isStandartAutoComplete; }
            set
            {
                isStandartAutoComplete = value;
            }
        }

        #endregion

        #region Автодополнение

        /// <summary>
        /// Задает список автодополнения
        /// </summary>
        /// <param name="lst"> Список параметров автодополнения</param>
        public void Set_AutoComplete_List(List<string> lst)
        {

            autoCompleteVar = lst;

            //Задание параметров стандартного автодополнения
            if (isStandartAutoComplete)
            {
                textBox2.AutoCompleteSource = AutoCompleteSource.CustomSource;

                AutoCompleteStringCollection lst_coll = new AutoCompleteStringCollection();
                foreach (string item in autoCompleteVar)
                {
                    lst_coll.Add(item);
                }
                textBox2.AutoCompleteCustomSource = lst_coll;
            }
        }

        /// <summary>
        /// Активация автодополнения
        /// </summary>
        public void Enable_AutoComplete()
        {
            is_AutoComplited = true;

            if (isStandartAutoComplete)
            {
                textBox2.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            }
            else
            {
                textBox2.TextChanged += TextBox_Changed;

                popUp = new MyAutoCompletePopUp(
                    this.textBox2, autoCompleteVar
                   );
            }
        }

        /// <summary>
        /// Отключение автодополнения
        /// </summary>
        public void Disable_AutoComplete()
        {
            if (isStandartAutoComplete)
            {
                textBox2.AutoCompleteMode = AutoCompleteMode.None;
            }
            else
            {
                //if (filterPopup != null) filterPopup.Dispose();

                textBox2.TextChanged -= TextBox_Changed;
                is_AutoComplited = false;
            }
        }

        /// <summary>
        /// Возвращает состояние автодополнения
        /// </summary>
        public bool Is_AutoComplete_Active()
        {
            if (is_AutoComplited)
            {
                return false;
            }
            else return true;
        }

        /// <summary>
        /// Выборка вариантов автодополнения при введении текста
        /// </summary>
        private void TextBox_Changed(object sender, EventArgs e)
        {
            Rectangle r = RectangleToScreen(this.ClientRectangle);
            Point p = new Point(
                      r.X + textBox2.Location.X + 8, //Коордната X в системе координат экрана
                      r.Y + textBox2.Height);       //Коордната Y в системе координат экрана

            if (popUp != null)
            {
                popUp.popUpCoord = p;
                popUp.UpdateAndShow();
            }
        }
        #endregion

        #region Методы

        public void Set_validating(bool value)
        {
            if (value == true)
            {
                this.textBox2.KeyUp +=
                  new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyUp);
            }
            else
            {
                this.textBox2.KeyUp -=
                  new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyUp);
            }
        }

        public bool Is_Valid()
        {
            if (textBox2.BackColor == Color.Red)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        void redraw()
        {
            if ((textBox2.Width + label2.Width + 5) > this.Width)
            { this.Width = textBox2.Width + label2.Width + 5; }
            label2.Left = this.Width - (textBox2.Width + label2.Width + 5);
            this.Height = textBox2.Height;

            //if ((textBoxAC.Width + label2.Width + 5) > this.Width)
            //{ this.Width = textBoxAC.Width + label2.Width + 5; }
            //label2.Left = this.Width - (textBoxAC.Width + label2.Width + 5);
            //this.Height = textBoxAC.Height;
        }

        public void Set_Params(List<string> AutoComplete_List,
          bool AutoComplete_Enabled, bool Validating)
        {
            this.Set_AutoComplete_List(AutoComplete_List);

            if (AutoComplete_Enabled == true)
            { this.Enable_AutoComplete(); }
            else { this.Disable_AutoComplete(); }

            if (Validating == true)
            { this.Set_validating(true); }
            else { this.Set_validating(false); }
        }


        public void Set_Digit_Only(bool value)
        {
            if (value == true)
            {
                this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(
                    this.textBox2_KeyDown);
            }
            else
            {
                this.textBox2.KeyDown -= new System.Windows.Forms.KeyEventHandler(
                    this.textBox2_KeyDown);
            }
        }

        private void Validation()
        {
            if ((autoCompleteVar.IndexOf(textBox2.Text) > -1) & (is_AutoComplited == true))
            {
                textBox2.BackColor = Color.Red;
            }
            else
            {
                textBox2.BackColor = Color.White;
            }
        }
        #endregion

        #region События

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if ((autoCompleteVar.IndexOf(textBox2.Text) > -1) & (is_AutoComplited == true))
            {
                textBox2.BackColor = Color.Red;
            }
            else
            {
                textBox2.BackColor = Color.White;
            }
        }

        private void TextBox_uc_Resize(object sender, EventArgs e)
        {
            redraw();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            // Пропускаем цифровые кнопки
            if ((e.KeyCode >= Keys.D0) && (e.KeyCode <= Keys.D9))
            {
                e.SuppressKeyPress = false;
            }
            // Пропускаем цифровые кнопки с NumPad'а
            if ((e.KeyCode >= Keys.NumPad0) && (e.KeyCode <= Keys.NumPad9))
            {
                e.SuppressKeyPress = false;
            }
            // Пропускаем Delete, Back, Left и Right
            if ((e.KeyCode == Keys.Delete) || (e.KeyCode == Keys.Back) ||
                (e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right) ||
                (e.Modifiers == Keys.Control && e.KeyCode == Keys.C) ||
                (e.Modifiers == Keys.Control && e.KeyCode == Keys.V))
            {
                e.SuppressKeyPress = false;
            }
        }
        #endregion
    }
}
