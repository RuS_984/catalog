﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyControls
{
    public partial class DateTime_uc : UserControl
    {

        #region Конструкторы

        public DateTime_uc()
        {
            InitializeComponent();
        }

        #endregion


        #region Переменные

        List<string> a_compl = new List<string>();
        Font _font;

        #endregion


        #region Параметры
        [Category("Params"), Description("Шрифт контрола")]
        public override Font Font
        {
            get { return base.Font; }
            set
            {
                _font = base.Font = value;

                label2.Font = _font;
                dateTimePicker1.Font = _font;
                Invalidate();
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public string Lable_txt
        {
            get { return label2.Text; }
            set
            {
                label2.Text = value;
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public int dateTimePicker1_char_col
        {
            get { return (dateTimePicker1.Width - 15) / 8; }
            set
            {
                dateTimePicker1.Width = value * 8 + 15;
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public int dateTimePicker1_width
        {
            get { return dateTimePicker1.Width; }
            set
            {
                dateTimePicker1.Width = value;
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public int control_width
        {
            get { return this.Width; }
            set
            {
                this.Width = value;
                redraw();
            }
        }

        //[Category("Params"), Description("Название")]
        [Browsable(false)]
        public string dateTime_txt
        {
            get { return dateTimePicker1.Value.ToString("dd-MM-yyyy"); }
            //set { textBox1.Text = value; }
        }
        #endregion



        #region Методы
        void redraw()
        {
            if ((dateTimePicker1.Width + label2.Width + 5) > this.Width)
            { this.Width = dateTimePicker1.Width + label2.Width + 5; }
            label2.Left = this.Width - (dateTimePicker1.Width + label2.Width + 5);
            this.Height = dateTimePicker1.Height;
        }

        public void Set_Date(DateTime date)
        {
            dateTimePicker1.Value = date;
        }

        public void Set_Date(string date)
        {
            dateTimePicker1.Value = Convert.ToDateTime(date);
        }
        #endregion

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void comboBox1_KeyUp(object sender, KeyEventArgs e)
        {
            //if ((a_compl.IndexOf(comboBox1.Text) > -1) & (Is_AutoComplete_Active() == true))
            //{
            //    comboBox1.BackColor = Color.Red;
            //}
            //else
            //{
            //    comboBox1.BackColor = Color.White;

            //}
        }

        private void DateTime_uc_Resize(object sender, EventArgs e)
        {
            redraw();
        }
    }
}
