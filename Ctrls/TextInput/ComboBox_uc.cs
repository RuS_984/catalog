﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyControls
{
    public partial class ComboBox_uc : UserControl
    {

        #region Конструкторы

        public ComboBox_uc()
        {
            InitializeComponent();
        }

        #endregion

        #region Переменные
        Font _font;

        bool is_valid = true;

        public List<string> autoCompleteVar = new List<string>();

        private MyAutoCompletePopUp popUp;

        bool isStandartAutoComplete = true;
        bool is_AutoComplited = false;
        #endregion

        #region Параметры

        [Category("Params"), Description("Шрифт контрола")]
        public override Font Font
        {
            get { return base.Font; }
            set
            {
                _font = base.Font = value;

                label2.Font = _font;
                comboBox1.Font = _font;
                Invalidate();
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public string Lable_txt
        {
            get { return label2.Text; }
            set
            {
                label2.Text = value;
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public int comboBox1_width
        {
            get { return comboBox1.Width; }
            set
            {
                comboBox1.Width = value;
                redraw();
            }
        }

        [Category("Params"), Description("Название")]
        public int control_width
        {
            get { return this.Width; }
            set
            {
                this.Width = value;
                redraw();
            }
        }

        [Browsable(false)]
        public string ComboBox_txt
        {
            get { return comboBox1.SelectedItem.ToString(); }
            //set { textBox1.Text = value; }
        }

        [Category("Params"), Description("Вариант автодополнения"),
         DisplayName("IsStandartAutoComplete")]
        public bool IsStandartAutoComplete
        {
            get { return isStandartAutoComplete; }
            set
            {
                isStandartAutoComplete = value;

            }
        }
        #endregion

        #region Методы
        public void set_values(List<string> value_list,
            string add_first_val = "")
        {
            if (add_first_val != "")
            { comboBox1.Items.Add(add_first_val); }
            foreach (string val in value_list)
            {
                comboBox1.Items.Add(val);
            }
            comboBox1.SelectedIndex = 0;
        }

        public bool Is_Valid()
        {
            if (comboBox1.BackColor == Color.Red)
            {
                return false;

            }
            else
            {
                return true;
            }
        }

        void redraw()
        {
            if ((comboBox1.Width + label2.Width + 5) > this.Width)
            { this.Width = comboBox1.Width + label2.Width + 5; }
            label2.Left = this.Width - (comboBox1.Width + label2.Width + 5);
            this.Height = comboBox1.Height;

        }

        public void Set_validating(bool value)
        {
            if (value == true)
            {
                this.comboBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(
                                                            this.comboBox1_KeyUp);
            }
            else
            {
                this.comboBox1.KeyUp -= new System.Windows.Forms.KeyEventHandler(
                                                            this.comboBox1_KeyUp);
            }
        }

        public void Set_Params(List<string> AutoComplete_List, string first_val,
                                bool AutoComplete_Enabled, bool Validating)
        {
            this.set_values(AutoComplete_List, first_val);


            this.Set_AutoComplete_List(AutoComplete_List);

            if (AutoComplete_Enabled == true)
            { this.Enable_AutoComplete(); }
            else { this.Disable_AutoComplete(); }

            if (Validating == true)
            { this.Set_validating(true); }
            else { this.Set_validating(false); }
        }

        public void Set_by_value(string val)
        {
            comboBox1.SelectedItem = val;
        }


        #endregion

        #region Автодополнение

        /// <summary>
        /// Задает список автодополнения
        /// </summary>
        /// <param name="lst"> Список параметров автодополнения</param>
        public void Set_AutoComplete_List(List<string> lst)
        {
            autoCompleteVar = lst;

            //Задание параметров стандартного автодополнения
            if (isStandartAutoComplete)
            {
                comboBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;

                AutoCompleteStringCollection lst_coll = new AutoCompleteStringCollection();
                foreach (string item in autoCompleteVar)
                {
                    lst_coll.Add(item);
                }
                comboBox1.AutoCompleteCustomSource = lst_coll;
            }
        }

        /// <summary>
        /// Активация автодополнения
        /// </summary>
        public void Enable_AutoComplete()
        {
            is_AutoComplited = true;

            if (isStandartAutoComplete)
            {
                comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            }
            else
            {
                comboBox1.TextChanged += ComboBox_Changed;

                popUp = new MyAutoCompletePopUp(
                    this.comboBox1, autoCompleteVar
                   );
            }
        }

        /// <summary>
        /// Отключение автодополнения
        /// </summary>
        public void Disable_AutoComplete()
        {
            if (isStandartAutoComplete)
            {
                comboBox1.AutoCompleteMode = AutoCompleteMode.None;
            }
            else
            {
                comboBox1.TextChanged -= ComboBox_Changed;
                is_AutoComplited = false;
            }
        }

        /// <summary>
        /// Возвращает состояние автодополнения
        /// </summary>
        public bool Is_AutoComplete_Active()
        {
            if (comboBox1.AutoCompleteMode == AutoCompleteMode.None)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        private void comboBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if ((autoCompleteVar.IndexOf(comboBox1.Text) > -1) & (Is_AutoComplete_Active() == true))
            {
                comboBox1.BackColor = Color.Red;
                is_valid = false;
            }
            else
            {
                comboBox1.BackColor = Color.White;
                is_valid = true;
            }
        }

        private void ComboBox_uc_Resize(object sender, EventArgs e)
        {
            redraw();
        }


        private void ComboBox_Changed(object sender, EventArgs e)
        {

            Rectangle r = RectangleToScreen(this.ClientRectangle);
            Point p = new Point(
                      r.X + comboBox1.Location.X + 8, //Коордната X в системе координат экрана
                      r.Y + comboBox1.Height);       //Коордната Y в системе координат экрана

            if (popUp != null)
            {
                popUp.popUpCoord = p;
                popUp.UpdateAndShow();
            }
        }
    }
}
